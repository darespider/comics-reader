// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'onboarding_routes_data.dart';

// **************************************************************************
// GoRouterGenerator
// **************************************************************************

List<RouteBase> get $appRoutes => [
      $onboardingShellRoute,
    ];

RouteBase get $onboardingShellRoute => ShellRouteData.$route(
      factory: $OnboardingShellRouteExtension._fromState,
      routes: [
        GoRouteData.$route(
          path: '/login/server',
          factory: $ServerInfoFormRouteExtension._fromState,
        ),
        GoRouteData.$route(
          path: '/login/user',
          factory: $LoginFormRouteExtension._fromState,
        ),
      ],
    );

extension $OnboardingShellRouteExtension on OnboardingShellRoute {
  static OnboardingShellRoute _fromState(GoRouterState state) =>
      const OnboardingShellRoute();
}

extension $ServerInfoFormRouteExtension on ServerInfoFormRoute {
  static ServerInfoFormRoute _fromState(GoRouterState state) =>
      const ServerInfoFormRoute();

  String get location => GoRouteData.$location(
        '/login/server',
      );

  void go(BuildContext context) => context.go(location);

  Future<T?> push<T>(BuildContext context) => context.push<T>(location);

  void pushReplacement(BuildContext context) =>
      context.pushReplacement(location);

  void replace(BuildContext context) => context.replace(location);
}

extension $LoginFormRouteExtension on LoginFormRoute {
  static LoginFormRoute _fromState(GoRouterState state) =>
      const LoginFormRoute();

  String get location => GoRouteData.$location(
        '/login/user',
      );

  void go(BuildContext context) => context.go(location);

  Future<T?> push<T>(BuildContext context) => context.push<T>(location);

  void pushReplacement(BuildContext context) =>
      context.pushReplacement(location);

  void replace(BuildContext context) => context.replace(location);
}
