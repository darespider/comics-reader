import 'dart:async';

import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:kapoow/src/features/library_management/books/ui/screens/book_screen.dart';
import 'package:kapoow/src/features/library_management/collections/ui/screens/collections_screen.dart';
import 'package:kapoow/src/features/library_management/read_lists/ui/screens/readlist_screen.dart';
import 'package:kapoow/src/features/library_management/search/ui/screens/search_screen.dart';
import 'package:kapoow/src/features/library_management/series/ui/screens/series_screen.dart';
import 'package:kapoow/src/features/read_book/domain/models/change_book_direction.dart';
import 'package:kapoow/src/features/read_book/domain/models/read_book_page_query_params.dart';
import 'package:kapoow/src/features/read_book/domain/models/read_context.dart';
import 'package:kapoow/src/features/read_book/ui/screens/change_book_screen.dart';
import 'package:kapoow/src/features/read_book/ui/screens/read_book_screen.dart';
import 'package:kapoow/src/routes/dashboard_routes_data.dart';

part 'routes_data.g.dart';

/// All routes of the app
final appRoutes = $appRoutes;

/// Route that contains and allows to go back to recommendations page when on
/// a sub-route does a pop. It will redirect any non valid sub-route or itself
/// to [RecommendedRoute]
///
/// has a path of `'/'`
@TypedGoRoute<RootRoute>(
  path: RootRoute.path,
  routes: [
    TypedGoRoute<SearchRoute>(path: SearchRoute.path),
    TypedGoRoute<SeriesRoute>(path: SeriesRoute.path),
    TypedGoRoute<ReadlistRoute>(path: ReadlistRoute.path),
    TypedGoRoute<CollectionRoute>(path: CollectionRoute.path),
    TypedGoRoute<BookRoute>(
      path: BookRoute.path,
      routes: [
        TypedGoRoute<ReadBookRoute>(path: ReadBookRoute.path),
      ],
    ),
  ],
)
@immutable
class RootRoute extends GoRouteData {
  /// Creates a [RootRoute] instance
  const RootRoute();

  /// Value of `/`
  static const path = '/';

  static const _validRoutes = [
    'search',
    'series',
    'collections',
    'books',
    'readlists',
  ];

  bool _shouldRedirect(GoRouterState state) {
    // Gets first part of the route
    final paths = state.fullPath?.split('/').where((s) => s.isNotEmpty);

    if (paths?.isEmpty ?? true) return true;

    return !_validRoutes.contains(paths!.first);
  }

  @override
  FutureOr<String?> redirect(BuildContext context, GoRouterState state) {
    if (_shouldRedirect(state)) {
      return const RecommendedRoute().location;
    }
    return null;
  }

  @override
  Widget build(BuildContext context, GoRouterState state) {
    // TODO(rurickdev): look if this route can be removed
    return const Scaffold();
  }
}

/// Route that displays the settings of the app
///
/// has a path of `'/search'`
@immutable
class SearchRoute extends GoRouteData {
  /// Creates a [SearchRoute] instance
  const SearchRoute([this.s]);

  /// Value of `/search`
  static const path = 'search';

  /// Query parameter to search for
  final String? s;

  @override
  Widget build(BuildContext context, GoRouterState state) =>
      _PopToRecommendedScreen(
        child: SearchScreen(
          query: s,
        ),
      );
}

/// Route that displays the series
///
/// has a path of `'/series/:id'`
@immutable
class SeriesRoute extends GoRouteData {
  /// Creates a [SeriesRoute] instance
  const SeriesRoute({
    required this.id,
  });

  /// Id of the series selected
  final String id;

  // TODO(rurickdev): handle collection context

  /// Value of `/series/:id`
  static const path = 'series/:id';

  @override
  Widget build(BuildContext context, GoRouterState state) =>
      _PopToRecommendedScreen(
        child: SeriesScreen(
          seriesId: id,
        ),
      );
}

/// Route that displays the book
///
/// has a path of `'/books/:id'`
@immutable
class BookRoute extends GoRouteData {
  /// Creates a [BookRoute] instance
  const BookRoute({
    required this.id,
    this.context,
    this.contextId,
  }) : assert(
          context != ReadContext.readlist || contextId != null,
          'If a [ReadContext.readlist] is provided the [readContextId] '
          'should not be null or empty',
        );

  /// Id of the book selected
  final String id;

  /// Indicated if the book history should proceed as the series numeric order
  /// or as the readlist order
  final ReadContext? context;

  /// Used if the [context] is equal to [ReadContext.readlist] to identify
  /// which issue proceed or precede the book from the readlist.
  final String? contextId;

  /// Value of `/books/:id`
  static const path = 'books/:id';

  @override
  Widget build(BuildContext context, GoRouterState state) =>
      _PopToRecommendedScreen(
        child: BookScreen(
          bookId: id,
          readContext: this.context,
          readContextId: contextId,
        ),
      );
}

/// Route that displays the book
///
/// has a path of `'/books/:id/read'`
@immutable
class ReadBookRoute extends GoRouteData {
  /// Creates a [ReadBookRoute] instance
  const ReadBookRoute({
    required this.id,
    required this.page,
    this.context,
    this.contextId,
    this.incognito = false,
  });

  /// Current page reading
  final int page;

  /// Indicated if the book history should proceed as the series numeric order
  /// or as the readlist order
  final ReadContext? context;

  /// Used if the [context] is equal to [ReadContext.readlist] to identify
  /// which issue proceed or precede the book from the readlist.
  final String? contextId;

  /// Indicated if the read progress should be saved.
  final bool incognito;

  /// Id of the book selected
  final String id;

  /// Value of `read`
  static const path = 'read';

  @override
  Widget build(BuildContext context, GoRouterState state) => ReadBookScreen(
        bookId: id,
        params: ReadBookPageQueryParams(
          page: page,
          readContext: this.context,
          readContextId: contextId,
          incognito: incognito,
        ),
      );
}

/// Route that displays the book
///
/// has a path of `'/books/:id/:direction'`
@immutable
class ChangeBookRoute extends GoRouteData {
  /// Creates a [ChangeBookRoute] instance
  const ChangeBookRoute({
    required this.id,
    required this.direction,
    this.context,
    this.contextId,
    this.incognito = false,
  });

  /// Indicated if the book history should proceed as the series numeric order
  /// or as the readlist order
  final ReadContext? context;

  /// Used if the [context] is equal to [ReadContext.readlist] to identify
  /// which issue proceed or precede the book from the readlist.
  final String? contextId;

  /// Indicated if the read progress should be saved.
  final bool incognito;

  /// Id of the book selected
  final String id;

  /// Indicates if we should show the next or the previous book
  final ChangeBookDirection direction;

  /// Value of `read`
  static const path = ':direction';

  @override
  Widget build(BuildContext context, GoRouterState state) => ChangeBookScreen(
        bookId: id,
        direction: direction,
        readContext: this.context,
        readContextId: contextId,
      );
}

/// Route that displays the readlist
///
/// has a path of `'/readlists/:id'`
@immutable
class ReadlistRoute extends GoRouteData {
  /// Creates a [ReadlistRoute] instance
  const ReadlistRoute({
    required this.id,
  });

  /// Id of the readlist selected
  final String id;

  /// Value of `/readlists/:id`
  static const path = 'readlists/:id';

  @override
  Widget build(BuildContext context, GoRouterState state) =>
      _PopToRecommendedScreen(
        child: ReadlistScreen(
          readlistId: id,
        ),
      );
}

/// Route that displays the collection
///
/// has a path of `'/collections/:id'`
@immutable
class CollectionRoute extends GoRouteData {
  /// Creates a [CollectionRoute] instance
  const CollectionRoute({
    required this.id,
  });

  /// Id of the collection selected
  final String id;

  /// Value of `/collections/:id`
  static const path = 'collections/:id';

  @override
  Widget build(BuildContext context, GoRouterState state) =>
      _PopToRecommendedScreen(
        child: CollectionScreen(
          collectionId: id,
        ),
      );
}

/// Will prevent pop to the parent route and instead will navigate to
/// [RecommendedRoute]
class _PopToRecommendedScreen extends StatelessWidget {
  const _PopToRecommendedScreen({required this.child});

  final Widget child;

  @override
  Widget build(BuildContext context) {
    return PopScope(
      canPop: false,
      onPopInvokedWithResult: (didPop, result) {
        const RecommendedRoute().go(context);
      },
      child: child,
    );
  }
}
