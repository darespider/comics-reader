import 'package:flutter/foundation.dart';
import 'package:go_router/go_router.dart';
import 'package:kapoow/src/features/app_init/ui/controllers/init_pods.dart';
import 'package:kapoow/src/features/app_init/ui/controllers/init_state.dart';
import 'package:kapoow/src/routes/bootstrap_routes_data.dart';
import 'package:kapoow/src/routes/dashboard_routes_data.dart';
import 'package:kapoow/src/routes/onboarding_routes_data.dart';
import 'package:kapoow/src/routes/routes_data.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'router_pod.g.dart';

/// Provides the router of the app
@riverpod
GoRouter goRouter(GoRouterRef ref) {
  final initialLocation = ref.watch(initialLocationPod);

  final routes = [
    ...bootstrapRoutes,
    ...onboardingRoutes,
    ...dashboardRoutes,
    ...appRoutes,
  ];

  return GoRouter(
    initialLocation: initialLocation,
    routes: routes,
    debugLogDiagnostics: kDebugMode,
  );
}

/// Computes the right initial location of the app based on the init state
@riverpod
String initialLocation(InitialLocationRef ref) {
  final initAsync = ref.watch(initPod);

  return initAsync.when(
    loading: () => const SplashRoute().location,
    error: (error, stackTrace) => const ErrorInitRoute().location,
    data: (state) => switch (state) {
      InitStateSession() => const RecommendedRoute().location,
      InitStateNoSession() => const ServerInfoFormRoute().location,
      InitStateInvalid() => const LoginFormRoute().location,
    },
  );
}
