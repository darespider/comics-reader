// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'router_pod.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$goRouterHash() => r'07ec229affb0d6c9e75e858cb75202565b743a2b';

/// Provides the router of the app
///
/// Copied from [goRouter].
@ProviderFor(goRouter)
final goRouterPod = AutoDisposeProvider<GoRouter>.internal(
  goRouter,
  name: r'goRouterPod',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$goRouterHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef GoRouterRef = AutoDisposeProviderRef<GoRouter>;
String _$initialLocationHash() => r'd5fb84192f51c951fa2e85b9c07bc5632bd3ea09';

/// Computes the right initial location of the app based on the init state
///
/// Copied from [initialLocation].
@ProviderFor(initialLocation)
final initialLocationPod = AutoDisposeProvider<String>.internal(
  initialLocation,
  name: r'initialLocationPod',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$initialLocationHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef InitialLocationRef = AutoDisposeProviderRef<String>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
