import 'dart:async';

import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:kapoow/src/features/dashboard/ui/widgets/dashboard_scaffold.dart';
import 'package:kapoow/src/features/library_management/libraries/ui/screens/library_screen.dart';
import 'package:kapoow/src/features/library_management/recommended/ui/screens/recommended_screen.dart';

part 'dashboard_routes_data.g.dart';

/// All routes of the app
final dashboardRoutes = $appRoutes;

/// [DashboardShellRoute] is the root of the app
///
/// has a path of `'/'`
@TypedShellRoute<DashboardShellRoute>(
  routes: [
    TypedGoRoute<RecommendedRoute>(path: RecommendedRoute.path),
    TypedGoRoute<LibraryRoute>(path: LibraryRoute.path),
    TypedGoRoute<SettingsRoute>(path: SettingsRoute.path),
  ],
)
@immutable
class DashboardShellRoute extends ShellRouteData {
  /// Creates a [DashboardShellRoute] instance
  const DashboardShellRoute();

  @override
  Widget builder(BuildContext context, GoRouterState state, Widget navigator) {
    final bottomBarIndex = switch (state.fullPath) {
      '/library/:libraryId' => 1,
      '/settings' => 2,
      '/recommended' || _ => 0,
    };
    return DashboardScaffold(
      body: navigator,
      bottomBarIndex: bottomBarIndex,
      safeAreaTop: bottomBarIndex != 0,
    );
  }
}

/// Route that displays the recommended book and series
///
/// has a path of `'/recommended'`
@immutable
class RecommendedRoute extends GoRouteData {
  /// Creates a [RecommendedRoute] instance
  const RecommendedRoute();

  /// Value of `/recommended`
  static const path = '/recommended';

  @override
  Widget build(BuildContext context, GoRouterState state) =>
      const RecommendedScreen();
}

/// Route that displays the libraries
///
/// has a path of `'/libraries/:id'`
///
@immutable
class LibraryRoute extends GoRouteData {
  /// Creates a [LibraryRoute] instance
  const LibraryRoute({
    required this.libraryId,
  });

  /// Id of the library selected
  final String libraryId;

  /// Value of `/library/:libraryId`
  static const path = '/library/:libraryId';

  /// Common used id to refer to all the libraries
  ///
  /// has a value of `'All'`
  static const allLibrariesId = 'All';

  @override
  Widget build(BuildContext context, GoRouterState state) {
    final id = libraryId == LibraryRoute.allLibrariesId ? null : libraryId;
    return LibraryScreen(
      libraryId: id,
      key: ValueKey('library_$libraryId'),
    );
  }
}

/// Route that displays the settings of the app
///
/// has a path of `'/settings'`
@immutable
class SettingsRoute extends GoRouteData {
  /// Creates a [SettingsRoute] instance
  const SettingsRoute();

  /// Value of `/settings`
  static const path = '/settings';

  @override
  Widget build(BuildContext context, GoRouterState state) => const Center(
        child: Text('Settings'),
      );
}
