// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'routes_data.dart';

// **************************************************************************
// GoRouterGenerator
// **************************************************************************

List<RouteBase> get $appRoutes => [
      $rootRoute,
    ];

RouteBase get $rootRoute => GoRouteData.$route(
      path: '/',
      factory: $RootRouteExtension._fromState,
      routes: [
        GoRouteData.$route(
          path: 'search',
          factory: $SearchRouteExtension._fromState,
        ),
        GoRouteData.$route(
          path: 'series/:id',
          factory: $SeriesRouteExtension._fromState,
        ),
        GoRouteData.$route(
          path: 'readlists/:id',
          factory: $ReadlistRouteExtension._fromState,
        ),
        GoRouteData.$route(
          path: 'collections/:id',
          factory: $CollectionRouteExtension._fromState,
        ),
        GoRouteData.$route(
          path: 'books/:id',
          factory: $BookRouteExtension._fromState,
          routes: [
            GoRouteData.$route(
              path: 'read',
              factory: $ReadBookRouteExtension._fromState,
            ),
          ],
        ),
      ],
    );

extension $RootRouteExtension on RootRoute {
  static RootRoute _fromState(GoRouterState state) => const RootRoute();

  String get location => GoRouteData.$location(
        '/',
      );

  void go(BuildContext context) => context.go(location);

  Future<T?> push<T>(BuildContext context) => context.push<T>(location);

  void pushReplacement(BuildContext context) =>
      context.pushReplacement(location);

  void replace(BuildContext context) => context.replace(location);
}

extension $SearchRouteExtension on SearchRoute {
  static SearchRoute _fromState(GoRouterState state) => SearchRoute(
        state.uri.queryParameters['s'],
      );

  String get location => GoRouteData.$location(
        '/search',
        queryParams: {
          if (s != null) 's': s,
        },
      );

  void go(BuildContext context) => context.go(location);

  Future<T?> push<T>(BuildContext context) => context.push<T>(location);

  void pushReplacement(BuildContext context) =>
      context.pushReplacement(location);

  void replace(BuildContext context) => context.replace(location);
}

extension $SeriesRouteExtension on SeriesRoute {
  static SeriesRoute _fromState(GoRouterState state) => SeriesRoute(
        id: state.pathParameters['id']!,
      );

  String get location => GoRouteData.$location(
        '/series/${Uri.encodeComponent(id)}',
      );

  void go(BuildContext context) => context.go(location);

  Future<T?> push<T>(BuildContext context) => context.push<T>(location);

  void pushReplacement(BuildContext context) =>
      context.pushReplacement(location);

  void replace(BuildContext context) => context.replace(location);
}

extension $ReadlistRouteExtension on ReadlistRoute {
  static ReadlistRoute _fromState(GoRouterState state) => ReadlistRoute(
        id: state.pathParameters['id']!,
      );

  String get location => GoRouteData.$location(
        '/readlists/${Uri.encodeComponent(id)}',
      );

  void go(BuildContext context) => context.go(location);

  Future<T?> push<T>(BuildContext context) => context.push<T>(location);

  void pushReplacement(BuildContext context) =>
      context.pushReplacement(location);

  void replace(BuildContext context) => context.replace(location);
}

extension $CollectionRouteExtension on CollectionRoute {
  static CollectionRoute _fromState(GoRouterState state) => CollectionRoute(
        id: state.pathParameters['id']!,
      );

  String get location => GoRouteData.$location(
        '/collections/${Uri.encodeComponent(id)}',
      );

  void go(BuildContext context) => context.go(location);

  Future<T?> push<T>(BuildContext context) => context.push<T>(location);

  void pushReplacement(BuildContext context) =>
      context.pushReplacement(location);

  void replace(BuildContext context) => context.replace(location);
}

extension $BookRouteExtension on BookRoute {
  static BookRoute _fromState(GoRouterState state) => BookRoute(
        id: state.pathParameters['id']!,
        context: _$convertMapValue('context', state.uri.queryParameters,
            _$ReadContextEnumMap._$fromName),
        contextId: state.uri.queryParameters['context-id'],
      );

  String get location => GoRouteData.$location(
        '/books/${Uri.encodeComponent(id)}',
        queryParams: {
          if (context != null) 'context': _$ReadContextEnumMap[context!],
          if (contextId != null) 'context-id': contextId,
        },
      );

  void go(BuildContext context) => context.go(location);

  Future<T?> push<T>(BuildContext context) => context.push<T>(location);

  void pushReplacement(BuildContext context) =>
      context.pushReplacement(location);

  void replace(BuildContext context) => context.replace(location);
}

const _$ReadContextEnumMap = {
  ReadContext.readlist: 'readlist',
  ReadContext.series: 'series',
};

extension $ReadBookRouteExtension on ReadBookRoute {
  static ReadBookRoute _fromState(GoRouterState state) => ReadBookRoute(
        id: state.pathParameters['id']!,
        page: int.parse(state.uri.queryParameters['page']!),
        context: _$convertMapValue('context', state.uri.queryParameters,
            _$ReadContextEnumMap._$fromName),
        contextId: state.uri.queryParameters['context-id'],
        incognito: _$convertMapValue(
                'incognito', state.uri.queryParameters, _$boolConverter) ??
            false,
      );

  String get location => GoRouteData.$location(
        '/books/${Uri.encodeComponent(id)}/read',
        queryParams: {
          'page': page.toString(),
          if (context != null) 'context': _$ReadContextEnumMap[context!],
          if (contextId != null) 'context-id': contextId,
          if (incognito != false) 'incognito': incognito.toString(),
        },
      );

  void go(BuildContext context) => context.go(location);

  Future<T?> push<T>(BuildContext context) => context.push<T>(location);

  void pushReplacement(BuildContext context) =>
      context.pushReplacement(location);

  void replace(BuildContext context) => context.replace(location);
}

T? _$convertMapValue<T>(
  String key,
  Map<String, String> map,
  T Function(String) converter,
) {
  final value = map[key];
  return value == null ? null : converter(value);
}

extension<T extends Enum> on Map<T, String> {
  T _$fromName(String value) =>
      entries.singleWhere((element) => element.value == value).key;
}

bool _$boolConverter(String value) {
  switch (value) {
    case 'true':
      return true;
    case 'false':
      return false;
    default:
      throw UnsupportedError('Cannot convert "$value" into a bool.');
  }
}
