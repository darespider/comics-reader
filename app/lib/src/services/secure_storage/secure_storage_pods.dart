// coverage:ignore-file
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:kapoow/src/services/secure_storage/secure_storage_service.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'secure_storage_pods.g.dart';

/// Provides an instance of [FlutterSecureStorage]
@Riverpod(keepAlive: true)
FlutterSecureStorage flutterSecureStorage(FlutterSecureStorageRef ref) {
  return const FlutterSecureStorage(
    aOptions: AndroidOptions(
      encryptedSharedPreferences: true,
    ),
  );
}

/// Provides an instance of [SecureStorageService]
///
/// consumes [flutterSecureStoragePod]
@Riverpod(keepAlive: true)
SecureStorageService secureStorageService(SecureStorageServiceRef ref) {
  return SecureStorageService(ref.watch(flutterSecureStoragePod));
}
