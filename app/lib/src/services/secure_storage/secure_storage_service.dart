import 'package:flutter_secure_storage/flutter_secure_storage.dart';

/// Service that give access to the secure storage
class SecureStorageService {
  /// Creates instance of [SecureStorageService]
  SecureStorageService(this._secureStorage);

  final FlutterSecureStorage _secureStorage;

  /// Saves a [key] with the [value] in the secure storage
  Future<void> saveValue(String key, String value) => _secureStorage.write(
        key: key,
        value: value,
      );

  /// Reads the value in the [key] from the storage
  ///
  /// if the key doesn't exist the result will be null
  Future<String?> getValue(String key) => _secureStorage.read(key: key);

  /// Deletes the [key] and hist from the storage
  ///
  /// if the key doesn't nothing will happen
  Future<void> deleteValue(String key) => _secureStorage.delete(key: key);
}
