import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:kapoow/gen/assets.gen.dart';

/// Simple loader to reuse in all the app
class AppLoader extends StatelessWidget {
  /// Creates an instance of [AppLoader] widget
  const AppLoader({
    super.key,
    this.animate = true,
    this.scale = 125,
  });

  /// Enable or disable the animation
  final bool animate;

  /// Scale of the loader
  final double scale;

  @override
  Widget build(BuildContext context) {
    return Pulse(
      infinite: true,
      animate: animate,
      child: Assets.images.loader.onomatopoeia.svg(
        height: scale,
      ),
    );
  }
}
