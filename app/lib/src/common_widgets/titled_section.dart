import 'package:flutter/material.dart';
import 'package:kapoow/src/common_widgets/extensions.dart';
import 'package:kapoow/src/constants/app_paddings.dart';
import 'package:kapoow/src/theme/extensions.dart';
import 'package:kapoow/src/utils/extensions.dart';

/// Reusable widget for the titled sections of different app screens
class TitledSection extends StatelessWidget {
  /// Creates a [TitledSection] instance
  const TitledSection({
    required this.title,
    required this.body,
    this.titleTrailing,
    this.hide = false,
    super.key,
  });

  /// Text title to display above the [body]
  final Widget title;

  /// Widget to display at the end of the title
  final Widget? titleTrailing;

  /// Widget to display below the [title]
  final Widget body;

  /// If true this widget will be replaced with a SizedBox.shrink
  final bool hide;

  @override
  Widget build(BuildContext context) {
    final sectionHeightFactor = context.height / 4;

    return Visibility(
      visible: !hide,
      child: SizedBox(
        height: sectionHeightFactor,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: AppPaddings.small),
              child: Row(
                children: [
                  Expanded(
                    child: DefaultTextStyle.merge(
                      style: context.textTheme.titleLarge,
                      child: title,
                    ),
                  ),
                  if (titleTrailing != null) titleTrailing!,
                ],
              ),
            ),
            Flexible(child: body),
          ].separated(),
        ),
      ),
    );
  }
}
