import 'package:flutter/material.dart';

/// Wrapper around [SegmentedButton] that hides [showSelectedIcon] and
/// fices a tapTargetSize of MaterialTapTargetSize.shrinkWrap
class AppSegmentedButton<T> extends SegmentedButton<T> {
  /// Creates a [AppSegmentedButton] instance
  AppSegmentedButton({
    required super.segments,
    required super.selected,
    Color? backgroundColor,
    super.key,
    super.onSelectionChanged,
    super.multiSelectionEnabled = false,
    super.emptySelectionAllowed = false,
    super.selectedIcon,
  }) : super(
          style: ButtonStyle(
            tapTargetSize: MaterialTapTargetSize.shrinkWrap,
            backgroundColor: WidgetStateProperty.resolveWith((states) {
              if (states.contains(WidgetState.selected)) {
                return null;
              } else {
                return backgroundColor;
              }
            }),
          ),
          showSelectedIcon: false,
        );
}
