import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:kapoow/src/common_widgets/extensions.dart';
import 'package:kapoow/src/theme/extensions.dart';

/// Textfield that follows the app style
class AppTextField extends HookWidget {
  /// Creates an [AppTextField] instance
  const AppTextField({
    required this.prefixIcon,
    required this.hint,
    this.label,
    this.autofocus = false,
    this.onChanged,
    this.initialValue,
    this.keyboardType,
    this.inputFormatters = const [],
    this.suffixIcon,
    this.validator,
    this.autofillHints,
    this.controller,
    this.obscureText = false,
    super.key,
  });

  /// {@macro flutter.widgets.editableText.onChanged}
  final ValueChanged<String>? onChanged;

  /// {@macro flutter.widgets.editableText.keyboardType}
  final TextInputType? keyboardType;

  /// {@macro flutter.widgets.editableText.inputFormatters}
  final List<TextInputFormatter> inputFormatters;

  /// Widget to show before the text
  final Widget prefixIcon;

  /// Widget to show after the text
  final Widget? suffixIcon;

  /// Text that describes the input field.
  ///
  /// {@macro flutter.material.inputDecoration.label}
  final String? label;

  /// Text that suggests what sort of input the field accepts.
  final String hint;

  /// {@macro flutter.widgets.editableText.obscureText}
  final bool obscureText;

  /// An optional method that validates an input. Returns an error string to
  /// display if the input is invalid, or null otherwise.
  ///
  /// The returned value is exposed by the [FormFieldState.errorText] property.
  /// The [TextFormField] uses this to override the [InputDecoration.errorText]
  /// value.
  final FormFieldValidator<String>? validator;

  /// If not null the field will start with this value
  final String? initialValue;

  /// {@macro flutter.widgets.editableText.autofillHints}
  /// {@macro flutter.services.AutofillConfiguration.autofillHints}
  final Iterable<String>? autofillHints;

  /// Controls the text being edited.
  final TextEditingController? controller;

  /// {@macro flutter.widgets.editableText.autofocus}
  final bool autofocus;

  @override
  Widget build(BuildContext context) {
    final focusNode = useFocusNode();
    final hasFocus = useState(false);
    useEffect(
      () {
        focusNode.addListener(() {
          hasFocus.value = focusNode.hasFocus;
        });

        return null;
      },
      [focusNode],
    );

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        if (label != null)
          Align(
            alignment: AlignmentDirectional.centerStart,
            child: Text(
              label!,
              style: context.textTheme.labelLarge?.copyWith(
                color: hasFocus.value
                    ? context.colorScheme.primary
                    : context.colorScheme.onSurface,
              ),
            ),
          ),
        TextFormField(
          autofocus: autofocus,
          controller: controller,
          initialValue: initialValue,
          focusNode: focusNode,
          autofillHints: autofillHints,
          decoration: InputDecoration(
            hintText: hint,
            prefixIcon: prefixIcon,
            suffixIcon: suffixIcon,
          ),
          inputFormatters: inputFormatters,
          keyboardType: keyboardType,
          obscureText: obscureText,
          onChanged: onChanged,
          validator: validator,
        ),
      ].separated(),
    );
  }
}
