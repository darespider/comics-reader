import 'package:gap/gap.dart';
import 'package:kapoow/src/constants/app_paddings.dart';

/// Gap widgets that use [AppPaddings] as values.
///
/// See [AppPaddings] for more details.
class AppGap extends Gap {
  /// Gap with value of 4.0
  const AppGap.xxSmall({
    super.key,
    super.crossAxisExtent,
    super.color,
  }) : super(AppPaddings.xxSmall);

  /// Gap with value of 8.0
  const AppGap.xSmall({
    super.key,
    super.crossAxisExtent,
    super.color,
  }) : super(AppPaddings.xSmall);

  /// Gap with value of 12.0
  const AppGap.small({
    super.key,
    super.crossAxisExtent,
    super.color,
  }) : super(AppPaddings.small);

  /// Gap with value of 24.0
  const AppGap.medium({
    super.key,
    super.crossAxisExtent,
    super.color,
  }) : super(AppPaddings.medium);

  /// Gap with value of 36.0
  const AppGap.large({
    super.key,
    super.crossAxisExtent,
    super.color,
  }) : super(AppPaddings.large);

  /// Gap with value of 48.0
  const AppGap.xLarge({
    super.key,
    super.crossAxisExtent,
    super.color,
  }) : super(AppPaddings.xLarge);

  /// Gap with value of 54.0
  const AppGap.big({
    super.key,
    super.crossAxisExtent,
    super.color,
  }) : super(AppPaddings.big);

  /// Gap with value of 72.0
  const AppGap.xBig({
    super.key,
    super.crossAxisExtent,
    super.color,
  }) : super(AppPaddings.xBig);
}
