import 'dart:math' as math;

import 'package:flutter/material.dart';

/// Reusable persisten sliver header
class SliverPersistentHeaderBuilder extends SliverPersistentHeaderDelegate {
  /// Creates instance of [SliverPersistentHeaderBuilder]
  const SliverPersistentHeaderBuilder({
    required this.builder,
    required this.rebuild,
    this.minExtent = kToolbarHeight,
    this.maxHeight = kToolbarHeight,
  });

  /// Min space to use for the persistent space
  @override
  final double minExtent;

  /// Max space to use for the persistent space
  final double maxHeight;

  /// builder to render in the persistent space
  final Widget Function(
    BuildContext context,
    double shrinkOffset,
    // ignore: avoid_positional_boolean_parameters
    bool overlapsContent,
  ) builder;

  /// Callback used to know if the widget should rebuild
  final bool Function(SliverPersistentHeaderBuilder oldDelegate) rebuild;

  @override
  double get maxExtent => math.max(maxHeight, minExtent);

  @override
  Widget build(
    BuildContext context,
    double shrinkOffset,
    bool overlapsContent,
  ) =>
      build(context, shrinkOffset, overlapsContent);

  @override
  bool shouldRebuild(SliverPersistentHeaderBuilder oldDelegate) =>
      rebuild(oldDelegate);
}
