import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:kapoow/src/common_widgets/app_loader.dart';

/// Useful widget to do common transformation to widgets to AsyncValues
class TypicalWhenBuilder<T> extends StatelessWidget {
  /// Creates a [TypicalWhenBuilder] instance
  const TypicalWhenBuilder({
    required this.asyncValue,
    required this.dataBuilder,
    super.key,
    this.errorBuilder,
    this.loadingBuilder,
    this.refreshingBuilder,
  });

  /// Async value used to call [dataBuilder] [errorBuilder] [loadingBuilder]
  /// based on its state
  final AsyncValue<T> asyncValue;

  /// Call when [asyncValue] result is data
  final Widget Function(BuildContext context, T data) dataBuilder;

  /// Call when [asyncValue] result is error
  final Widget Function(Object error, StackTrace stack, BuildContext context)?
      errorBuilder;

  /// Call when [asyncValue] is loading
  final Widget Function(BuildContext context)? loadingBuilder;

  /// Call when [asyncValue] is refreshing or reloading.
  final Widget Function(BuildContext context)? refreshingBuilder;

  @override
  Widget build(BuildContext context) {
    if (refreshingBuilder != null &&
        (asyncValue.isRefreshing || asyncValue.isReloading)) {
      return refreshingBuilder!.call(context);
    }
    return asyncValue.when(
      loading: () =>
          loadingBuilder?.call(context) ?? const Center(child: AppLoader()),
      error: (error, stackTrace) =>
          errorBuilder?.call(error, stackTrace, context) ??
          // TODO(rurickdev): [l10n] translate
          const Text('Something When Wrong. Please try again later'),
      data: (data) => dataBuilder(context, data),
    );
  }
}
