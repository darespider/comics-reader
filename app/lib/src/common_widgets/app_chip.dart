import 'package:flutter/material.dart';

/// Chip like widget with a shrink tap target size
class AppChip extends Chip {
  /// Creates an [AppChip] instance with a Text as label
  AppChip({
    required String text,
    super.key,
  }) : super(
          materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
          label: Text(text),
        );
}
