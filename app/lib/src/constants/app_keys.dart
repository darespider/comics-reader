import 'package:flutter/material.dart';

/// Global Keys user all over the app
abstract class AppKeys {
  /// Navigator State Key to navigate easily from any part of the app
  ///
  /// this works even without a context
  static final navigator = GlobalKey<NavigatorState>();

  /// Scaffold Messenger State Key to get access to scaffold messenger
  /// functions from any part of the app
  ///
  /// this works even without a context
  static final scaffold = GlobalKey<ScaffoldMessengerState>();
}
