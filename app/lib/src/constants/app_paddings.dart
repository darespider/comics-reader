/// This paddings respects the Material suggested sizes
/// [Spacing Methods](https://material.io/design/layout/spacing-methods.html)
abstract class AppPaddings {
  /// xxSmall value is 4.0
  static const xxSmall = 4.0;

  /// xSmall value is 8.0
  static const xSmall = 8.0;

  /// small value is 12.0
  static const small = 12.0;

  /// medium value is 24.0
  static const medium = 24.0;

  /// large value is 36.0
  static const large = 36.0;

  /// xLarge value is 48.0
  static const xLarge = 48.0;

  /// big value is 54.0
  static const big = 54.0;

  /// xBig value is 72.0
  static const xBig = 72.0;
}
