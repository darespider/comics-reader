import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

/// Text theme that follows the Material Specs.
///
/// Uses Maven Pro font for titles and bigger sizes.
///
/// Uses Questrial font for body and smaller sizes.
///
/// it fetch the font files using Google Fonts package.
final textTheme = TextTheme(
  displayLarge: GoogleFonts.mavenPro(
    fontSize: 102,
    fontWeight: FontWeight.w300,
    letterSpacing: -1.5,
  ),
  displayMedium: GoogleFonts.mavenPro(
    fontSize: 64,
    fontWeight: FontWeight.w300,
    letterSpacing: -0.5,
  ),
  displaySmall: GoogleFonts.mavenPro(
    fontSize: 51,
    fontWeight: FontWeight.w400,
  ),
  headlineMedium: GoogleFonts.mavenPro(
    fontSize: 36,
    fontWeight: FontWeight.w400,
    letterSpacing: 0.25,
  ),
  headlineSmall: GoogleFonts.mavenPro(
    fontSize: 25,
    fontWeight: FontWeight.w400,
  ),
  titleLarge: GoogleFonts.mavenPro(
    fontSize: 21,
    fontWeight: FontWeight.w500,
    letterSpacing: 0.15,
  ),
  titleMedium: GoogleFonts.mavenPro(
    fontSize: 17,
    fontWeight: FontWeight.w400,
    letterSpacing: 0.15,
  ),
  titleSmall: GoogleFonts.mavenPro(
    fontSize: 15,
    fontWeight: FontWeight.w500,
    letterSpacing: 0.1,
  ),
  bodyLarge: GoogleFonts.questrial(
    fontSize: 17,
    fontWeight: FontWeight.w400,
    letterSpacing: 0.5,
  ),
  bodyMedium: GoogleFonts.questrial(
    fontSize: 15,
    fontWeight: FontWeight.w400,
    letterSpacing: 0.25,
  ),
  labelLarge: GoogleFonts.questrial(
    fontSize: 15,
    fontWeight: FontWeight.w500,
    letterSpacing: 1.25,
  ),
  bodySmall: GoogleFonts.questrial(
    fontSize: 13,
    fontWeight: FontWeight.w400,
    letterSpacing: 0.4,
  ),
  labelSmall: GoogleFonts.questrial(
    fontSize: 11,
    fontWeight: FontWeight.w400,
    letterSpacing: 1.5,
  ),
);
