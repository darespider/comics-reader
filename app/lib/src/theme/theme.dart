import 'package:flutter/material.dart';
import 'package:kapoow/src/theme/color_schemes.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';

/// Material Three Theme built from dark color scheme
///
/// ![Preview](https://gitlab.com/darespider/flugma/-/raw/main/app/docs/images/theme_preview_dark.png)
final darkTheme = ThemeData(
  colorScheme: darkColorScheme,
  useMaterial3: true,
  cardTheme: _baseCardTheme.copyWith(
    color: darkColorScheme.surfaceContainerHighest,
    surfaceTintColor: darkColorScheme.surfaceTint,
  ),
  actionIconTheme: _actionIconTheme,
  chipTheme: _chipTheme,
  inputDecorationTheme: _outlineDecoration,
  appBarTheme: _appBarTheme,
);

/// Material Three Theme built from light color scheme
///
/// ![Preview](https://gitlab.com/darespider/flugma/-/raw/main/app/docs/images/theme_preview_light.png)
final lightTheme = ThemeData(
  colorScheme: lightColorScheme,
  useMaterial3: true,
  cardTheme: _baseCardTheme.copyWith(
    color: lightColorScheme.surfaceContainerHighest,
    surfaceTintColor: lightColorScheme.surfaceTint,
  ),
  actionIconTheme: _actionIconTheme,
  inputDecorationTheme: _outlineDecoration,
  chipTheme: _chipTheme,
  appBarTheme: _appBarTheme,
);

const _baseCardTheme = CardTheme(
  elevation: 0,
  clipBehavior: Clip.antiAlias,
  margin: EdgeInsets.zero,
);

final _outlineDecoration = InputDecorationTheme(
  border: OutlineInputBorder(
    borderRadius: BorderRadius.circular(8),
  ),
);

final _actionIconTheme = ActionIconThemeData(
  backButtonIconBuilder: (_) => PhosphorIcon(
    PhosphorIcons.arrowLeft(PhosphorIconsStyle.duotone),
  ),
  drawerButtonIconBuilder: (_) => PhosphorIcon(
    PhosphorIcons.listDashes(PhosphorIconsStyle.duotone),
  ),
  closeButtonIconBuilder: (_) => PhosphorIcon(
    PhosphorIcons.xSquare(PhosphorIconsStyle.duotone),
  ),
  endDrawerButtonIconBuilder: (_) => Transform.flip(
    flipX: true,
    child: PhosphorIcon(
      PhosphorIcons.listDashes(PhosphorIconsStyle.duotone),
    ),
  ),
);

const _chipTheme = ChipThemeData(
  side: BorderSide.none,
  shape: RoundedRectangleBorder(
    borderRadius: BorderRadius.all(Radius.circular(12)),
  ),
);

const _appBarTheme = AppBarTheme(
  elevation: 0,
  backgroundColor: Colors.transparent,
);
