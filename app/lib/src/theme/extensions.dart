import 'package:flutter/material.dart';

/// Useful getters to common used theme properties
extension ThemeContext on BuildContext {
  /// Returns the Theme from the context
  ThemeData get theme => Theme.of(this);

  /// Returns the TextTheme from the context
  TextTheme get textTheme => theme.textTheme;

  /// Returns the ColorScheme from the context
  ColorScheme get colorScheme => theme.colorScheme;
}
