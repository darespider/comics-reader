import 'package:logger/logger.dart' as lib_logger;
import 'package:logging/logging.dart' as lib_logging;

lib_logger.Level _mapLibLoggingLevelToLibLoggerLevel(lib_logging.Level level) {
  if (level == lib_logging.Level.ALL) return lib_logger.Level.trace;
  if (level == lib_logging.Level.FINEST) return lib_logger.Level.trace;
  if (level == lib_logging.Level.FINER) return lib_logger.Level.trace;
  if (level == lib_logging.Level.FINE) return lib_logger.Level.debug;
  if (level == lib_logging.Level.CONFIG) return lib_logger.Level.debug;
  if (level == lib_logging.Level.INFO) return lib_logger.Level.info;
  if (level == lib_logging.Level.WARNING) return lib_logger.Level.warning;
  if (level == lib_logging.Level.SEVERE) return lib_logger.Level.error;
  if (level == lib_logging.Level.SHOUT) return lib_logger.Level.fatal;
  if (level == lib_logging.Level.OFF) return lib_logger.Level.off;
  throw Exception('unmatched log level');
}

/// Global configuration for logging
void setupGlobalLogging(lib_logging.Level level) {
  lib_logging.recordStackTraceAtLevel = lib_logging.Level.WARNING;

  lib_logging.Logger.root.level = level;

  final logger = lib_logger.Logger(
    printer: lib_logger.PrettyPrinter(methodCount: 0, errorMethodCount: 50),
  );

  lib_logging.Logger.root.onRecord.listen((record) {
    logger.log(
      _mapLibLoggingLevelToLibLoggerLevel(record.level),
      record.message,
      error: record.error,
      stackTrace: record.stackTrace,
    );
  });
}
