import 'dart:async';

import 'package:flutter/widgets.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

/// Creates a [Timer.periodic] that will be disposed automatically.
///
/// See also:
/// - [Timer]
Timer usePeriodicTimer(
  Duration duration,
  ValueChanged<Timer> callback, [
  List<Object?>? keys,
]) {
  return use(
    _PeriodicTimerHook(
      callback: callback,
      duration: duration,
      keys: keys,
    ),
  );
}

class _PeriodicTimerHook extends Hook<Timer> {
  const _PeriodicTimerHook({
    required this.duration,
    required this.callback,
    super.keys,
  });

  final Duration duration;
  final ValueChanged<Timer> callback;

  @override
  HookState<Timer, Hook<Timer>> createState() => _PeriodicTimerHookState();
}

class _PeriodicTimerHookState extends HookState<Timer, _PeriodicTimerHook> {
  late final timer = Timer.periodic(
    hook.duration,
    hook.callback,
  );

  @override
  Timer build(BuildContext context) => timer;

  @override
  void dispose() => timer.cancel();

  @override
  String get debugLabel => 'usePeriodicTimer';
}
