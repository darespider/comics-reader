import 'dart:math';

import 'package:flutter/material.dart';
import 'package:kapoow/gen/l10n.gen.dart';

/// Useful extension to easy access translations from context
extension ContextGetters on BuildContext {
  /// Returns the result of Translations.of(context)
  Translations get translations => Translations.of(this);

  /// Returns the Media Query Data from the context
  MediaQueryData get mediaQuery => MediaQuery.of(this);

  /// Returns the Media Query Size from the context
  Size get mediaQuerySize => mediaQuery.size;

  /// Returns the width from the media query size from the context
  double get width => mediaQuerySize.width;

  /// Returns the height from the media query size from the context
  double get height => mediaQuerySize.height;
}

/// Allows to convert strings to lists that contain itself only
extension ListableString on String? {
  /// if `this` is not `null` or empty returns a list containing only `this`,
  /// returns `null` otherwise
  List<String>? toNullableList() => this?.isNotEmpty ?? false ? [this!] : null;

  /// if `this` is not `null` or empty returns a list containing only `this`,
  /// returns an empty list otherwise
  List<String> toEmptyList() => [if (this?.isNotEmpty ?? false) this!];
}

/// Extension to make operations on Iterable
extension RandomItem<T> on Iterable<T> {
  /// Returns a Pseudo random item from the iterable
  T get randomElement {
    final index = Random().nextInt(length - 1);
    return elementAt(index);
  }
}

/// Commonly used extensions to format the case of Strings
extension StringCasing on String {
  /// Transform the current [String] to follow Proper Case
  ///
  /// "this is a string" -> "This Is A String"
  String toProperCase() {
    final words = split(' ');
    var result = '';

    for (final word in words) {
      final lower = word.toLowerCase();
      if (lower.isNotEmpty) {
        final proper =
            '${lower.substring(0, 1).toUpperCase()}${lower.substring(1)}';
        result = result.isEmpty ? proper : '$result $proper';
      }
    }
    return result;
  }
}
