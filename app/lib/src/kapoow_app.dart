import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:kapoow/gen/l10n.gen.dart';
import 'package:kapoow/src/routes/router_pod.dart';
import 'package:kapoow/src/theme/theme.dart';

/// Main Application
///
/// Generates a MaterialApp.router using the router from the [goRouterPod]
class KapoowApp extends ConsumerWidget {
  /// Main Application constructor
  const KapoowApp({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final router = ref.watch(goRouterPod);

    return MaterialApp.router(
      title: 'Kapoow!',
      theme: lightTheme,
      darkTheme: darkTheme,
      routerConfig: router,
      debugShowCheckedModeBanner: false,
      localizationsDelegates: Translations.localizationsDelegates,
      supportedLocales: Translations.supportedLocales,
    );
  }
}
