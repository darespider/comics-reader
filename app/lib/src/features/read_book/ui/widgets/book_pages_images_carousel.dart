import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:kapoow/src/common_widgets/app_loader.dart';
import 'package:kapoow/src/features/read_book/domain/models/read_book_page_query_params.dart';
import 'package:kapoow/src/features/read_book/ui/controllers/pages_images_notifier.dart';
import 'package:kapoow/src/routes/routes_data.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';

/// PageView with all the pages images of the book with the [bookId]
class BookPagesImagesCarousel extends HookConsumerWidget {
  /// Creates an instance of [BookPagesImagesCarousel]
  const BookPagesImagesCarousel({
    required this.bookId,
    required this.params,
    required this.onTap,
    this.controller,
    super.key,
  });

  /// Id of the book to display the pages
  final String bookId;

  /// Query params from the route to display the right book page
  final ReadBookPageQueryParams params;

  /// What to do when tapping a page, will pass the page number
  final ValueChanged<int> onTap;

  /// [PageController] used to manage the pages
  final PageController? controller;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final pod = pagesImagesNotifierPod(bookId);
    final state = ref.watch(pod);
    final notifier = ref.watch(pod.notifier);
    final currentPage = useRef(params.page);
    final pageController =
        controller ?? usePageController(initialPage: currentPage.value - 1);

    if (pageController.hasClients) {
      final debouncedPage = useDebounced(
        currentPage.value,
        const Duration(milliseconds: 300),
      );
      useEffect(
        () {
          if (debouncedPage != null) {
            WidgetsBinding.instance.addPostFrameCallback(
              (_) => ReadBookRoute(
                id: bookId,
                context: params.readContext,
                contextId: params.readContextId,
                page: debouncedPage,
                incognito: params.incognito,
              ).go(context),
            );
          }
          return null;
        },
        [debouncedPage],
      );
    }

    // TODO(rurickdev): [Enhancement] Replace for a more ergonomic widget
    if (state.pages.isEmpty) return const Center(child: AppLoader());

    return _BookPagesImagesCarousel(
      pageController: pageController,
      pagesCount: state.pagesCount,
      pagesBytes: state.pages,
      onTap: onTap,
      onPageChange: (value) {
        notifier.loadPage(value);
        currentPage.value = value;
      },
    );
  }
}

class _BookPagesImagesCarousel extends HookConsumerWidget {
  const _BookPagesImagesCarousel({
    required this.pageController,
    required this.pagesCount,
    required this.pagesBytes,
    required this.onTap,
    this.onPageChange,
  });

  final PageController pageController;
  final int pagesCount;
  final Map<int, Uint8List?> pagesBytes;
  final ValueChanged<int>? onPageChange;
  final ValueChanged<int> onTap;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    // TODO(rurickdev): [Enhancement] Replace for a more ergonomic widget
    const loadingWidget = Center(child: AppLoader());

    return PhotoViewGallery.builder(
      pageController: pageController,
      itemCount: pagesCount,
      loadingBuilder: (context, event) => loadingWidget,
      onPageChanged: (value) {
        final newPage = value + 1;
        onPageChange?.call(newPage);
      },
      builder: (context, index) {
        final pageNumber = index + 1;

        Widget page = loadingWidget;
        if (pagesBytes.containsKey(pageNumber)) {
          final bytes = pagesBytes[pageNumber];
          if (bytes != null) {
            page = Image.memory(bytes);
          } else {
            page = const Center(child: Text('Error Loading Page'));
          }
        }

        return PhotoViewGalleryPageOptions.customChild(
          minScale: PhotoViewComputedScale.covered,
          child: GestureDetector(
            // TODO(rurickdev): [Enhancement] add side tap navigation
            onTap: () => onTap(pageNumber),
            child: page,
          ),
        );
      },
    );
  }
}
