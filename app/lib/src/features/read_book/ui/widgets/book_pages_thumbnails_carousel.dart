import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:kapoow/src/features/library_management/books/domain/models/book.dart';
import 'package:kapoow/src/features/library_management/common/ui/widgets/item_cover/cover_aspect_ratio.dart';
import 'package:kapoow/src/features/library_management/common/ui/widgets/item_cover/cover_decoration.dart';
import 'package:kapoow/src/features/library_management/common/ui/widgets/item_cover/cover_image.dart';
import 'package:kapoow/src/features/read_book/ui/controllers/pods.dart';
import 'package:kapoow/src/features/read_book/ui/widgets/book_pages_images_carousel.dart';

/// Horizontal scroll list with all the thumbnails of the current [book]
class BookPagesThumbnailsCarousel extends ConsumerWidget {
  /// Creates an instance of [BookPagesImagesCarousel]
  const BookPagesThumbnailsCarousel({
    required this.book,
    required this.selectedPage,
    required this.onPageTap,
    super.key,
  });

  /// Book to fetch the pages thumbnails from
  final Book book;

  /// The displayed page when the thumbnails carousel was trigger
  final int selectedPage;

  /// Called when a thumbnail is tapped, will pass the number of the page.
  final ValueChanged<int> onPageTap;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final state = ref.watch(bookPagesThumbnailsPod(book));

    return _BookPagesThumbnailsCarousel(
      selectedPage: selectedPage,
      thumbnails: state,
      onPageTap: onPageTap,
    );
  }
}

class _BookPagesThumbnailsCarousel extends HookWidget {
  const _BookPagesThumbnailsCarousel({
    required this.thumbnails,
    required this.selectedPage,
    required this.onPageTap,
  });

  final int selectedPage;
  final Map<int, String> thumbnails;
  final ValueChanged<int> onPageTap;

  static const _spacing = 12.0;
  static const _padding = 20.0;
  static const _coverHeight = 215.0;
  static const _coverWidth = (_coverHeight - _padding) * coverAspectRatioValue;

  @override
  Widget build(BuildContext context) {
    // Add ghost cover at start and end
    final effectiveLength = thumbnails.length + 2;
    final currentPagePosition = _coverWidth * (selectedPage - 1);
    final controller = useScrollController(
      initialScrollOffset: currentPagePosition,
    );

    return SizedBox(
      height: _coverHeight,
      child: ListView.separated(
        controller: controller,
        itemCount: effectiveLength,
        scrollDirection: Axis.horizontal,
        padding: const EdgeInsets.symmetric(horizontal: _spacing),
        separatorBuilder: (context, index) => const SizedBox(width: _spacing),
        itemBuilder: (context, index) {
          final pageNumber = index;
          final isStandOutPage = pageNumber == selectedPage;

          Widget child = AspectRatio(
            aspectRatio: coverAspectRatioValue,
            child: Container(
              color: Colors.transparent,
              width: _coverHeight,
            ),
          );
          if (thumbnails.containsKey(index)) {
            final thumbUrl = thumbnails[pageNumber];
            // Should never be `null` but its a fail safe
            if (thumbUrl == null) {
              child = const CoverImage.placeHolder();
            } else {
              child = CoverImage(
                coverUrl: thumbUrl,
                onTap: () => onPageTap(pageNumber),
                decoration: CoverDecorationData(
                  finished: isStandOutPage,
                  itemsCount: pageNumber,
                ),
              );
            }
          }

          return Padding(
            padding: EdgeInsets.symmetric(
              vertical: isStandOutPage ? 0 : _padding,
            ),
            child: child,
          );
        },
      ),
    );
  }
}
