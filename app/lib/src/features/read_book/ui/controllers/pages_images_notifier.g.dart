// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pages_images_notifier.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$pagesImagesNotifierHash() =>
    r'30357a4acd35c40197287d36716c04a815040cbe';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

abstract class _$PagesImagesNotifier
    extends BuildlessAutoDisposeNotifier<BookPagesState> {
  late final String bookId;

  BookPagesState build(
    String bookId,
  );
}

/// Notifier used to download and cache the bytes of the images for the pages
/// of the book with the [bookId].
///
/// The state consist of a [Map<int, Uint8List?>] which contains the bytes
/// of every page image.
///
/// If the state is empty that means the notifier hasn't downloaded any page.
///
/// To validate if a page was already tried to be downloaded but failed use
/// [Map.containsKey()] method, this will return `true` if the key exists
/// on the map even when the value is `null`.
///
/// Copied from [PagesImagesNotifier].
@ProviderFor(PagesImagesNotifier)
const pagesImagesNotifierPod = PagesImagesNotifierFamily();

/// Notifier used to download and cache the bytes of the images for the pages
/// of the book with the [bookId].
///
/// The state consist of a [Map<int, Uint8List?>] which contains the bytes
/// of every page image.
///
/// If the state is empty that means the notifier hasn't downloaded any page.
///
/// To validate if a page was already tried to be downloaded but failed use
/// [Map.containsKey()] method, this will return `true` if the key exists
/// on the map even when the value is `null`.
///
/// Copied from [PagesImagesNotifier].
class PagesImagesNotifierFamily extends Family<BookPagesState> {
  /// Notifier used to download and cache the bytes of the images for the pages
  /// of the book with the [bookId].
  ///
  /// The state consist of a [Map<int, Uint8List?>] which contains the bytes
  /// of every page image.
  ///
  /// If the state is empty that means the notifier hasn't downloaded any page.
  ///
  /// To validate if a page was already tried to be downloaded but failed use
  /// [Map.containsKey()] method, this will return `true` if the key exists
  /// on the map even when the value is `null`.
  ///
  /// Copied from [PagesImagesNotifier].
  const PagesImagesNotifierFamily();

  /// Notifier used to download and cache the bytes of the images for the pages
  /// of the book with the [bookId].
  ///
  /// The state consist of a [Map<int, Uint8List?>] which contains the bytes
  /// of every page image.
  ///
  /// If the state is empty that means the notifier hasn't downloaded any page.
  ///
  /// To validate if a page was already tried to be downloaded but failed use
  /// [Map.containsKey()] method, this will return `true` if the key exists
  /// on the map even when the value is `null`.
  ///
  /// Copied from [PagesImagesNotifier].
  PagesImagesNotifierProvider call(
    String bookId,
  ) {
    return PagesImagesNotifierProvider(
      bookId,
    );
  }

  @override
  PagesImagesNotifierProvider getProviderOverride(
    covariant PagesImagesNotifierProvider provider,
  ) {
    return call(
      provider.bookId,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'pagesImagesNotifierPod';
}

/// Notifier used to download and cache the bytes of the images for the pages
/// of the book with the [bookId].
///
/// The state consist of a [Map<int, Uint8List?>] which contains the bytes
/// of every page image.
///
/// If the state is empty that means the notifier hasn't downloaded any page.
///
/// To validate if a page was already tried to be downloaded but failed use
/// [Map.containsKey()] method, this will return `true` if the key exists
/// on the map even when the value is `null`.
///
/// Copied from [PagesImagesNotifier].
class PagesImagesNotifierProvider extends AutoDisposeNotifierProviderImpl<
    PagesImagesNotifier, BookPagesState> {
  /// Notifier used to download and cache the bytes of the images for the pages
  /// of the book with the [bookId].
  ///
  /// The state consist of a [Map<int, Uint8List?>] which contains the bytes
  /// of every page image.
  ///
  /// If the state is empty that means the notifier hasn't downloaded any page.
  ///
  /// To validate if a page was already tried to be downloaded but failed use
  /// [Map.containsKey()] method, this will return `true` if the key exists
  /// on the map even when the value is `null`.
  ///
  /// Copied from [PagesImagesNotifier].
  PagesImagesNotifierProvider(
    String bookId,
  ) : this._internal(
          () => PagesImagesNotifier()..bookId = bookId,
          from: pagesImagesNotifierPod,
          name: r'pagesImagesNotifierPod',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$pagesImagesNotifierHash,
          dependencies: PagesImagesNotifierFamily._dependencies,
          allTransitiveDependencies:
              PagesImagesNotifierFamily._allTransitiveDependencies,
          bookId: bookId,
        );

  PagesImagesNotifierProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.bookId,
  }) : super.internal();

  final String bookId;

  @override
  BookPagesState runNotifierBuild(
    covariant PagesImagesNotifier notifier,
  ) {
    return notifier.build(
      bookId,
    );
  }

  @override
  Override overrideWith(PagesImagesNotifier Function() create) {
    return ProviderOverride(
      origin: this,
      override: PagesImagesNotifierProvider._internal(
        () => create()..bookId = bookId,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        bookId: bookId,
      ),
    );
  }

  @override
  AutoDisposeNotifierProviderElement<PagesImagesNotifier, BookPagesState>
      createElement() {
    return _PagesImagesNotifierProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is PagesImagesNotifierProvider && other.bookId == bookId;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, bookId.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin PagesImagesNotifierRef on AutoDisposeNotifierProviderRef<BookPagesState> {
  /// The parameter `bookId` of this provider.
  String get bookId;
}

class _PagesImagesNotifierProviderElement
    extends AutoDisposeNotifierProviderElement<PagesImagesNotifier,
        BookPagesState> with PagesImagesNotifierRef {
  _PagesImagesNotifierProviderElement(super.provider);

  @override
  String get bookId => (origin as PagesImagesNotifierProvider).bookId;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
