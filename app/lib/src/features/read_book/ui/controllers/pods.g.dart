// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pods.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$bookPagesThumbnailsHash() =>
    r'2d61c22ab3bab65559ab7964a56a5159c4fb9103';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// Pod that builds the thumbnails relative urls for all the pages of the [book]
/// and returns a [Map<int, String>] that matches the page number with the url
///
/// Copied from [bookPagesThumbnails].
@ProviderFor(bookPagesThumbnails)
const bookPagesThumbnailsPod = BookPagesThumbnailsFamily();

/// Pod that builds the thumbnails relative urls for all the pages of the [book]
/// and returns a [Map<int, String>] that matches the page number with the url
///
/// Copied from [bookPagesThumbnails].
class BookPagesThumbnailsFamily extends Family<Map<int, String>> {
  /// Pod that builds the thumbnails relative urls for all the pages of the [book]
  /// and returns a [Map<int, String>] that matches the page number with the url
  ///
  /// Copied from [bookPagesThumbnails].
  const BookPagesThumbnailsFamily();

  /// Pod that builds the thumbnails relative urls for all the pages of the [book]
  /// and returns a [Map<int, String>] that matches the page number with the url
  ///
  /// Copied from [bookPagesThumbnails].
  BookPagesThumbnailsProvider call(
    Book book,
  ) {
    return BookPagesThumbnailsProvider(
      book,
    );
  }

  @override
  BookPagesThumbnailsProvider getProviderOverride(
    covariant BookPagesThumbnailsProvider provider,
  ) {
    return call(
      provider.book,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'bookPagesThumbnailsPod';
}

/// Pod that builds the thumbnails relative urls for all the pages of the [book]
/// and returns a [Map<int, String>] that matches the page number with the url
///
/// Copied from [bookPagesThumbnails].
class BookPagesThumbnailsProvider
    extends AutoDisposeProvider<Map<int, String>> {
  /// Pod that builds the thumbnails relative urls for all the pages of the [book]
  /// and returns a [Map<int, String>] that matches the page number with the url
  ///
  /// Copied from [bookPagesThumbnails].
  BookPagesThumbnailsProvider(
    Book book,
  ) : this._internal(
          (ref) => bookPagesThumbnails(
            ref as BookPagesThumbnailsRef,
            book,
          ),
          from: bookPagesThumbnailsPod,
          name: r'bookPagesThumbnailsPod',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$bookPagesThumbnailsHash,
          dependencies: BookPagesThumbnailsFamily._dependencies,
          allTransitiveDependencies:
              BookPagesThumbnailsFamily._allTransitiveDependencies,
          book: book,
        );

  BookPagesThumbnailsProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.book,
  }) : super.internal();

  final Book book;

  @override
  Override overrideWith(
    Map<int, String> Function(BookPagesThumbnailsRef provider) create,
  ) {
    return ProviderOverride(
      origin: this,
      override: BookPagesThumbnailsProvider._internal(
        (ref) => create(ref as BookPagesThumbnailsRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        book: book,
      ),
    );
  }

  @override
  AutoDisposeProviderElement<Map<int, String>> createElement() {
    return _BookPagesThumbnailsProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is BookPagesThumbnailsProvider && other.book == book;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, book.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin BookPagesThumbnailsRef on AutoDisposeProviderRef<Map<int, String>> {
  /// The parameter `book` of this provider.
  Book get book;
}

class _BookPagesThumbnailsProviderElement
    extends AutoDisposeProviderElement<Map<int, String>>
    with BookPagesThumbnailsRef {
  _BookPagesThumbnailsProviderElement(super.provider);

  @override
  Book get book => (origin as BookPagesThumbnailsProvider).book;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
