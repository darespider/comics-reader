import 'package:kapoow/src/features/library_management/books/domain/models/book.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'pods.g.dart';

/// Pod that builds the thumbnails relative urls for all the pages of the [book]
/// and returns a [Map<int, String>] that matches the page number with the url
@riverpod
Map<int, String> bookPagesThumbnails(BookPagesThumbnailsRef ref, Book book) {
  const replaceString = '{{PAGE}}';
  final baseUrl = 'books/${book.id}/pages/$replaceString/thumbnail';
  final entries = List.generate(
    book.media.pagesCount,
    (i) => MapEntry(
      i + 1,
      baseUrl.replaceAll(replaceString, (i + 1).toString()),
    ),
  );

  return Map.fromEntries(entries);
}
