/// Contains the posible replacements of the current book, that could be:
/// - [ChangeBookDirection.next]
/// - [ChangeBookDirection.previous]
enum ChangeBookDirection {
  /// Indicates a change to the next book
  next,

  /// Indicates a change to the previous book
  previous;
}
