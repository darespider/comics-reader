import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kapoow/src/features/read_book/domain/models/read_context.dart';

part 'read_book_page_query_params.freezed.dart';

/// Media information of a book
@freezed
class ReadBookPageQueryParams with _$ReadBookPageQueryParams {
  /// Creates a [ReadBookPageQueryParams] instance
  const factory ReadBookPageQueryParams({
    @Default(1) int page,
    ReadContext? readContext,
    String? readContextId,
    @Default(false) bool incognito,
  }) = _ReadBookPageQueryParams;
}
