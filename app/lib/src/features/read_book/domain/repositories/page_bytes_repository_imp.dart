import 'dart:async';
import 'dart:typed_data';

import 'package:kapoow/src/features/read_book/data/page_bytes_repository.dart';
import 'package:kapoow/src/features/rest_api/data/datasources/books/books_datasource.dart';
import 'package:kapoow/src/features/rest_api/data/datasources/cached_images/cached_images_datasource.dart';

/// Reads the bytes from local temp files, if there is no files then downloads
/// the bytes and saves the files.
class PageBytesRepositoryImp extends PageBytesRepository {
  /// Creates a [PageBytesRepositoryImp] and requieres a [BooksDatasource]
  PageBytesRepositoryImp(this._datasource, this._cache);

  final BooksDatasource _datasource;
  final CachedImagesDatasource _cache;

  @override
  Future<Uint8List> downloadPage(String bookId, int page) async {
    var result = await _cache.getCachePageByBookId(bookId, page: page);
    if (result == null) {
      result = await _datasource.getPageByBookId(bookId, page: page);
      // no need to wait for cache to save the file
      unawaited(_cache.putCachePageByBookId(bookId, page: page, bytes: result));
    }

    return result;
  }

  @override
  Future<Uint8List> downloadPageThumbnail(String bookId, int page) async {
    return _datasource.getPageThumbnailByBookId(bookId, page: page);
  }
}
