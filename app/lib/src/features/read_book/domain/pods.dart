import 'package:kapoow/src/features/read_book/data/page_bytes_repository.dart';
import 'package:kapoow/src/features/read_book/domain/repositories/page_bytes_repository_imp.dart';
import 'package:kapoow/src/features/rest_api/data/datasources/pods.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'pods.g.dart';

/// Provides an instance of [PageBytesRepository]
@Riverpod(keepAlive: true)
PageBytesRepository pageBytesRepo(PageBytesRepoRef ref) {
  final datasource = ref.watch(booksDatasourcePod);
  final cache = ref.watch(cachedImagesDatasourcePod);
  return PageBytesRepositoryImp(datasource, cache);
}
