// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pods.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$pageBytesRepoHash() => r'2f9b5851a4ff1be69c2eff2b6b9154fb60276e77';

/// Provides an instance of [PageBytesRepository]
///
/// Copied from [pageBytesRepo].
@ProviderFor(pageBytesRepo)
final pageBytesRepoPod = Provider<PageBytesRepository>.internal(
  pageBytesRepo,
  name: r'pageBytesRepoPod',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$pageBytesRepoHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef PageBytesRepoRef = ProviderRef<PageBytesRepository>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
