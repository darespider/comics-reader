import 'package:kapoow/src/features/session/domain/models/session_data.dart';

/// Representation of the initial state of the app
sealed class InitState {}

/// The app has a current valid session
class InitStateSession implements InitState {
  /// Creates an [InitStateSession] instance
  const InitStateSession(this.session);

  /// Current session
  final SessionData session;
}

/// The app has a session but is invalid
class InitStateInvalid implements InitState {
  /// Creates an [InitStateInvalid] instance
  const InitStateInvalid();
}

/// The app has no session
class InitStateNoSession implements InitState {
  /// Creates an [InitStateNoSession] instance
  const InitStateNoSession();
}
