import 'package:kapoow/src/features/app_init/ui/controllers/init_state.dart';
import 'package:kapoow/src/features/rest_api/domain/api_exceptions.dart';
import 'package:kapoow/src/features/rest_api/domain/pods.dart';
import 'package:kapoow/src/features/session/domain/controllers/session_pods.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'init_pods.g.dart';

/// Pod used to handle the async init process
@riverpod
FutureOr<InitState> init(InitRef ref) async {
  final session = await ref.watch(sessionAsyncPod.future);
  final userRepo = ref.watch(userRepositoryPod);
  if (session.invalid) return const InitStateNoSession();

  try {
    await userRepo.getMeData();
    return InitStateSession(session);
  } on UnauthorizedApiException {
    return const InitStateInvalid();
  } catch (e) {
    rethrow;
  }
}
