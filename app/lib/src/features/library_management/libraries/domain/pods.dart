import 'package:kapoow/src/features/library_management/libraries/data/libraries_repository.dart';
import 'package:kapoow/src/features/library_management/libraries/domain/repositories/libraries_repository_imp.dart';
import 'package:kapoow/src/features/rest_api/data/datasources/pods.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'pods.g.dart';

/// Provides the [LibrariesRepository]
@Riverpod(keepAlive: true)
LibrariesRepository librariesRepo(LibrariesRepoRef ref) {
  return LibrariesRepositoryImp(ref.watch(librariesDatasourcePod));
}
