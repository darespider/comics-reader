// ignore_for_file: public_member_api_docs
import 'package:freezed_annotation/freezed_annotation.dart';

part 'library.freezed.dart';

@freezed
class Library with _$Library {
  const factory Library({
    required String id,
    required String name,
    required String root,
    required bool importComicInfoBook,
    required bool importComicInfoSeries,
    required bool importComicInfoCollection,
    required bool importComicInfoReadList,
    required bool importComicInfoSeriesAppendVolume,
    required bool importEpubBook,
    required bool importEpubSeries,
    required bool importMylarSeries,
    required bool importLocalArtwork,
    required bool importBarcodeIsbn,
    required bool scanForceModifiedTime,
    required String scanInterval,
    required bool scanOnStartup,
    required bool scanCbx,
    required bool scanPdf,
    required bool scanEpub,
    required Set<String> scanDirectoryExclusions,
    required bool repairExtensions,
    required bool convertToCbz,
    required bool emptyTrashAfterScan,
    required String seriesCover,
    required bool hashFiles,
    required bool hashPages,
    required bool analyzeDimensions,
    required bool unavailable,
    String? oneshotsDirectory,
  }) = _Library;
}
