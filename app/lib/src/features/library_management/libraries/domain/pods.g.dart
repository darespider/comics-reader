// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pods.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$librariesRepoHash() => r'd64fed75732cd95b23cdca2e1f98e1fccc27dafe';

/// Provides the [LibrariesRepository]
///
/// Copied from [librariesRepo].
@ProviderFor(librariesRepo)
final librariesRepoPod = Provider<LibrariesRepository>.internal(
  librariesRepo,
  name: r'librariesRepoPod',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$librariesRepoHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef LibrariesRepoRef = ProviderRef<LibrariesRepository>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
