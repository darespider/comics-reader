import 'package:kapoow/src/features/library_management/libraries/data/libraries_repository.dart';
import 'package:kapoow/src/features/library_management/libraries/domain/models/library.dart';
import 'package:kapoow/src/features/library_management/libraries/domain/repositories/dto_domain_parser.dart';
import 'package:kapoow/src/features/rest_api/data/datasources/libraries/libraries_datasource.dart';

/// Libraries repository that get the data from the api
class LibrariesRepositoryImp implements LibrariesRepository {
  /// Creates instance of [LibrariesRepository]
  LibrariesRepositoryImp(this._datasource);

  final LibrariesDatasource _datasource;

  @override
  Future<List<Library>> getLibraries() async {
    final dtos = await _datasource.getLibraries();
    return dtos.map((dto) => dto.toDomain()).toList();
  }

  @override
  Future<Library> getLibraryById(String id) async {
    final dto = await _datasource.getLibraryById(id);
    return dto.toDomain();
  }
}
