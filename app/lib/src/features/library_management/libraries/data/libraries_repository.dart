import 'package:kapoow/src/features/library_management/libraries/domain/models/library.dart';

/// Interface describing how the repository should work
abstract class LibrariesRepository {
  /// Returns a list of [Library] fetch from the api
  Future<List<Library>> getLibraries();

  /// Returns a single [Library] fetch from the api
  Future<Library> getLibraryById(String id);
}
