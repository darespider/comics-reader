import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:kapoow/src/features/library_management/libraries/ui/controllers/library_readlists_paginated_pod.dart';
import 'package:kapoow/src/features/library_management/libraries/ui/widgets/library_items_list.dart';
import 'package:kapoow/src/features/library_management/read_lists/ui/widgets/readlist_books_row.dart';

/// Displays an "infinite" list of readlists from the library.
class LibraryReadlistsList extends ConsumerWidget {
  /// Creates a [LibraryReadlistsList] instance
  const LibraryReadlistsList({
    required this.libraryId,
    super.key,
  });

  /// Library Id from where the collections are
  final String? libraryId;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final readlistsPages = ref.watch(libraryReadListPaginatedPod(libraryId));

    return LibraryItemsList(
      paginatedState: readlistsPages,
      nextPageLoader: ref
          .read(libraryReadListPaginatedPod(libraryId).notifier)
          .loadNextPage,
      itemBuilder: (_, item) => ReadlistFirstBooksRow(readlist: item),
    );
  }
}
