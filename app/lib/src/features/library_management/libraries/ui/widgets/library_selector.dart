import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:kapoow/src/common_widgets/typical_when_builder.dart';
import 'package:kapoow/src/features/library_management/libraries/domain/models/library.dart';
import 'package:kapoow/src/features/library_management/libraries/ui/controllers/pods.dart';
import 'package:kapoow/src/routes/dashboard_routes_data.dart';
import 'package:kapoow/src/theme/extensions.dart';
import 'package:kapoow/src/utils/extensions.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';

/// Dropdown to select one of the libraries from the user, the default
/// value is `null` that is represented as "All Libraries"
class LibrarySelector extends ConsumerWidget {
  /// Creates a [LibrarySelector] instance
  const LibrarySelector({
    this.initialId,
    super.key,
  });

  /// Preselected library id if exist
  final String? initialId;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final librariesAsync = ref.watch(librariesAsyncPod);

    return TypicalWhenBuilder(
      asyncValue: librariesAsync,
      dataBuilder: (context, libraries) => _LibrarySelector(
        libraries: libraries,
        initialId: initialId,
      ),
    );
  }
}

class _LibrarySelector extends ConsumerWidget {
  const _LibrarySelector({
    required this.initialId,
    required this.libraries,
  });

  final String? initialId;
  final List<Library> libraries;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final checkIcon = PhosphorIcon(
      PhosphorIcons.check(),
    );

    return LayoutBuilder(
      builder: (context, constraints) => DropdownMenu(
        textStyle: context.textTheme.headlineSmall,
        width: constraints.maxWidth,
        enableSearch: false,
        initialSelection: initialId,
        trailingIcon: PhosphorIcon(
          PhosphorIcons.caretDown(PhosphorIconsStyle.duotone),
        ),
        selectedTrailingIcon: PhosphorIcon(
          PhosphorIcons.caretUp(PhosphorIconsStyle.duotone),
        ),
        onSelected: (value) =>
            LibraryRoute(libraryId: value ?? LibraryRoute.allLibrariesId)
                .go(context),
        dropdownMenuEntries: [
          DropdownMenuEntry(
            value: null,
            label: context.translations.libraryWidgetsLibrarySelector,
            style: ButtonStyle(
              textStyle: WidgetStatePropertyAll(
                context.textTheme.titleMedium,
              ),
            ),
            leadingIcon: initialId == null ? checkIcon : null,
          ),
          for (final library in libraries)
            DropdownMenuEntry(
              style: ButtonStyle(
                textStyle: WidgetStatePropertyAll(
                  context.textTheme.titleMedium,
                ),
              ),
              value: library.id,
              label: library.name,
              leadingIcon: initialId == library.id ? checkIcon : null,
            ),
        ],
      ),
    );
  }
}
