import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:kapoow/src/features/library_management/libraries/ui/controllers/library_series_paginated_pod.dart';
import 'package:kapoow/src/features/library_management/libraries/ui/widgets/library_items_list.dart';
import 'package:kapoow/src/features/library_management/series/ui/widgets/series_books_row.dart';

/// Displays an "infinite" list of series from the library.
class LibrarySeriesList extends ConsumerWidget {
  /// Creates a [LibrarySeriesList] instance
  const LibrarySeriesList({
    required this.libraryId,
    super.key,
  });

  /// Library Id from where the collections are
  final String? libraryId;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final seriesPages = ref.watch(librarySeriesPaginatedPod(libraryId));

    return LibraryItemsList(
      paginatedState: seriesPages,
      itemBuilder: (_, series) => SeriesFirstBooksRow(series: series),
      nextPageLoader:
          ref.watch(librarySeriesPaginatedPod(libraryId).notifier).loadNextPage,
    );
  }
}
