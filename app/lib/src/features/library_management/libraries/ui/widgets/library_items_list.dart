import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:kapoow/src/common_widgets/app_loader.dart';
import 'package:kapoow/src/features/pagination/ui/controllers/paginated_state.dart';
import 'package:kapoow/src/features/pagination/ui/widgets/vertical_paginated_list.dart';
import 'package:kapoow/src/utils/extensions.dart';

/// Displays an "infinite" list of Items from the library.
class LibraryItemsList<T> extends ConsumerWidget {
  /// Creates a [LibraryItemsList] instance
  const LibraryItemsList({
    required this.paginatedState,
    required this.nextPageLoader,
    required this.itemBuilder,
    super.key,
  });

  /// Pages to get the Items
  final PaginatedState<T> paginatedState;

  /// {@macro kapoow.pagination.paginatedList.nextPageLoader}
  final VoidCallback nextPageLoader;

  /// {@macro kapoow.pagination.paginatedList.itemBuilder}
  final Widget Function(BuildContext context, T item) itemBuilder;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    if (paginatedState.isLoadingFirstPage) {
      return const Center(
        child: AppLoader(),
      );
    }

    return VerticalPaginatedList(
      paginatedState: paginatedState,
      nextPageLoader: nextPageLoader,
      itemBuilder: (context, item) => SizedBox(
        height: context.height / 3.7,
        child: itemBuilder(context, item),
      ),
    );
  }
}
