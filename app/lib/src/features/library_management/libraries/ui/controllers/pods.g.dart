// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pods.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$librariesAsyncHash() => r'cc3e899db9fbd325b14c0414791a5c67fa9c11bb';

/// Provides all the libraries of the user
///
/// Copied from [librariesAsync].
@ProviderFor(librariesAsync)
final librariesAsyncPod = FutureProvider<List<Library>>.internal(
  librariesAsync,
  name: r'librariesAsyncPod',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$librariesAsyncHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef LibrariesAsyncRef = FutureProviderRef<List<Library>>;
String _$libraryByIdAsyncHash() => r'3cc145d71338fd7ef63d5ccf853b75f5b972b1ea';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// Provides a library of the user base on the id
///
/// Copied from [libraryByIdAsync].
@ProviderFor(libraryByIdAsync)
const libraryByIdAsyncPod = LibraryByIdAsyncFamily();

/// Provides a library of the user base on the id
///
/// Copied from [libraryByIdAsync].
class LibraryByIdAsyncFamily extends Family<AsyncValue<Library>> {
  /// Provides a library of the user base on the id
  ///
  /// Copied from [libraryByIdAsync].
  const LibraryByIdAsyncFamily();

  /// Provides a library of the user base on the id
  ///
  /// Copied from [libraryByIdAsync].
  LibraryByIdAsyncProvider call(
    String id,
  ) {
    return LibraryByIdAsyncProvider(
      id,
    );
  }

  @override
  LibraryByIdAsyncProvider getProviderOverride(
    covariant LibraryByIdAsyncProvider provider,
  ) {
    return call(
      provider.id,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'libraryByIdAsyncPod';
}

/// Provides a library of the user base on the id
///
/// Copied from [libraryByIdAsync].
class LibraryByIdAsyncProvider extends FutureProvider<Library> {
  /// Provides a library of the user base on the id
  ///
  /// Copied from [libraryByIdAsync].
  LibraryByIdAsyncProvider(
    String id,
  ) : this._internal(
          (ref) => libraryByIdAsync(
            ref as LibraryByIdAsyncRef,
            id,
          ),
          from: libraryByIdAsyncPod,
          name: r'libraryByIdAsyncPod',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$libraryByIdAsyncHash,
          dependencies: LibraryByIdAsyncFamily._dependencies,
          allTransitiveDependencies:
              LibraryByIdAsyncFamily._allTransitiveDependencies,
          id: id,
        );

  LibraryByIdAsyncProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.id,
  }) : super.internal();

  final String id;

  @override
  Override overrideWith(
    FutureOr<Library> Function(LibraryByIdAsyncRef provider) create,
  ) {
    return ProviderOverride(
      origin: this,
      override: LibraryByIdAsyncProvider._internal(
        (ref) => create(ref as LibraryByIdAsyncRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        id: id,
      ),
    );
  }

  @override
  FutureProviderElement<Library> createElement() {
    return _LibraryByIdAsyncProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is LibraryByIdAsyncProvider && other.id == id;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, id.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin LibraryByIdAsyncRef on FutureProviderRef<Library> {
  /// The parameter `id` of this provider.
  String get id;
}

class _LibraryByIdAsyncProviderElement extends FutureProviderElement<Library>
    with LibraryByIdAsyncRef {
  _LibraryByIdAsyncProviderElement(super.provider);

  @override
  String get id => (origin as LibraryByIdAsyncProvider).id;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
