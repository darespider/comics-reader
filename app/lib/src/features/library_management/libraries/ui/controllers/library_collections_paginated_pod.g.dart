// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'library_collections_paginated_pod.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$libraryCollectionsFirstPageAsyncHash() =>
    r'ea134b86342e1b70d688d6390c1468d6d19c3b50';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// Provides the first page of a list of Collections from the specified library,
/// if the [libraryId] is `null` then it will fetch the pagination from all
/// libraries
///
/// Copied from [libraryCollectionsFirstPageAsync].
@ProviderFor(libraryCollectionsFirstPageAsync)
const libraryCollectionsFirstPageAsyncPod =
    LibraryCollectionsFirstPageAsyncFamily();

/// Provides the first page of a list of Collections from the specified library,
/// if the [libraryId] is `null` then it will fetch the pagination from all
/// libraries
///
/// Copied from [libraryCollectionsFirstPageAsync].
class LibraryCollectionsFirstPageAsyncFamily
    extends Family<AsyncValue<Pagination<Collection>>> {
  /// Provides the first page of a list of Collections from the specified library,
  /// if the [libraryId] is `null` then it will fetch the pagination from all
  /// libraries
  ///
  /// Copied from [libraryCollectionsFirstPageAsync].
  const LibraryCollectionsFirstPageAsyncFamily();

  /// Provides the first page of a list of Collections from the specified library,
  /// if the [libraryId] is `null` then it will fetch the pagination from all
  /// libraries
  ///
  /// Copied from [libraryCollectionsFirstPageAsync].
  LibraryCollectionsFirstPageAsyncProvider call({
    required String? libraryId,
  }) {
    return LibraryCollectionsFirstPageAsyncProvider(
      libraryId: libraryId,
    );
  }

  @override
  LibraryCollectionsFirstPageAsyncProvider getProviderOverride(
    covariant LibraryCollectionsFirstPageAsyncProvider provider,
  ) {
    return call(
      libraryId: provider.libraryId,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'libraryCollectionsFirstPageAsyncPod';
}

/// Provides the first page of a list of Collections from the specified library,
/// if the [libraryId] is `null` then it will fetch the pagination from all
/// libraries
///
/// Copied from [libraryCollectionsFirstPageAsync].
class LibraryCollectionsFirstPageAsyncProvider
    extends FutureProvider<Pagination<Collection>> {
  /// Provides the first page of a list of Collections from the specified library,
  /// if the [libraryId] is `null` then it will fetch the pagination from all
  /// libraries
  ///
  /// Copied from [libraryCollectionsFirstPageAsync].
  LibraryCollectionsFirstPageAsyncProvider({
    required String? libraryId,
  }) : this._internal(
          (ref) => libraryCollectionsFirstPageAsync(
            ref as LibraryCollectionsFirstPageAsyncRef,
            libraryId: libraryId,
          ),
          from: libraryCollectionsFirstPageAsyncPod,
          name: r'libraryCollectionsFirstPageAsyncPod',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$libraryCollectionsFirstPageAsyncHash,
          dependencies: LibraryCollectionsFirstPageAsyncFamily._dependencies,
          allTransitiveDependencies:
              LibraryCollectionsFirstPageAsyncFamily._allTransitiveDependencies,
          libraryId: libraryId,
        );

  LibraryCollectionsFirstPageAsyncProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.libraryId,
  }) : super.internal();

  final String? libraryId;

  @override
  Override overrideWith(
    FutureOr<Pagination<Collection>> Function(
            LibraryCollectionsFirstPageAsyncRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: LibraryCollectionsFirstPageAsyncProvider._internal(
        (ref) => create(ref as LibraryCollectionsFirstPageAsyncRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        libraryId: libraryId,
      ),
    );
  }

  @override
  FutureProviderElement<Pagination<Collection>> createElement() {
    return _LibraryCollectionsFirstPageAsyncProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is LibraryCollectionsFirstPageAsyncProvider &&
        other.libraryId == libraryId;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, libraryId.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin LibraryCollectionsFirstPageAsyncRef
    on FutureProviderRef<Pagination<Collection>> {
  /// The parameter `libraryId` of this provider.
  String? get libraryId;
}

class _LibraryCollectionsFirstPageAsyncProviderElement
    extends FutureProviderElement<Pagination<Collection>>
    with LibraryCollectionsFirstPageAsyncRef {
  _LibraryCollectionsFirstPageAsyncProviderElement(super.provider);

  @override
  String? get libraryId =>
      (origin as LibraryCollectionsFirstPageAsyncProvider).libraryId;
}

String _$libraryCollectionPaginatedHash() =>
    r'85530b5723ae796638420ea6dbb649f1c8eb68f0';

abstract class _$LibraryCollectionPaginated
    extends BuildlessNotifier<PaginatedState<Collection>> {
  late final String? params;

  PaginatedState<Collection> build([
    String? params,
  ]);
}

/// Notifier dedicated to handle the pagination state for Collections from a
/// specific library, the [params] param should contain the library id.
/// if the [params] is `null` then it will fetch the  pagination from all
/// libraries
///
/// Copied from [LibraryCollectionPaginated].
@ProviderFor(LibraryCollectionPaginated)
const libraryCollectionPaginatedPod = LibraryCollectionPaginatedFamily();

/// Notifier dedicated to handle the pagination state for Collections from a
/// specific library, the [params] param should contain the library id.
/// if the [params] is `null` then it will fetch the  pagination from all
/// libraries
///
/// Copied from [LibraryCollectionPaginated].
class LibraryCollectionPaginatedFamily
    extends Family<PaginatedState<Collection>> {
  /// Notifier dedicated to handle the pagination state for Collections from a
  /// specific library, the [params] param should contain the library id.
  /// if the [params] is `null` then it will fetch the  pagination from all
  /// libraries
  ///
  /// Copied from [LibraryCollectionPaginated].
  const LibraryCollectionPaginatedFamily();

  /// Notifier dedicated to handle the pagination state for Collections from a
  /// specific library, the [params] param should contain the library id.
  /// if the [params] is `null` then it will fetch the  pagination from all
  /// libraries
  ///
  /// Copied from [LibraryCollectionPaginated].
  LibraryCollectionPaginatedProvider call([
    String? params,
  ]) {
    return LibraryCollectionPaginatedProvider(
      params,
    );
  }

  @override
  LibraryCollectionPaginatedProvider getProviderOverride(
    covariant LibraryCollectionPaginatedProvider provider,
  ) {
    return call(
      provider.params,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'libraryCollectionPaginatedPod';
}

/// Notifier dedicated to handle the pagination state for Collections from a
/// specific library, the [params] param should contain the library id.
/// if the [params] is `null` then it will fetch the  pagination from all
/// libraries
///
/// Copied from [LibraryCollectionPaginated].
class LibraryCollectionPaginatedProvider extends NotifierProviderImpl<
    LibraryCollectionPaginated, PaginatedState<Collection>> {
  /// Notifier dedicated to handle the pagination state for Collections from a
  /// specific library, the [params] param should contain the library id.
  /// if the [params] is `null` then it will fetch the  pagination from all
  /// libraries
  ///
  /// Copied from [LibraryCollectionPaginated].
  LibraryCollectionPaginatedProvider([
    String? params,
  ]) : this._internal(
          () => LibraryCollectionPaginated()..params = params,
          from: libraryCollectionPaginatedPod,
          name: r'libraryCollectionPaginatedPod',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$libraryCollectionPaginatedHash,
          dependencies: LibraryCollectionPaginatedFamily._dependencies,
          allTransitiveDependencies:
              LibraryCollectionPaginatedFamily._allTransitiveDependencies,
          params: params,
        );

  LibraryCollectionPaginatedProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.params,
  }) : super.internal();

  final String? params;

  @override
  PaginatedState<Collection> runNotifierBuild(
    covariant LibraryCollectionPaginated notifier,
  ) {
    return notifier.build(
      params,
    );
  }

  @override
  Override overrideWith(LibraryCollectionPaginated Function() create) {
    return ProviderOverride(
      origin: this,
      override: LibraryCollectionPaginatedProvider._internal(
        () => create()..params = params,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        params: params,
      ),
    );
  }

  @override
  NotifierProviderElement<LibraryCollectionPaginated,
      PaginatedState<Collection>> createElement() {
    return _LibraryCollectionPaginatedProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is LibraryCollectionPaginatedProvider &&
        other.params == params;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, params.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin LibraryCollectionPaginatedRef
    on NotifierProviderRef<PaginatedState<Collection>> {
  /// The parameter `params` of this provider.
  String? get params;
}

class _LibraryCollectionPaginatedProviderElement
    extends NotifierProviderElement<LibraryCollectionPaginated,
        PaginatedState<Collection>> with LibraryCollectionPaginatedRef {
  _LibraryCollectionPaginatedProviderElement(super.provider);

  @override
  String? get params => (origin as LibraryCollectionPaginatedProvider).params;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
