import 'package:kapoow/src/features/library_management/series/domain/models/series.dart';
import 'package:kapoow/src/features/library_management/series/domain/pods.dart';
import 'package:kapoow/src/features/pagination/domain/models/pagination.dart';
import 'package:kapoow/src/features/pagination/ui/controllers/paginated_mixin.dart';
import 'package:kapoow/src/features/pagination/ui/controllers/paginated_state.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'library_series_paginated_pod.g.dart';

/// Provides the first page of a list of Series from the specified library,
/// if the [libraryId] is `null` then it will fetch the pagination from all
/// libraries
@Riverpod(keepAlive: true)
FutureOr<Pagination<Series>> librarySeriesFirstPageAsync(
  LibrarySeriesFirstPageAsyncRef ref, {
  required String? libraryId,
}) {
  return ref.watch(seriesRepoPod).getSeries(
        libraryIds: libraryId != null ? [libraryId] : null,
        page: 0,
      );
}

/// Notifier dedicated to handle the pagination state for Series from a
/// specific library, if the [params] is `null` then it will fetch the
/// pagination from all libraries
@Riverpod(keepAlive: true)
class LibrarySeriesPaginated extends _$LibrarySeriesPaginated
    with PaginatedMixin<Series, String?> {
  @override
  PaginatedState<Series> build([String? params]) {
    final firstPageAsync = ref.watch(
      librarySeriesFirstPageAsyncPod(libraryId: params),
    );

    return buildInitState(firstPageAsync);
  }

  @override
  Future<Pagination<Series>> fetchPage(int page) async {
    return ref.read(seriesRepoPod).getSeries(
          libraryIds: params != null ? [params!] : null,
          page: page,
        );
  }
}
