import 'package:kapoow/src/features/library_management/libraries/domain/models/library.dart';
import 'package:kapoow/src/features/library_management/libraries/domain/pods.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'pods.g.dart';

/// Provides all the libraries of the user
@Riverpod(keepAlive: true)
FutureOr<List<Library>> librariesAsync(LibrariesAsyncRef ref) {
  return ref.watch(librariesRepoPod).getLibraries();
}

/// Provides a library of the user base on the id
@Riverpod(keepAlive: true)
FutureOr<Library> libraryByIdAsync(LibraryByIdAsyncRef ref, String id) {
  return ref.watch(librariesRepoPod).getLibraryById(id);
}
