import 'package:kapoow/src/features/library_management/read_lists/domain/models/read_list.dart';
import 'package:kapoow/src/features/library_management/read_lists/domain/pods.dart';
import 'package:kapoow/src/features/pagination/domain/models/pagination.dart';
import 'package:kapoow/src/features/pagination/ui/controllers/paginated_mixin.dart';
import 'package:kapoow/src/features/pagination/ui/controllers/paginated_state.dart';
import 'package:kapoow/src/utils/extensions.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'library_readlists_paginated_pod.g.dart';

/// Provides the first page of a list of ReadLists from the specified library,
/// if the [libraryId] is `null` then it will fetch the pagination from all
/// libraries
@Riverpod(keepAlive: true)
FutureOr<Pagination<ReadList>> libraryReadListsFirstPageAsync(
  LibraryReadListsFirstPageAsyncRef ref, {
  required String? libraryId,
}) {
  return ref.watch(readlistsRepoPod).fetchReadlists(
        libraryIds: libraryId != null ? [libraryId] : null,
        page: 0,
      );
}

/// Notifier dedicated to handle the pagination state for ReadLists from a
/// specific library, the [params] param should contain the library id.
/// if the [params] is `null` then it will fetch the pagination from all
/// libraries
@Riverpod(keepAlive: true)
class LibraryReadListPaginated extends _$LibraryReadListPaginated
    with PaginatedMixin<ReadList, String?> {
  @override
  PaginatedState<ReadList> build([String? params]) {
    final firstPageAsync =
        ref.watch(libraryReadListsFirstPageAsyncPod(libraryId: params));

    return buildInitState(firstPageAsync);
  }

  @override
  Future<Pagination<ReadList>> fetchPage(int page) async {
    return ref.read(readlistsRepoPod).fetchReadlists(
          libraryIds: params.toNullableList(),
          page: page,
        );
  }
}
