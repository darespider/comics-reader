import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:kapoow/src/common_widgets/advice.dart';
import 'package:kapoow/src/common_widgets/app_segmented_button.dart';
import 'package:kapoow/src/common_widgets/extensions.dart';
import 'package:kapoow/src/common_widgets/typical_when_builder.dart';
import 'package:kapoow/src/constants/app_paddings.dart';
import 'package:kapoow/src/features/library_management/libraries/ui/controllers/library_collections_paginated_pod.dart';
import 'package:kapoow/src/features/library_management/libraries/ui/controllers/library_readlists_paginated_pod.dart';
import 'package:kapoow/src/features/library_management/libraries/ui/controllers/library_series_paginated_pod.dart';
import 'package:kapoow/src/features/library_management/libraries/ui/controllers/pods.dart';
import 'package:kapoow/src/features/library_management/libraries/ui/widgets/library_collections_list.dart';
import 'package:kapoow/src/features/library_management/libraries/ui/widgets/library_readlists_list.dart';
import 'package:kapoow/src/features/library_management/libraries/ui/widgets/library_selector.dart';
import 'package:kapoow/src/features/library_management/libraries/ui/widgets/library_series_list.dart';
import 'package:kapoow/src/utils/extensions.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';

/// [LibraryScreen] allows to navigate between the libraries series, collections
/// and readlists.
class LibraryScreen extends HookConsumerWidget {
  /// Creates a [LibraryScreen] instance
  const LibraryScreen({
    required this.libraryId,
    super.key,
    this.section = LibrarySection.series,
  });

  /// Indicates which list should be display; series, collections and readlists
  final LibrarySection section;

  /// Library id to filter series, collection and readlists, if `null` the
  /// lists will show the result for all the libraries
  final String? libraryId;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final segment = useState(section);
    final librariesAsync = ref.watch(librariesAsyncPod);

    return RefreshIndicator(
      onRefresh: () async {
        await Future.wait([
          ref.refresh(librariesAsyncPod.future),
          ref.refresh(
            libraryCollectionsFirstPageAsyncPod(libraryId: libraryId).future,
          ),
          ref.refresh(
            libraryReadListsFirstPageAsyncPod(libraryId: libraryId).future,
          ),
          ref.refresh(
            librarySeriesFirstPageAsyncPod(libraryId: libraryId).future,
          ),
        ]);
      },
      child: TypicalWhenBuilder(
        asyncValue: librariesAsync,
        dataBuilder: (context, libraries) => Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(AppPaddings.small),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisSize: MainAxisSize.min,
                children: [
                  LibrarySelector(initialId: libraryId),
                  Advice(
                    icon: PhosphorIcons.question(PhosphorIconsStyle.duotone),
                    text: context.translations.libraryScreenAdvice,
                  ),
                  AppSegmentedButton(
                    selected: {segment.value},
                    onSelectionChanged: (value) {
                      segment.value = value.first;
                    },
                    segments: [
                      ButtonSegment(
                        value: LibrarySection.series,
                        label: Text(LibrarySection.series.name),
                      ),
                      ButtonSegment(
                        value: LibrarySection.collections,
                        label: Text(LibrarySection.collections.name),
                      ),
                      ButtonSegment(
                        value: LibrarySection.readlists,
                        label: Text(LibrarySection.readlists.name),
                      ),
                    ],
                  ),
                ].separated(mainAxisExtent: AppPaddings.small),
              ),
            ),
            Expanded(
              child: Center(
                child: switch (segment.value) {
                  // TODO(rurickdev): Going to the Collections and Readlists
                  //  screen is being redirected to recommended
                  LibrarySection.collections => LibraryCollectionsList(
                      libraryId: libraryId,
                    ),
                  LibrarySection.readlists => LibraryReadlistsList(
                      libraryId: libraryId,
                    ),
                  LibrarySection.series => LibrarySeriesList(
                      libraryId: libraryId,
                    ),
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

/// Options for the different sections of a library
enum LibrarySection {
  /// series
  series,

  /// collections
  collections,

  /// readlists
  readlists;
}
