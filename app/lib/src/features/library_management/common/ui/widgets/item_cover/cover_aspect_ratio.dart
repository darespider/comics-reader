import 'package:flutter/material.dart';
import 'package:kapoow/src/constants/app_paddings.dart';

/// Aspect ratio of a comic book image: 663/1024
const coverAspectRatioValue = 663 / 1024;

/// Common used aspect ratio that respects a comic book cover ratio
class CoverAspectRatio extends AspectRatio {
  /// Creates a [CoverAspectRatio] instance
  const CoverAspectRatio({super.key, super.child})
      : super(aspectRatio: coverAspectRatioValue);
}

/// Sliver Grid Delegate with a fixed cross axis count of 3 and an aspect ratio
/// equal to the covers
class CoverGridDelegate extends SliverGridDelegateWithFixedCrossAxisCount {
  /// Creates a [CoverGridDelegate] instance
  const CoverGridDelegate({
    super.crossAxisSpacing = AppPaddings.small,
    super.mainAxisSpacing = AppPaddings.small,
  }) : super(
          childAspectRatio: coverAspectRatioValue,
          crossAxisCount: 3,
        );
}
