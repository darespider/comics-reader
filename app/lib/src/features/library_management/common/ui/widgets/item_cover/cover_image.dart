import 'package:flutter/material.dart';
import 'package:flutter_svg_provider/flutter_svg_provider.dart';
import 'package:kapoow/gen/assets.gen.dart';
import 'package:kapoow/src/features/library_management/common/ui/widgets/item_cover/cover_aspect_ratio.dart';
import 'package:kapoow/src/features/library_management/common/ui/widgets/item_cover/cover_decoration.dart';
import 'package:kapoow/src/features/rest_api/ui/widgets/cached_api_image_decorated_box.dart';
import 'package:kapoow/src/utils/extensions.dart';

/// Common radius for cover corners
const coverRadius = Radius.circular(6);

/// Cached image from a url displayed with an aspect ratio of 663:1024
///
/// It can be tap or long press
class CoverImage extends StatelessWidget {
  /// Creates a [CoverImage] instance
  const CoverImage({
    required this.coverUrl,
    this.decoration,
    this.onTap,
    this.onLongPress,
    super.key,
  });

  /// Displays a cover image widget without loading any url image, use a
  /// placeholder one.
  const CoverImage.placeHolder({
    super.key,
  })  : coverUrl = '',
        decoration = null,
        onTap = null,
        onLongPress = null;

  /// What to do when the image is tapped
  final VoidCallback? onTap;

  /// What to do when the image is long pressed
  final VoidCallback? onLongPress;

  /// Url of the cover to load and display
  final String coverUrl;

  /// Decoration info to add on the cover image
  final CoverDecorationData? decoration;

  @override
  Widget build(BuildContext context) {
    final placeHolder = _CoverDecoratedBox.randomPlaceHolder();

    return CoverAspectRatio(
      child: coverUrl.isEmpty
          ? placeHolder
          : Stack(
              fit: StackFit.expand,
              children: [
                CachedApiImageDecoratedBox(
                  relativeUrl: coverUrl,
                  borderRadius: _coverBorderRadiusAll,
                  placeholder: (_, __) => placeHolder,
                  // TODO(rurickdev): [Beta] Create a broken cover image
                  errorWidget: (_, __, ___) => placeHolder,
                  imageBuilder: (_, provider) =>
                      _CoverDecoratedBox(imageProvider: provider),
                ),
                if (decoration != null)
                  Positioned.fill(
                    child: CoverDecoration(
                      decoration: decoration!,
                    ),
                  ),
                // hacky way to get the InkWell to work with images and
                // border radius
                Positioned.fill(
                  child: Material(
                    color: Colors.transparent,
                    child: InkWell(
                      onTap: onTap,
                      onLongPress: onLongPress,
                      borderRadius: _coverBorderRadiusAll,
                    ),
                  ),
                ),
              ],
            ),
    );
  }
}

class _CoverDecoratedBox extends StatelessWidget {
  const _CoverDecoratedBox({
    required this.imageProvider,
  });

  factory _CoverDecoratedBox.randomPlaceHolder() =>
      _CoverDecoratedBox.placeHolder(
        _CoverPlaceHolderVariant.values.randomElement,
      );

  _CoverDecoratedBox.placeHolder(_CoverPlaceHolderVariant variant)
      : imageProvider = Svg(
          switch (variant) {
            _CoverPlaceHolderVariant.v1 =>
              Assets.images.coverPlaceholder.v1.path,
            _CoverPlaceHolderVariant.v2 =>
              Assets.images.coverPlaceholder.v2.path,
            _CoverPlaceHolderVariant.v3 =>
              Assets.images.coverPlaceholder.v3.path,
            _CoverPlaceHolderVariant.v4 =>
              Assets.images.coverPlaceholder.v4.path,
            _CoverPlaceHolderVariant.v5 =>
              Assets.images.coverPlaceholder.v5.path,
          },
        );

  final ImageProvider imageProvider;

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: BoxDecoration(
        borderRadius: _coverBorderRadiusAll,
        image: DecorationImage(
          fit: BoxFit.cover,
          image: imageProvider,
        ),
      ),
      child: const SizedBox.expand(),
    );
  }
}

enum _CoverPlaceHolderVariant {
  v1,
  v2,
  v3,
  v4,
  v5;
}

const _coverBorderRadiusAll = BorderRadius.all(coverRadius);
