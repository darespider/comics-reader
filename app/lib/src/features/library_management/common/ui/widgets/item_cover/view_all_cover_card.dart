import 'package:flutter/material.dart';
import 'package:kapoow/src/common_widgets/extensions.dart';
import 'package:kapoow/src/features/library_management/common/ui/widgets/item_cover/cover_aspect_ratio.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';

/// Card that follows the Aspect ratio of a Cover, displays a "View All" text
/// and allows to have a tap action
class ViewAllCoverCard extends StatelessWidget {
  /// Creates a [ViewAllCoverCard] instance
  const ViewAllCoverCard({
    super.key,
    this.onTap,
  });

  /// What to do when card is taped
  final VoidCallback? onTap;

  @override
  Widget build(BuildContext context) {
    return CoverAspectRatio(
      child: Card(
        child: InkWell(
          onTap: onTap,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              PhosphorIcon(PhosphorIcons.archive(PhosphorIconsStyle.duotone)),
              const Text(
                'View All',
                textAlign: TextAlign.center,
              ),
            ].separated(),
          ),
        ),
      ),
    );
  }
}
