import 'package:flutter/material.dart';
import 'package:kapoow/src/common_widgets/extensions.dart';
import 'package:kapoow/src/constants/app_paddings.dart';
import 'package:kapoow/src/features/library_management/common/ui/widgets/item_cover/cover_decoration.dart';
import 'package:kapoow/src/features/library_management/common/ui/widgets/item_cover/cover_image.dart';
import 'package:kapoow/src/theme/extensions.dart';

/// Layout for the right side of the Cover Decoration
class CoverRightDecoration extends StatelessWidget {
  /// Creates a [CoverRightDecoration] instance
  const CoverRightDecoration({
    required this.decoration,
    super.key,
  });

  /// Decoration data
  final CoverDecorationData decoration;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: context.colorScheme.tertiaryContainer,
        borderRadius: const BorderRadius.only(
          topRight: coverRadius,
          bottomLeft: coverRadius,
        ),
      ),
      padding: const EdgeInsets.all(AppPaddings.xxSmall),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          if (decoration.finishedItemsCount != null)
            Text(
              '${decoration.finishedItemsCount} of',
            ),
          if (decoration.itemsCount != null) Text('${decoration.itemsCount}'),
        ].separated(mainAxisExtent: AppPaddings.xxSmall),
      ),
    );
  }
}
