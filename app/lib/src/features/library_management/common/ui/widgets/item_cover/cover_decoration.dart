import 'package:flutter/material.dart';
import 'package:kapoow/src/features/library_management/common/ui/widgets/item_cover/cover_image.dart';
import 'package:kapoow/src/features/library_management/common/ui/widgets/item_cover/cover_left_decoration.dart';
import 'package:kapoow/src/features/library_management/common/ui/widgets/item_cover/cover_right_decoration.dart';
import 'package:kapoow/src/theme/extensions.dart';

/// Widget that renders the decoration that will be over a [CoverImage]
class CoverDecoration extends StatelessWidget {
  /// Creates a [CoverDecoration] instance
  const CoverDecoration({
    required this.decoration,
    super.key,
  });

  /// Decoration data
  final CoverDecorationData decoration;

  @override
  Widget build(BuildContext context) {
    return DefaultTextStyle.merge(
      style: context.textTheme.labelLarge?.copyWith(
        color: context.colorScheme.onSecondaryContainer,
      ),
      maxLines: 1,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Align(
                alignment: AlignmentDirectional.topStart,
                child: decoration.hasLeftDecoration
                    ? CoverLeftDecoration(decoration: decoration)
                    : const SizedBox.shrink(),
              ),
              Align(
                alignment: AlignmentDirectional.topEnd,
                child: decoration.hasRightDecoration
                    ? CoverRightDecoration(decoration: decoration)
                    : const SizedBox.shrink(),
              ),
            ],
          ),
          if (decoration.progress != null)
            LinearProgressIndicator(
              backgroundColor: context.colorScheme.onSecondaryContainer,
              color: context.colorScheme.secondaryContainer,
              value: decoration.progress,
              minHeight: 7,
              borderRadius: const BorderRadius.only(
                bottomLeft: coverRadius,
                bottomRight: coverRadius,
              ),
            ),
        ],
      ),
    );
  }
}

/// Decoration information for cover item
@immutable
class CoverDecorationData {
  /// Creates a [CoverDecorationData] instance
  const CoverDecorationData({
    this.issueNumber,
    this.itemsCount,
    this.finishedItemsCount,
    this.progress,
    this.finished,
    this.newItem,
    this.annual,
  }) : assert(
          (progress == null) || (progress >= 0 && progress <= 1),
          'progress should be in the range of 0.0 to 1.0 inclusive',
        );

  /// Show at the right
  final int? itemsCount;

  /// Show at the right, if [itemsCount] is pass it will add a `of` between
  final int? finishedItemsCount;

  /// Show at the left with a `#` prefix
  final double? issueNumber;

  /// Shows a linear progress bar at the bottom
  final double? progress;

  /// Shows a check mar at the left
  final bool? finished;

  /// Shows a new mark at the right
  final bool? newItem;

  /// Shows annual mark at the left
  final String? annual;

  /// True if the decoration has value to display on the left
  bool get hasLeftDecoration =>
      annual != null ||
      issueNumber != null ||
      (finished ?? false) ||
      (newItem ?? false);

  /// True if the decoration has value to display on the right
  bool get hasRightDecoration =>
      itemsCount != null || finishedItemsCount != null;
}
