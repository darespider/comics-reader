import 'package:flutter/material.dart';
import 'package:kapoow/src/common_widgets/extensions.dart';
import 'package:kapoow/src/features/library_management/common/ui/widgets/item_cover/cover_aspect_ratio.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';

/// Card that follows the cover aspect ratio and displays a Text error
/// message and an image error icon at its center.
class CoverCardError extends StatelessWidget {
  /// Creates a [CoverCardError] instance
  const CoverCardError({
    required this.error,
    super.key,
  });

  /// Error text to display.
  final String error;

  @override
  Widget build(BuildContext context) {
    return CoverAspectRatio(
      child: Card(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              error,
              textAlign: TextAlign.center,
              overflow: TextOverflow.ellipsis,
            ),
            PhosphorIcon(
              PhosphorIcons.fileX(PhosphorIconsStyle.duotone),
              size: 48,
            ),
          ].separated(),
        ),
      ),
    );
  }
}
