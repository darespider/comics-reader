import 'package:freezed_annotation/freezed_annotation.dart';

part 'author.freezed.dart';

/// Author of a Book
@freezed
class Author with _$Author {
  /// Creates an [Author] instance
  const factory Author({
    required String name,
    required String role,
  }) = _Author;
}
