/// Enum with the valid status for the series
enum SeriesStatus {
  /// The serie has ended
  ended('ENDED'),

  /// The serie is going
  ongoing('ONGOING'),

  /// The serie has been abandoned
  abandoned('ABANDONED'),

  /// The serie is in hiatus
  hiatus('HIATUS');

  /// Constructor of the enum
  const SeriesStatus(this.value);

  factory SeriesStatus.fromValue(String value) {
    return switch (value.toUpperCase()) {
      'ENDED' => SeriesStatus.ended,
      'ONGOING' => SeriesStatus.ongoing,
      'ABANDONED' => SeriesStatus.abandoned,
      'HIATUS' => SeriesStatus.hiatus,
      _ => throw Exception('Unknown Series Status [$value]'),
    };
  }

  /// Value as used by the api
  final String value;
}
