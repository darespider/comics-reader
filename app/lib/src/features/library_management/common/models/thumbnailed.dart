/// Indicates that an object can have a thumbnail
abstract class Thumbnailed {
  /// returns the uri segment of the thumbnail
  String get thumbnailUri;
}
