/// Media Status to filter the api queries
enum MediaStatus {
  /// value of `UNKNOWN`
  unknown('UNKNOWN'),

  /// value of `ERROR`
  error('ERROR'),

  /// value of `READY`
  ready('READY'),

  /// value of `UNSUPPORTED`
  unsupported('UNSUPPORTED'),

  /// value of `OUTDATED`
  outdated('OUTDATED');

  /// Constructor of the enum
  const MediaStatus(this.value);

  /// Value as used by the api
  final String value;
}
