/// Read Status to filter the api queries
enum ReadStatus {
  /// value of `UNREAD`
  unread('UNREAD'),

  /// value of `READ`
  read('READ'),

  /// value of `IN_PROGRESS`
  inProgress('IN_PROGRESS');

  /// Constructor of the enum
  const ReadStatus(this.value);

  /// Value as used by the api
  final String value;
}
