const _separatorString = '*List of covers and their creators:*\n';

/// Formatter for the common summary layout for books and series.
class SummaryFormatter {
  /// Creates a [SummaryFormatter] instance
  const SummaryFormatter();

  /// Returns the Summary text and the Markdown artist table.
  ///
  /// If the passed summary is empty both results will be empty.
  ///
  /// It will split the summary and table by [_separatorString], if the string
  /// is not found then the table result will be empty.
  (String, String) format(String summary) {
    if (summary.isEmpty) return const ('', '');
    final results = summary.split(_separatorString);
    if (results.length < 2) return (results.first, '');
    final table = _addMissingTableDivider(results.last);
    return (results.first, table);
  }

  /// The markdown table doesn't come with the right header divider.
  /// This function tries to add if missing or fix it if bad provided
  String _addMissingTableDivider(String table) {
    final lines = table.split('\n');
    final isDashedLine = lines[1].split('').every((e) => e == '-');
    if (isDashedLine) {
      lines[1] = '-|-|-|-';
    } else {
      lines.insert(1, '-|-|-|-');
    }
    return lines.join('\n');
  }
}
