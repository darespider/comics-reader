import 'package:kapoow/src/features/library_management/collections/domain/models/collection.dart';
import 'package:kapoow/src/features/library_management/common/models/read_status.dart';
import 'package:kapoow/src/features/library_management/common/models/series_status.dart';
import 'package:kapoow/src/features/library_management/common/models/typedefs.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/collection_dto.dart';

/// Interface describing how the series repositories should work
abstract class CollectionsRepository {
  /// Returns a list of series fetch from the api, accepts many
  /// search filters
  Future<PageCollection> getCollections({
    String? search,
    List<String>? libraryIds,
    bool? unpaged,
    int? page,
    int? size,
  });

  /// Returns a single [CollectionDto] based on the [id]
  Future<Collection> getCollectionById({required String id});

  /// Returns a [PageSeries] of the collection with the passed [collectionId]
  Future<PageSeries> getCollectionSeries({
    required String collectionId,
    List<String>? libraryIds,
    List<SeriesStatus>? status,
    ReadStatus? readStatus,
    List<String>? publisher,
    List<String>? language,
    List<String>? genre,
    List<String>? tags,
    List<String>? ageRating,
    List<String>? releaseYears,
    bool? deleted,
    bool? complete,
    bool? unpaged,
    int? page,
    int? size,
    List<String>? authors,
  });

  /// Search collections based on the [query]
  Future<PageCollection> searchCollections(String query, {int page = 0});
}
