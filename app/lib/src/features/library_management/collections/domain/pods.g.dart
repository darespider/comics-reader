// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pods.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$collectionsRepoHash() => r'95a482ddc3b3c1e7922c419a4b27c6c0c09e69dc';

/// Provides the [CollectionsRepository]
///
/// Copied from [collectionsRepo].
@ProviderFor(collectionsRepo)
final collectionsRepoPod = Provider<CollectionsRepository>.internal(
  collectionsRepo,
  name: r'collectionsRepoPod',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$collectionsRepoHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef CollectionsRepoRef = ProviderRef<CollectionsRepository>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
