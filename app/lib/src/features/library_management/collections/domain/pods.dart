import 'package:kapoow/src/features/library_management/collections/data/collections_repository.dart';
import 'package:kapoow/src/features/library_management/collections/domain/repositories/collections_repository_imp.dart';
import 'package:kapoow/src/features/rest_api/data/datasources/pods.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'pods.g.dart';

/// Provides the [CollectionsRepository]
@Riverpod(keepAlive: true)
CollectionsRepository collectionsRepo(CollectionsRepoRef ref) {
  return CollectionsRepositoryImp(ref.watch(collectionsDatasourcePod));
}
