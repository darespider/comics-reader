// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'collection.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$Collection {
  String get id => throw _privateConstructorUsedError;
  String get name => throw _privateConstructorUsedError;
  bool get ordered => throw _privateConstructorUsedError;
  List<String> get seriesIds => throw _privateConstructorUsedError;
  DateTime get createdDate => throw _privateConstructorUsedError;
  DateTime get lastModifiedDate => throw _privateConstructorUsedError;
  bool get filtered => throw _privateConstructorUsedError;

  /// Create a copy of Collection
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $CollectionCopyWith<Collection> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CollectionCopyWith<$Res> {
  factory $CollectionCopyWith(
          Collection value, $Res Function(Collection) then) =
      _$CollectionCopyWithImpl<$Res, Collection>;
  @useResult
  $Res call(
      {String id,
      String name,
      bool ordered,
      List<String> seriesIds,
      DateTime createdDate,
      DateTime lastModifiedDate,
      bool filtered});
}

/// @nodoc
class _$CollectionCopyWithImpl<$Res, $Val extends Collection>
    implements $CollectionCopyWith<$Res> {
  _$CollectionCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of Collection
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? name = null,
    Object? ordered = null,
    Object? seriesIds = null,
    Object? createdDate = null,
    Object? lastModifiedDate = null,
    Object? filtered = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      ordered: null == ordered
          ? _value.ordered
          : ordered // ignore: cast_nullable_to_non_nullable
              as bool,
      seriesIds: null == seriesIds
          ? _value.seriesIds
          : seriesIds // ignore: cast_nullable_to_non_nullable
              as List<String>,
      createdDate: null == createdDate
          ? _value.createdDate
          : createdDate // ignore: cast_nullable_to_non_nullable
              as DateTime,
      lastModifiedDate: null == lastModifiedDate
          ? _value.lastModifiedDate
          : lastModifiedDate // ignore: cast_nullable_to_non_nullable
              as DateTime,
      filtered: null == filtered
          ? _value.filtered
          : filtered // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$CollectionImplCopyWith<$Res>
    implements $CollectionCopyWith<$Res> {
  factory _$$CollectionImplCopyWith(
          _$CollectionImpl value, $Res Function(_$CollectionImpl) then) =
      __$$CollectionImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String id,
      String name,
      bool ordered,
      List<String> seriesIds,
      DateTime createdDate,
      DateTime lastModifiedDate,
      bool filtered});
}

/// @nodoc
class __$$CollectionImplCopyWithImpl<$Res>
    extends _$CollectionCopyWithImpl<$Res, _$CollectionImpl>
    implements _$$CollectionImplCopyWith<$Res> {
  __$$CollectionImplCopyWithImpl(
      _$CollectionImpl _value, $Res Function(_$CollectionImpl) _then)
      : super(_value, _then);

  /// Create a copy of Collection
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? name = null,
    Object? ordered = null,
    Object? seriesIds = null,
    Object? createdDate = null,
    Object? lastModifiedDate = null,
    Object? filtered = null,
  }) {
    return _then(_$CollectionImpl(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      ordered: null == ordered
          ? _value.ordered
          : ordered // ignore: cast_nullable_to_non_nullable
              as bool,
      seriesIds: null == seriesIds
          ? _value._seriesIds
          : seriesIds // ignore: cast_nullable_to_non_nullable
              as List<String>,
      createdDate: null == createdDate
          ? _value.createdDate
          : createdDate // ignore: cast_nullable_to_non_nullable
              as DateTime,
      lastModifiedDate: null == lastModifiedDate
          ? _value.lastModifiedDate
          : lastModifiedDate // ignore: cast_nullable_to_non_nullable
              as DateTime,
      filtered: null == filtered
          ? _value.filtered
          : filtered // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$CollectionImpl extends _Collection {
  const _$CollectionImpl(
      {required this.id,
      required this.name,
      required this.ordered,
      required final List<String> seriesIds,
      required this.createdDate,
      required this.lastModifiedDate,
      required this.filtered})
      : _seriesIds = seriesIds,
        super._();

  @override
  final String id;
  @override
  final String name;
  @override
  final bool ordered;
  final List<String> _seriesIds;
  @override
  List<String> get seriesIds {
    if (_seriesIds is EqualUnmodifiableListView) return _seriesIds;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_seriesIds);
  }

  @override
  final DateTime createdDate;
  @override
  final DateTime lastModifiedDate;
  @override
  final bool filtered;

  @override
  String toString() {
    return 'Collection(id: $id, name: $name, ordered: $ordered, seriesIds: $seriesIds, createdDate: $createdDate, lastModifiedDate: $lastModifiedDate, filtered: $filtered)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CollectionImpl &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.ordered, ordered) || other.ordered == ordered) &&
            const DeepCollectionEquality()
                .equals(other._seriesIds, _seriesIds) &&
            (identical(other.createdDate, createdDate) ||
                other.createdDate == createdDate) &&
            (identical(other.lastModifiedDate, lastModifiedDate) ||
                other.lastModifiedDate == lastModifiedDate) &&
            (identical(other.filtered, filtered) ||
                other.filtered == filtered));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      id,
      name,
      ordered,
      const DeepCollectionEquality().hash(_seriesIds),
      createdDate,
      lastModifiedDate,
      filtered);

  /// Create a copy of Collection
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$CollectionImplCopyWith<_$CollectionImpl> get copyWith =>
      __$$CollectionImplCopyWithImpl<_$CollectionImpl>(this, _$identity);
}

abstract class _Collection extends Collection {
  const factory _Collection(
      {required final String id,
      required final String name,
      required final bool ordered,
      required final List<String> seriesIds,
      required final DateTime createdDate,
      required final DateTime lastModifiedDate,
      required final bool filtered}) = _$CollectionImpl;
  const _Collection._() : super._();

  @override
  String get id;
  @override
  String get name;
  @override
  bool get ordered;
  @override
  List<String> get seriesIds;
  @override
  DateTime get createdDate;
  @override
  DateTime get lastModifiedDate;
  @override
  bool get filtered;

  /// Create a copy of Collection
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$CollectionImplCopyWith<_$CollectionImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
