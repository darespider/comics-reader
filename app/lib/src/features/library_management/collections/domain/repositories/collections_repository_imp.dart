import 'package:kapoow/src/features/library_management/collections/data/collections_repository.dart';
import 'package:kapoow/src/features/library_management/collections/domain/models/collection.dart';
import 'package:kapoow/src/features/library_management/collections/domain/repositories/dto_domain_parser.dart';
import 'package:kapoow/src/features/library_management/common/models/read_status.dart';
import 'package:kapoow/src/features/library_management/common/models/series_status.dart';
import 'package:kapoow/src/features/library_management/common/models/typedefs.dart';
import 'package:kapoow/src/features/pagination/domain/repositories/dto_domain_parser.dart';
import 'package:kapoow/src/features/rest_api/data/datasources/collections/collections_datasource.dart';

/// Collections repository that get the data from the api
class CollectionsRepositoryImp implements CollectionsRepository {
  /// Creates instance of [CollectionsRepository]
  CollectionsRepositoryImp(this._datasource);

  final CollectionsDatasource _datasource;

  @override
  Future<Collection> getCollectionById({required String id}) async {
    final dto = await _datasource.getCollectionById(id);
    return dto.toDomain();
  }

  @override
  Future<PageCollection> getCollections({
    String? search,
    List<String>? libraryIds,
    bool? unpaged,
    int? page,
    int? size,
  }) async {
    final dto = await _datasource.getCollections(
      search: search,
      libraryIds: libraryIds,
      page: page,
      size: size,
      unpaged: unpaged,
    );
    return dto.toDomain();
  }

  @override
  Future<PageSeries> getCollectionSeries({
    required String collectionId,
    List<String>? libraryIds,
    List<SeriesStatus>? status,
    ReadStatus? readStatus,
    List<String>? publisher,
    List<String>? language,
    List<String>? genre,
    List<String>? tags,
    List<String>? ageRating,
    List<String>? releaseYears,
    bool? deleted,
    bool? complete,
    bool? unpaged,
    int? page,
    int? size,
    List<String>? authors,
  }) async {
    final dto = await _datasource.getCollectionSeries(
      collectionId: collectionId,
      ageRating: ageRating,
      authors: authors,
      complete: complete,
      deleted: deleted,
      genre: genre,
      language: language,
      libraryIds: libraryIds,
      page: page,
      publisher: publisher,
      readStatus: readStatus,
      releaseYears: releaseYears,
      size: size,
      status: status,
      tags: tags,
      unpaged: unpaged,
    );
    return dto.toDomain();
  }

  @override
  Future<PageCollection> searchCollections(
    String query, {
    int page = 0,
  }) async {
    final dto = await _datasource.getCollections(
      search: query,
      page: page,
    );
    return dto.toDomain();
  }
}
