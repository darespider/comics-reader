import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:kapoow/src/common_widgets/typical_when_builder.dart';
import 'package:kapoow/src/features/library_management/collections/ui/controllers/pods.dart';
import 'package:kapoow/src/features/library_management/series/ui/widgets/series_cover.dart';
import 'package:kapoow/src/features/pagination/ui/widgets/paginated_grid.dart';
import 'package:kapoow/src/routes/dashboard_routes_data.dart';

/// Screen to display the details of a collection
class CollectionScreen extends ConsumerWidget {
  /// Creates instance of [CollectionScreen] widget
  const CollectionScreen({
    required this.collectionId,
    super.key,
  });

  /// id of the item to get
  final String collectionId;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final collectionAsync = ref.watch(collectionPod(collectionId));
    final paginatedPod = collectionSeriesPaginatedPod(collectionId);
    final collectionSeriesPageState = ref.watch(paginatedPod);

    return TypicalWhenBuilder(
      asyncValue: collectionAsync,
      dataBuilder: (context, collection) => Scaffold(
        appBar: AppBar(
          leading: BackButton(
            onPressed: () => const RecommendedRoute().go(context),
          ),
          title: Text(
            collection.name,
            overflow: TextOverflow.ellipsis,
            maxLines: 1,
          ),
        ),
        body: PaginatedGrid(
          paginatedState: collectionSeriesPageState,
          nextPageLoader: ref.watch(paginatedPod.notifier).loadNextPage,
          scrollDirection: Axis.vertical,
          itemBuilder: (context, series) => SeriesCover(series: series),
        ),
      ),
    );
  }
}
