import 'package:kapoow/src/features/library_management/collections/domain/models/collection.dart';
import 'package:kapoow/src/features/library_management/collections/domain/pods.dart';
import 'package:kapoow/src/features/library_management/series/domain/models/series.dart';
import 'package:kapoow/src/features/pagination/domain/models/pagination.dart';
import 'package:kapoow/src/features/pagination/ui/controllers/paginated_mixin.dart';
import 'package:kapoow/src/features/pagination/ui/controllers/paginated_state.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'pods.g.dart';

/// Provides the collection data from an api call
@Riverpod(keepAlive: true)
FutureOr<Collection> collection(CollectionRef ref, String id) =>
    ref.watch(collectionsRepoPod).getCollectionById(id: id);

/// The first 10 series of a collection
@Riverpod(keepAlive: true)
FutureOr<Pagination<Series>> firstSeriesByCollection(
  FirstSeriesByCollectionRef ref,
  String collectionId,
) async {
  final seriesPage =
      await ref.watch(collectionSeriesFirstPageAsyncPod(collectionId).future);
  return seriesPage.copyWith(
    content: seriesPage.content.take(10).toList(),
  );
}

/// Provides the first page of a list of Series from the specified library,
/// if the [collectionId] is `null` then it will fetch the pagination from all
/// libraries
@Riverpod(keepAlive: true)
FutureOr<Pagination<Series>> collectionSeriesFirstPageAsync(
  CollectionSeriesFirstPageAsyncRef ref,
  String collectionId,
) =>
    ref.watch(collectionsRepoPod).getCollectionSeries(
          collectionId: collectionId,
          page: 0,
        );

/// Notifier dedicated to handle the pagination state for Series from a
/// specific library, if the [params] is `null` then it will fetch the
/// pagination from all libraries
@Riverpod(keepAlive: true)
class CollectionSeriesPaginated extends _$CollectionSeriesPaginated
    with PaginatedMixin<Series, String> {
  @override
  PaginatedState<Series> build([String params = '']) {
    assert(params.isNotEmpty, 'The [params] should not be empty');

    final firstPageAsync = ref.watch(
      collectionSeriesFirstPageAsyncPod(params),
    );

    return buildInitState(firstPageAsync);
  }

  @override
  Future<Pagination<Series>> fetchPage(int page) async =>
      ref.watch(collectionsRepoPod).getCollectionSeries(
            collectionId: params,
            page: page,
          );
}
