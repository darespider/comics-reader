import 'package:flutter/material.dart';
import 'package:kapoow/src/features/library_management/collections/domain/models/collection.dart';
import 'package:kapoow/src/features/library_management/common/ui/widgets/item_cover/cover_decoration.dart';
import 'package:kapoow/src/features/library_management/common/ui/widgets/item_cover/cover_image.dart';
import 'package:kapoow/src/routes/routes_data.dart';

/// Displays the cover image of the passed [collection]
class CollectionCover extends StatelessWidget {
  /// Creates a [CollectionCover] instance
  const CollectionCover({
    required this.collection,
    super.key,
  });

  /// [Collection] to read data from
  final Collection collection;

  @override
  Widget build(BuildContext context) {
    return CoverImage(
      onTap: () => CollectionRoute(id: collection.id).go(context),
      coverUrl: collection.thumbnailUri,
      decoration: CoverDecorationData(
        itemsCount: collection.seriesIds.length,
      ),
    );
  }
}
