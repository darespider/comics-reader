import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:kapoow/gen/assets.gen.dart';
import 'package:kapoow/src/common_widgets/app_text_field.dart';
import 'package:kapoow/src/constants/app_paddings.dart';
import 'package:kapoow/src/features/library_management/search/ui/widgets/search_results_body.dart';
import 'package:kapoow/src/routes/routes_data.dart';
import 'package:kapoow/src/theme/extensions.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';

/// Search screen that shows result for Books, Series, Collections and
/// Read Lists based on the user query input
class SearchScreen extends HookWidget {
  /// Creates instance of [SearchScreen] widget
  const SearchScreen({
    super.key,
    this.query,
  });

  /// {@macro kapoow.library_management.search.SearchResultBody}
  final String? query;

  @override
  Widget build(BuildContext context) {
    final controller = useTextEditingController(text: query);
    final debouncedText = useDebounced(
      controller.text,
      const Duration(milliseconds: 500),
    );

    useEffect(
      () {
        if (debouncedText?.isEmpty ?? true) return;
        SearchRoute(controller.text).go(context);
        return null;
      },
      [debouncedText],
    );

    return Scaffold(
      appBar: AppBar(
        toolbarHeight: kToolbarHeight * 1.5,
        title: AppTextField(
          autofocus: true,
          controller: controller,
          prefixIcon: PhosphorIcon(
            PhosphorIcons.magnifyingGlass(PhosphorIconsStyle.duotone),
          ),
          hint: 'Search...',
          suffixIcon: IconButton(
            onPressed: controller.clear,
            icon: PhosphorIcon(PhosphorIcons.x()),
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: controller.text.isEmpty
            ? Center(
                child: Padding(
                  padding: const EdgeInsets.all(AppPaddings.medium),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(
                        // TODO(rurickdev): [l10n] translate
                        'Try searching for your favorite books!',
                        style: context.textTheme.headlineMedium,
                        textAlign: TextAlign.center,
                      ),
                      Assets.images.search.searchPlaceholder.svg(),
                    ],
                  ),
                ),
              )
            : SearchResultBody(query: controller.text),
      ),
    );
  }
}
