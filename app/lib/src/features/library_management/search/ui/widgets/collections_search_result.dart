import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:kapoow/src/common_widgets/titled_section.dart';
import 'package:kapoow/src/features/library_management/collections/ui/widgets/collection_cover.dart';
import 'package:kapoow/src/features/library_management/search/ui/controllers/collections_paginated_search_pod.dart';
import 'package:kapoow/src/features/pagination/ui/widgets/horizontal_paginated_list.dart';

/// Horizontal Infinite list with Recently Added Collections
class CollectionsSearchResult extends ConsumerWidget {
  /// Creates a [CollectionsSearchResult] instance
  const CollectionsSearchResult({
    required this.query,
    super.key,
  });

  /// {@macro kapoow.library_management.search.SearchResultBody}
  final String query;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final pod = collectionsPaginatedSearchPod(query);
    final foundCollections = ref.watch(pod);

    return TitledSection(
      title: const Text('Collections'),
      hide: foundCollections.hasNoResults,
      body: HorizontalPaginatedList(
        paginatedState: foundCollections,
        nextPageLoader: ref.watch(pod.notifier).loadNextPage,
        itemBuilder: (context, collection) => CollectionCover(
          collection: collection,
        ),
      ),
    );
  }
}
