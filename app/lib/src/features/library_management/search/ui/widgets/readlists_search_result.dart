import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:kapoow/src/common_widgets/titled_section.dart';
import 'package:kapoow/src/features/library_management/read_lists/ui/widgets/readlist_cover.dart';
import 'package:kapoow/src/features/library_management/search/ui/controllers/readlists_paginated_search_pod.dart';
import 'package:kapoow/src/features/pagination/ui/widgets/horizontal_paginated_list.dart';

/// Horizontal Infinite list with Recently Added Readlists
class ReadlistsSearchResult extends ConsumerWidget {
  /// Creates a [ReadlistsSearchResult] instance
  const ReadlistsSearchResult({
    required this.query,
    super.key,
  });

  /// {@macro kapoow.library_management.search.SearchResultBody}
  final String query;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final pod = readListsPaginatedSearchPod(query);
    final foundReadlists = ref.watch(pod);

    return TitledSection(
      title: const Text('Readlists'),
      hide: foundReadlists.hasNoResults,
      body: HorizontalPaginatedList(
        paginatedState: foundReadlists,
        nextPageLoader: ref.watch(pod.notifier).loadNextPage,
        itemBuilder: (context, readlist) => ReadlistCover(readlist: readlist),
      ),
    );
  }
}
