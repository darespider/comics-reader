import 'package:kapoow/src/features/library_management/books/domain/models/book.dart';
import 'package:kapoow/src/features/library_management/books/domain/pods.dart';
import 'package:kapoow/src/features/pagination/domain/models/pagination.dart';
import 'package:kapoow/src/features/pagination/ui/controllers/paginated_mixin.dart';
import 'package:kapoow/src/features/pagination/ui/controllers/paginated_state.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'books_paginated_search_pod.g.dart';

/// Provides the first page of a list of searched Books
@riverpod
FutureOr<Pagination<Book>> booksFirstPageSearchAsync(
  BooksFirstPageSearchAsyncRef ref,
  String query,
) {
  return ref.watch(booksRepoPod).getBooks(
        page: 0,
        search: query,
      );
}

/// Notifier dedicated to handle the pagination state to search Books
@riverpod
class BooksPaginatedSearch extends _$BooksPaginatedSearch
    with PaginatedMixin<Book, String> {
  @override
  PaginatedState<Book> build([String params = '']) {
    final firstPageAsync = ref.watch(booksFirstPageSearchAsyncPod(params));

    return buildInitState(firstPageAsync);
  }

  @override
  Future<Pagination<Book>> fetchPage(int page) async {
    return ref.read(booksRepoPod).getBooks(
          page: page,
          search: params,
        );
  }
}
