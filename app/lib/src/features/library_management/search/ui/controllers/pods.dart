import 'package:kapoow/src/features/library_management/search/ui/controllers/books_paginated_search_pod.dart';
import 'package:kapoow/src/features/library_management/search/ui/controllers/collections_paginated_search_pod.dart';
import 'package:kapoow/src/features/library_management/search/ui/controllers/readlists_paginated_search_pod.dart';
import 'package:kapoow/src/features/library_management/search/ui/controllers/series_paginated_search_pod.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'pods.g.dart';

/// Provides information to know if there are any result from the search
@riverpod
Future<bool> somethingFound(SomethingFoundRef ref, String query) async {
  final results = await Future.wait([
    ref.watch(booksFirstPageSearchAsyncPod(query).future),
    ref.watch(seriesFirstPageSearchAsyncPod(query).future),
    ref.watch(collectionsFirstPageSearchAsyncPod(query).future),
    ref.watch(readlistsFirstPageSearchAsyncPod(query).future),
  ]);
  return results.any((element) => element.totalElements != 0);
}
