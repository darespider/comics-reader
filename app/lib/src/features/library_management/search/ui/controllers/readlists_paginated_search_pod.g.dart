// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'readlists_paginated_search_pod.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$readlistsFirstPageSearchAsyncHash() =>
    r'35621619e32d272dad0f5fa92579d30fbd2170e0';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// Provides the first page of a list of searched ReadLists
///
/// Copied from [readlistsFirstPageSearchAsync].
@ProviderFor(readlistsFirstPageSearchAsync)
const readlistsFirstPageSearchAsyncPod = ReadlistsFirstPageSearchAsyncFamily();

/// Provides the first page of a list of searched ReadLists
///
/// Copied from [readlistsFirstPageSearchAsync].
class ReadlistsFirstPageSearchAsyncFamily
    extends Family<AsyncValue<Pagination<ReadList>>> {
  /// Provides the first page of a list of searched ReadLists
  ///
  /// Copied from [readlistsFirstPageSearchAsync].
  const ReadlistsFirstPageSearchAsyncFamily();

  /// Provides the first page of a list of searched ReadLists
  ///
  /// Copied from [readlistsFirstPageSearchAsync].
  ReadlistsFirstPageSearchAsyncProvider call(
    String query,
  ) {
    return ReadlistsFirstPageSearchAsyncProvider(
      query,
    );
  }

  @override
  ReadlistsFirstPageSearchAsyncProvider getProviderOverride(
    covariant ReadlistsFirstPageSearchAsyncProvider provider,
  ) {
    return call(
      provider.query,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'readlistsFirstPageSearchAsyncPod';
}

/// Provides the first page of a list of searched ReadLists
///
/// Copied from [readlistsFirstPageSearchAsync].
class ReadlistsFirstPageSearchAsyncProvider
    extends AutoDisposeFutureProvider<Pagination<ReadList>> {
  /// Provides the first page of a list of searched ReadLists
  ///
  /// Copied from [readlistsFirstPageSearchAsync].
  ReadlistsFirstPageSearchAsyncProvider(
    String query,
  ) : this._internal(
          (ref) => readlistsFirstPageSearchAsync(
            ref as ReadlistsFirstPageSearchAsyncRef,
            query,
          ),
          from: readlistsFirstPageSearchAsyncPod,
          name: r'readlistsFirstPageSearchAsyncPod',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$readlistsFirstPageSearchAsyncHash,
          dependencies: ReadlistsFirstPageSearchAsyncFamily._dependencies,
          allTransitiveDependencies:
              ReadlistsFirstPageSearchAsyncFamily._allTransitiveDependencies,
          query: query,
        );

  ReadlistsFirstPageSearchAsyncProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.query,
  }) : super.internal();

  final String query;

  @override
  Override overrideWith(
    FutureOr<Pagination<ReadList>> Function(
            ReadlistsFirstPageSearchAsyncRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: ReadlistsFirstPageSearchAsyncProvider._internal(
        (ref) => create(ref as ReadlistsFirstPageSearchAsyncRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        query: query,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Pagination<ReadList>> createElement() {
    return _ReadlistsFirstPageSearchAsyncProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is ReadlistsFirstPageSearchAsyncProvider &&
        other.query == query;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, query.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin ReadlistsFirstPageSearchAsyncRef
    on AutoDisposeFutureProviderRef<Pagination<ReadList>> {
  /// The parameter `query` of this provider.
  String get query;
}

class _ReadlistsFirstPageSearchAsyncProviderElement
    extends AutoDisposeFutureProviderElement<Pagination<ReadList>>
    with ReadlistsFirstPageSearchAsyncRef {
  _ReadlistsFirstPageSearchAsyncProviderElement(super.provider);

  @override
  String get query => (origin as ReadlistsFirstPageSearchAsyncProvider).query;
}

String _$readListsPaginatedSearchHash() =>
    r'eeceff1a6984287164427e7988739f8b802ec9b5';

abstract class _$ReadListsPaginatedSearch
    extends BuildlessAutoDisposeNotifier<PaginatedState<ReadList>> {
  late final String params;

  PaginatedState<ReadList> build([
    String params = '',
  ]);
}

/// Notifier dedicated to handle the pagination state to search ReadLists
///
/// Copied from [ReadListsPaginatedSearch].
@ProviderFor(ReadListsPaginatedSearch)
const readListsPaginatedSearchPod = ReadListsPaginatedSearchFamily();

/// Notifier dedicated to handle the pagination state to search ReadLists
///
/// Copied from [ReadListsPaginatedSearch].
class ReadListsPaginatedSearchFamily extends Family<PaginatedState<ReadList>> {
  /// Notifier dedicated to handle the pagination state to search ReadLists
  ///
  /// Copied from [ReadListsPaginatedSearch].
  const ReadListsPaginatedSearchFamily();

  /// Notifier dedicated to handle the pagination state to search ReadLists
  ///
  /// Copied from [ReadListsPaginatedSearch].
  ReadListsPaginatedSearchProvider call([
    String params = '',
  ]) {
    return ReadListsPaginatedSearchProvider(
      params,
    );
  }

  @override
  ReadListsPaginatedSearchProvider getProviderOverride(
    covariant ReadListsPaginatedSearchProvider provider,
  ) {
    return call(
      provider.params,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'readListsPaginatedSearchPod';
}

/// Notifier dedicated to handle the pagination state to search ReadLists
///
/// Copied from [ReadListsPaginatedSearch].
class ReadListsPaginatedSearchProvider extends AutoDisposeNotifierProviderImpl<
    ReadListsPaginatedSearch, PaginatedState<ReadList>> {
  /// Notifier dedicated to handle the pagination state to search ReadLists
  ///
  /// Copied from [ReadListsPaginatedSearch].
  ReadListsPaginatedSearchProvider([
    String params = '',
  ]) : this._internal(
          () => ReadListsPaginatedSearch()..params = params,
          from: readListsPaginatedSearchPod,
          name: r'readListsPaginatedSearchPod',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$readListsPaginatedSearchHash,
          dependencies: ReadListsPaginatedSearchFamily._dependencies,
          allTransitiveDependencies:
              ReadListsPaginatedSearchFamily._allTransitiveDependencies,
          params: params,
        );

  ReadListsPaginatedSearchProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.params,
  }) : super.internal();

  final String params;

  @override
  PaginatedState<ReadList> runNotifierBuild(
    covariant ReadListsPaginatedSearch notifier,
  ) {
    return notifier.build(
      params,
    );
  }

  @override
  Override overrideWith(ReadListsPaginatedSearch Function() create) {
    return ProviderOverride(
      origin: this,
      override: ReadListsPaginatedSearchProvider._internal(
        () => create()..params = params,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        params: params,
      ),
    );
  }

  @override
  AutoDisposeNotifierProviderElement<ReadListsPaginatedSearch,
      PaginatedState<ReadList>> createElement() {
    return _ReadListsPaginatedSearchProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is ReadListsPaginatedSearchProvider && other.params == params;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, params.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin ReadListsPaginatedSearchRef
    on AutoDisposeNotifierProviderRef<PaginatedState<ReadList>> {
  /// The parameter `params` of this provider.
  String get params;
}

class _ReadListsPaginatedSearchProviderElement
    extends AutoDisposeNotifierProviderElement<ReadListsPaginatedSearch,
        PaginatedState<ReadList>> with ReadListsPaginatedSearchRef {
  _ReadListsPaginatedSearchProviderElement(super.provider);

  @override
  String get params => (origin as ReadListsPaginatedSearchProvider).params;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
