// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'books_paginated_search_pod.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$booksFirstPageSearchAsyncHash() =>
    r'8c5b96749fc6862414a679940237b490592eb247';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// Provides the first page of a list of searched Books
///
/// Copied from [booksFirstPageSearchAsync].
@ProviderFor(booksFirstPageSearchAsync)
const booksFirstPageSearchAsyncPod = BooksFirstPageSearchAsyncFamily();

/// Provides the first page of a list of searched Books
///
/// Copied from [booksFirstPageSearchAsync].
class BooksFirstPageSearchAsyncFamily
    extends Family<AsyncValue<Pagination<Book>>> {
  /// Provides the first page of a list of searched Books
  ///
  /// Copied from [booksFirstPageSearchAsync].
  const BooksFirstPageSearchAsyncFamily();

  /// Provides the first page of a list of searched Books
  ///
  /// Copied from [booksFirstPageSearchAsync].
  BooksFirstPageSearchAsyncProvider call(
    String query,
  ) {
    return BooksFirstPageSearchAsyncProvider(
      query,
    );
  }

  @override
  BooksFirstPageSearchAsyncProvider getProviderOverride(
    covariant BooksFirstPageSearchAsyncProvider provider,
  ) {
    return call(
      provider.query,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'booksFirstPageSearchAsyncPod';
}

/// Provides the first page of a list of searched Books
///
/// Copied from [booksFirstPageSearchAsync].
class BooksFirstPageSearchAsyncProvider
    extends AutoDisposeFutureProvider<Pagination<Book>> {
  /// Provides the first page of a list of searched Books
  ///
  /// Copied from [booksFirstPageSearchAsync].
  BooksFirstPageSearchAsyncProvider(
    String query,
  ) : this._internal(
          (ref) => booksFirstPageSearchAsync(
            ref as BooksFirstPageSearchAsyncRef,
            query,
          ),
          from: booksFirstPageSearchAsyncPod,
          name: r'booksFirstPageSearchAsyncPod',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$booksFirstPageSearchAsyncHash,
          dependencies: BooksFirstPageSearchAsyncFamily._dependencies,
          allTransitiveDependencies:
              BooksFirstPageSearchAsyncFamily._allTransitiveDependencies,
          query: query,
        );

  BooksFirstPageSearchAsyncProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.query,
  }) : super.internal();

  final String query;

  @override
  Override overrideWith(
    FutureOr<Pagination<Book>> Function(BooksFirstPageSearchAsyncRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: BooksFirstPageSearchAsyncProvider._internal(
        (ref) => create(ref as BooksFirstPageSearchAsyncRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        query: query,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Pagination<Book>> createElement() {
    return _BooksFirstPageSearchAsyncProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is BooksFirstPageSearchAsyncProvider && other.query == query;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, query.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin BooksFirstPageSearchAsyncRef
    on AutoDisposeFutureProviderRef<Pagination<Book>> {
  /// The parameter `query` of this provider.
  String get query;
}

class _BooksFirstPageSearchAsyncProviderElement
    extends AutoDisposeFutureProviderElement<Pagination<Book>>
    with BooksFirstPageSearchAsyncRef {
  _BooksFirstPageSearchAsyncProviderElement(super.provider);

  @override
  String get query => (origin as BooksFirstPageSearchAsyncProvider).query;
}

String _$booksPaginatedSearchHash() =>
    r'72f7d3b40c13318f2abe7080caaa31fd1d84fb60';

abstract class _$BooksPaginatedSearch
    extends BuildlessAutoDisposeNotifier<PaginatedState<Book>> {
  late final String params;

  PaginatedState<Book> build([
    String params = '',
  ]);
}

/// Notifier dedicated to handle the pagination state to search Books
///
/// Copied from [BooksPaginatedSearch].
@ProviderFor(BooksPaginatedSearch)
const booksPaginatedSearchPod = BooksPaginatedSearchFamily();

/// Notifier dedicated to handle the pagination state to search Books
///
/// Copied from [BooksPaginatedSearch].
class BooksPaginatedSearchFamily extends Family<PaginatedState<Book>> {
  /// Notifier dedicated to handle the pagination state to search Books
  ///
  /// Copied from [BooksPaginatedSearch].
  const BooksPaginatedSearchFamily();

  /// Notifier dedicated to handle the pagination state to search Books
  ///
  /// Copied from [BooksPaginatedSearch].
  BooksPaginatedSearchProvider call([
    String params = '',
  ]) {
    return BooksPaginatedSearchProvider(
      params,
    );
  }

  @override
  BooksPaginatedSearchProvider getProviderOverride(
    covariant BooksPaginatedSearchProvider provider,
  ) {
    return call(
      provider.params,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'booksPaginatedSearchPod';
}

/// Notifier dedicated to handle the pagination state to search Books
///
/// Copied from [BooksPaginatedSearch].
class BooksPaginatedSearchProvider extends AutoDisposeNotifierProviderImpl<
    BooksPaginatedSearch, PaginatedState<Book>> {
  /// Notifier dedicated to handle the pagination state to search Books
  ///
  /// Copied from [BooksPaginatedSearch].
  BooksPaginatedSearchProvider([
    String params = '',
  ]) : this._internal(
          () => BooksPaginatedSearch()..params = params,
          from: booksPaginatedSearchPod,
          name: r'booksPaginatedSearchPod',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$booksPaginatedSearchHash,
          dependencies: BooksPaginatedSearchFamily._dependencies,
          allTransitiveDependencies:
              BooksPaginatedSearchFamily._allTransitiveDependencies,
          params: params,
        );

  BooksPaginatedSearchProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.params,
  }) : super.internal();

  final String params;

  @override
  PaginatedState<Book> runNotifierBuild(
    covariant BooksPaginatedSearch notifier,
  ) {
    return notifier.build(
      params,
    );
  }

  @override
  Override overrideWith(BooksPaginatedSearch Function() create) {
    return ProviderOverride(
      origin: this,
      override: BooksPaginatedSearchProvider._internal(
        () => create()..params = params,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        params: params,
      ),
    );
  }

  @override
  AutoDisposeNotifierProviderElement<BooksPaginatedSearch, PaginatedState<Book>>
      createElement() {
    return _BooksPaginatedSearchProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is BooksPaginatedSearchProvider && other.params == params;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, params.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin BooksPaginatedSearchRef
    on AutoDisposeNotifierProviderRef<PaginatedState<Book>> {
  /// The parameter `params` of this provider.
  String get params;
}

class _BooksPaginatedSearchProviderElement
    extends AutoDisposeNotifierProviderElement<BooksPaginatedSearch,
        PaginatedState<Book>> with BooksPaginatedSearchRef {
  _BooksPaginatedSearchProviderElement(super.provider);

  @override
  String get params => (origin as BooksPaginatedSearchProvider).params;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
