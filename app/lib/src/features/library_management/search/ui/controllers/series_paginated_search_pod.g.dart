// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'series_paginated_search_pod.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$seriesFirstPageSearchAsyncHash() =>
    r'35c99b7ef1833ee38e617d2985c33e371d886988';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// Provides the first page of a list of searched Series
///
/// Copied from [seriesFirstPageSearchAsync].
@ProviderFor(seriesFirstPageSearchAsync)
const seriesFirstPageSearchAsyncPod = SeriesFirstPageSearchAsyncFamily();

/// Provides the first page of a list of searched Series
///
/// Copied from [seriesFirstPageSearchAsync].
class SeriesFirstPageSearchAsyncFamily
    extends Family<AsyncValue<Pagination<Series>>> {
  /// Provides the first page of a list of searched Series
  ///
  /// Copied from [seriesFirstPageSearchAsync].
  const SeriesFirstPageSearchAsyncFamily();

  /// Provides the first page of a list of searched Series
  ///
  /// Copied from [seriesFirstPageSearchAsync].
  SeriesFirstPageSearchAsyncProvider call(
    String query,
  ) {
    return SeriesFirstPageSearchAsyncProvider(
      query,
    );
  }

  @override
  SeriesFirstPageSearchAsyncProvider getProviderOverride(
    covariant SeriesFirstPageSearchAsyncProvider provider,
  ) {
    return call(
      provider.query,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'seriesFirstPageSearchAsyncPod';
}

/// Provides the first page of a list of searched Series
///
/// Copied from [seriesFirstPageSearchAsync].
class SeriesFirstPageSearchAsyncProvider
    extends AutoDisposeFutureProvider<Pagination<Series>> {
  /// Provides the first page of a list of searched Series
  ///
  /// Copied from [seriesFirstPageSearchAsync].
  SeriesFirstPageSearchAsyncProvider(
    String query,
  ) : this._internal(
          (ref) => seriesFirstPageSearchAsync(
            ref as SeriesFirstPageSearchAsyncRef,
            query,
          ),
          from: seriesFirstPageSearchAsyncPod,
          name: r'seriesFirstPageSearchAsyncPod',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$seriesFirstPageSearchAsyncHash,
          dependencies: SeriesFirstPageSearchAsyncFamily._dependencies,
          allTransitiveDependencies:
              SeriesFirstPageSearchAsyncFamily._allTransitiveDependencies,
          query: query,
        );

  SeriesFirstPageSearchAsyncProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.query,
  }) : super.internal();

  final String query;

  @override
  Override overrideWith(
    FutureOr<Pagination<Series>> Function(
            SeriesFirstPageSearchAsyncRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: SeriesFirstPageSearchAsyncProvider._internal(
        (ref) => create(ref as SeriesFirstPageSearchAsyncRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        query: query,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Pagination<Series>> createElement() {
    return _SeriesFirstPageSearchAsyncProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is SeriesFirstPageSearchAsyncProvider && other.query == query;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, query.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin SeriesFirstPageSearchAsyncRef
    on AutoDisposeFutureProviderRef<Pagination<Series>> {
  /// The parameter `query` of this provider.
  String get query;
}

class _SeriesFirstPageSearchAsyncProviderElement
    extends AutoDisposeFutureProviderElement<Pagination<Series>>
    with SeriesFirstPageSearchAsyncRef {
  _SeriesFirstPageSearchAsyncProviderElement(super.provider);

  @override
  String get query => (origin as SeriesFirstPageSearchAsyncProvider).query;
}

String _$seriesPaginatedSearchHash() =>
    r'5a6f6536e691baa7530f80881bdc257316446985';

abstract class _$SeriesPaginatedSearch
    extends BuildlessAutoDisposeNotifier<PaginatedState<Series>> {
  late final String params;

  PaginatedState<Series> build([
    String params = '',
  ]);
}

/// Notifier dedicated to handle the pagination state to search Series
///
/// Copied from [SeriesPaginatedSearch].
@ProviderFor(SeriesPaginatedSearch)
const seriesPaginatedSearchPod = SeriesPaginatedSearchFamily();

/// Notifier dedicated to handle the pagination state to search Series
///
/// Copied from [SeriesPaginatedSearch].
class SeriesPaginatedSearchFamily extends Family<PaginatedState<Series>> {
  /// Notifier dedicated to handle the pagination state to search Series
  ///
  /// Copied from [SeriesPaginatedSearch].
  const SeriesPaginatedSearchFamily();

  /// Notifier dedicated to handle the pagination state to search Series
  ///
  /// Copied from [SeriesPaginatedSearch].
  SeriesPaginatedSearchProvider call([
    String params = '',
  ]) {
    return SeriesPaginatedSearchProvider(
      params,
    );
  }

  @override
  SeriesPaginatedSearchProvider getProviderOverride(
    covariant SeriesPaginatedSearchProvider provider,
  ) {
    return call(
      provider.params,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'seriesPaginatedSearchPod';
}

/// Notifier dedicated to handle the pagination state to search Series
///
/// Copied from [SeriesPaginatedSearch].
class SeriesPaginatedSearchProvider extends AutoDisposeNotifierProviderImpl<
    SeriesPaginatedSearch, PaginatedState<Series>> {
  /// Notifier dedicated to handle the pagination state to search Series
  ///
  /// Copied from [SeriesPaginatedSearch].
  SeriesPaginatedSearchProvider([
    String params = '',
  ]) : this._internal(
          () => SeriesPaginatedSearch()..params = params,
          from: seriesPaginatedSearchPod,
          name: r'seriesPaginatedSearchPod',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$seriesPaginatedSearchHash,
          dependencies: SeriesPaginatedSearchFamily._dependencies,
          allTransitiveDependencies:
              SeriesPaginatedSearchFamily._allTransitiveDependencies,
          params: params,
        );

  SeriesPaginatedSearchProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.params,
  }) : super.internal();

  final String params;

  @override
  PaginatedState<Series> runNotifierBuild(
    covariant SeriesPaginatedSearch notifier,
  ) {
    return notifier.build(
      params,
    );
  }

  @override
  Override overrideWith(SeriesPaginatedSearch Function() create) {
    return ProviderOverride(
      origin: this,
      override: SeriesPaginatedSearchProvider._internal(
        () => create()..params = params,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        params: params,
      ),
    );
  }

  @override
  AutoDisposeNotifierProviderElement<SeriesPaginatedSearch,
      PaginatedState<Series>> createElement() {
    return _SeriesPaginatedSearchProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is SeriesPaginatedSearchProvider && other.params == params;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, params.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin SeriesPaginatedSearchRef
    on AutoDisposeNotifierProviderRef<PaginatedState<Series>> {
  /// The parameter `params` of this provider.
  String get params;
}

class _SeriesPaginatedSearchProviderElement
    extends AutoDisposeNotifierProviderElement<SeriesPaginatedSearch,
        PaginatedState<Series>> with SeriesPaginatedSearchRef {
  _SeriesPaginatedSearchProviderElement(super.provider);

  @override
  String get params => (origin as SeriesPaginatedSearchProvider).params;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
