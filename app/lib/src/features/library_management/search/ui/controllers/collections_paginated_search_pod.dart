import 'package:kapoow/src/features/library_management/collections/domain/models/collection.dart';
import 'package:kapoow/src/features/library_management/collections/domain/pods.dart';
import 'package:kapoow/src/features/pagination/domain/models/pagination.dart';
import 'package:kapoow/src/features/pagination/ui/controllers/paginated_mixin.dart';
import 'package:kapoow/src/features/pagination/ui/controllers/paginated_state.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'collections_paginated_search_pod.g.dart';

/// Provides the first page of a list of searched Collections
@riverpod
FutureOr<Pagination<Collection>> collectionsFirstPageSearchAsync(
  CollectionsFirstPageSearchAsyncRef ref,
  String query,
) {
  return ref.watch(collectionsRepoPod).getCollections(
        page: 0,
        search: query,
      );
}

/// Notifier dedicated to handle the pagination state to search Collections
@riverpod
class CollectionsPaginatedSearch extends _$CollectionsPaginatedSearch
    with PaginatedMixin<Collection, String> {
  @override
  PaginatedState<Collection> build([String params = '']) {
    final firstPageAsync =
        ref.watch(collectionsFirstPageSearchAsyncPod(params));

    return buildInitState(firstPageAsync);
  }

  @override
  Future<Pagination<Collection>> fetchPage(int page) async {
    return ref.read(collectionsRepoPod).getCollections(
          page: page,
          search: params,
        );
  }
}
