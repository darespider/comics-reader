// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pods.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$somethingFoundHash() => r'c77a90697fbbc7688820f863fc0b521679ed48cc';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// Provides information to know if there are any result from the search
///
/// Copied from [somethingFound].
@ProviderFor(somethingFound)
const somethingFoundPod = SomethingFoundFamily();

/// Provides information to know if there are any result from the search
///
/// Copied from [somethingFound].
class SomethingFoundFamily extends Family<AsyncValue<bool>> {
  /// Provides information to know if there are any result from the search
  ///
  /// Copied from [somethingFound].
  const SomethingFoundFamily();

  /// Provides information to know if there are any result from the search
  ///
  /// Copied from [somethingFound].
  SomethingFoundProvider call(
    String query,
  ) {
    return SomethingFoundProvider(
      query,
    );
  }

  @override
  SomethingFoundProvider getProviderOverride(
    covariant SomethingFoundProvider provider,
  ) {
    return call(
      provider.query,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'somethingFoundPod';
}

/// Provides information to know if there are any result from the search
///
/// Copied from [somethingFound].
class SomethingFoundProvider extends AutoDisposeFutureProvider<bool> {
  /// Provides information to know if there are any result from the search
  ///
  /// Copied from [somethingFound].
  SomethingFoundProvider(
    String query,
  ) : this._internal(
          (ref) => somethingFound(
            ref as SomethingFoundRef,
            query,
          ),
          from: somethingFoundPod,
          name: r'somethingFoundPod',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$somethingFoundHash,
          dependencies: SomethingFoundFamily._dependencies,
          allTransitiveDependencies:
              SomethingFoundFamily._allTransitiveDependencies,
          query: query,
        );

  SomethingFoundProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.query,
  }) : super.internal();

  final String query;

  @override
  Override overrideWith(
    FutureOr<bool> Function(SomethingFoundRef provider) create,
  ) {
    return ProviderOverride(
      origin: this,
      override: SomethingFoundProvider._internal(
        (ref) => create(ref as SomethingFoundRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        query: query,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<bool> createElement() {
    return _SomethingFoundProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is SomethingFoundProvider && other.query == query;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, query.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin SomethingFoundRef on AutoDisposeFutureProviderRef<bool> {
  /// The parameter `query` of this provider.
  String get query;
}

class _SomethingFoundProviderElement
    extends AutoDisposeFutureProviderElement<bool> with SomethingFoundRef {
  _SomethingFoundProviderElement(super.provider);

  @override
  String get query => (origin as SomethingFoundProvider).query;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
