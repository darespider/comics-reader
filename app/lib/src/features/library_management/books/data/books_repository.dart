import 'package:kapoow/src/features/library_management/books/domain/models/book.dart';
import 'package:kapoow/src/features/library_management/books/domain/models/page.dart';
import 'package:kapoow/src/features/library_management/common/models/media_status.dart';
import 'package:kapoow/src/features/library_management/common/models/read_status.dart';
import 'package:kapoow/src/features/library_management/common/models/typedefs.dart';

/// Interface describing how the books repositories should work
abstract class BooksRepository {
  /// Deletes a book
  Future<void> analyzeBook(String bookId);

  /// Deletes a book
  Future<void> deleteBook(String bookId);

  /// Fetch a single book based on his id
  /// if the id doesn't exist it return null
  Future<Book> getBook(String id);

  /// Returns a list of the books fetch from the api, accepts many
  /// search filters
  Future<PageBook> getBooks({
    String? search,
    List<String>? libraryIds,
    List<MediaStatus>? mediaStatus,
    List<ReadStatus>? readStatus,
    DateTime? releasedAfter,
    List<String>? tags,
    bool? unpaged,
    int? page,
    int? size,
    List<String>? sort,
  });

  /// Fetch a page of on deck elements
  Future<PageBook> getBooksOnDeck({
    String? libraryId,
    int page = 0,
    int pageSize = 20,
  });

  /// Fetch the next book based on the current one
  Future<Book?> getNext(String bookId);

  /// Fetch the pages of the current book
  Future<List<Page>> getPages(String bookId);

  /// Fetch the previous book based on the current one
  Future<Book?> getPrevious(String bookId);

  /// Fetch a page of on deck elements
  Future<PageBook> getReadingBooks({
    List<String>? libraryIds,
    int page = 0,
    int pageSize = 20,
  });

  /// Marks a book as completed/read
  Future<void> markBookAsRead(String id);

  /// Marks a book as un read (not started)
  Future<void> markBookAsUnread(String id);

  /// Deletes a book
  Future<void> refreshBookMetadata(String bookId);

  /// Search books based on the [query]
  Future<PageBook> searchBooks(String query, {int page = 0});

  /// Patch the progress of a book
  Future<void> updateBookProgress(String id, {int page = 0});
}
