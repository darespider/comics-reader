// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pods.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$booksRepoHash() => r'2e9b3260cb861847ca692bbf76144618a0dacb67';

/// Provides the [BooksRepository]
///
/// Copied from [booksRepo].
@ProviderFor(booksRepo)
final booksRepoPod = Provider<BooksRepository>.internal(
  booksRepo,
  name: r'booksRepoPod',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$booksRepoHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef BooksRepoRef = ProviderRef<BooksRepository>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
