import 'package:kapoow/src/features/library_management/books/data/books_repository.dart';
import 'package:kapoow/src/features/library_management/books/domain/repositories/books_repository_imp.dart';
import 'package:kapoow/src/features/rest_api/data/datasources/pods.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'pods.g.dart';

/// Provides the [BooksRepository]
@Riverpod(keepAlive: true)
BooksRepository booksRepo(BooksRepoRef ref) {
  return BooksRepositoryImp(ref.watch(booksDatasourcePod));
}
