import 'package:kapoow/src/features/library_management/books/data/books_repository.dart';
import 'package:kapoow/src/features/library_management/books/domain/models/book.dart';
import 'package:kapoow/src/features/library_management/books/domain/models/page.dart';
import 'package:kapoow/src/features/library_management/books/domain/repositories/dto_domain_parser.dart';
import 'package:kapoow/src/features/library_management/common/models/media_status.dart';
import 'package:kapoow/src/features/library_management/common/models/read_status.dart';
import 'package:kapoow/src/features/library_management/common/models/typedefs.dart';
import 'package:kapoow/src/features/pagination/domain/repositories/dto_domain_parser.dart';
import 'package:kapoow/src/features/rest_api/data/datasources/books/books_datasource.dart';

/// Book repository that gets the books from an api client
class BooksRepositoryImp extends BooksRepository {
  /// Creates a [BooksRepository] and requieres a [BooksDatasource]
  BooksRepositoryImp(this._datasource);

  final BooksDatasource _datasource;

  @override
  Future<void> analyzeBook(String bookId) async =>
      _datasource.postAnalyzeBookById(bookId);

  @override
  Future<void> deleteBook(String bookId) async =>
      _datasource.deleteBookById(bookId);

  @override
  Future<Book> getBook(String id) async {
    final dto = await _datasource.getBookById(id);
    return dto.toDomain();
  }

  @override
  Future<PageBook> getBooks({
    String? search,
    List<String>? libraryIds,
    List<MediaStatus>? mediaStatus,
    List<ReadStatus>? readStatus,
    DateTime? releasedAfter,
    List<String>? tags,
    bool? unpaged,
    int? page,
    int? size,
    List<String>? sort,
  }) async {
    final dto = await _datasource.getBooks(
      search: search,
      libraryIds: libraryIds,
      mediaStatus: mediaStatus,
      readStatus: readStatus,
      releasedAfter: releasedAfter,
      tags: tags,
      unpaged: unpaged,
      page: page,
      size: size,
      sort: sort,
    );
    return dto.toDomain<Book>();
  }

  @override
  Future<PageBook> getBooksOnDeck({
    String? libraryId,
    int page = 0,
    int pageSize = 20,
  }) async {
    final dto = await _datasource.getBooksOnDeck(
      libraryId: libraryId,
      page: page,
      pageSize: pageSize,
    );
    return dto.toDomain();
  }

  @override
  Future<Book?> getNext(String bookId) async {
    final dto = await _datasource.getNextBookById(bookId);
    return dto?.toDomain();
  }

  @override
  Future<List<Page>> getPages(String bookId) async {
    final dto = await _datasource.getPagesByBookId(bookId);
    return dto.map((e) => e.toDomain()).toList();
  }

  @override
  Future<Book?> getPrevious(String bookId) async {
    final dto = await _datasource.getPreviousBookById(bookId);
    return dto?.toDomain();
  }

  @override
  Future<PageBook> getReadingBooks({
    List<String>? libraryIds,
    int page = 0,
    int pageSize = 20,
  }) async {
    final dto = await _datasource.getBooks(
      libraryIds: libraryIds,
      page: page,
      size: pageSize,
      readStatus: [ReadStatus.inProgress],
    );
    return dto.toDomain();
  }

  @override
  Future<void> markBookAsRead(String id) async =>
      _datasource.patchBookProgressById(
        id,
        completed: true,
      );

  @override
  Future<void> markBookAsUnread(String id) async =>
      _datasource.patchBookProgressById(
        id,
        page: 0,
        completed: false,
      );

  @override
  Future<void> refreshBookMetadata(String bookId) async =>
      _datasource.postRefreshBookById(bookId);

  @override
  Future<PageBook> searchBooks(String query, {int page = 0}) async {
    final dto = await _datasource.getBooks(
      search: query,
      page: page,
    );
    return dto.toDomain();
  }

  @override
  Future<void> updateBookProgress(String id, {int page = 0}) async =>
      _datasource.patchBookProgressById(
        id,
        page: page,
        completed: false,
      );
}
