import 'package:freezed_annotation/freezed_annotation.dart';

part 'web_link.freezed.dart';

/// External links with reference of the book
@freezed
class WebLink with _$WebLink {
  /// Creates an [WebLink] instance
  const factory WebLink({
    required String label,
    required Uri url,
  }) = _WebLink;
}
