import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kapoow/src/features/library_management/books/domain/models/web_link.dart';
import 'package:kapoow/src/features/library_management/common/models/author.dart';

part 'book_metadata.freezed.dart';

/// Metadata of a Book
@freezed
class BookMetadata with _$BookMetadata {
  /// Creates a [BookMetadata] instance
  const factory BookMetadata({
    required String title,
    required bool titleLock,
    required String summary,
    required bool summaryLock,
    required double number,
    required bool numberLock,
    required double numberSort,
    required bool numberSortLock,
    required bool releaseDateLock,
    required List<Author> authors,
    required bool authorsLock,
    required Set<String> tags,
    required bool tagsLock,
    required DateTime created,
    required DateTime lastModified,
    required String isbn,
    required bool isbnLock,
    required bool linksLock,
    required List<WebLink> links,
    DateTime? releaseDate,
  }) = _BookMetadata;
}
