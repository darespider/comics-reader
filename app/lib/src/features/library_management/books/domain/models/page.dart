import 'package:freezed_annotation/freezed_annotation.dart';

part 'page.freezed.dart';

/// Page information from a book
@freezed
class Page with _$Page {
  /// Creates a [Page] instance
  const factory Page({
    required int number,
    required String fileName,
    required String mediaType,
    int? width,
    int? height,
  }) = _Page;
}
