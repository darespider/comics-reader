// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'book.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$Book {
  String get id => throw _privateConstructorUsedError;
  String get seriesId => throw _privateConstructorUsedError;
  String get seriesTitle => throw _privateConstructorUsedError;
  String get libraryId => throw _privateConstructorUsedError;
  String get name => throw _privateConstructorUsedError;
  String get url => throw _privateConstructorUsedError;
  bool get deleted => throw _privateConstructorUsedError;
  int get number => throw _privateConstructorUsedError;
  DateTime get created => throw _privateConstructorUsedError;
  DateTime get lastModified => throw _privateConstructorUsedError;
  DateTime get fileLastModified => throw _privateConstructorUsedError;
  int get sizeBytes => throw _privateConstructorUsedError;
  String get size => throw _privateConstructorUsedError;
  Media get media => throw _privateConstructorUsedError;
  BookMetadata get metadata => throw _privateConstructorUsedError;
  String get fileHash => throw _privateConstructorUsedError;
  bool get oneshot => throw _privateConstructorUsedError;
  ReadProgress? get readProgress => throw _privateConstructorUsedError;

  /// Create a copy of Book
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $BookCopyWith<Book> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $BookCopyWith<$Res> {
  factory $BookCopyWith(Book value, $Res Function(Book) then) =
      _$BookCopyWithImpl<$Res, Book>;
  @useResult
  $Res call(
      {String id,
      String seriesId,
      String seriesTitle,
      String libraryId,
      String name,
      String url,
      bool deleted,
      int number,
      DateTime created,
      DateTime lastModified,
      DateTime fileLastModified,
      int sizeBytes,
      String size,
      Media media,
      BookMetadata metadata,
      String fileHash,
      bool oneshot,
      ReadProgress? readProgress});

  $MediaCopyWith<$Res> get media;
  $BookMetadataCopyWith<$Res> get metadata;
  $ReadProgressCopyWith<$Res>? get readProgress;
}

/// @nodoc
class _$BookCopyWithImpl<$Res, $Val extends Book>
    implements $BookCopyWith<$Res> {
  _$BookCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of Book
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? seriesId = null,
    Object? seriesTitle = null,
    Object? libraryId = null,
    Object? name = null,
    Object? url = null,
    Object? deleted = null,
    Object? number = null,
    Object? created = null,
    Object? lastModified = null,
    Object? fileLastModified = null,
    Object? sizeBytes = null,
    Object? size = null,
    Object? media = null,
    Object? metadata = null,
    Object? fileHash = null,
    Object? oneshot = null,
    Object? readProgress = freezed,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      seriesId: null == seriesId
          ? _value.seriesId
          : seriesId // ignore: cast_nullable_to_non_nullable
              as String,
      seriesTitle: null == seriesTitle
          ? _value.seriesTitle
          : seriesTitle // ignore: cast_nullable_to_non_nullable
              as String,
      libraryId: null == libraryId
          ? _value.libraryId
          : libraryId // ignore: cast_nullable_to_non_nullable
              as String,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      url: null == url
          ? _value.url
          : url // ignore: cast_nullable_to_non_nullable
              as String,
      deleted: null == deleted
          ? _value.deleted
          : deleted // ignore: cast_nullable_to_non_nullable
              as bool,
      number: null == number
          ? _value.number
          : number // ignore: cast_nullable_to_non_nullable
              as int,
      created: null == created
          ? _value.created
          : created // ignore: cast_nullable_to_non_nullable
              as DateTime,
      lastModified: null == lastModified
          ? _value.lastModified
          : lastModified // ignore: cast_nullable_to_non_nullable
              as DateTime,
      fileLastModified: null == fileLastModified
          ? _value.fileLastModified
          : fileLastModified // ignore: cast_nullable_to_non_nullable
              as DateTime,
      sizeBytes: null == sizeBytes
          ? _value.sizeBytes
          : sizeBytes // ignore: cast_nullable_to_non_nullable
              as int,
      size: null == size
          ? _value.size
          : size // ignore: cast_nullable_to_non_nullable
              as String,
      media: null == media
          ? _value.media
          : media // ignore: cast_nullable_to_non_nullable
              as Media,
      metadata: null == metadata
          ? _value.metadata
          : metadata // ignore: cast_nullable_to_non_nullable
              as BookMetadata,
      fileHash: null == fileHash
          ? _value.fileHash
          : fileHash // ignore: cast_nullable_to_non_nullable
              as String,
      oneshot: null == oneshot
          ? _value.oneshot
          : oneshot // ignore: cast_nullable_to_non_nullable
              as bool,
      readProgress: freezed == readProgress
          ? _value.readProgress
          : readProgress // ignore: cast_nullable_to_non_nullable
              as ReadProgress?,
    ) as $Val);
  }

  /// Create a copy of Book
  /// with the given fields replaced by the non-null parameter values.
  @override
  @pragma('vm:prefer-inline')
  $MediaCopyWith<$Res> get media {
    return $MediaCopyWith<$Res>(_value.media, (value) {
      return _then(_value.copyWith(media: value) as $Val);
    });
  }

  /// Create a copy of Book
  /// with the given fields replaced by the non-null parameter values.
  @override
  @pragma('vm:prefer-inline')
  $BookMetadataCopyWith<$Res> get metadata {
    return $BookMetadataCopyWith<$Res>(_value.metadata, (value) {
      return _then(_value.copyWith(metadata: value) as $Val);
    });
  }

  /// Create a copy of Book
  /// with the given fields replaced by the non-null parameter values.
  @override
  @pragma('vm:prefer-inline')
  $ReadProgressCopyWith<$Res>? get readProgress {
    if (_value.readProgress == null) {
      return null;
    }

    return $ReadProgressCopyWith<$Res>(_value.readProgress!, (value) {
      return _then(_value.copyWith(readProgress: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$BookImplCopyWith<$Res> implements $BookCopyWith<$Res> {
  factory _$$BookImplCopyWith(
          _$BookImpl value, $Res Function(_$BookImpl) then) =
      __$$BookImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String id,
      String seriesId,
      String seriesTitle,
      String libraryId,
      String name,
      String url,
      bool deleted,
      int number,
      DateTime created,
      DateTime lastModified,
      DateTime fileLastModified,
      int sizeBytes,
      String size,
      Media media,
      BookMetadata metadata,
      String fileHash,
      bool oneshot,
      ReadProgress? readProgress});

  @override
  $MediaCopyWith<$Res> get media;
  @override
  $BookMetadataCopyWith<$Res> get metadata;
  @override
  $ReadProgressCopyWith<$Res>? get readProgress;
}

/// @nodoc
class __$$BookImplCopyWithImpl<$Res>
    extends _$BookCopyWithImpl<$Res, _$BookImpl>
    implements _$$BookImplCopyWith<$Res> {
  __$$BookImplCopyWithImpl(_$BookImpl _value, $Res Function(_$BookImpl) _then)
      : super(_value, _then);

  /// Create a copy of Book
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? seriesId = null,
    Object? seriesTitle = null,
    Object? libraryId = null,
    Object? name = null,
    Object? url = null,
    Object? deleted = null,
    Object? number = null,
    Object? created = null,
    Object? lastModified = null,
    Object? fileLastModified = null,
    Object? sizeBytes = null,
    Object? size = null,
    Object? media = null,
    Object? metadata = null,
    Object? fileHash = null,
    Object? oneshot = null,
    Object? readProgress = freezed,
  }) {
    return _then(_$BookImpl(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      seriesId: null == seriesId
          ? _value.seriesId
          : seriesId // ignore: cast_nullable_to_non_nullable
              as String,
      seriesTitle: null == seriesTitle
          ? _value.seriesTitle
          : seriesTitle // ignore: cast_nullable_to_non_nullable
              as String,
      libraryId: null == libraryId
          ? _value.libraryId
          : libraryId // ignore: cast_nullable_to_non_nullable
              as String,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      url: null == url
          ? _value.url
          : url // ignore: cast_nullable_to_non_nullable
              as String,
      deleted: null == deleted
          ? _value.deleted
          : deleted // ignore: cast_nullable_to_non_nullable
              as bool,
      number: null == number
          ? _value.number
          : number // ignore: cast_nullable_to_non_nullable
              as int,
      created: null == created
          ? _value.created
          : created // ignore: cast_nullable_to_non_nullable
              as DateTime,
      lastModified: null == lastModified
          ? _value.lastModified
          : lastModified // ignore: cast_nullable_to_non_nullable
              as DateTime,
      fileLastModified: null == fileLastModified
          ? _value.fileLastModified
          : fileLastModified // ignore: cast_nullable_to_non_nullable
              as DateTime,
      sizeBytes: null == sizeBytes
          ? _value.sizeBytes
          : sizeBytes // ignore: cast_nullable_to_non_nullable
              as int,
      size: null == size
          ? _value.size
          : size // ignore: cast_nullable_to_non_nullable
              as String,
      media: null == media
          ? _value.media
          : media // ignore: cast_nullable_to_non_nullable
              as Media,
      metadata: null == metadata
          ? _value.metadata
          : metadata // ignore: cast_nullable_to_non_nullable
              as BookMetadata,
      fileHash: null == fileHash
          ? _value.fileHash
          : fileHash // ignore: cast_nullable_to_non_nullable
              as String,
      oneshot: null == oneshot
          ? _value.oneshot
          : oneshot // ignore: cast_nullable_to_non_nullable
              as bool,
      readProgress: freezed == readProgress
          ? _value.readProgress
          : readProgress // ignore: cast_nullable_to_non_nullable
              as ReadProgress?,
    ));
  }
}

/// @nodoc

class _$BookImpl extends _Book {
  const _$BookImpl(
      {required this.id,
      required this.seriesId,
      required this.seriesTitle,
      required this.libraryId,
      required this.name,
      required this.url,
      required this.deleted,
      required this.number,
      required this.created,
      required this.lastModified,
      required this.fileLastModified,
      required this.sizeBytes,
      required this.size,
      required this.media,
      required this.metadata,
      required this.fileHash,
      required this.oneshot,
      this.readProgress})
      : super._();

  @override
  final String id;
  @override
  final String seriesId;
  @override
  final String seriesTitle;
  @override
  final String libraryId;
  @override
  final String name;
  @override
  final String url;
  @override
  final bool deleted;
  @override
  final int number;
  @override
  final DateTime created;
  @override
  final DateTime lastModified;
  @override
  final DateTime fileLastModified;
  @override
  final int sizeBytes;
  @override
  final String size;
  @override
  final Media media;
  @override
  final BookMetadata metadata;
  @override
  final String fileHash;
  @override
  final bool oneshot;
  @override
  final ReadProgress? readProgress;

  @override
  String toString() {
    return 'Book(id: $id, seriesId: $seriesId, seriesTitle: $seriesTitle, libraryId: $libraryId, name: $name, url: $url, deleted: $deleted, number: $number, created: $created, lastModified: $lastModified, fileLastModified: $fileLastModified, sizeBytes: $sizeBytes, size: $size, media: $media, metadata: $metadata, fileHash: $fileHash, oneshot: $oneshot, readProgress: $readProgress)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$BookImpl &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.seriesId, seriesId) ||
                other.seriesId == seriesId) &&
            (identical(other.seriesTitle, seriesTitle) ||
                other.seriesTitle == seriesTitle) &&
            (identical(other.libraryId, libraryId) ||
                other.libraryId == libraryId) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.url, url) || other.url == url) &&
            (identical(other.deleted, deleted) || other.deleted == deleted) &&
            (identical(other.number, number) || other.number == number) &&
            (identical(other.created, created) || other.created == created) &&
            (identical(other.lastModified, lastModified) ||
                other.lastModified == lastModified) &&
            (identical(other.fileLastModified, fileLastModified) ||
                other.fileLastModified == fileLastModified) &&
            (identical(other.sizeBytes, sizeBytes) ||
                other.sizeBytes == sizeBytes) &&
            (identical(other.size, size) || other.size == size) &&
            (identical(other.media, media) || other.media == media) &&
            (identical(other.metadata, metadata) ||
                other.metadata == metadata) &&
            (identical(other.fileHash, fileHash) ||
                other.fileHash == fileHash) &&
            (identical(other.oneshot, oneshot) || other.oneshot == oneshot) &&
            (identical(other.readProgress, readProgress) ||
                other.readProgress == readProgress));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      id,
      seriesId,
      seriesTitle,
      libraryId,
      name,
      url,
      deleted,
      number,
      created,
      lastModified,
      fileLastModified,
      sizeBytes,
      size,
      media,
      metadata,
      fileHash,
      oneshot,
      readProgress);

  /// Create a copy of Book
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$BookImplCopyWith<_$BookImpl> get copyWith =>
      __$$BookImplCopyWithImpl<_$BookImpl>(this, _$identity);
}

abstract class _Book extends Book {
  const factory _Book(
      {required final String id,
      required final String seriesId,
      required final String seriesTitle,
      required final String libraryId,
      required final String name,
      required final String url,
      required final bool deleted,
      required final int number,
      required final DateTime created,
      required final DateTime lastModified,
      required final DateTime fileLastModified,
      required final int sizeBytes,
      required final String size,
      required final Media media,
      required final BookMetadata metadata,
      required final String fileHash,
      required final bool oneshot,
      final ReadProgress? readProgress}) = _$BookImpl;
  const _Book._() : super._();

  @override
  String get id;
  @override
  String get seriesId;
  @override
  String get seriesTitle;
  @override
  String get libraryId;
  @override
  String get name;
  @override
  String get url;
  @override
  bool get deleted;
  @override
  int get number;
  @override
  DateTime get created;
  @override
  DateTime get lastModified;
  @override
  DateTime get fileLastModified;
  @override
  int get sizeBytes;
  @override
  String get size;
  @override
  Media get media;
  @override
  BookMetadata get metadata;
  @override
  String get fileHash;
  @override
  bool get oneshot;
  @override
  ReadProgress? get readProgress;

  /// Create a copy of Book
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$BookImplCopyWith<_$BookImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
