import 'package:freezed_annotation/freezed_annotation.dart';

part 'read_progress.freezed.dart';

/// Read Progress of a Book
@freezed
class ReadProgress with _$ReadProgress {
  /// Creates a [ReadProgress] instance
  const factory ReadProgress({
    required int page,
    required bool completed,
    required DateTime created,
    required DateTime lastModified,
  }) = _ReadProgress;
}
