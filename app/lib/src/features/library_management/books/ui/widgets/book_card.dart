import 'package:flutter/material.dart';
import 'package:kapoow/src/common_widgets/extensions.dart';
import 'package:kapoow/src/constants/app_paddings.dart';
import 'package:kapoow/src/features/library_management/books/domain/models/book.dart';
import 'package:kapoow/src/features/library_management/books/ui/widgets/read_book_button.dart';
import 'package:kapoow/src/features/library_management/common/ui/widgets/item_cover/cover_image.dart';
import 'package:kapoow/src/features/read_book/domain/models/read_context.dart';
import 'package:kapoow/src/routes/routes_data.dart';
import 'package:kapoow/src/theme/extensions.dart';

/// Horizontal card that displays the book information
class BookCard extends StatelessWidget {
  /// Creates a [BookCard] instance
  const BookCard({
    required this.book,
    this.readContext,
    this.readContextId,
    super.key,
  }) : assert(
          readContext != ReadContext.readlist || readContextId != null,
          'If a [ReadContext.readlist] is provided the [readContextId] '
          'should not be null',
        );

  /// Context on which the book will be open
  final ReadContext? readContext;

  /// Id of the read list of the [readContext]
  final String? readContextId;

  /// Book to read data from
  final Book book;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Stack(
        children: [
          Row(
            children: [
              CoverImage(coverUrl: book.thumbnailUri),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: AppPaddings.xSmall,
                    vertical: AppPaddings.xxSmall,
                  ),
                  child: Column(
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(
                            child: Text(
                              book.name,
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              style: context.textTheme.titleMedium,
                            ),
                          ),
                          Container(
                            decoration: BoxDecoration(
                              color: context.colorScheme.secondaryContainer,
                              borderRadius: const BorderRadius.all(
                                Radius.circular(6),
                              ),
                            ),
                            padding: const EdgeInsets.all(AppPaddings.xxSmall),
                            child: Text(
                              '#${book.number}',
                              maxLines: 1,
                              style: context.textTheme.titleMedium?.copyWith(
                                color: context.colorScheme.onSecondaryContainer,
                              ),
                            ),
                          ),
                        ],
                      ),
                      Expanded(
                        child: Text(
                          book.metadata.summary,
                          maxLines: 3,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                    ].separated(mainAxisExtent: AppPaddings.xxSmall),
                  ),
                ),
              ),
            ],
          ),
          // hacky way to get the InkWell to work with images and
          // border radius by using a stack
          Positioned.fill(
            child: Material(
              color: Colors.transparent,
              child: InkWell(
                onTap: () => BookRoute(id: book.id).go(context),
              ),
            ),
          ),
          Positioned(
            right: AppPaddings.xSmall,
            bottom: AppPaddings.xxSmall,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                // TODO(rurickdev): restore this action when download
                //  feature works
                // OutlinedButton(
                //   onPressed: () {
                //     // `TODO`(rurickdev): implement
                //   },
                //   child: PhosphorIcon(
                //     PhosphorIcons.duotone.cloudArrowDown,
                //   ),
                //   // label: const Text('Download'),
                // ),
                ReadBookButton(book: book),
              ].separated(),
            ),
          ),
        ],
      ),
    );
  }
}
