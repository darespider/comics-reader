import 'package:flutter/material.dart';
import 'package:kapoow/src/features/library_management/books/domain/models/book.dart';
import 'package:kapoow/src/features/library_management/common/models/read_status.dart';
import 'package:kapoow/src/features/library_management/common/ui/widgets/item_cover/cover_decoration.dart';
import 'package:kapoow/src/features/library_management/common/ui/widgets/item_cover/cover_image.dart';
import 'package:kapoow/src/features/read_book/domain/models/read_context.dart';
import 'package:kapoow/src/routes/routes_data.dart';

/// Displays the cover image of the passed [book]
class BookCover extends StatelessWidget {
  /// Creates a [BookCover] instance
  const BookCover({
    required this.book,
    this.showProgress = false,
    this.hideDecoration = false,
    this.readContext,
    this.readContextId,
    super.key,
  }) : assert(
          readContext != ReadContext.readlist || readContextId != null,
          'If a [ReadContext.readlist] is provided the [readContextId] '
          'should not be null',
        );

  /// Context on which the book will be open
  final ReadContext? readContext;

  /// Id of the read list of the [readContext]
  final String? readContextId;

  /// [Book] to read data from
  final Book book;

  /// if `true` will display a progress bar at the bottom of the cover and
  /// the current page of the total pages
  final bool showProgress;

  /// Hides all decoration
  final bool hideDecoration;

  @override
  Widget build(BuildContext context) {
    final currentPage = book.readProgress?.page ?? 0;
    // TODO(rurickdev): Rework to allow patterns from settings and refactor to
    //  make it more readable
    final isAnnual = book.name.toLowerCase().contains('annual');

    final CoverDecorationData decoration;

    if (hideDecoration) {
      decoration = CoverDecorationData(
        progress: showProgress ? currentPage / book.media.pagesCount : null,
      );
    } else {
      decoration = CoverDecorationData(
        finished: book.readStatus == ReadStatus.read,
        issueNumber: isAnnual ? null : book.metadata.number,
        annual: isAnnual
            ? book.metadata.releaseDate?.year.toString() ?? '???'
            : null,
        itemsCount: book.media.pagesCount,
        finishedItemsCount: showProgress ? currentPage : null,
        progress: showProgress ? currentPage / book.media.pagesCount : null,
      );
    }

    return CoverImage(
      onLongPress: () => BookRoute(id: book.id).go(context),
      onTap: () => ReadBookRoute(
        id: book.id,
        page: book.readProgress?.page ?? 1,
        context: readContext,
        contextId: readContextId,
      ).go(context),
      coverUrl: book.thumbnailUri,
      decoration: decoration,
    );
  }
}
