import 'package:flutter/material.dart';
import 'package:kapoow/src/constants/app_paddings.dart';
import 'package:kapoow/src/theme/extensions.dart';

/// Shows the [number] prefix with # as a badge
class BookNumberBadge extends StatelessWidget {
  /// Create a [BookNumberBadge] instance
  const BookNumberBadge({required this.number, super.key});

  /// Number to display inside the badge
  final int number;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: context.colorScheme.secondaryContainer,
        borderRadius: const BorderRadius.all(
          Radius.circular(6),
        ),
      ),
      padding: const EdgeInsets.all(AppPaddings.xxSmall),
      child: Text(
        '#$number',
        maxLines: 1,
        style: context.textTheme.titleMedium?.copyWith(
          color: context.colorScheme.onSecondaryContainer,
        ),
      ),
    );
  }
}
