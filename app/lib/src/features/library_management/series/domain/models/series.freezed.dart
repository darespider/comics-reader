// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'series.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$Series {
  String get id => throw _privateConstructorUsedError;
  String get libraryId => throw _privateConstructorUsedError;
  String get name => throw _privateConstructorUsedError;
  String get url => throw _privateConstructorUsedError;
  DateTime get created => throw _privateConstructorUsedError;
  DateTime get lastModified => throw _privateConstructorUsedError;
  DateTime get fileLastModified => throw _privateConstructorUsedError;
  int get booksCount => throw _privateConstructorUsedError;
  int get booksReadCount => throw _privateConstructorUsedError;
  int get booksUnreadCount => throw _privateConstructorUsedError;
  int get booksInProgressCount => throw _privateConstructorUsedError;
  SeriesMetadata get metadata => throw _privateConstructorUsedError;
  BookMetadataAggregation get booksMetadata =>
      throw _privateConstructorUsedError;
  bool get deleted => throw _privateConstructorUsedError;
  bool get oneshot => throw _privateConstructorUsedError;

  /// Create a copy of Series
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $SeriesCopyWith<Series> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SeriesCopyWith<$Res> {
  factory $SeriesCopyWith(Series value, $Res Function(Series) then) =
      _$SeriesCopyWithImpl<$Res, Series>;
  @useResult
  $Res call(
      {String id,
      String libraryId,
      String name,
      String url,
      DateTime created,
      DateTime lastModified,
      DateTime fileLastModified,
      int booksCount,
      int booksReadCount,
      int booksUnreadCount,
      int booksInProgressCount,
      SeriesMetadata metadata,
      BookMetadataAggregation booksMetadata,
      bool deleted,
      bool oneshot});

  $SeriesMetadataCopyWith<$Res> get metadata;
  $BookMetadataAggregationCopyWith<$Res> get booksMetadata;
}

/// @nodoc
class _$SeriesCopyWithImpl<$Res, $Val extends Series>
    implements $SeriesCopyWith<$Res> {
  _$SeriesCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of Series
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? libraryId = null,
    Object? name = null,
    Object? url = null,
    Object? created = null,
    Object? lastModified = null,
    Object? fileLastModified = null,
    Object? booksCount = null,
    Object? booksReadCount = null,
    Object? booksUnreadCount = null,
    Object? booksInProgressCount = null,
    Object? metadata = null,
    Object? booksMetadata = null,
    Object? deleted = null,
    Object? oneshot = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      libraryId: null == libraryId
          ? _value.libraryId
          : libraryId // ignore: cast_nullable_to_non_nullable
              as String,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      url: null == url
          ? _value.url
          : url // ignore: cast_nullable_to_non_nullable
              as String,
      created: null == created
          ? _value.created
          : created // ignore: cast_nullable_to_non_nullable
              as DateTime,
      lastModified: null == lastModified
          ? _value.lastModified
          : lastModified // ignore: cast_nullable_to_non_nullable
              as DateTime,
      fileLastModified: null == fileLastModified
          ? _value.fileLastModified
          : fileLastModified // ignore: cast_nullable_to_non_nullable
              as DateTime,
      booksCount: null == booksCount
          ? _value.booksCount
          : booksCount // ignore: cast_nullable_to_non_nullable
              as int,
      booksReadCount: null == booksReadCount
          ? _value.booksReadCount
          : booksReadCount // ignore: cast_nullable_to_non_nullable
              as int,
      booksUnreadCount: null == booksUnreadCount
          ? _value.booksUnreadCount
          : booksUnreadCount // ignore: cast_nullable_to_non_nullable
              as int,
      booksInProgressCount: null == booksInProgressCount
          ? _value.booksInProgressCount
          : booksInProgressCount // ignore: cast_nullable_to_non_nullable
              as int,
      metadata: null == metadata
          ? _value.metadata
          : metadata // ignore: cast_nullable_to_non_nullable
              as SeriesMetadata,
      booksMetadata: null == booksMetadata
          ? _value.booksMetadata
          : booksMetadata // ignore: cast_nullable_to_non_nullable
              as BookMetadataAggregation,
      deleted: null == deleted
          ? _value.deleted
          : deleted // ignore: cast_nullable_to_non_nullable
              as bool,
      oneshot: null == oneshot
          ? _value.oneshot
          : oneshot // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }

  /// Create a copy of Series
  /// with the given fields replaced by the non-null parameter values.
  @override
  @pragma('vm:prefer-inline')
  $SeriesMetadataCopyWith<$Res> get metadata {
    return $SeriesMetadataCopyWith<$Res>(_value.metadata, (value) {
      return _then(_value.copyWith(metadata: value) as $Val);
    });
  }

  /// Create a copy of Series
  /// with the given fields replaced by the non-null parameter values.
  @override
  @pragma('vm:prefer-inline')
  $BookMetadataAggregationCopyWith<$Res> get booksMetadata {
    return $BookMetadataAggregationCopyWith<$Res>(_value.booksMetadata,
        (value) {
      return _then(_value.copyWith(booksMetadata: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$SeriesImplCopyWith<$Res> implements $SeriesCopyWith<$Res> {
  factory _$$SeriesImplCopyWith(
          _$SeriesImpl value, $Res Function(_$SeriesImpl) then) =
      __$$SeriesImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String id,
      String libraryId,
      String name,
      String url,
      DateTime created,
      DateTime lastModified,
      DateTime fileLastModified,
      int booksCount,
      int booksReadCount,
      int booksUnreadCount,
      int booksInProgressCount,
      SeriesMetadata metadata,
      BookMetadataAggregation booksMetadata,
      bool deleted,
      bool oneshot});

  @override
  $SeriesMetadataCopyWith<$Res> get metadata;
  @override
  $BookMetadataAggregationCopyWith<$Res> get booksMetadata;
}

/// @nodoc
class __$$SeriesImplCopyWithImpl<$Res>
    extends _$SeriesCopyWithImpl<$Res, _$SeriesImpl>
    implements _$$SeriesImplCopyWith<$Res> {
  __$$SeriesImplCopyWithImpl(
      _$SeriesImpl _value, $Res Function(_$SeriesImpl) _then)
      : super(_value, _then);

  /// Create a copy of Series
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? libraryId = null,
    Object? name = null,
    Object? url = null,
    Object? created = null,
    Object? lastModified = null,
    Object? fileLastModified = null,
    Object? booksCount = null,
    Object? booksReadCount = null,
    Object? booksUnreadCount = null,
    Object? booksInProgressCount = null,
    Object? metadata = null,
    Object? booksMetadata = null,
    Object? deleted = null,
    Object? oneshot = null,
  }) {
    return _then(_$SeriesImpl(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      libraryId: null == libraryId
          ? _value.libraryId
          : libraryId // ignore: cast_nullable_to_non_nullable
              as String,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      url: null == url
          ? _value.url
          : url // ignore: cast_nullable_to_non_nullable
              as String,
      created: null == created
          ? _value.created
          : created // ignore: cast_nullable_to_non_nullable
              as DateTime,
      lastModified: null == lastModified
          ? _value.lastModified
          : lastModified // ignore: cast_nullable_to_non_nullable
              as DateTime,
      fileLastModified: null == fileLastModified
          ? _value.fileLastModified
          : fileLastModified // ignore: cast_nullable_to_non_nullable
              as DateTime,
      booksCount: null == booksCount
          ? _value.booksCount
          : booksCount // ignore: cast_nullable_to_non_nullable
              as int,
      booksReadCount: null == booksReadCount
          ? _value.booksReadCount
          : booksReadCount // ignore: cast_nullable_to_non_nullable
              as int,
      booksUnreadCount: null == booksUnreadCount
          ? _value.booksUnreadCount
          : booksUnreadCount // ignore: cast_nullable_to_non_nullable
              as int,
      booksInProgressCount: null == booksInProgressCount
          ? _value.booksInProgressCount
          : booksInProgressCount // ignore: cast_nullable_to_non_nullable
              as int,
      metadata: null == metadata
          ? _value.metadata
          : metadata // ignore: cast_nullable_to_non_nullable
              as SeriesMetadata,
      booksMetadata: null == booksMetadata
          ? _value.booksMetadata
          : booksMetadata // ignore: cast_nullable_to_non_nullable
              as BookMetadataAggregation,
      deleted: null == deleted
          ? _value.deleted
          : deleted // ignore: cast_nullable_to_non_nullable
              as bool,
      oneshot: null == oneshot
          ? _value.oneshot
          : oneshot // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$SeriesImpl extends _Series {
  const _$SeriesImpl(
      {required this.id,
      required this.libraryId,
      required this.name,
      required this.url,
      required this.created,
      required this.lastModified,
      required this.fileLastModified,
      required this.booksCount,
      required this.booksReadCount,
      required this.booksUnreadCount,
      required this.booksInProgressCount,
      required this.metadata,
      required this.booksMetadata,
      required this.deleted,
      required this.oneshot})
      : super._();

  @override
  final String id;
  @override
  final String libraryId;
  @override
  final String name;
  @override
  final String url;
  @override
  final DateTime created;
  @override
  final DateTime lastModified;
  @override
  final DateTime fileLastModified;
  @override
  final int booksCount;
  @override
  final int booksReadCount;
  @override
  final int booksUnreadCount;
  @override
  final int booksInProgressCount;
  @override
  final SeriesMetadata metadata;
  @override
  final BookMetadataAggregation booksMetadata;
  @override
  final bool deleted;
  @override
  final bool oneshot;

  @override
  String toString() {
    return 'Series(id: $id, libraryId: $libraryId, name: $name, url: $url, created: $created, lastModified: $lastModified, fileLastModified: $fileLastModified, booksCount: $booksCount, booksReadCount: $booksReadCount, booksUnreadCount: $booksUnreadCount, booksInProgressCount: $booksInProgressCount, metadata: $metadata, booksMetadata: $booksMetadata, deleted: $deleted, oneshot: $oneshot)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SeriesImpl &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.libraryId, libraryId) ||
                other.libraryId == libraryId) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.url, url) || other.url == url) &&
            (identical(other.created, created) || other.created == created) &&
            (identical(other.lastModified, lastModified) ||
                other.lastModified == lastModified) &&
            (identical(other.fileLastModified, fileLastModified) ||
                other.fileLastModified == fileLastModified) &&
            (identical(other.booksCount, booksCount) ||
                other.booksCount == booksCount) &&
            (identical(other.booksReadCount, booksReadCount) ||
                other.booksReadCount == booksReadCount) &&
            (identical(other.booksUnreadCount, booksUnreadCount) ||
                other.booksUnreadCount == booksUnreadCount) &&
            (identical(other.booksInProgressCount, booksInProgressCount) ||
                other.booksInProgressCount == booksInProgressCount) &&
            (identical(other.metadata, metadata) ||
                other.metadata == metadata) &&
            (identical(other.booksMetadata, booksMetadata) ||
                other.booksMetadata == booksMetadata) &&
            (identical(other.deleted, deleted) || other.deleted == deleted) &&
            (identical(other.oneshot, oneshot) || other.oneshot == oneshot));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      id,
      libraryId,
      name,
      url,
      created,
      lastModified,
      fileLastModified,
      booksCount,
      booksReadCount,
      booksUnreadCount,
      booksInProgressCount,
      metadata,
      booksMetadata,
      deleted,
      oneshot);

  /// Create a copy of Series
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$SeriesImplCopyWith<_$SeriesImpl> get copyWith =>
      __$$SeriesImplCopyWithImpl<_$SeriesImpl>(this, _$identity);
}

abstract class _Series extends Series {
  const factory _Series(
      {required final String id,
      required final String libraryId,
      required final String name,
      required final String url,
      required final DateTime created,
      required final DateTime lastModified,
      required final DateTime fileLastModified,
      required final int booksCount,
      required final int booksReadCount,
      required final int booksUnreadCount,
      required final int booksInProgressCount,
      required final SeriesMetadata metadata,
      required final BookMetadataAggregation booksMetadata,
      required final bool deleted,
      required final bool oneshot}) = _$SeriesImpl;
  const _Series._() : super._();

  @override
  String get id;
  @override
  String get libraryId;
  @override
  String get name;
  @override
  String get url;
  @override
  DateTime get created;
  @override
  DateTime get lastModified;
  @override
  DateTime get fileLastModified;
  @override
  int get booksCount;
  @override
  int get booksReadCount;
  @override
  int get booksUnreadCount;
  @override
  int get booksInProgressCount;
  @override
  SeriesMetadata get metadata;
  @override
  BookMetadataAggregation get booksMetadata;
  @override
  bool get deleted;
  @override
  bool get oneshot;

  /// Create a copy of Series
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$SeriesImplCopyWith<_$SeriesImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
