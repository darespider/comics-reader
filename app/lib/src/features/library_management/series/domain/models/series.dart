import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kapoow/src/features/library_management/common/models/thumbnailed.dart';
import 'package:kapoow/src/features/library_management/series/domain/models/book_metadata_aggregation.dart';
import 'package:kapoow/src/features/library_management/series/domain/models/series_metadata.dart';

part 'series.freezed.dart';

/// Representation of an series of books
@freezed
class Series with _$Series implements Thumbnailed {
  /// Creates a [Series] instance
  const factory Series({
    required String id,
    required String libraryId,
    required String name,
    required String url,
    required DateTime created,
    required DateTime lastModified,
    required DateTime fileLastModified,
    required int booksCount,
    required int booksReadCount,
    required int booksUnreadCount,
    required int booksInProgressCount,
    required SeriesMetadata metadata,
    required BookMetadataAggregation booksMetadata,
    required bool deleted,
    required bool oneshot,
  }) = _Series;

  const Series._();

  @override
  String get thumbnailUri => 'series/$id/thumbnail';
}
