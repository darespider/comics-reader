import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kapoow/src/features/library_management/common/models/series_status.dart';

part 'series_metadata.freezed.dart';

/// Meta info from a series
@freezed
class SeriesMetadata with _$SeriesMetadata {
  /// Creates a [SeriesMetadata] instance
  const factory SeriesMetadata({
    required SeriesStatus status,
    required bool statusLock,
    required String title,
    required bool titleLock,
    required String titleSort,
    required bool titleSortLock,
    required String summary,
    required bool summaryLock,
    required String readingDirection,
    required bool readingDirectionLock,
    required String publisher,
    required bool publisherLock,
    required bool ageRatingLock,
    required String language,
    required bool languageLock,
    required Set<String> genres,
    required bool genresLock,
    required Set<String> tags,
    required bool tagsLock,
    required DateTime created,
    required DateTime lastModified,
    required bool totalBookCountLock,
    required Set<String> sharingLabels,
    required bool sharingLabelsLock,
    required Set<String> links,
    required bool linksLock,
    required Set<String> alternateTitles,
    required bool alternateTitlesLock,
    int? totalBookCount,
    int? ageRating,
  }) = _SeriesMetadata;
}
