import 'package:kapoow/src/features/library_management/common/models/media_status.dart';
import 'package:kapoow/src/features/library_management/common/models/read_status.dart';
import 'package:kapoow/src/features/library_management/common/models/series_status.dart';
import 'package:kapoow/src/features/library_management/common/models/typedefs.dart';
import 'package:kapoow/src/features/library_management/series/data/series_repository.dart';
import 'package:kapoow/src/features/library_management/series/domain/models/series.dart';
import 'package:kapoow/src/features/library_management/series/domain/repositories/dto_domain_parser.dart';
import 'package:kapoow/src/features/pagination/domain/repositories/dto_domain_parser.dart';
import 'package:kapoow/src/features/rest_api/data/datasources/series/series_datasource.dart';

/// Series repository that gets the series from an api client
class SeriesRepositoryImp implements SeriesRepository {
  /// Creates a [SeriesRepository] and requires a [SeriesDatasource]
  SeriesRepositoryImp(this._datasource);

  final SeriesDatasource _datasource;

  @override
  Future<PageBook> getInProgressBooks({
    required String seriesId,
    int? page,
    int? pageSize,
  }) async {
    final dto = await _datasource.getSeriesBooks(
      seriesId: seriesId,
      deleted: false,
      mediaStatus: MediaStatus.ready,
      readStatus: ReadStatus.inProgress,
      page: page,
      pageSize: pageSize,
    );
    return dto.toDomain();
  }

  @override
  Future<PageSeries> getLatest({
    String? libraryId,
    bool? oneshot,
    bool? deleted,
    bool? unpaged,
    int? page,
    int? pageSize,
  }) async {
    final dto = await _datasource.getLatestSeries(
      libraryId: libraryId,
      deleted: deleted,
      unpaged: unpaged,
      page: page,
      pageSize: pageSize,
    );
    return dto.toDomain();
  }

  @override
  Future<PageSeries> getNew({
    String? libraryId,
    bool? oneshot,
    bool? deleted,
    bool? unpaged,
    int? page,
    int? pageSize,
  }) async {
    final dto = await _datasource.getNewSeries(
      libraryId: libraryId,
      deleted: deleted,
      unpaged: unpaged,
      page: page,
      pageSize: pageSize,
    );
    return dto.toDomain();
  }

  @override
  Future<PageBook> getReadBooks({
    required String seriesId,
    int? page,
    int? pageSize,
  }) async {
    final dto = await _datasource.getSeriesBooks(
      seriesId: seriesId,
      deleted: false,
      mediaStatus: MediaStatus.ready,
      readStatus: ReadStatus.read,
      page: page,
      pageSize: pageSize,
    );
    return dto.toDomain();
  }

  @override
  Future<PageSeries> getSeries({
    String? search,
    List<String>? libraryIds,
    List<String>? collectionsIds,
    List<SeriesStatus>? seriesStatuses,
    List<ReadStatus>? readStatus,
    List<String>? publishers,
    List<String>? languages,
    List<String>? genres,
    List<String>? tags,
    List<String>? ageRating,
    List<DateTime>? releaseYears,
    bool? deleted,
    bool? completed,
    bool? unpaged,
    String? searchRegex,
    int? page,
    int? size,
    List<String>? sort,
    List<String>? authors,
  }) async {
    final dto = await _datasource.getSeries(
      search: search,
      libraryIds: libraryIds,
      collectionsIds: collectionsIds,
      seriesStatuses: seriesStatuses,
      readStatus: readStatus,
      publishers: publishers,
      languages: languages,
      genres: genres,
      tags: tags,
      ageRating: ageRating,
      releaseYears: releaseYears,
      deleted: deleted,
      completed: completed,
      unpaged: unpaged,
      searchRegex: searchRegex,
      page: page,
      size: size,
      sort: sort,
      authors: authors,
    );
    return dto.toDomain();
  }

  @override
  Future<Series> getSeriesById({required String seriesId}) async {
    final dto = await _datasource.getSeriesById(seriesId);
    return dto.toDomain();
  }

  @override
  Future<PageBook> getUnreadBooks({
    required String seriesId,
    int? page,
    int? pageSize,
  }) async {
    final dto = await _datasource.getSeriesBooks(
      seriesId: seriesId,
      deleted: false,
      mediaStatus: MediaStatus.ready,
      readStatus: ReadStatus.unread,
      page: page,
      pageSize: pageSize,
    );
    return dto.toDomain();
  }

  @override
  Future<PageSeries> getUpdated({
    String? libraryId,
    bool? oneshot,
    bool? deleted,
    bool? unpaged,
    int? page,
    int? pageSize,
  }) async {
    final dto = await _datasource.getLatestSeries(
      libraryId: libraryId,
      deleted: deleted,
      unpaged: unpaged,
      page: page,
      pageSize: pageSize,
    );
    return dto.toDomain();
  }

  @override
  Future<PageSeries> searchSeries(String query, {int page = 0}) async {
    final dto = await _datasource.getSeries(
      search: query,
      page: page,
    );
    return dto.toDomain();
  }

  @override
  Future<PageBook> getBooks({
    required String seriesId,
    MediaStatus? mediaStatus,
    ReadStatus? readStatus,
    List<String>? tags,
    bool? deleted,
    bool? unpaged,
    int? page,
    int? pageSize,
    List<String>? sort,
    List<String>? authors,
  }) async {
    final dto = await _datasource.getSeriesBooks(
      seriesId: seriesId,
      page: page,
      pageSize: pageSize,
      authors: authors,
      deleted: deleted,
      mediaStatus: mediaStatus,
      readStatus: readStatus,
      sort: sort,
      tags: tags,
      unpaged: unpaged,
    );
    return dto.toDomain();
  }
}
