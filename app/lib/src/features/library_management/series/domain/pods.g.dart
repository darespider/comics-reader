// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pods.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$seriesRepoHash() => r'e50d46fb625d30fc7904fcc561412bfb90f6a57a';

/// Provides the [SeriesRepository]
///
/// Copied from [seriesRepo].
@ProviderFor(seriesRepo)
final seriesRepoPod = Provider<SeriesRepository>.internal(
  seriesRepo,
  name: r'seriesRepoPod',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$seriesRepoHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef SeriesRepoRef = ProviderRef<SeriesRepository>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
