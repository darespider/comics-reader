import 'package:kapoow/src/features/library_management/common/models/media_status.dart';
import 'package:kapoow/src/features/library_management/common/models/read_status.dart';
import 'package:kapoow/src/features/library_management/common/models/series_status.dart';
import 'package:kapoow/src/features/library_management/common/models/typedefs.dart';
import 'package:kapoow/src/features/library_management/series/domain/models/series.dart';

/// Interface describing how the series repositories should work
abstract class SeriesRepository {
  /// Returns a list of series fetch from the api, accepts many
  /// search filters
  Future<PageSeries> getSeries({
    String? search,
    List<String>? libraryIds,
    List<String>? collectionsIds,
    List<SeriesStatus>? seriesStatuses,
    List<ReadStatus>? readStatus,
    List<String>? publishers,
    List<String>? languages,
    List<String>? genres,
    List<String>? tags,
    List<String>? ageRating,
    List<DateTime>? releaseYears,
    bool? deleted,
    bool? completed,
    bool? unpaged,
    String? searchRegex,
    int? page,
    int? size,
    List<String>? sort,
    List<String>? authors,
  });

  /// Return recently added or updated series.
  Future<PageSeries> getLatest({
    String? libraryId,
    bool? oneshot,
    bool? deleted,
    bool? unpaged,
    int? page,
    int? pageSize,
  });

  /// Return newly added series.
  Future<PageSeries> getNew({
    String? libraryId,
    bool? oneshot,
    bool? deleted,
    bool? unpaged,
    int? page,
    int? pageSize,
  });

  /// Return recently updated series, but not newly added ones.
  Future<PageSeries> getUpdated({
    String? libraryId,
    bool? oneshot,
    bool? deleted,
    bool? unpaged,
    int? page,
    int? pageSize,
  });

  /// Return in progress series books.
  Future<PageBook> getBooks({
    required String seriesId,
    MediaStatus? mediaStatus,
    ReadStatus? readStatus,
    List<String>? tags,
    bool? deleted,
    bool? unpaged,
    int? page,
    int? pageSize,
    List<String>? sort,
    List<String>? authors,
  });

  /// Return unread series books.
  Future<PageBook> getUnreadBooks({
    required String seriesId,
    int? page,
    int? pageSize,
  });

  /// Return in progress series books.
  Future<PageBook> getInProgressBooks({
    required String seriesId,
    int? page,
    int? pageSize,
  });

  /// Return unread series books.
  Future<PageBook> getReadBooks({
    required String seriesId,
    int? page,
    int? pageSize,
  });

  /// Return a [Series] by the series id
  Future<Series> getSeriesById({
    required String seriesId,
  });

  /// Search series based on the [query]
  Future<PageSeries> searchSeries(String query, {int page = 0});
}
