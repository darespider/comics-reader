// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pods.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$seriesHash() => r'3d295d239b2770db4025818ab6ca556c253666ff';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// Provides the series data from an api call
///
/// Copied from [series].
@ProviderFor(series)
const seriesPod = SeriesFamily();

/// Provides the series data from an api call
///
/// Copied from [series].
class SeriesFamily extends Family<AsyncValue<Series>> {
  /// Provides the series data from an api call
  ///
  /// Copied from [series].
  const SeriesFamily();

  /// Provides the series data from an api call
  ///
  /// Copied from [series].
  SeriesProvider call(
    String id,
  ) {
    return SeriesProvider(
      id,
    );
  }

  @override
  SeriesProvider getProviderOverride(
    covariant SeriesProvider provider,
  ) {
    return call(
      provider.id,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'seriesPod';
}

/// Provides the series data from an api call
///
/// Copied from [series].
class SeriesProvider extends FutureProvider<Series> {
  /// Provides the series data from an api call
  ///
  /// Copied from [series].
  SeriesProvider(
    String id,
  ) : this._internal(
          (ref) => series(
            ref as SeriesRef,
            id,
          ),
          from: seriesPod,
          name: r'seriesPod',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$seriesHash,
          dependencies: SeriesFamily._dependencies,
          allTransitiveDependencies: SeriesFamily._allTransitiveDependencies,
          id: id,
        );

  SeriesProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.id,
  }) : super.internal();

  final String id;

  @override
  Override overrideWith(
    FutureOr<Series> Function(SeriesRef provider) create,
  ) {
    return ProviderOverride(
      origin: this,
      override: SeriesProvider._internal(
        (ref) => create(ref as SeriesRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        id: id,
      ),
    );
  }

  @override
  FutureProviderElement<Series> createElement() {
    return _SeriesProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is SeriesProvider && other.id == id;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, id.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin SeriesRef on FutureProviderRef<Series> {
  /// The parameter `id` of this provider.
  String get id;
}

class _SeriesProviderElement extends FutureProviderElement<Series>
    with SeriesRef {
  _SeriesProviderElement(super.provider);

  @override
  String get id => (origin as SeriesProvider).id;
}

String _$seriesBooksFirstPageHash() =>
    r'f154328f444316482cbff57f3200a2fc491ba4f1';

/// The first page of books of a series
///
/// Copied from [seriesBooksFirstPage].
@ProviderFor(seriesBooksFirstPage)
const seriesBooksFirstPagePod = SeriesBooksFirstPageFamily();

/// The first page of books of a series
///
/// Copied from [seriesBooksFirstPage].
class SeriesBooksFirstPageFamily extends Family<AsyncValue<Pagination<Book>>> {
  /// The first page of books of a series
  ///
  /// Copied from [seriesBooksFirstPage].
  const SeriesBooksFirstPageFamily();

  /// The first page of books of a series
  ///
  /// Copied from [seriesBooksFirstPage].
  SeriesBooksFirstPageProvider call(
    String seriesId, {
    ReadStatus? readStatus,
  }) {
    return SeriesBooksFirstPageProvider(
      seriesId,
      readStatus: readStatus,
    );
  }

  @override
  SeriesBooksFirstPageProvider getProviderOverride(
    covariant SeriesBooksFirstPageProvider provider,
  ) {
    return call(
      provider.seriesId,
      readStatus: provider.readStatus,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'seriesBooksFirstPagePod';
}

/// The first page of books of a series
///
/// Copied from [seriesBooksFirstPage].
class SeriesBooksFirstPageProvider extends FutureProvider<Pagination<Book>> {
  /// The first page of books of a series
  ///
  /// Copied from [seriesBooksFirstPage].
  SeriesBooksFirstPageProvider(
    String seriesId, {
    ReadStatus? readStatus,
  }) : this._internal(
          (ref) => seriesBooksFirstPage(
            ref as SeriesBooksFirstPageRef,
            seriesId,
            readStatus: readStatus,
          ),
          from: seriesBooksFirstPagePod,
          name: r'seriesBooksFirstPagePod',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$seriesBooksFirstPageHash,
          dependencies: SeriesBooksFirstPageFamily._dependencies,
          allTransitiveDependencies:
              SeriesBooksFirstPageFamily._allTransitiveDependencies,
          seriesId: seriesId,
          readStatus: readStatus,
        );

  SeriesBooksFirstPageProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.seriesId,
    required this.readStatus,
  }) : super.internal();

  final String seriesId;
  final ReadStatus? readStatus;

  @override
  Override overrideWith(
    FutureOr<Pagination<Book>> Function(SeriesBooksFirstPageRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: SeriesBooksFirstPageProvider._internal(
        (ref) => create(ref as SeriesBooksFirstPageRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        seriesId: seriesId,
        readStatus: readStatus,
      ),
    );
  }

  @override
  FutureProviderElement<Pagination<Book>> createElement() {
    return _SeriesBooksFirstPageProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is SeriesBooksFirstPageProvider &&
        other.seriesId == seriesId &&
        other.readStatus == readStatus;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, seriesId.hashCode);
    hash = _SystemHash.combine(hash, readStatus.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin SeriesBooksFirstPageRef on FutureProviderRef<Pagination<Book>> {
  /// The parameter `seriesId` of this provider.
  String get seriesId;

  /// The parameter `readStatus` of this provider.
  ReadStatus? get readStatus;
}

class _SeriesBooksFirstPageProviderElement
    extends FutureProviderElement<Pagination<Book>>
    with SeriesBooksFirstPageRef {
  _SeriesBooksFirstPageProviderElement(super.provider);

  @override
  String get seriesId => (origin as SeriesBooksFirstPageProvider).seriesId;
  @override
  ReadStatus? get readStatus =>
      (origin as SeriesBooksFirstPageProvider).readStatus;
}

String _$firstBooksBySeriesHash() =>
    r'c264f2a079f3d2a03d6c3ebeffa9d90e09d6e187';

/// The first 10 books of a series
///
/// Copied from [firstBooksBySeries].
@ProviderFor(firstBooksBySeries)
const firstBooksBySeriesPod = FirstBooksBySeriesFamily();

/// The first 10 books of a series
///
/// Copied from [firstBooksBySeries].
class FirstBooksBySeriesFamily extends Family<AsyncValue<Pagination<Book>>> {
  /// The first 10 books of a series
  ///
  /// Copied from [firstBooksBySeries].
  const FirstBooksBySeriesFamily();

  /// The first 10 books of a series
  ///
  /// Copied from [firstBooksBySeries].
  FirstBooksBySeriesProvider call(
    String seriesId,
  ) {
    return FirstBooksBySeriesProvider(
      seriesId,
    );
  }

  @override
  FirstBooksBySeriesProvider getProviderOverride(
    covariant FirstBooksBySeriesProvider provider,
  ) {
    return call(
      provider.seriesId,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'firstBooksBySeriesPod';
}

/// The first 10 books of a series
///
/// Copied from [firstBooksBySeries].
class FirstBooksBySeriesProvider extends FutureProvider<Pagination<Book>> {
  /// The first 10 books of a series
  ///
  /// Copied from [firstBooksBySeries].
  FirstBooksBySeriesProvider(
    String seriesId,
  ) : this._internal(
          (ref) => firstBooksBySeries(
            ref as FirstBooksBySeriesRef,
            seriesId,
          ),
          from: firstBooksBySeriesPod,
          name: r'firstBooksBySeriesPod',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$firstBooksBySeriesHash,
          dependencies: FirstBooksBySeriesFamily._dependencies,
          allTransitiveDependencies:
              FirstBooksBySeriesFamily._allTransitiveDependencies,
          seriesId: seriesId,
        );

  FirstBooksBySeriesProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.seriesId,
  }) : super.internal();

  final String seriesId;

  @override
  Override overrideWith(
    FutureOr<Pagination<Book>> Function(FirstBooksBySeriesRef provider) create,
  ) {
    return ProviderOverride(
      origin: this,
      override: FirstBooksBySeriesProvider._internal(
        (ref) => create(ref as FirstBooksBySeriesRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        seriesId: seriesId,
      ),
    );
  }

  @override
  FutureProviderElement<Pagination<Book>> createElement() {
    return _FirstBooksBySeriesProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is FirstBooksBySeriesProvider && other.seriesId == seriesId;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, seriesId.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin FirstBooksBySeriesRef on FutureProviderRef<Pagination<Book>> {
  /// The parameter `seriesId` of this provider.
  String get seriesId;
}

class _FirstBooksBySeriesProviderElement
    extends FutureProviderElement<Pagination<Book>> with FirstBooksBySeriesRef {
  _FirstBooksBySeriesProviderElement(super.provider);

  @override
  String get seriesId => (origin as FirstBooksBySeriesProvider).seriesId;
}

String _$seriesBooksPaginatedHash() =>
    r'2103101d4c5035d2d2f3948c8fe41b1dd988221d';

abstract class _$SeriesBooksPaginated
    extends BuildlessNotifier<PaginatedState<Book>> {
  late final (String, ReadStatus?) params;

  PaginatedState<Book> build([
    (String, ReadStatus?) params = ('', null),
  ]);
}

/// Notifier dedicated to handle the pagination state for Series books.
///
/// Copied from [SeriesBooksPaginated].
@ProviderFor(SeriesBooksPaginated)
const seriesBooksPaginatedPod = SeriesBooksPaginatedFamily();

/// Notifier dedicated to handle the pagination state for Series books.
///
/// Copied from [SeriesBooksPaginated].
class SeriesBooksPaginatedFamily extends Family<PaginatedState<Book>> {
  /// Notifier dedicated to handle the pagination state for Series books.
  ///
  /// Copied from [SeriesBooksPaginated].
  const SeriesBooksPaginatedFamily();

  /// Notifier dedicated to handle the pagination state for Series books.
  ///
  /// Copied from [SeriesBooksPaginated].
  SeriesBooksPaginatedProvider call([
    (String, ReadStatus?) params = ('', null),
  ]) {
    return SeriesBooksPaginatedProvider(
      params,
    );
  }

  @override
  SeriesBooksPaginatedProvider getProviderOverride(
    covariant SeriesBooksPaginatedProvider provider,
  ) {
    return call(
      provider.params,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'seriesBooksPaginatedPod';
}

/// Notifier dedicated to handle the pagination state for Series books.
///
/// Copied from [SeriesBooksPaginated].
class SeriesBooksPaginatedProvider
    extends NotifierProviderImpl<SeriesBooksPaginated, PaginatedState<Book>> {
  /// Notifier dedicated to handle the pagination state for Series books.
  ///
  /// Copied from [SeriesBooksPaginated].
  SeriesBooksPaginatedProvider([
    (String, ReadStatus?) params = ('', null),
  ]) : this._internal(
          () => SeriesBooksPaginated()..params = params,
          from: seriesBooksPaginatedPod,
          name: r'seriesBooksPaginatedPod',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$seriesBooksPaginatedHash,
          dependencies: SeriesBooksPaginatedFamily._dependencies,
          allTransitiveDependencies:
              SeriesBooksPaginatedFamily._allTransitiveDependencies,
          params: params,
        );

  SeriesBooksPaginatedProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.params,
  }) : super.internal();

  final (String, ReadStatus?) params;

  @override
  PaginatedState<Book> runNotifierBuild(
    covariant SeriesBooksPaginated notifier,
  ) {
    return notifier.build(
      params,
    );
  }

  @override
  Override overrideWith(SeriesBooksPaginated Function() create) {
    return ProviderOverride(
      origin: this,
      override: SeriesBooksPaginatedProvider._internal(
        () => create()..params = params,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        params: params,
      ),
    );
  }

  @override
  NotifierProviderElement<SeriesBooksPaginated, PaginatedState<Book>>
      createElement() {
    return _SeriesBooksPaginatedProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is SeriesBooksPaginatedProvider && other.params == params;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, params.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin SeriesBooksPaginatedRef on NotifierProviderRef<PaginatedState<Book>> {
  /// The parameter `params` of this provider.
  (String, ReadStatus?) get params;
}

class _SeriesBooksPaginatedProviderElement
    extends NotifierProviderElement<SeriesBooksPaginated, PaginatedState<Book>>
    with SeriesBooksPaginatedRef {
  _SeriesBooksPaginatedProviderElement(super.provider);

  @override
  (String, ReadStatus?) get params =>
      (origin as SeriesBooksPaginatedProvider).params;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
