import 'package:kapoow/src/features/library_management/books/domain/models/book.dart';
import 'package:kapoow/src/features/library_management/common/models/read_status.dart';
import 'package:kapoow/src/features/library_management/series/domain/models/series.dart';
import 'package:kapoow/src/features/library_management/series/domain/pods.dart';
import 'package:kapoow/src/features/pagination/domain/models/pagination.dart';
import 'package:kapoow/src/features/pagination/ui/controllers/paginated_mixin.dart';
import 'package:kapoow/src/features/pagination/ui/controllers/paginated_state.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'pods.g.dart';

const _sort = ['metadata.numberSort,asc'];

/// Provides the series data from an api call
@Riverpod(keepAlive: true)
FutureOr<Series> series(SeriesRef ref, String id) =>
    ref.watch(seriesRepoPod).getSeriesById(seriesId: id);

/// The first page of books of a series
@Riverpod(keepAlive: true)
FutureOr<Pagination<Book>> seriesBooksFirstPage(
  SeriesBooksFirstPageRef ref,
  String seriesId, {
  ReadStatus? readStatus,
}) async =>
    ref.watch(seriesRepoPod).getBooks(
          seriesId: seriesId,
          readStatus: readStatus,
          sort: _sort,
          page: 0,
        );

/// The first 10 books of a series
@Riverpod(keepAlive: true)
FutureOr<Pagination<Book>> firstBooksBySeries(
  FirstBooksBySeriesRef ref,
  String seriesId,
) async {
  final booksPage = await ref.watch(seriesBooksFirstPagePod(seriesId).future);
  return booksPage.copyWith(
    content: booksPage.content.take(10).toList(),
  );
}

/// Notifier dedicated to handle the pagination state for Series books.
@Riverpod(keepAlive: true)
class SeriesBooksPaginated extends _$SeriesBooksPaginated
    with PaginatedMixin<Book, (String, ReadStatus?)> {
  @override
  PaginatedState<Book> build([(String, ReadStatus?) params = ('', null)]) {
    assert(params.$1.isNotEmpty, 'The [params] should not be empty');

    final firstPageAsync = ref.watch(
      seriesBooksFirstPagePod(
        params.$1,
        readStatus: params.$2,
      ),
    );

    return buildInitState(firstPageAsync);
  }

  @override
  Future<Pagination<Book>> fetchPage(int page) async =>
      ref.watch(seriesRepoPod).getBooks(
            seriesId: params.$1,
            readStatus: params.$2,
            page: page,
            sort: _sort,
          );
}
