import 'package:flutter/material.dart';
import 'package:kapoow/src/common_widgets/app_segmented_button.dart';
import 'package:kapoow/src/features/library_management/common/models/read_status.dart';
import 'package:kapoow/src/features/library_management/series/domain/models/series.dart';
import 'package:kapoow/src/theme/extensions.dart';

/// Series Screen Book Read Status selector, is an implementation of
/// [SliverPersistentHeaderDelegate] and displays an [AppSegmentedButton]
/// with the 'All' , 'Read' and ' Unread'  options.
class SeriesBookStatusPersistentTabs extends SliverPersistentHeaderDelegate {
  /// Creates instance of [SeriesBookStatusPersistentTabs]
  SeriesBookStatusPersistentTabs({
    required this.status,
    required this.series,
    required this.onSelectionChanged,
  });

  /// Selected statuses
  final Set<ReadStatus?> status;

  /// Series to get the counts for every tab
  final Series series;

  /// What to do when tap any of the tabs
  final ValueChanged<Set<ReadStatus?>> onSelectionChanged;

  @override
  double get minExtent => 40;
  @override
  double get maxExtent => 40;

  @override
  Widget build(
    BuildContext context,
    double shrinkOffset,
    bool overlapsContent,
  ) {
    return SizedBox.expand(
      child: AppSegmentedButton(
        onSelectionChanged: onSelectionChanged,
        selected: status,
        backgroundColor: context.colorScheme.surface,
        segments: [
          ButtonSegment(
            value: null,
            label: Text('All - ${series.booksCount}'),
          ),
          ButtonSegment(
            value: ReadStatus.unread,
            label: Text('Unread - ${series.booksUnreadCount}'),
          ),
          ButtonSegment(
            value: ReadStatus.read,
            label: Text('Read - ${series.booksReadCount}'),
          ),
        ],
      ),
    );
  }

  @override
  bool shouldRebuild(SeriesBookStatusPersistentTabs oldDelegate) => true;
}
