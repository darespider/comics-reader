import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:kapoow/src/common_widgets/typical_when_builder.dart';
import 'package:kapoow/src/constants/app_paddings.dart';
import 'package:kapoow/src/features/library_management/books/ui/widgets/book_cover.dart';
import 'package:kapoow/src/features/library_management/common/ui/widgets/item_cover/cover_images_row.dart';
import 'package:kapoow/src/features/library_management/common/ui/widgets/item_cover/view_all_cover_card.dart';
import 'package:kapoow/src/features/library_management/series/domain/models/series.dart';
import 'package:kapoow/src/features/library_management/series/ui/controllers/pods.dart';
import 'package:kapoow/src/features/pagination/ui/widgets/horizontal_paginated_list.dart';
import 'package:kapoow/src/routes/routes_data.dart';
import 'package:kapoow/src/theme/extensions.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';

/// Horizontal list with the first 5 books of a series.
///
/// Displays the covers, the title of the series and a button to access to
/// the series details page.
class SeriesFirstBooksRow extends ConsumerWidget {
  /// Creates a [SeriesFirstBooksRow] instance
  const SeriesFirstBooksRow({
    required this.series,
    super.key,
  });

  /// The series to display the books
  final Series series;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final booksPageAsync = ref.watch(firstBooksBySeriesPod(series.id));

    void goToSeries() => SeriesRoute(id: series.id).go(context);

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Row(
          children: [
            Expanded(
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: AppPaddings.small,
                ),
                child: Text(
                  series.name,
                  style: context.textTheme.titleLarge,
                ),
              ),
            ),
            IconButton(
              icon: PhosphorIcon(
                PhosphorIcons.archive(PhosphorIconsStyle.duotone),
              ),
              onPressed: goToSeries,
            ),
          ],
        ),
        Flexible(
          child: TypicalWhenBuilder(
            asyncValue: booksPageAsync,
            loadingBuilder: (_) => CoverImagesRow.placeholders(
              series.booksCount > 10 ? 10 : series.booksCount,
            ),
            dataBuilder: (_, page) {
              return HorizontalPaginatedList.singlePage(
                page: page,
                trailingBuilder: (context) => ViewAllCoverCard(
                  onTap: goToSeries,
                ),
                itemBuilder: (context, book) => BookCover(book: book),
              );
            },
          ),
        ),
      ],
    );
  }
}
