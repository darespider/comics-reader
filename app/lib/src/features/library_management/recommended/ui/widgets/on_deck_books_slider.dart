import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:kapoow/src/common_widgets/app_loader.dart';
import 'package:kapoow/src/common_widgets/titled_section.dart';
import 'package:kapoow/src/constants/app_paddings.dart';
import 'package:kapoow/src/features/library_management/books/ui/widgets/book_card.dart';
import 'package:kapoow/src/features/library_management/recommended/ui/controllers/on_deck_paginated_pod.dart';
import 'package:kapoow/src/features/pagination/ui/widgets/paginated_slider.dart';
import 'package:kapoow/src/theme/extensions.dart';

final _pod = onDeckPaginatedPod();

/// Page view of book cards
class OnDeckBooksSlider extends HookConsumerWidget {
  /// Creates an [OnDeckBooksSlider] instance
  const OnDeckBooksSlider({
    super.key,
  });

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final onDeckBooks = ref.watch(_pod);
    final notifier = ref.watch(_pod.notifier);

    final pageController = usePageController();
    final currentPage = useListenableSelector(
      pageController,
      () => pageController.hasClients
          ? pageController.page!.round()
          : pageController.initialPage,
    );

    return TitledSection(
      title: const Text('On Deck'),
      hide: onDeckBooks.hasNoResults,
      titleTrailing: onDeckBooks.pages.isNotEmpty
          ? Text(
              '${currentPage + 1} / ${onDeckBooks.totalItems}',
              style: context.textTheme.titleLarge,
            )
          : null,
      body: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: AppPaddings.small,
        ),
        child: PaginatedSlider(
          controller: pageController,
          paginatedState: onDeckBooks,
          nextPageLoader: notifier.loadNextPage,
          loadingPageIndicator: const Card(
            child: Center(
              child: AppLoader(),
            ),
          ),
          errorInPageIndicator: Card(
            child: Center(
              child: Text(
                // TODO(rurickdev): this error should not be null but some
                //  times it is
                onDeckBooks.errorLoadingPage?.toString() ??
                    'On Deck Pagination Error State',
              ),
            ),
          ),
          itemBuilder: (context, item) => BookCard(book: item),
        ),
      ),
    );
  }
}
