import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:kapoow/src/common_widgets/titled_section.dart';
import 'package:kapoow/src/features/library_management/books/ui/widgets/book_cover.dart';
import 'package:kapoow/src/features/library_management/recommended/ui/controllers/recently_added_books_paginated_pod.dart';
import 'package:kapoow/src/features/pagination/ui/widgets/horizontal_paginated_list.dart';

final _pod = recentlyAddedBooksPaginatedPod();

/// Horizontal Infinite list with Recently Added Books
class RecentlyAddedBooksSection extends ConsumerWidget {
  /// Creates a [RecentlyAddedBooksSection] instance
  const RecentlyAddedBooksSection({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final recentlyAddedBooks = ref.watch(_pod);

    return TitledSection(
      title: const Text('Recently Added Books'),
      hide: recentlyAddedBooks.hasNoResults,
      body: HorizontalPaginatedList(
        paginatedState: recentlyAddedBooks,
        nextPageLoader: ref.watch(_pod.notifier).loadNextPage,
        itemBuilder: (context, book) => BookCover(book: book),
      ),
    );
  }
}
