import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:kapoow/src/common_widgets/titled_section.dart';
import 'package:kapoow/src/features/library_management/books/ui/widgets/book_cover.dart';
import 'package:kapoow/src/features/library_management/recommended/ui/controllers/recently_released_books_paginated_pod.dart';
import 'package:kapoow/src/features/pagination/ui/widgets/horizontal_paginated_list.dart';

final _pod = recentlyReleasedBooksPaginatedPod();

/// Horizontal Infinite list with Recently Released Books
class RecentlyReleasedBooksSection extends ConsumerWidget {
  /// Creates a [RecentlyReleasedBooksSection] instance
  const RecentlyReleasedBooksSection({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final recentlyReleasedBooks = ref.watch(_pod);

    return TitledSection(
      hide: recentlyReleasedBooks.hasNoResults,
      title: const Text('Recently Released Books'),
      body: HorizontalPaginatedList(
        paginatedState: recentlyReleasedBooks,
        nextPageLoader: ref.watch(_pod.notifier).loadNextPage,
        itemBuilder: (context, book) => BookCover(book: book),
      ),
    );
  }
}
