import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:kapoow/src/common_widgets/titled_section.dart';
import 'package:kapoow/src/features/library_management/books/ui/widgets/book_cover.dart';
import 'package:kapoow/src/features/library_management/recommended/ui/controllers/recently_read_books_paginated_pod.dart';
import 'package:kapoow/src/features/pagination/ui/widgets/horizontal_paginated_list.dart';

final _pod = recentlyReadBooksPaginatedPod();

/// Horizontal Infinite list with Recently Read Books
class RecentlyReadBooksSection extends ConsumerWidget {
  /// Creates a [RecentlyReadBooksSection] instance
  const RecentlyReadBooksSection({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final recentlyReadBooks = ref.watch(_pod);
    return TitledSection(
      hide: recentlyReadBooks.hasNoResults,
      title: const Text('Recently Read Books'),
      body: HorizontalPaginatedList(
        paginatedState: recentlyReadBooks,
        nextPageLoader: ref.watch(_pod.notifier).loadNextPage,
        itemBuilder: (context, book) => BookCover(book: book),
      ),
    );
  }
}
