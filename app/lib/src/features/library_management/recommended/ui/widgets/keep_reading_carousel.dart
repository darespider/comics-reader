import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:gap/gap.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:kapoow/src/constants/app_paddings.dart';
import 'package:kapoow/src/features/library_management/books/ui/widgets/book_cover.dart';
import 'package:kapoow/src/features/library_management/common/ui/widgets/item_cover/cover_image.dart';
import 'package:kapoow/src/features/library_management/recommended/ui/controllers/keep_reading_paginated_pod.dart';
import 'package:kapoow/src/features/pagination/ui/widgets/paginated_slider.dart';
import 'package:kapoow/src/features/rest_api/ui/widgets/cached_api_image_decorated_box.dart';
import 'package:kapoow/src/theme/extensions.dart';
import 'package:kapoow/src/utils/extensions.dart';
import 'package:kapoow/src/utils/hooks/use_timer.dart';

final _pod = keepReadingPaginatedPod();

/// Carousel of books that are in progress, shows the book cover and a
/// background with the bigger blur version of the cover
class KeepReadingCarousel extends HookConsumerWidget {
  /// Creates a [KeepReadingCarousel] instance
  const KeepReadingCarousel({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final keepReadingPage = ref.watch(_pod);

    final backgroundController = usePageController();
    final foregroundController = usePageController();
    final pauseTimer = useRef(false);

    usePeriodicTimer(
      const Duration(seconds: 5),
      (timer) {
        if (pauseTimer.value) return;
        final currentPage = foregroundController.page?.round() ?? 0;
        final nextPage =
            currentPage == keepReadingPage.totalItems - 1 ? 0 : currentPage + 1;
        foregroundController.animateToPage(
          nextPage,
          duration: const Duration(milliseconds: 300),
          curve: Curves.easeIn,
        );
      },
      [keepReadingPage.totalItems],
    );

    final colorScheme = context.colorScheme;

    return SizedBox(
      height: context.height / 3,
      child: GestureDetector(
        onPanDown: (details) => pauseTimer.value = true,
        onTapUp: (details) => pauseTimer.value = false,
        onLongPressEnd: (details) => pauseTimer.value = false,
        child: Stack(
          children: [
            SizedBox(
              height: context.height / 3.5,
              width: context.width,
              child: Stack(
                children: [
                  PaginatedSlider(
                    paginatedState: keepReadingPage,
                    errorInPageIndicator: const SizedBox.shrink(),
                    loadingPageIndicator: DecoratedBox(
                      decoration: BoxDecoration(
                        color: colorScheme.surfaceContainerHighest,
                      ),
                      child: const SizedBox.expand(),
                    ),
                    nextPageLoader: ref.watch(_pod.notifier).loadNextPage,
                    controller: backgroundController,
                    physics: const NeverScrollableScrollPhysics(),
                    itemBuilder: (context, item) => CachedApiImageDecoratedBox(
                      relativeUrl: item.thumbnailUri,
                    ),
                  ),
                  BackdropFilter(
                    // TODO(rurickdev): replace or mix with dots filter
                    filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
                    child: DecoratedBox(
                      decoration: BoxDecoration(
                        color: Colors.black.withOpacity(0.45),
                        gradient: const LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.center,
                          stops: [0.2, 0.5, 0.75, 1],
                          colors: [
                            Colors.black,
                            Colors.black,
                            Colors.black,
                            Colors.transparent,
                          ],
                        ),
                      ),
                      child: const SizedBox.expand(),
                    ),
                  ),
                ],
              ),
            ),
            SafeArea(
              child: PaginatedSlider(
                paginatedState: keepReadingPage,
                errorInPageIndicator: const SizedBox.shrink(),
                loadingPageIndicator: Align(
                  alignment: AlignmentDirectional.bottomCenter,
                  child: SizedBox(
                    height: context.height / 4.5,
                    child: const CoverImage.placeHolder(),
                  ),
                ),
                nextPageLoader: ref.watch(_pod.notifier).loadNextPage,
                controller: foregroundController,
                onPageChanged: (value) {
                  backgroundController.animateToPage(
                    value,
                    duration: const Duration(milliseconds: 200),
                    curve: Curves.easeIn,
                  );
                },
                itemBuilder: (context, book) => Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Gap(AppPaddings.small),
                    Text(
                      book.name,
                      style: context.textTheme.titleLarge?.copyWith(
                        color: Colors.white,
                      ),
                      textAlign: TextAlign.center,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                    ),
                    SizedBox(
                      height: context.height / 4.5,
                      child: BookCover(
                        book: book,
                        showProgress: true,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
