import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:kapoow/src/common_widgets/extensions.dart';
import 'package:kapoow/src/constants/app_paddings.dart';
import 'package:kapoow/src/features/library_management/recommended/ui/controllers/keep_reading_paginated_pod.dart';
import 'package:kapoow/src/features/library_management/recommended/ui/controllers/on_deck_paginated_pod.dart';
import 'package:kapoow/src/features/library_management/recommended/ui/controllers/recently_added_books_paginated_pod.dart';
import 'package:kapoow/src/features/library_management/recommended/ui/controllers/recently_added_series_paginated_pod.dart';
import 'package:kapoow/src/features/library_management/recommended/ui/controllers/recently_read_books_paginated_pod.dart';
import 'package:kapoow/src/features/library_management/recommended/ui/controllers/recently_released_books_paginated_pod.dart';
import 'package:kapoow/src/features/library_management/recommended/ui/controllers/recently_updated_series_paginated_pod.dart';
import 'package:kapoow/src/features/library_management/recommended/ui/widgets/keep_reading_carousel.dart';
import 'package:kapoow/src/features/library_management/recommended/ui/widgets/on_deck_books_slider.dart';
import 'package:kapoow/src/features/library_management/recommended/ui/widgets/recently_added_books_section.dart';
import 'package:kapoow/src/features/library_management/recommended/ui/widgets/recently_added_series_section.dart';
import 'package:kapoow/src/features/library_management/recommended/ui/widgets/recently_read_books_section.dart';
import 'package:kapoow/src/features/library_management/recommended/ui/widgets/recently_released_books_section.dart';
import 'package:kapoow/src/features/library_management/recommended/ui/widgets/recently_updated_series_section.dart';

/// Main screen of the app, displays the next sections
///
/// Keep Reading (Header)
///
/// On Deck (Sub Header Card Page View)
///
/// Recently Released Books
///
/// Recently Added Books
///
/// One Shots
///
/// Recently Added Series
///
/// Recently Updated Series
///
/// Recently Read Books
class RecommendedScreen extends ConsumerWidget {
  /// Creates a [RecommendedScreen] instance
  const RecommendedScreen({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return RefreshIndicator(
      onRefresh: () async {
        await Future.wait([
          ref.refresh(keepReadingFirstPageAsyncPod.future),
          ref.refresh(onDeckFirstPageAsyncPod.future),
          ref.refresh(recentlyAddedBooksFirstPageAsyncPod.future),
          ref.refresh(recentlyReleasedBooksFirstPageAsyncPod.future),
          ref.refresh(recentlyAddedSeriesFirstPageAsyncPod.future),
          ref.refresh(recentlyUpdatedSeriesFirstPageAsyncPod.future),
          ref.refresh(recentlyReadBooksFirstPageAsyncPod.future),
        ]);
      },
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(bottom: AppPaddings.medium),
          child: Column(
            children: [
              const KeepReadingCarousel(),
              const OnDeckBooksSlider(),
              const RecentlyAddedBooksSection(),
              const RecentlyReleasedBooksSection(),
              const RecentlyAddedSeriesSection(),
              const RecentlyUpdatedSeriesSection(),
              const RecentlyReadBooksSection(),
            ].separated(mainAxisExtent: AppPaddings.medium),
          ),
        ),
      ),
    );
  }
}
