// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'recently_updated_series_paginated_pod.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$recentlyUpdatedSeriesFirstPageAsyncHash() =>
    r'afea1347c9da9b67fd7e81c54539f28816667d5f';

/// Provides the first page of a list of keep reading books
///
/// Copied from [recentlyUpdatedSeriesFirstPageAsync].
@ProviderFor(recentlyUpdatedSeriesFirstPageAsync)
final recentlyUpdatedSeriesFirstPageAsyncPod =
    FutureProvider<Pagination<Series>>.internal(
  recentlyUpdatedSeriesFirstPageAsync,
  name: r'recentlyUpdatedSeriesFirstPageAsyncPod',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$recentlyUpdatedSeriesFirstPageAsyncHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef RecentlyUpdatedSeriesFirstPageAsyncRef
    = FutureProviderRef<Pagination<Series>>;
String _$recentlyUpdatedSeriesPaginatedHash() =>
    r'a093b83d597027280b27ed5a90e12b640c51e703';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

abstract class _$RecentlyUpdatedSeriesPaginated
    extends BuildlessNotifier<PaginatedState<Series>> {
  late final Object? params;

  PaginatedState<Series> build([
    Object? params,
  ]);
}

/// Notifier dedicated to handle the pagination state for books updated in last
/// month, the resulted pagination will be sorted by release date in descending
/// order.
///
/// Copied from [RecentlyUpdatedSeriesPaginated].
@ProviderFor(RecentlyUpdatedSeriesPaginated)
const recentlyUpdatedSeriesPaginatedPod =
    RecentlyUpdatedSeriesPaginatedFamily();

/// Notifier dedicated to handle the pagination state for books updated in last
/// month, the resulted pagination will be sorted by release date in descending
/// order.
///
/// Copied from [RecentlyUpdatedSeriesPaginated].
class RecentlyUpdatedSeriesPaginatedFamily
    extends Family<PaginatedState<Series>> {
  /// Notifier dedicated to handle the pagination state for books updated in last
  /// month, the resulted pagination will be sorted by release date in descending
  /// order.
  ///
  /// Copied from [RecentlyUpdatedSeriesPaginated].
  const RecentlyUpdatedSeriesPaginatedFamily();

  /// Notifier dedicated to handle the pagination state for books updated in last
  /// month, the resulted pagination will be sorted by release date in descending
  /// order.
  ///
  /// Copied from [RecentlyUpdatedSeriesPaginated].
  RecentlyUpdatedSeriesPaginatedProvider call([
    Object? params,
  ]) {
    return RecentlyUpdatedSeriesPaginatedProvider(
      params,
    );
  }

  @override
  RecentlyUpdatedSeriesPaginatedProvider getProviderOverride(
    covariant RecentlyUpdatedSeriesPaginatedProvider provider,
  ) {
    return call(
      provider.params,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'recentlyUpdatedSeriesPaginatedPod';
}

/// Notifier dedicated to handle the pagination state for books updated in last
/// month, the resulted pagination will be sorted by release date in descending
/// order.
///
/// Copied from [RecentlyUpdatedSeriesPaginated].
class RecentlyUpdatedSeriesPaginatedProvider extends NotifierProviderImpl<
    RecentlyUpdatedSeriesPaginated, PaginatedState<Series>> {
  /// Notifier dedicated to handle the pagination state for books updated in last
  /// month, the resulted pagination will be sorted by release date in descending
  /// order.
  ///
  /// Copied from [RecentlyUpdatedSeriesPaginated].
  RecentlyUpdatedSeriesPaginatedProvider([
    Object? params,
  ]) : this._internal(
          () => RecentlyUpdatedSeriesPaginated()..params = params,
          from: recentlyUpdatedSeriesPaginatedPod,
          name: r'recentlyUpdatedSeriesPaginatedPod',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$recentlyUpdatedSeriesPaginatedHash,
          dependencies: RecentlyUpdatedSeriesPaginatedFamily._dependencies,
          allTransitiveDependencies:
              RecentlyUpdatedSeriesPaginatedFamily._allTransitiveDependencies,
          params: params,
        );

  RecentlyUpdatedSeriesPaginatedProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.params,
  }) : super.internal();

  final Object? params;

  @override
  PaginatedState<Series> runNotifierBuild(
    covariant RecentlyUpdatedSeriesPaginated notifier,
  ) {
    return notifier.build(
      params,
    );
  }

  @override
  Override overrideWith(RecentlyUpdatedSeriesPaginated Function() create) {
    return ProviderOverride(
      origin: this,
      override: RecentlyUpdatedSeriesPaginatedProvider._internal(
        () => create()..params = params,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        params: params,
      ),
    );
  }

  @override
  NotifierProviderElement<RecentlyUpdatedSeriesPaginated,
      PaginatedState<Series>> createElement() {
    return _RecentlyUpdatedSeriesPaginatedProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is RecentlyUpdatedSeriesPaginatedProvider &&
        other.params == params;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, params.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin RecentlyUpdatedSeriesPaginatedRef
    on NotifierProviderRef<PaginatedState<Series>> {
  /// The parameter `params` of this provider.
  Object? get params;
}

class _RecentlyUpdatedSeriesPaginatedProviderElement
    extends NotifierProviderElement<RecentlyUpdatedSeriesPaginated,
        PaginatedState<Series>> with RecentlyUpdatedSeriesPaginatedRef {
  _RecentlyUpdatedSeriesPaginatedProviderElement(super.provider);

  @override
  Object? get params =>
      (origin as RecentlyUpdatedSeriesPaginatedProvider).params;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
