import 'package:kapoow/src/features/library_management/books/domain/models/book.dart';
import 'package:kapoow/src/features/library_management/books/domain/pods.dart';
import 'package:kapoow/src/features/pagination/domain/models/pagination.dart';
import 'package:kapoow/src/features/pagination/ui/controllers/paginated_mixin.dart';
import 'package:kapoow/src/features/pagination/ui/controllers/paginated_state.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'on_deck_paginated_pod.g.dart';

/// Provides the first page of a list of on deck books
@Riverpod(keepAlive: true)
FutureOr<Pagination<Book>> onDeckFirstPageAsync(OnDeckFirstPageAsyncRef ref) =>
    ref.watch(booksRepoPod).getBooksOnDeck();

/// Notifier dedicated to handle the pagination state for on deck books.
@Riverpod(keepAlive: true)
class OnDeckPaginated extends _$OnDeckPaginated
    with PaginatedMixin<Book, Object?> {
  @override
  PaginatedState<Book> build([Object? params]) {
    final firstPageAsync = ref.watch(onDeckFirstPageAsyncPod);

    return buildInitState(firstPageAsync);
  }

  @override
  Future<Pagination<Book>> fetchPage(int page) async {
    return ref.read(booksRepoPod).getBooksOnDeck(
          page: page,
        );
  }
}
