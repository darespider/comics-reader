// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'on_deck_paginated_pod.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$onDeckFirstPageAsyncHash() =>
    r'd3cf73413680c475af7fc8f8b4a437caf102194e';

/// Provides the first page of a list of on deck books
///
/// Copied from [onDeckFirstPageAsync].
@ProviderFor(onDeckFirstPageAsync)
final onDeckFirstPageAsyncPod = FutureProvider<Pagination<Book>>.internal(
  onDeckFirstPageAsync,
  name: r'onDeckFirstPageAsyncPod',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$onDeckFirstPageAsyncHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef OnDeckFirstPageAsyncRef = FutureProviderRef<Pagination<Book>>;
String _$onDeckPaginatedHash() => r'0db8ee9f6a7ff7d9fc5d1bcf32dec1680e6f8257';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

abstract class _$OnDeckPaginated
    extends BuildlessNotifier<PaginatedState<Book>> {
  late final Object? params;

  PaginatedState<Book> build([
    Object? params,
  ]);
}

/// Notifier dedicated to handle the pagination state for on deck books.
///
/// Copied from [OnDeckPaginated].
@ProviderFor(OnDeckPaginated)
const onDeckPaginatedPod = OnDeckPaginatedFamily();

/// Notifier dedicated to handle the pagination state for on deck books.
///
/// Copied from [OnDeckPaginated].
class OnDeckPaginatedFamily extends Family<PaginatedState<Book>> {
  /// Notifier dedicated to handle the pagination state for on deck books.
  ///
  /// Copied from [OnDeckPaginated].
  const OnDeckPaginatedFamily();

  /// Notifier dedicated to handle the pagination state for on deck books.
  ///
  /// Copied from [OnDeckPaginated].
  OnDeckPaginatedProvider call([
    Object? params,
  ]) {
    return OnDeckPaginatedProvider(
      params,
    );
  }

  @override
  OnDeckPaginatedProvider getProviderOverride(
    covariant OnDeckPaginatedProvider provider,
  ) {
    return call(
      provider.params,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'onDeckPaginatedPod';
}

/// Notifier dedicated to handle the pagination state for on deck books.
///
/// Copied from [OnDeckPaginated].
class OnDeckPaginatedProvider
    extends NotifierProviderImpl<OnDeckPaginated, PaginatedState<Book>> {
  /// Notifier dedicated to handle the pagination state for on deck books.
  ///
  /// Copied from [OnDeckPaginated].
  OnDeckPaginatedProvider([
    Object? params,
  ]) : this._internal(
          () => OnDeckPaginated()..params = params,
          from: onDeckPaginatedPod,
          name: r'onDeckPaginatedPod',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$onDeckPaginatedHash,
          dependencies: OnDeckPaginatedFamily._dependencies,
          allTransitiveDependencies:
              OnDeckPaginatedFamily._allTransitiveDependencies,
          params: params,
        );

  OnDeckPaginatedProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.params,
  }) : super.internal();

  final Object? params;

  @override
  PaginatedState<Book> runNotifierBuild(
    covariant OnDeckPaginated notifier,
  ) {
    return notifier.build(
      params,
    );
  }

  @override
  Override overrideWith(OnDeckPaginated Function() create) {
    return ProviderOverride(
      origin: this,
      override: OnDeckPaginatedProvider._internal(
        () => create()..params = params,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        params: params,
      ),
    );
  }

  @override
  NotifierProviderElement<OnDeckPaginated, PaginatedState<Book>>
      createElement() {
    return _OnDeckPaginatedProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is OnDeckPaginatedProvider && other.params == params;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, params.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin OnDeckPaginatedRef on NotifierProviderRef<PaginatedState<Book>> {
  /// The parameter `params` of this provider.
  Object? get params;
}

class _OnDeckPaginatedProviderElement
    extends NotifierProviderElement<OnDeckPaginated, PaginatedState<Book>>
    with OnDeckPaginatedRef {
  _OnDeckPaginatedProviderElement(super.provider);

  @override
  Object? get params => (origin as OnDeckPaginatedProvider).params;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
