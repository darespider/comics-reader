import 'package:kapoow/src/features/library_management/books/domain/models/book.dart';
import 'package:kapoow/src/features/library_management/books/domain/pods.dart';
import 'package:kapoow/src/features/pagination/domain/models/pagination.dart';
import 'package:kapoow/src/features/pagination/ui/controllers/paginated_mixin.dart';
import 'package:kapoow/src/features/pagination/ui/controllers/paginated_state.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'recently_added_books_paginated_pod.g.dart';

const _sort = ['createdDate,desc'];

/// Provides the first page of a list of keep reading books
@Riverpod(keepAlive: true)
FutureOr<Pagination<Book>> recentlyAddedBooksFirstPageAsync(
  RecentlyAddedBooksFirstPageAsyncRef ref,
) {
  return ref.watch(booksRepoPod).getBooks(
        sort: _sort,
        page: 0,
      );
}

/// Notifier dedicated to handle the pagination state for books added in last
/// month, the resulted pagination will be sorted by release date in descending
/// order.
@Riverpod(keepAlive: true)
class RecentlyAddedBooksPaginated extends _$RecentlyAddedBooksPaginated
    with PaginatedMixin<Book, Object?> {
  @override
  PaginatedState<Book> build([Object? params]) {
    final firstPageAsync = ref.watch(recentlyAddedBooksFirstPageAsyncPod);

    return buildInitState(firstPageAsync);
  }

  @override
  Future<Pagination<Book>> fetchPage(int page) async {
    return ref.read(booksRepoPod).getBooks(
          sort: _sort,
          page: page,
        );
  }
}
