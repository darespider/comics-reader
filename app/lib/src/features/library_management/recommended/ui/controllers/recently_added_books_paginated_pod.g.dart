// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'recently_added_books_paginated_pod.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$recentlyAddedBooksFirstPageAsyncHash() =>
    r'2fef8a3a6949ebfe39588778167554f33bdfa274';

/// Provides the first page of a list of keep reading books
///
/// Copied from [recentlyAddedBooksFirstPageAsync].
@ProviderFor(recentlyAddedBooksFirstPageAsync)
final recentlyAddedBooksFirstPageAsyncPod =
    FutureProvider<Pagination<Book>>.internal(
  recentlyAddedBooksFirstPageAsync,
  name: r'recentlyAddedBooksFirstPageAsyncPod',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$recentlyAddedBooksFirstPageAsyncHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef RecentlyAddedBooksFirstPageAsyncRef
    = FutureProviderRef<Pagination<Book>>;
String _$recentlyAddedBooksPaginatedHash() =>
    r'5fe452d4163fb50eb45d5469d29b99ef755161d7';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

abstract class _$RecentlyAddedBooksPaginated
    extends BuildlessNotifier<PaginatedState<Book>> {
  late final Object? params;

  PaginatedState<Book> build([
    Object? params,
  ]);
}

/// Notifier dedicated to handle the pagination state for books added in last
/// month, the resulted pagination will be sorted by release date in descending
/// order.
///
/// Copied from [RecentlyAddedBooksPaginated].
@ProviderFor(RecentlyAddedBooksPaginated)
const recentlyAddedBooksPaginatedPod = RecentlyAddedBooksPaginatedFamily();

/// Notifier dedicated to handle the pagination state for books added in last
/// month, the resulted pagination will be sorted by release date in descending
/// order.
///
/// Copied from [RecentlyAddedBooksPaginated].
class RecentlyAddedBooksPaginatedFamily extends Family<PaginatedState<Book>> {
  /// Notifier dedicated to handle the pagination state for books added in last
  /// month, the resulted pagination will be sorted by release date in descending
  /// order.
  ///
  /// Copied from [RecentlyAddedBooksPaginated].
  const RecentlyAddedBooksPaginatedFamily();

  /// Notifier dedicated to handle the pagination state for books added in last
  /// month, the resulted pagination will be sorted by release date in descending
  /// order.
  ///
  /// Copied from [RecentlyAddedBooksPaginated].
  RecentlyAddedBooksPaginatedProvider call([
    Object? params,
  ]) {
    return RecentlyAddedBooksPaginatedProvider(
      params,
    );
  }

  @override
  RecentlyAddedBooksPaginatedProvider getProviderOverride(
    covariant RecentlyAddedBooksPaginatedProvider provider,
  ) {
    return call(
      provider.params,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'recentlyAddedBooksPaginatedPod';
}

/// Notifier dedicated to handle the pagination state for books added in last
/// month, the resulted pagination will be sorted by release date in descending
/// order.
///
/// Copied from [RecentlyAddedBooksPaginated].
class RecentlyAddedBooksPaginatedProvider extends NotifierProviderImpl<
    RecentlyAddedBooksPaginated, PaginatedState<Book>> {
  /// Notifier dedicated to handle the pagination state for books added in last
  /// month, the resulted pagination will be sorted by release date in descending
  /// order.
  ///
  /// Copied from [RecentlyAddedBooksPaginated].
  RecentlyAddedBooksPaginatedProvider([
    Object? params,
  ]) : this._internal(
          () => RecentlyAddedBooksPaginated()..params = params,
          from: recentlyAddedBooksPaginatedPod,
          name: r'recentlyAddedBooksPaginatedPod',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$recentlyAddedBooksPaginatedHash,
          dependencies: RecentlyAddedBooksPaginatedFamily._dependencies,
          allTransitiveDependencies:
              RecentlyAddedBooksPaginatedFamily._allTransitiveDependencies,
          params: params,
        );

  RecentlyAddedBooksPaginatedProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.params,
  }) : super.internal();

  final Object? params;

  @override
  PaginatedState<Book> runNotifierBuild(
    covariant RecentlyAddedBooksPaginated notifier,
  ) {
    return notifier.build(
      params,
    );
  }

  @override
  Override overrideWith(RecentlyAddedBooksPaginated Function() create) {
    return ProviderOverride(
      origin: this,
      override: RecentlyAddedBooksPaginatedProvider._internal(
        () => create()..params = params,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        params: params,
      ),
    );
  }

  @override
  NotifierProviderElement<RecentlyAddedBooksPaginated, PaginatedState<Book>>
      createElement() {
    return _RecentlyAddedBooksPaginatedProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is RecentlyAddedBooksPaginatedProvider &&
        other.params == params;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, params.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin RecentlyAddedBooksPaginatedRef
    on NotifierProviderRef<PaginatedState<Book>> {
  /// The parameter `params` of this provider.
  Object? get params;
}

class _RecentlyAddedBooksPaginatedProviderElement
    extends NotifierProviderElement<RecentlyAddedBooksPaginated,
        PaginatedState<Book>> with RecentlyAddedBooksPaginatedRef {
  _RecentlyAddedBooksPaginatedProviderElement(super.provider);

  @override
  Object? get params => (origin as RecentlyAddedBooksPaginatedProvider).params;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
