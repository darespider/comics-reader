import 'package:kapoow/src/features/library_management/series/domain/models/series.dart';
import 'package:kapoow/src/features/library_management/series/domain/pods.dart';
import 'package:kapoow/src/features/pagination/domain/models/pagination.dart';
import 'package:kapoow/src/features/pagination/ui/controllers/paginated_mixin.dart';
import 'package:kapoow/src/features/pagination/ui/controllers/paginated_state.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'recently_added_series_paginated_pod.g.dart';

const _oneshot = false;

/// Provides the first page of a list of keep reading books
@Riverpod(keepAlive: true)
FutureOr<Pagination<Series>> recentlyAddedSeriesFirstPageAsync(
  RecentlyAddedSeriesFirstPageAsyncRef ref,
) {
  return ref.watch(seriesRepoPod).getNew(
        oneshot: _oneshot,
        page: 0,
      );
}

/// Notifier dedicated to handle the pagination state for books added in last
/// month, the resulted pagination will be sorted by release date in descending
/// order.
@Riverpod(keepAlive: true)
class RecentlyAddedSeriesPaginated extends _$RecentlyAddedSeriesPaginated
    with PaginatedMixin<Series, Object?> {
  @override
  PaginatedState<Series> build([Object? params]) {
    final firstPageAsync = ref.watch(recentlyAddedSeriesFirstPageAsyncPod);

    return buildInitState(firstPageAsync);
  }

  @override
  Future<Pagination<Series>> fetchPage(int page) async {
    return ref.read(seriesRepoPod).getNew(
          oneshot: _oneshot,
          page: page,
        );
  }
}
