// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'recently_read_books_paginated_pod.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$recentlyReadBooksFirstPageAsyncHash() =>
    r'f84f3ee28be063d0bd8f60ff89e83e647270f0c0';

/// Provides the first page of a list of keep reading books
///
/// Copied from [recentlyReadBooksFirstPageAsync].
@ProviderFor(recentlyReadBooksFirstPageAsync)
final recentlyReadBooksFirstPageAsyncPod =
    FutureProvider<Pagination<Book>>.internal(
  recentlyReadBooksFirstPageAsync,
  name: r'recentlyReadBooksFirstPageAsyncPod',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$recentlyReadBooksFirstPageAsyncHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef RecentlyReadBooksFirstPageAsyncRef
    = FutureProviderRef<Pagination<Book>>;
String _$recentlyReadBooksPaginatedHash() =>
    r'9ec2d355b0c7133ab2a6f52940aaa21f5a3f4b43';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

abstract class _$RecentlyReadBooksPaginated
    extends BuildlessNotifier<PaginatedState<Book>> {
  late final Object? params;

  PaginatedState<Book> build([
    Object? params,
  ]);
}

/// Notifier dedicated to handle the pagination state for books added in last
/// month, the resulted pagination will be sorted by release date in descending
/// order.
///
/// Copied from [RecentlyReadBooksPaginated].
@ProviderFor(RecentlyReadBooksPaginated)
const recentlyReadBooksPaginatedPod = RecentlyReadBooksPaginatedFamily();

/// Notifier dedicated to handle the pagination state for books added in last
/// month, the resulted pagination will be sorted by release date in descending
/// order.
///
/// Copied from [RecentlyReadBooksPaginated].
class RecentlyReadBooksPaginatedFamily extends Family<PaginatedState<Book>> {
  /// Notifier dedicated to handle the pagination state for books added in last
  /// month, the resulted pagination will be sorted by release date in descending
  /// order.
  ///
  /// Copied from [RecentlyReadBooksPaginated].
  const RecentlyReadBooksPaginatedFamily();

  /// Notifier dedicated to handle the pagination state for books added in last
  /// month, the resulted pagination will be sorted by release date in descending
  /// order.
  ///
  /// Copied from [RecentlyReadBooksPaginated].
  RecentlyReadBooksPaginatedProvider call([
    Object? params,
  ]) {
    return RecentlyReadBooksPaginatedProvider(
      params,
    );
  }

  @override
  RecentlyReadBooksPaginatedProvider getProviderOverride(
    covariant RecentlyReadBooksPaginatedProvider provider,
  ) {
    return call(
      provider.params,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'recentlyReadBooksPaginatedPod';
}

/// Notifier dedicated to handle the pagination state for books added in last
/// month, the resulted pagination will be sorted by release date in descending
/// order.
///
/// Copied from [RecentlyReadBooksPaginated].
class RecentlyReadBooksPaginatedProvider extends NotifierProviderImpl<
    RecentlyReadBooksPaginated, PaginatedState<Book>> {
  /// Notifier dedicated to handle the pagination state for books added in last
  /// month, the resulted pagination will be sorted by release date in descending
  /// order.
  ///
  /// Copied from [RecentlyReadBooksPaginated].
  RecentlyReadBooksPaginatedProvider([
    Object? params,
  ]) : this._internal(
          () => RecentlyReadBooksPaginated()..params = params,
          from: recentlyReadBooksPaginatedPod,
          name: r'recentlyReadBooksPaginatedPod',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$recentlyReadBooksPaginatedHash,
          dependencies: RecentlyReadBooksPaginatedFamily._dependencies,
          allTransitiveDependencies:
              RecentlyReadBooksPaginatedFamily._allTransitiveDependencies,
          params: params,
        );

  RecentlyReadBooksPaginatedProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.params,
  }) : super.internal();

  final Object? params;

  @override
  PaginatedState<Book> runNotifierBuild(
    covariant RecentlyReadBooksPaginated notifier,
  ) {
    return notifier.build(
      params,
    );
  }

  @override
  Override overrideWith(RecentlyReadBooksPaginated Function() create) {
    return ProviderOverride(
      origin: this,
      override: RecentlyReadBooksPaginatedProvider._internal(
        () => create()..params = params,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        params: params,
      ),
    );
  }

  @override
  NotifierProviderElement<RecentlyReadBooksPaginated, PaginatedState<Book>>
      createElement() {
    return _RecentlyReadBooksPaginatedProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is RecentlyReadBooksPaginatedProvider &&
        other.params == params;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, params.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin RecentlyReadBooksPaginatedRef
    on NotifierProviderRef<PaginatedState<Book>> {
  /// The parameter `params` of this provider.
  Object? get params;
}

class _RecentlyReadBooksPaginatedProviderElement
    extends NotifierProviderElement<RecentlyReadBooksPaginated,
        PaginatedState<Book>> with RecentlyReadBooksPaginatedRef {
  _RecentlyReadBooksPaginatedProviderElement(super.provider);

  @override
  Object? get params => (origin as RecentlyReadBooksPaginatedProvider).params;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
