import 'package:flutter/material.dart';
import 'package:kapoow/src/features/library_management/common/ui/widgets/item_cover/cover_decoration.dart';
import 'package:kapoow/src/features/library_management/common/ui/widgets/item_cover/cover_image.dart';
import 'package:kapoow/src/features/library_management/read_lists/domain/models/read_list.dart';
import 'package:kapoow/src/routes/routes_data.dart';

/// Displays the cover image of the passed [readlist]
class ReadlistCover extends StatelessWidget {
  /// Creates a [ReadlistCover] instance
  const ReadlistCover({
    required this.readlist,
    super.key,
  });

  /// [ReadList] to read data from
  final ReadList readlist;

  @override
  Widget build(BuildContext context) {
    return CoverImage(
      onTap: () => ReadlistRoute(id: readlist.id).go(context),
      coverUrl: readlist.thumbnailUri,
      decoration: CoverDecorationData(
        itemsCount: readlist.bookIds.length,
      ),
    );
  }
}
