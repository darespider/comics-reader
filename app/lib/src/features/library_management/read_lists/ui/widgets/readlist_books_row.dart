import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:kapoow/src/common_widgets/typical_when_builder.dart';
import 'package:kapoow/src/constants/app_paddings.dart';
import 'package:kapoow/src/features/library_management/books/ui/widgets/book_cover.dart';
import 'package:kapoow/src/features/library_management/common/ui/widgets/item_cover/cover_images_row.dart';
import 'package:kapoow/src/features/library_management/common/ui/widgets/item_cover/view_all_cover_card.dart';
import 'package:kapoow/src/features/library_management/read_lists/domain/models/read_list.dart';
import 'package:kapoow/src/features/library_management/read_lists/ui/controllers/pods.dart';
import 'package:kapoow/src/features/pagination/ui/widgets/horizontal_paginated_list.dart';
import 'package:kapoow/src/features/read_book/domain/models/read_context.dart';
import 'package:kapoow/src/routes/routes_data.dart';
import 'package:kapoow/src/theme/extensions.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';

/// Horizontal list with the first 5 books of a series.
///
/// Displays the covers, the title of the series and a button to access to
/// the series details page.
class ReadlistFirstBooksRow extends ConsumerWidget {
  /// Creates a [ReadlistFirstBooksRow] instance
  const ReadlistFirstBooksRow({
    required this.readlist,
    super.key,
  });

  /// The readlist to display the books
  final ReadList readlist;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final booksPageAsync = ref.watch(firstBooksByReadlistPod(readlist.id));

    void goToReadList() => ReadlistRoute(id: readlist.id).go(context);

    return Column(
      children: [
        Row(
          children: [
            Expanded(
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: AppPaddings.small,
                ),
                child: Text(
                  readlist.name,
                  style: context.textTheme.titleLarge,
                ),
              ),
            ),
            IconButton(
              icon: PhosphorIcon(
                PhosphorIcons.archive(PhosphorIconsStyle.duotone),
              ),
              onPressed: goToReadList,
            ),
          ],
        ),
        Flexible(
          child: TypicalWhenBuilder(
            asyncValue: booksPageAsync,
            loadingBuilder: (_) => CoverImagesRow.placeholders(
              readlist.bookIds.length > 10 ? 10 : readlist.bookIds.length,
            ),
            dataBuilder: (_, page) {
              return HorizontalPaginatedList.singlePage(
                page: page,
                trailingBuilder: (context) => ViewAllCoverCard(
                  onTap: goToReadList,
                ),
                itemBuilder: (context, book) => BookCover(
                  book: book,
                  readContext: ReadContext.readlist,
                  readContextId: readlist.id,
                ),
              );
            },
          ),
        ),
      ],
    );
  }
}
