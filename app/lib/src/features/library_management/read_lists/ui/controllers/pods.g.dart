// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pods.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$readlistHash() => r'b8c609144b14f9f323bd5fb944cb154cc36d7d78';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// Provides the Readlist data from an api call
///
/// Copied from [readlist].
@ProviderFor(readlist)
const readlistPod = ReadlistFamily();

/// Provides the Readlist data from an api call
///
/// Copied from [readlist].
class ReadlistFamily extends Family<AsyncValue<ReadList>> {
  /// Provides the Readlist data from an api call
  ///
  /// Copied from [readlist].
  const ReadlistFamily();

  /// Provides the Readlist data from an api call
  ///
  /// Copied from [readlist].
  ReadlistProvider call(
    String id,
  ) {
    return ReadlistProvider(
      id,
    );
  }

  @override
  ReadlistProvider getProviderOverride(
    covariant ReadlistProvider provider,
  ) {
    return call(
      provider.id,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'readlistPod';
}

/// Provides the Readlist data from an api call
///
/// Copied from [readlist].
class ReadlistProvider extends FutureProvider<ReadList> {
  /// Provides the Readlist data from an api call
  ///
  /// Copied from [readlist].
  ReadlistProvider(
    String id,
  ) : this._internal(
          (ref) => readlist(
            ref as ReadlistRef,
            id,
          ),
          from: readlistPod,
          name: r'readlistPod',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$readlistHash,
          dependencies: ReadlistFamily._dependencies,
          allTransitiveDependencies: ReadlistFamily._allTransitiveDependencies,
          id: id,
        );

  ReadlistProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.id,
  }) : super.internal();

  final String id;

  @override
  Override overrideWith(
    FutureOr<ReadList> Function(ReadlistRef provider) create,
  ) {
    return ProviderOverride(
      origin: this,
      override: ReadlistProvider._internal(
        (ref) => create(ref as ReadlistRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        id: id,
      ),
    );
  }

  @override
  FutureProviderElement<ReadList> createElement() {
    return _ReadlistProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is ReadlistProvider && other.id == id;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, id.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin ReadlistRef on FutureProviderRef<ReadList> {
  /// The parameter `id` of this provider.
  String get id;
}

class _ReadlistProviderElement extends FutureProviderElement<ReadList>
    with ReadlistRef {
  _ReadlistProviderElement(super.provider);

  @override
  String get id => (origin as ReadlistProvider).id;
}

String _$readlistBooksFirstPageHash() =>
    r'9fe256a252ca28c42cca2c7756ac3af1a6287806';

/// The first page of books of a readlist
///
/// Copied from [readlistBooksFirstPage].
@ProviderFor(readlistBooksFirstPage)
const readlistBooksFirstPagePod = ReadlistBooksFirstPageFamily();

/// The first page of books of a readlist
///
/// Copied from [readlistBooksFirstPage].
class ReadlistBooksFirstPageFamily
    extends Family<AsyncValue<Pagination<Book>>> {
  /// The first page of books of a readlist
  ///
  /// Copied from [readlistBooksFirstPage].
  const ReadlistBooksFirstPageFamily();

  /// The first page of books of a readlist
  ///
  /// Copied from [readlistBooksFirstPage].
  ReadlistBooksFirstPageProvider call(
    String readlistId,
  ) {
    return ReadlistBooksFirstPageProvider(
      readlistId,
    );
  }

  @override
  ReadlistBooksFirstPageProvider getProviderOverride(
    covariant ReadlistBooksFirstPageProvider provider,
  ) {
    return call(
      provider.readlistId,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'readlistBooksFirstPagePod';
}

/// The first page of books of a readlist
///
/// Copied from [readlistBooksFirstPage].
class ReadlistBooksFirstPageProvider extends FutureProvider<Pagination<Book>> {
  /// The first page of books of a readlist
  ///
  /// Copied from [readlistBooksFirstPage].
  ReadlistBooksFirstPageProvider(
    String readlistId,
  ) : this._internal(
          (ref) => readlistBooksFirstPage(
            ref as ReadlistBooksFirstPageRef,
            readlistId,
          ),
          from: readlistBooksFirstPagePod,
          name: r'readlistBooksFirstPagePod',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$readlistBooksFirstPageHash,
          dependencies: ReadlistBooksFirstPageFamily._dependencies,
          allTransitiveDependencies:
              ReadlistBooksFirstPageFamily._allTransitiveDependencies,
          readlistId: readlistId,
        );

  ReadlistBooksFirstPageProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.readlistId,
  }) : super.internal();

  final String readlistId;

  @override
  Override overrideWith(
    FutureOr<Pagination<Book>> Function(ReadlistBooksFirstPageRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: ReadlistBooksFirstPageProvider._internal(
        (ref) => create(ref as ReadlistBooksFirstPageRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        readlistId: readlistId,
      ),
    );
  }

  @override
  FutureProviderElement<Pagination<Book>> createElement() {
    return _ReadlistBooksFirstPageProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is ReadlistBooksFirstPageProvider &&
        other.readlistId == readlistId;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, readlistId.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin ReadlistBooksFirstPageRef on FutureProviderRef<Pagination<Book>> {
  /// The parameter `readlistId` of this provider.
  String get readlistId;
}

class _ReadlistBooksFirstPageProviderElement
    extends FutureProviderElement<Pagination<Book>>
    with ReadlistBooksFirstPageRef {
  _ReadlistBooksFirstPageProviderElement(super.provider);

  @override
  String get readlistId =>
      (origin as ReadlistBooksFirstPageProvider).readlistId;
}

String _$firstBooksByReadlistHash() =>
    r'0eea12539428340af4f70089db38aaf84b08fc8a';

/// The first 10 books of a read list
///
/// Copied from [firstBooksByReadlist].
@ProviderFor(firstBooksByReadlist)
const firstBooksByReadlistPod = FirstBooksByReadlistFamily();

/// The first 10 books of a read list
///
/// Copied from [firstBooksByReadlist].
class FirstBooksByReadlistFamily extends Family<AsyncValue<Pagination<Book>>> {
  /// The first 10 books of a read list
  ///
  /// Copied from [firstBooksByReadlist].
  const FirstBooksByReadlistFamily();

  /// The first 10 books of a read list
  ///
  /// Copied from [firstBooksByReadlist].
  FirstBooksByReadlistProvider call(
    String readlistId,
  ) {
    return FirstBooksByReadlistProvider(
      readlistId,
    );
  }

  @override
  FirstBooksByReadlistProvider getProviderOverride(
    covariant FirstBooksByReadlistProvider provider,
  ) {
    return call(
      provider.readlistId,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'firstBooksByReadlistPod';
}

/// The first 10 books of a read list
///
/// Copied from [firstBooksByReadlist].
class FirstBooksByReadlistProvider extends FutureProvider<Pagination<Book>> {
  /// The first 10 books of a read list
  ///
  /// Copied from [firstBooksByReadlist].
  FirstBooksByReadlistProvider(
    String readlistId,
  ) : this._internal(
          (ref) => firstBooksByReadlist(
            ref as FirstBooksByReadlistRef,
            readlistId,
          ),
          from: firstBooksByReadlistPod,
          name: r'firstBooksByReadlistPod',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$firstBooksByReadlistHash,
          dependencies: FirstBooksByReadlistFamily._dependencies,
          allTransitiveDependencies:
              FirstBooksByReadlistFamily._allTransitiveDependencies,
          readlistId: readlistId,
        );

  FirstBooksByReadlistProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.readlistId,
  }) : super.internal();

  final String readlistId;

  @override
  Override overrideWith(
    FutureOr<Pagination<Book>> Function(FirstBooksByReadlistRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: FirstBooksByReadlistProvider._internal(
        (ref) => create(ref as FirstBooksByReadlistRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        readlistId: readlistId,
      ),
    );
  }

  @override
  FutureProviderElement<Pagination<Book>> createElement() {
    return _FirstBooksByReadlistProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is FirstBooksByReadlistProvider &&
        other.readlistId == readlistId;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, readlistId.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin FirstBooksByReadlistRef on FutureProviderRef<Pagination<Book>> {
  /// The parameter `readlistId` of this provider.
  String get readlistId;
}

class _FirstBooksByReadlistProviderElement
    extends FutureProviderElement<Pagination<Book>>
    with FirstBooksByReadlistRef {
  _FirstBooksByReadlistProviderElement(super.provider);

  @override
  String get readlistId => (origin as FirstBooksByReadlistProvider).readlistId;
}

String _$readlistBooksPaginatedHash() =>
    r'2583156964df66a976fdd7d1a8123827c2056653';

abstract class _$ReadlistBooksPaginated
    extends BuildlessNotifier<PaginatedState<Book>> {
  late final String params;

  PaginatedState<Book> build([
    String params = '',
  ]);
}

/// Notifier dedicated to handle the pagination state for Readlist books.
///
/// Copied from [ReadlistBooksPaginated].
@ProviderFor(ReadlistBooksPaginated)
const readlistBooksPaginatedPod = ReadlistBooksPaginatedFamily();

/// Notifier dedicated to handle the pagination state for Readlist books.
///
/// Copied from [ReadlistBooksPaginated].
class ReadlistBooksPaginatedFamily extends Family<PaginatedState<Book>> {
  /// Notifier dedicated to handle the pagination state for Readlist books.
  ///
  /// Copied from [ReadlistBooksPaginated].
  const ReadlistBooksPaginatedFamily();

  /// Notifier dedicated to handle the pagination state for Readlist books.
  ///
  /// Copied from [ReadlistBooksPaginated].
  ReadlistBooksPaginatedProvider call([
    String params = '',
  ]) {
    return ReadlistBooksPaginatedProvider(
      params,
    );
  }

  @override
  ReadlistBooksPaginatedProvider getProviderOverride(
    covariant ReadlistBooksPaginatedProvider provider,
  ) {
    return call(
      provider.params,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'readlistBooksPaginatedPod';
}

/// Notifier dedicated to handle the pagination state for Readlist books.
///
/// Copied from [ReadlistBooksPaginated].
class ReadlistBooksPaginatedProvider
    extends NotifierProviderImpl<ReadlistBooksPaginated, PaginatedState<Book>> {
  /// Notifier dedicated to handle the pagination state for Readlist books.
  ///
  /// Copied from [ReadlistBooksPaginated].
  ReadlistBooksPaginatedProvider([
    String params = '',
  ]) : this._internal(
          () => ReadlistBooksPaginated()..params = params,
          from: readlistBooksPaginatedPod,
          name: r'readlistBooksPaginatedPod',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$readlistBooksPaginatedHash,
          dependencies: ReadlistBooksPaginatedFamily._dependencies,
          allTransitiveDependencies:
              ReadlistBooksPaginatedFamily._allTransitiveDependencies,
          params: params,
        );

  ReadlistBooksPaginatedProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.params,
  }) : super.internal();

  final String params;

  @override
  PaginatedState<Book> runNotifierBuild(
    covariant ReadlistBooksPaginated notifier,
  ) {
    return notifier.build(
      params,
    );
  }

  @override
  Override overrideWith(ReadlistBooksPaginated Function() create) {
    return ProviderOverride(
      origin: this,
      override: ReadlistBooksPaginatedProvider._internal(
        () => create()..params = params,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        params: params,
      ),
    );
  }

  @override
  NotifierProviderElement<ReadlistBooksPaginated, PaginatedState<Book>>
      createElement() {
    return _ReadlistBooksPaginatedProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is ReadlistBooksPaginatedProvider && other.params == params;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, params.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin ReadlistBooksPaginatedRef on NotifierProviderRef<PaginatedState<Book>> {
  /// The parameter `params` of this provider.
  String get params;
}

class _ReadlistBooksPaginatedProviderElement extends NotifierProviderElement<
    ReadlistBooksPaginated,
    PaginatedState<Book>> with ReadlistBooksPaginatedRef {
  _ReadlistBooksPaginatedProviderElement(super.provider);

  @override
  String get params => (origin as ReadlistBooksPaginatedProvider).params;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
