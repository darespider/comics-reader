import 'package:kapoow/src/features/library_management/books/domain/models/book.dart';
import 'package:kapoow/src/features/library_management/read_lists/domain/models/read_list.dart';
import 'package:kapoow/src/features/library_management/read_lists/domain/pods.dart';
import 'package:kapoow/src/features/pagination/domain/models/pagination.dart';
import 'package:kapoow/src/features/pagination/ui/controllers/paginated_mixin.dart';
import 'package:kapoow/src/features/pagination/ui/controllers/paginated_state.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'pods.g.dart';

/// Provides the Readlist data from an api call
@Riverpod(keepAlive: true)
FutureOr<ReadList> readlist(ReadlistRef ref, String id) =>
    ref.watch(readlistsRepoPod).fetchReadlistById(id);

/// The first page of books of a readlist
@Riverpod(keepAlive: true)
FutureOr<Pagination<Book>> readlistBooksFirstPage(
  ReadlistBooksFirstPageRef ref,
  String readlistId,
) async =>
    ref.watch(readlistsRepoPod).fetchReadlistBooks(
          readlistId: readlistId,
          page: 0,
        );

/// The first 10 books of a read list
@Riverpod(keepAlive: true)
FutureOr<Pagination<Book>> firstBooksByReadlist(
  FirstBooksByReadlistRef ref,
  String readlistId,
) async {
  final booksPage =
      await ref.watch(readlistBooksFirstPagePod(readlistId).future);
  return booksPage.copyWith(
    content: booksPage.content.take(10).toList(),
  );
}

/// Notifier dedicated to handle the pagination state for Readlist books.
@Riverpod(keepAlive: true)
class ReadlistBooksPaginated extends _$ReadlistBooksPaginated
    with PaginatedMixin<Book, String> {
  @override
  PaginatedState<Book> build([String params = '']) {
    assert(params.isNotEmpty, 'The [params] should not be empty');

    final firstPageAsync = ref.watch(
      readlistBooksFirstPagePod(params),
    );

    return buildInitState(firstPageAsync);
  }

  @override
  Future<Pagination<Book>> fetchPage(int page) async =>
      ref.watch(readlistsRepoPod).fetchReadlistBooks(
            readlistId: params,
            page: page,
          );
}
