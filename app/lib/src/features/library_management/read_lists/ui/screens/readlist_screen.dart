import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:kapoow/src/common_widgets/advice.dart';
import 'package:kapoow/src/common_widgets/extensions.dart';
import 'package:kapoow/src/common_widgets/typical_when_builder.dart';
import 'package:kapoow/src/constants/app_paddings.dart';
import 'package:kapoow/src/features/library_management/books/ui/widgets/book_card.dart';
import 'package:kapoow/src/features/library_management/common/ui/widgets/item_summary.dart';
import 'package:kapoow/src/features/library_management/read_lists/ui/controllers/pods.dart';
import 'package:kapoow/src/features/pagination/ui/widgets/vertical_paginated_list.dart';
import 'package:kapoow/src/features/read_book/domain/models/read_context.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';

/// Screen to display the details of a readlist
class ReadlistScreen extends ConsumerWidget {
  /// Creates instance of [ReadlistScreen] widget
  const ReadlistScreen({
    required this.readlistId,
    super.key,
  });

  /// id of the item to get
  final String readlistId;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final readlistAsync = ref.watch(readlistPod(readlistId));
    final paginatedPod = readlistBooksPaginatedPod(readlistId);
    final readlistBooksPageState = ref.watch(paginatedPod);

    return TypicalWhenBuilder(
      asyncValue: readlistAsync,
      dataBuilder: (context, readlist) => Scaffold(
        appBar: AppBar(
          // TODO(rurickdev): Add Toggle for order
          title: Text(
            readlist.name,
            overflow: TextOverflow.ellipsis,
            maxLines: 1,
          ),
        ),
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: AppPaddings.small),
          child: RefreshIndicator(
            notificationPredicate: (notification) => notification.depth == 1,
            onRefresh: () async {
              await Future.wait([
                ref.refresh(readlistPod(readlistId).future),
                ref.refresh(readlistBooksFirstPagePod(readlistId).future),
              ]);
            },
            child: NestedScrollView(
              headerSliverBuilder: (context, innerBoxIsScrolled) => [
                if (readlist.summary.isNotEmpty)
                  SliverToBoxAdapter(
                    child: Card(
                      child: Padding(
                        padding: const EdgeInsets.all(AppPaddings.small),
                        child: ItemSummary(summary: readlist.summary),
                      ),
                    ),
                  ),
                SliverToBoxAdapter(
                  child: Advice(
                    icon: PhosphorIcons.readCvLogo(PhosphorIconsStyle.duotone),
                    text: readlist.ordered
                        ? 'Readlist Displayed as Manual Ordering'
                        : 'This sequence is a recommendation for the arc, but '
                            'feel free to explore it as you like.',
                  ),
                ),
                const SliverToBoxAdapter(),
              ].separated(mainAxisExtent: AppPaddings.medium, sliver: true),
              body: VerticalPaginatedList(
                paginatedState: readlistBooksPageState,
                nextPageLoader: ref.watch(paginatedPod.notifier).loadNextPage,
                trailingBuilder: (context) =>
                    const SizedBox(height: AppPaddings.medium),
                itemBuilder: (context, book) => SizedBox(
                  height: 150,
                  child: BookCard(
                    book: book,
                    readContext: ReadContext.readlist,
                    readContextId: readlist.id,
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
