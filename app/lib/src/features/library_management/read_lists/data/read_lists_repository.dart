import 'package:kapoow/src/features/library_management/books/domain/models/book.dart';
import 'package:kapoow/src/features/library_management/common/models/media_status.dart';
import 'package:kapoow/src/features/library_management/common/models/read_status.dart';
import 'package:kapoow/src/features/library_management/common/models/typedefs.dart';
import 'package:kapoow/src/features/library_management/read_lists/domain/models/read_list.dart';

/// Interface describing how the readlists repositories should work
abstract class ReadListsRepository {
  /// Returns a Page with a list of readlists
  Future<PageReadList> fetchReadlists({
    String? search,
    List<String>? libraryIds,
    bool? unpaged,
    int? page,
    int? size,
  });

  /// Returns a single readlist based on the id
  Future<ReadList> fetchReadlistById(String id);

  /// Returns a Page of books from the readlist
  Future<PageBook> fetchReadlistBooks({
    required String readlistId,
    List<String>? libraryIds,
    List<ReadStatus>? readStatus,
    List<String>? tags,
    List<MediaStatus>? mediaStatus,
    bool? deleted,
    bool? unpaged,
    int? page,
    int? size,
    List<String>? authors,
  });

  /// Returns the next book in the read list if exist
  Future<Book?> fetchNext(String id, String bookId);

  /// Returns the previous book in the read list if exist
  Future<Book?> fetchPrevious(String id, String bookId);

  /// Set books to the read list
  Future<void> setBooks(String id, {List<String> bookIds = const []});

  /// Search read lists based on the [query]
  Future<PageReadList> searchReadLists(String query, {int page = 0});

  /// Return unread read list books.
  Future<PageBook> getUnreadBooks({
    required String readListId,
    int? page,
    int? pageSize,
  });

  /// Return in progress read list books.
  Future<PageBook> getInProgressBooks({
    required String readListId,
    int? page,
    int? pageSize,
  });
}
