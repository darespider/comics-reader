import 'package:kapoow/src/features/library_management/read_lists/domain/models/read_list.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/read_list_dto.dart';

/// Extension to parse Dto into domain object
extension ReadListModelParser on ReadListDto {
  /// Creates a [ReadList] domain object from this
  ReadList toDomain() => ReadList(
        id: id,
        name: name,
        bookIds: bookIds,
        createdDate: createdDate,
        lastModifiedDate: lastModifiedDate,
        filtered: filtered,
        ordered: ordered,
        summary: summary,
      );
}
