import 'package:kapoow/src/features/library_management/books/domain/models/book.dart';
import 'package:kapoow/src/features/library_management/books/domain/repositories/dto_domain_parser.dart';
import 'package:kapoow/src/features/library_management/common/models/media_status.dart';
import 'package:kapoow/src/features/library_management/common/models/read_status.dart';
import 'package:kapoow/src/features/library_management/common/models/typedefs.dart';
import 'package:kapoow/src/features/library_management/read_lists/data/read_lists_repository.dart';
import 'package:kapoow/src/features/library_management/read_lists/domain/models/read_list.dart';
import 'package:kapoow/src/features/library_management/read_lists/domain/repositories/dto_domain_parser.dart';
import 'package:kapoow/src/features/pagination/domain/repositories/dto_domain_parser.dart';
import 'package:kapoow/src/features/rest_api/data/datasources/read_lists/read_lists_datasource.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/read_list_update_dto.dart';

/// Readlists repository that fetch data from the api
class ReadListsRepositoryImp implements ReadListsRepository {
  /// Creates instance of [ReadListsRepositoryImp]
  const ReadListsRepositoryImp(this._datasource);

  final ReadListsDatasource _datasource;

  @override
  Future<Book?> fetchNext(String id, String bookId) async {
    final dto = await _datasource.getReadListNextBook(id, bookId);
    return dto?.toDomain();
  }

  @override
  Future<Book?> fetchPrevious(String id, String bookId) async {
    final dto = await _datasource.getReadListPreviousBook(id, bookId);
    return dto?.toDomain();
  }

  @override
  Future<PageBook> fetchReadlistBooks({
    required String readlistId,
    List<String>? libraryIds,
    List<ReadStatus>? readStatus,
    List<String>? tags,
    List<MediaStatus>? mediaStatus,
    bool? deleted,
    bool? unpaged,
    int? page,
    int? size,
    List<String>? authors,
  }) async {
    final dto = await _datasource.getReadListBooks(
      readlistId: readlistId,
      libraryIds: libraryIds,
      readStatus: readStatus,
      tags: tags,
      mediaStatus: mediaStatus,
      deleted: deleted,
      unpaged: unpaged,
      page: page,
      size: size,
      authors: authors,
    );
    return dto.toDomain();
  }

  @override
  Future<ReadList> fetchReadlistById(String id) async {
    final dto = await _datasource.getReadListById(id);
    return dto.toDomain();
  }

  @override
  Future<PageReadList> fetchReadlists({
    String? search,
    List<String>? libraryIds,
    bool? unpaged,
    int? page,
    int? size,
  }) async {
    final dto = await _datasource.getReadLists(
      search: search,
      libraryIds: libraryIds,
      unpaged: unpaged,
      page: page,
      size: size,
    );
    return dto.toDomain();
  }

  @override
  Future<PageReadList> searchReadLists(String query, {int page = 0}) async {
    final dto = await _datasource.getReadLists(
      search: query,
      page: page,
    );
    return dto.toDomain();
  }

  @override
  Future<void> setBooks(String id, {List<String> bookIds = const []}) async =>
      _datasource.patchReadList(
        id,
        ReadListUpdateDto(
          bookIds: bookIds,
        ),
      );

  @override
  Future<PageBook> getInProgressBooks({
    required String readListId,
    int? page,
    int? pageSize,
  }) async {
    final dto = await _datasource.getReadListBooks(
      readlistId: readListId,
      readStatus: [ReadStatus.inProgress],
      page: page,
    );
    return dto.toDomain();
  }

  @override
  Future<PageBook> getUnreadBooks({
    required String readListId,
    int? page,
    int? pageSize,
  }) async {
    final dto = await _datasource.getReadListBooks(
      readlistId: readListId,
      readStatus: [ReadStatus.unread],
      page: page,
    );
    return dto.toDomain();
  }
}
