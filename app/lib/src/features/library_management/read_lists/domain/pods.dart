import 'package:kapoow/src/features/library_management/read_lists/data/read_lists_repository.dart';
import 'package:kapoow/src/features/library_management/read_lists/domain/repositories/read_lists_repository_imp.dart';
import 'package:kapoow/src/features/rest_api/data/datasources/pods.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'pods.g.dart';

/// Provides the [ReadListsRepository]
@Riverpod(keepAlive: true)
ReadListsRepository readlistsRepo(ReadlistsRepoRef ref) {
  return ReadListsRepositoryImp(ref.watch(readListsDatasourcePod));
}
