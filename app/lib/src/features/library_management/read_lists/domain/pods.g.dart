// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pods.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$readlistsRepoHash() => r'265ef0b6bbbae4093d8284427eb23a7367916f27';

/// Provides the [ReadListsRepository]
///
/// Copied from [readlistsRepo].
@ProviderFor(readlistsRepo)
final readlistsRepoPod = Provider<ReadListsRepository>.internal(
  readlistsRepo,
  name: r'readlistsRepoPod',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$readlistsRepoHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef ReadlistsRepoRef = ProviderRef<ReadListsRepository>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
