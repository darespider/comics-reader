import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:kapoow/src/features/rest_api/domain/pods.dart';
import 'package:logging/logging.dart';

/// Wrapper for [CachedNetworkImage] that allows the use of relative urls
class CachedApiImage extends ConsumerWidget {
  /// Creates a [CachedApiImage] instance
  const CachedApiImage({
    required this.imageRelativeUrl,
    this.imageBuilder,
    this.placeholder,
    this.errorWidget,
    super.key,
  });

  /// url relative to the api host.
  final String imageRelativeUrl;

  /// Optional builder to further customize the display of the image.
  final ImageWidgetBuilder? imageBuilder;

  /// Widget displayed while the target [imageRelativeUrl] is loading.
  final PlaceholderWidgetBuilder? placeholder;

  /// Widget displayed while the target [imageRelativeUrl] failed loading.
  final LoadingErrorWidgetBuilder? errorWidget;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final headers = ref.watch(headersPod);
    final apiUrl = ref.watch(thumbnailUriPod);

    return CachedNetworkImage(
      imageUrl: '$apiUrl/$imageRelativeUrl',
      httpHeaders: headers,
      placeholder: placeholder,
      errorWidget: errorWidget,
      imageBuilder: imageBuilder,
      errorListener: (value) {
        final message = 'ImageRequest\nvalue:\n$Object';
        Logger('http').info(message);
      },
    );
  }
}
