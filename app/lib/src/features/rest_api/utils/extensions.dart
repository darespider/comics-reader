/// Extension on Uri object to make the writing of uris more intuitive
extension UriX on Uri {
  /// Handy operator to write Uris paths using the slash as separator
  Uri operator /(String pathSegment) => replace(
        pathSegments: [
          ...pathSegments,
          if (pathSegment.isNotEmpty) pathSegment,
        ],
      );

  /// Handy operator to add query params to the uri
  Uri operator &(Map<String, dynamic> queries) {
    assert(
      queries.values.every((e) => e is String || e is List<String>),
      'the query values should be String or List<String>',
    );
    return replace(queryParameters: {...queryParameters, ...queries});
  }

  /// Merges two Uris together using first the current uri properties
  /// then adding the [other] properties
  Uri merge(Uri other) {
    final resultingQueryParameters = {
      ...queryParameters,
      ...queryParametersAll,
      ...other.queryParameters,
      ...other.queryParametersAll,
    };
    return replace(
      pathSegments: [...pathSegments, ...other.pathSegments],
      queryParameters:
          resultingQueryParameters.isEmpty ? null : resultingQueryParameters,
    );
  }
}

/// Auxiliar operators and functions to write Uri paths from Strings
extension UriStringX on String {
  /// Concatenate two strings by the slash and turns the result into an Uri
  Uri operator /(String pathSegment) => Uri(pathSegments: [this, pathSegment]);

  /// Concatenate a string with a Map as query params for uri
  Uri operator &(Map<String, dynamic> queries) =>
      Uri(pathSegments: [this], queryParameters: queries);

  /// Auxiliar method to convert String to Uri
  Uri toUri() => Uri.parse(this);
}
