// coverage:ignore-file
import 'dart:io';

import 'package:http/http.dart';
import 'package:kapoow/src/features/rest_api/domain/rest_api/cache_downloads_api.dart';
import 'package:kapoow/src/features/rest_api/domain/rest_api/downloads_api.dart';
import 'package:kapoow/src/features/rest_api/domain/rest_api/rest_api.dart';
import 'package:kapoow/src/features/rest_api/domain/rest_api/rest_json_api.dart';
import 'package:kapoow/src/features/rest_api/utils/extensions.dart';
import 'package:kapoow/src/features/session/domain/controllers/session_pods.dart';
import 'package:kapoow/src/features/session/domain/models/session_data.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'pods.g.dart';

/// Provides the session data as async value
@Riverpod(keepAlive: true)
FutureOr<SessionData> sessionAsync(SessionAsyncRef ref) async {
  final repo = ref.watch(sessionRepositoryPod);
  return repo.getLocalSession();
}

/// Provides the session token
@Riverpod(keepAlive: true)
class BasicToken extends _$BasicToken {
  @override
  String build() {
    return ref.watch(sessionAsyncPod).requireValue.token;
  }

  /// Sets the token
  // ignore: use_setters_to_change_properties
  void setToken(String token) {
    state = token;
  }
}

/// Provides an http client
@Riverpod(keepAlive: true)
Client httpClient(HttpClientRef ref) {
  return Client();
}

/// Provides the base url read from the session
@Riverpod(keepAlive: true)
class BaseUri extends _$BaseUri {
  @override
  Uri build() {
    final host = ref.watch(sessionAsyncPod).requireValue.host;
    return Uri.parse(host);
  }

  /// Sets a custom uri parsed from the [host] string
  void setUri(String host) {
    state = Uri.parse(host);
  }
}

/// Provides the base uri path for thumbnails
@Riverpod(keepAlive: true)
Uri thumbnailUri(ThumbnailUriRef ref) {
  final apiUrl = ref.watch(baseUriPod);
  return apiUrl.merge('api' / 'v1');
}

/// Provides the headers for the api request and adds the token
@Riverpod(keepAlive: true)
Map<String, String> headers(HeadersRef ref) {
  return {
    HttpHeaders.authorizationHeader: 'Basic ${ref.watch(basicTokenPod)}',
    HttpHeaders.contentTypeHeader: ContentType.json.toString(),
  };
}

/// Provides the rest api client
@Riverpod(keepAlive: true)
RestApi restApi(RestApiRef ref) {
  return RestApi(
    client: ref.watch(httpClientPod),
    apiUrl: ref.watch(baseUriPod),
    headers: ref.watch(headersPod),
  );
}

/// Provides the rest json api client
@Riverpod(keepAlive: true)
RestJsonApi restJsonApi(RestJsonApiRef ref) {
  return RestJsonApi(ref.watch(restApiPod));
}

/// Provides the rest json api client
@Riverpod(keepAlive: true)
DownloadsApi downloadsApi(DownloadsApiRef ref) {
  return DownloadsApi(ref.watch(restApiPod));
}

/// Provides a cache api
@Riverpod(keepAlive: true)
CacheDownloadsApi cacheDownloadApi(CacheDownloadApiRef ref) {
  return CacheDownloadsApi(
    headers: ref.watch(headersPod),
    apiUrl: ref.watch(baseUriPod),
  );
}
