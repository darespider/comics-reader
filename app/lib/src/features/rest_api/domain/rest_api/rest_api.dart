import 'dart:async';
import 'dart:io';

import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:http/http.dart' as http;
import 'package:kapoow/src/features/rest_api/domain/api_exceptions.dart';
import 'package:kapoow/src/features/rest_api/domain/json_map.dart';
import 'package:kapoow/src/features/rest_api/utils/extensions.dart';
import 'package:logging/logging.dart';

/// This class responsibility is only to call the http methods using any
/// implementation of the [http.Client] on the passed [_apiUrl]. It will return
/// the non parsed responses or throw domain http exceptions based on the result
///
/// It should NOT parse the responses.
class RestApi {
  /// Created a [RestApi] instance
  const RestApi({
    required http.Client client,
    required Uri apiUrl,
    required Map<String, String> headers,
    Duration timeout = const Duration(seconds: 30),
  })  : _client = client,
        _apiUrl = apiUrl,
        _headers = headers,
        _timeout = timeout;

  final http.Client _client;
  final Uri _apiUrl;
  final Map<String, String> _headers;
  final Duration _timeout;

  Map<String, String> get _defaultHeaders => {
        // Add here application required global headers
        ..._headers,
      };

  /// Runs a get request and returns the [http.Response]
  ///
  /// If the response has an error it converts the error into an ApiException
  Future<http.Response> get(Uri uri) => _handleRequest(
        HttpMethod.get,
        _apiUrl.merge(uri),
      );

  /// Runs a post request and returns the [http.Response]
  ///
  /// If the response has an error it converts the error into an ApiException
  Future<http.Response> post(Uri uri, [Object? object]) => _handleRequest(
        HttpMethod.post,
        _apiUrl.merge(uri),
        body: object,
      );

  /// Runs a delete request and returns the [http.Response]
  ///
  /// If the response has an error it converts the error into an ApiException
  Future<http.Response> delete(Uri uri, [Object? object]) => _handleRequest(
        HttpMethod.delete,
        _apiUrl.merge(uri),
        body: object,
      );

  /// Runs a patch request and returns the [http.Response]
  ///
  /// If the response has an error it converts the error into an ApiException
  Future<http.Response> patch(Uri uri, [Object? object]) => _handleRequest(
        HttpMethod.patch,
        _apiUrl.merge(uri),
        body: object,
      );

  /// Runs a put request and returns the [http.Response]
  ///
  /// If the response has an error it converts the error into an ApiException
  Future<http.Response> put(Uri uri, [Object? object]) => _handleRequest(
        HttpMethod.put,
        _apiUrl.merge(uri),
        body: object,
      );

  @protected
  Future<http.Response> _callHttpMethod(
    HttpMethod method,
    Uri uri, {
    Object? body,
  }) =>
      switch (method) {
        HttpMethod.get => _client.get(uri, headers: _defaultHeaders),
        HttpMethod.post =>
          _client.post(uri, body: body, headers: _defaultHeaders),
        HttpMethod.delete =>
          _client.delete(uri, body: body, headers: _defaultHeaders),
        HttpMethod.patch =>
          _client.patch(uri, body: body, headers: _defaultHeaders),
        HttpMethod.put =>
          _client.put(uri, body: body, headers: _defaultHeaders),
      };

  Future<http.Response> _handleRequest(
    HttpMethod method,
    Uri uri, {
    Object? body,
  }) async {
    final http.Response response;
    try {
      final futureResponse = _callHttpMethod(method, uri, body: body);

      response = await futureResponse.timeout(_timeout);
    } on SocketException catch (e, s) {
      if ([60, 8].contains(e.osError?.errorCode)) {
        throw NoConnectionApiException(error: e, stackTrace: s);
      }
      rethrow;
    } on TimeoutException catch (e, s) {
      throw TimeoutApiException(error: e, stackTrace: s);
    }

    _apiLogging(response);

    // TODO(rurickdev): check for other specific error codes when needed
    return switch (response.statusCode) {
      (>= 200 && < 300) || == 304 => response,
      // TODO(rurickdev): define how to handle redirects
      >= 300 && < 400 => throw const RedirectApiException(),
      HttpStatus.badRequest => throw const BadRequestApiException(),
      HttpStatus.unauthorized => throw const UnauthorizedApiException(),
      HttpStatus.forbidden => throw const ForbiddenApiException(),
      HttpStatus.notFound => throw const NotFoundApiException(),
      HttpStatus.failedDependency => throw const FailedDependencyApiException(),
      >= 400 && < 500 =>
        throw UnknownCodeApiException(httpStatusCode: response.statusCode),
      >= 500 && < 600 => throw const InternalServerApiException(),
      _ => throw UnknownCodeApiException(httpStatusCode: response.statusCode),
    };
  }

  void _apiLogging(http.Response response) {
    final request = response.request;
    var message = '$request';
    if (request is http.Request && request.body.isNotEmpty) {
      message += '\nbody:\n'
          '${request.body}';
    }
    message += '\nresponse:\n'
        '${response.statusCode}';
    final responseContentType = response.headers[HttpHeaders.contentTypeHeader];
    if (responseContentType == ContentType.json.toString()) {
      message += '\n${response.body}';
    }
    Logger('http').info(message);
  }
}
