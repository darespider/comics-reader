import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:kapoow/src/features/rest_api/domain/api_exceptions.dart';
import 'package:kapoow/src/features/rest_api/domain/json_map.dart';
import 'package:kapoow/src/features/rest_api/domain/rest_api/rest_api.dart';

/// This class responsibility is to do the api request using a [RestApi] object
/// and parse the response's json to DTOs.
///
/// It should not parse the responses to domain neither use a [http.Client]
/// object directly.
class RestJsonApi {
  /// Created a [RestJsonApi] instance
  RestJsonApi(this._api);

  final RestApi _api;

  /// Runs the an [_api] request and tries to parse the response body if a
  /// [dtoParser] is provide
  ///
  /// If the response has an error it converts the error into an ApiException
  Future<DtoType> get<DtoType, JsonType>(
    Uri uri, {
    required JsonParser<DtoType, JsonType>? dtoParser,
  }) async {
    if (dtoParser == null) {
      await _api.get(uri);
      return Future<DtoType>.value();
    } else {
      return _decodeJsonResponse(await _api.get(uri), dtoParser);
    }
  }

  /// Runs the an [_api] request and tries to parse the response body if a
  /// [dtoParser] is provide
  ///
  /// If the response has an error it converts the error into an ApiException
  Future<DtoType> post<DtoType, JsonType>(
    Uri uri, {
    Object? body,
    JsonParser<DtoType, JsonType>? dtoParser,
  }) async {
    final jsonBody = jsonEncode(body);
    if (dtoParser == null) {
      await _api.post(uri, jsonBody);
      return Future<DtoType>.value();
    } else {
      return _decodeJsonResponse(await _api.post(uri, jsonBody), dtoParser);
    }
  }

  /// Runs the an [_api] request and tries to parse the response body if a
  /// [dtoParser] is provide
  ///
  /// If the response has an error it converts the error into an ApiException
  Future<DtoType> delete<DtoType, JsonType>(
    Uri uri, {
    Object? body,
    JsonParser<DtoType, JsonType>? dtoParser,
  }) async {
    final jsonBody = jsonEncode(body);
    if (dtoParser == null) {
      await _api.delete(uri, jsonBody);
      return Future<DtoType>.value();
    } else {
      return _decodeJsonResponse(await _api.delete(uri, jsonBody), dtoParser);
    }
  }

  /// Runs the an [_api] request and tries to parse the response body if a
  /// [dtoParser] is provide
  ///
  /// If the response has an error it converts the error into an ApiException
  Future<DtoType> patch<DtoType, JsonType>(
    Uri uri, {
    Object? body,
    JsonParser<DtoType, JsonType>? dtoParser,
  }) async {
    final jsonBody = jsonEncode(body);
    if (dtoParser == null) {
      await _api.patch(uri, jsonBody);
      return Future<DtoType>.value();
    } else {
      return _decodeJsonResponse(await _api.patch(uri, jsonBody), dtoParser);
    }
  }

  /// Runs the an [_api] request and tries to parse the response body if a
  /// [dtoParser] is provide
  ///
  /// If the response has an error it converts the error into an ApiException
  Future<DtoType> put<DtoType, JsonType>(
    Uri uri, {
    Object? body,
    JsonParser<DtoType, JsonType>? dtoParser,
  }) async {
    final jsonBody = jsonEncode(body);
    if (dtoParser == null) {
      await _api.put(uri, jsonBody);
      return Future<DtoType>.value();
    } else {
      return _decodeJsonResponse(await _api.put(uri, jsonBody), dtoParser);
    }
  }

  DtoType _decodeJsonResponse<DtoType, JsonType>(
    http.Response response,
    JsonParser<DtoType, JsonType> dtoParser,
  ) {
    try {
      final decoded = jsonDecode(response.body) as JsonType;
      return dtoParser(decoded);
    } on FormatException catch (e, s) {
      throw FormatApiException<DtoType>(
        error: e,
        stackTrace: s,
      );
    }
  }
}
