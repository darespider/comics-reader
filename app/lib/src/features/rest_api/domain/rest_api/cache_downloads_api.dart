import 'dart:async';
import 'dart:typed_data';

import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:kapoow/src/features/rest_api/utils/extensions.dart';
import 'package:logging/logging.dart';

/// Wrapper of [DefaultCacheManager] from "flutter_cache_manager" package.
///
/// It allows to cache images without passing the headers on every call.
class CacheDownloadsApi {
  /// Creates a [CacheDownloadsApi] instance
  const CacheDownloadsApi({
    required this.headers,
    required this.apiUrl,
  });

  /// Header to use on the cached requests
  final Map<String, String> headers;

  /// Base Uri to do the requests
  final Uri apiUrl;

  static final _cacheManager = DefaultCacheManager();

  static final _logger = Logger('CacheService');

  /// Read the bytes of the cached image using [DefaultCacheManager], if the
  /// read fails it will return null
  Future<Uint8List?> tryGetImageFile(Uri url) async {
    final uri = apiUrl.merge(url);
    final fileInfoStream =
        _cacheManager.getImageFile(uri.toString(), headers: headers);

    /// As implemented on [CacheManager] from package, the last item from the
    /// stream is always a [FileInfo] or an error
    try {
      final fileInfo = (await fileInfoStream.last) as FileInfo;
      return fileInfo.file.readAsBytesSync();
    } catch (e, s) {
      _logger.warning('Failed to read file [$url] from cache', e, s);
      return null;
    }
  }

  /// Saves the file [bytes] and links it to the [url] for future reads
  Future<void> putFile(Uri url, Uint8List bytes) async {
    final uri = apiUrl.merge(url);
    await _cacheManager.putFile(uri.toString(), bytes);
  }
}
