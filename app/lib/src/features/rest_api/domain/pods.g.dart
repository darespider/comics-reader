// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pods.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$sessionAsyncHash() => r'dc5b518e43788d31018437fbd827df8ce99c8a7c';

/// Provides the session data as async value
///
/// Copied from [sessionAsync].
@ProviderFor(sessionAsync)
final sessionAsyncPod = FutureProvider<SessionData>.internal(
  sessionAsync,
  name: r'sessionAsyncPod',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$sessionAsyncHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef SessionAsyncRef = FutureProviderRef<SessionData>;
String _$httpClientHash() => r'f3c4e4bddbb8a0f2b4a3723f2f77d50060156537';

/// Provides an http client
///
/// Copied from [httpClient].
@ProviderFor(httpClient)
final httpClientPod = Provider<Client>.internal(
  httpClient,
  name: r'httpClientPod',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$httpClientHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef HttpClientRef = ProviderRef<Client>;
String _$thumbnailUriHash() => r'3dbae4432bfcfa7725d3d16ea13c193865982624';

/// Provides the base uri path for thumbnails
///
/// Copied from [thumbnailUri].
@ProviderFor(thumbnailUri)
final thumbnailUriPod = Provider<Uri>.internal(
  thumbnailUri,
  name: r'thumbnailUriPod',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$thumbnailUriHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef ThumbnailUriRef = ProviderRef<Uri>;
String _$headersHash() => r'2da71bad7485f8bcd507a7c58da633f63e95558b';

/// Provides the headers for the api request and adds the token
///
/// Copied from [headers].
@ProviderFor(headers)
final headersPod = Provider<Map<String, String>>.internal(
  headers,
  name: r'headersPod',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$headersHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef HeadersRef = ProviderRef<Map<String, String>>;
String _$restApiHash() => r'8b2bb02c07f185637ef116a64d7e2d90293d4109';

/// Provides the rest api client
///
/// Copied from [restApi].
@ProviderFor(restApi)
final restApiPod = Provider<RestApi>.internal(
  restApi,
  name: r'restApiPod',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$restApiHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef RestApiRef = ProviderRef<RestApi>;
String _$restJsonApiHash() => r'a386470978f63d63e2309cf40333ac561ce67d72';

/// Provides the rest json api client
///
/// Copied from [restJsonApi].
@ProviderFor(restJsonApi)
final restJsonApiPod = Provider<RestJsonApi>.internal(
  restJsonApi,
  name: r'restJsonApiPod',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$restJsonApiHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef RestJsonApiRef = ProviderRef<RestJsonApi>;
String _$downloadsApiHash() => r'48516240d69da8674fedc23b3fd3a486e697db5f';

/// Provides the rest json api client
///
/// Copied from [downloadsApi].
@ProviderFor(downloadsApi)
final downloadsApiPod = Provider<DownloadsApi>.internal(
  downloadsApi,
  name: r'downloadsApiPod',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$downloadsApiHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef DownloadsApiRef = ProviderRef<DownloadsApi>;
String _$cacheDownloadApiHash() => r'6d1410729e44746ffc4611d5bfb6a33ee6ffdc31';

/// See also [cacheDownloadApi].
@ProviderFor(cacheDownloadApi)
final cacheDownloadApiPod = Provider<CacheDownloadsApi>.internal(
  cacheDownloadApi,
  name: r'cacheDownloadApiPod',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$cacheDownloadApiHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef CacheDownloadApiRef = ProviderRef<CacheDownloadsApi>;
String _$basicTokenHash() => r'20975dba320f0ae12d8b4169772746f338d2be76';

/// Provides the session token
///
/// Copied from [BasicToken].
@ProviderFor(BasicToken)
final basicTokenPod = NotifierProvider<BasicToken, String>.internal(
  BasicToken.new,
  name: r'basicTokenPod',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$basicTokenHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$BasicToken = Notifier<String>;
String _$baseUriHash() => r'393933ce360f250f382621fe9987c469e0b74aa3';

/// Provides the base url read from the session
///
/// Copied from [BaseUri].
@ProviderFor(BaseUri)
final baseUriPod = NotifierProvider<BaseUri, Uri>.internal(
  BaseUri.new,
  name: r'baseUriPod',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$baseUriHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$BaseUri = Notifier<Uri>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
