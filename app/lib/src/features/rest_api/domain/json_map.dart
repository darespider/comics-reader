/// Representation of a Json in Dart
///
/// ```js
/// {
///   ...
/// }
/// ```
typedef JsonMap = Map<String, dynamic>;

/// Representation of a Json List in Dart
///
/// ```js
/// [
///   { ... },
///   { ... },
///   { ... }
/// ]
/// ```
typedef JsonList = List<dynamic>;

/// Function that accepts a JsonList or JsonMap and transforms it to a Dto
typedef JsonParser<DtoType, JsonType> = DtoType Function(JsonType json);

/// Function definition that transforms a [JsonMap] into a Dto
typedef JsonMapParser<DtoType> = JsonParser<DtoType, JsonMap>;

/// Function definition that transforms a [JsonList] into a Dto
typedef JsonListParser<DtoType> = JsonParser<DtoType, JsonList>;

/// Common http methods, useful to reuse logic for handling the responses
enum HttpMethod {
  /// Represents GET http method
  get,

  /// Represents POST http method
  post,

  /// Represents DELETE http method
  delete,

  /// Represents PATCH http method
  patch,

  /// Represents PUT http method
  put,
}

/// Auxiliar adapter that allow to extract a key's value list from a json
/// and return a function that converts every json from the list into a dto
///
/// taking this example
/// ```js
/// {
///   "key1": value,
///   "key2": value,
///   "key3": [
///     { ... },
///     { ... },
///     { ... },
///   ],
/// }
/// ```
/// the dart result is a function that get the list from `key3` and returns
/// the corresponding list of dtos
JsonMapParser<List<DtoType>> listByKeyAdapter<DtoType>(
  String listField,
  JsonMapParser<DtoType> dtoParser,
) =>
    (json) => (json[listField] as List)
        .cast<Map<String, dynamic>>()
        .map(dtoParser)
        .toList();

/// Auxiliar extension that allow to convert every json from the list into a dto
extension ListAdapter<DtoType> on JsonMapParser<DtoType> {
  /// taking this example
  /// ```js
  /// [
  ///   { ... },
  ///   { ... },
  ///   { ... },
  /// ]
  /// ```
  /// the dart result is a function that get the list and returns the
  /// corresponding list of dtos
  JsonListParser<List<DtoType>> get list =>
      (json) => json.cast<JsonMap>().map(this).toList();
}
