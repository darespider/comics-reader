// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'shared_libraries_update_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$SharedLibrariesUpdateDtoImpl _$$SharedLibrariesUpdateDtoImplFromJson(
        Map<String, dynamic> json) =>
    _$SharedLibrariesUpdateDtoImpl(
      all: json['all'] as bool,
      libraryIds:
          (json['libraryIds'] as List<dynamic>).map((e) => e as String).toSet(),
    );

Map<String, dynamic> _$$SharedLibrariesUpdateDtoImplToJson(
        _$SharedLibrariesUpdateDtoImpl instance) =>
    <String, dynamic>{
      'all': instance.all,
      'libraryIds': instance.libraryIds.toList(),
    };
