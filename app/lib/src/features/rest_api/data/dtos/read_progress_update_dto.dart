// ignore_for_file: public_member_api_docs
import 'package:freezed_annotation/freezed_annotation.dart';

part 'read_progress_update_dto.freezed.dart';
part 'read_progress_update_dto.g.dart';

@Freezed(copyWith: false)
class ReadProgressUpdateDto with _$ReadProgressUpdateDto {
  const factory ReadProgressUpdateDto({
    int? page,
    bool? completed,
  }) = _ReadProgressUpdateDto;

  factory ReadProgressUpdateDto.fromJson(Map<String, dynamic> json) =>
      _$ReadProgressUpdateDtoFromJson(json);
}
