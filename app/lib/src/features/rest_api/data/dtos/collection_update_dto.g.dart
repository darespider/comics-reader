// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'collection_update_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$CollectionUpdateDtoImpl _$$CollectionUpdateDtoImplFromJson(
        Map<String, dynamic> json) =>
    _$CollectionUpdateDtoImpl(
      name: json['name'] as String,
      ordered: json['ordered'] as bool,
      seriesIds:
          (json['seriesIds'] as List<dynamic>).map((e) => e as String).toList(),
    );

Map<String, dynamic> _$$CollectionUpdateDtoImplToJson(
        _$CollectionUpdateDtoImpl instance) =>
    <String, dynamic>{
      'name': instance.name,
      'ordered': instance.ordered,
      'seriesIds': instance.seriesIds,
    };
