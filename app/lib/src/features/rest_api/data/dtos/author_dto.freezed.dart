// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'author_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

AuthorDto _$AuthorDtoFromJson(Map<String, dynamic> json) {
  return _AuthorDto.fromJson(json);
}

/// @nodoc
mixin _$AuthorDto {
  String get name => throw _privateConstructorUsedError;
  String get role => throw _privateConstructorUsedError;

  /// Serializes this AuthorDto to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
}

/// @nodoc
@JsonSerializable()
class _$AuthorDtoImpl implements _AuthorDto {
  const _$AuthorDtoImpl({required this.name, required this.role});

  factory _$AuthorDtoImpl.fromJson(Map<String, dynamic> json) =>
      _$$AuthorDtoImplFromJson(json);

  @override
  final String name;
  @override
  final String role;

  @override
  String toString() {
    return 'AuthorDto(name: $name, role: $role)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$AuthorDtoImpl &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.role, role) || other.role == role));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => Object.hash(runtimeType, name, role);

  @override
  Map<String, dynamic> toJson() {
    return _$$AuthorDtoImplToJson(
      this,
    );
  }
}

abstract class _AuthorDto implements AuthorDto {
  const factory _AuthorDto(
      {required final String name,
      required final String role}) = _$AuthorDtoImpl;

  factory _AuthorDto.fromJson(Map<String, dynamic> json) =
      _$AuthorDtoImpl.fromJson;

  @override
  String get name;
  @override
  String get role;
}
