// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'shared_library_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$SharedLibraryDtoImpl _$$SharedLibraryDtoImplFromJson(
        Map<String, dynamic> json) =>
    _$SharedLibraryDtoImpl(
      id: json['id'] as String,
    );

Map<String, dynamic> _$$SharedLibraryDtoImplToJson(
        _$SharedLibraryDtoImpl instance) =>
    <String, dynamic>{
      'id': instance.id,
    };
