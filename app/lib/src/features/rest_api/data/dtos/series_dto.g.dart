// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'series_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$SeriesDtoImpl _$$SeriesDtoImplFromJson(Map<String, dynamic> json) =>
    _$SeriesDtoImpl(
      id: json['id'] as String,
      libraryId: json['libraryId'] as String,
      name: json['name'] as String,
      url: json['url'] as String,
      created: DateTime.parse(json['created'] as String),
      lastModified: DateTime.parse(json['lastModified'] as String),
      fileLastModified: DateTime.parse(json['fileLastModified'] as String),
      booksCount: (json['booksCount'] as num).toInt(),
      booksReadCount: (json['booksReadCount'] as num).toInt(),
      booksUnreadCount: (json['booksUnreadCount'] as num).toInt(),
      booksInProgressCount: (json['booksInProgressCount'] as num).toInt(),
      metadata:
          SeriesMetadataDto.fromJson(json['metadata'] as Map<String, dynamic>),
      booksMetadata: BookMetadataAggregationDto.fromJson(
          json['booksMetadata'] as Map<String, dynamic>),
      deleted: json['deleted'] as bool,
      oneshot: json['oneshot'] as bool? ?? false,
    );

Map<String, dynamic> _$$SeriesDtoImplToJson(_$SeriesDtoImpl instance) =>
    <String, dynamic>{
      'id': instance.id,
      'libraryId': instance.libraryId,
      'name': instance.name,
      'url': instance.url,
      'created': instance.created.toIso8601String(),
      'lastModified': instance.lastModified.toIso8601String(),
      'fileLastModified': instance.fileLastModified.toIso8601String(),
      'booksCount': instance.booksCount,
      'booksReadCount': instance.booksReadCount,
      'booksUnreadCount': instance.booksUnreadCount,
      'booksInProgressCount': instance.booksInProgressCount,
      'metadata': instance.metadata,
      'booksMetadata': instance.booksMetadata,
      'deleted': instance.deleted,
      'oneshot': instance.oneshot,
    };
