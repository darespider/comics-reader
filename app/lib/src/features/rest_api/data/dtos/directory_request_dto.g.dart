// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'directory_request_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$DirectoryRequestDtoImpl _$$DirectoryRequestDtoImplFromJson(
        Map<String, dynamic> json) =>
    _$DirectoryRequestDtoImpl(
      path: json['path'] as String,
    );

Map<String, dynamic> _$$DirectoryRequestDtoImplToJson(
        _$DirectoryRequestDtoImpl instance) =>
    <String, dynamic>{
      'path': instance.path,
    };
