// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'user_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

UserDto _$UserDtoFromJson(Map<String, dynamic> json) {
  return _UserDto.fromJson(json);
}

/// @nodoc
mixin _$UserDto {
  String get id => throw _privateConstructorUsedError;
  String get email => throw _privateConstructorUsedError;
  Set<String> get roles => throw _privateConstructorUsedError;
  bool get sharedAllLibraries => throw _privateConstructorUsedError;
  Set<String> get sharedLibrariesIds => throw _privateConstructorUsedError;
  Set<String> get labelsAllow => throw _privateConstructorUsedError;
  Set<String> get labelsExclude => throw _privateConstructorUsedError;
  AgeRestrictionDto? get ageRestriction => throw _privateConstructorUsedError;

  /// Serializes this UserDto to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
}

/// @nodoc
@JsonSerializable()
class _$UserDtoImpl implements _UserDto {
  const _$UserDtoImpl(
      {required this.id,
      required this.email,
      required final Set<String> roles,
      required this.sharedAllLibraries,
      required final Set<String> sharedLibrariesIds,
      required final Set<String> labelsAllow,
      required final Set<String> labelsExclude,
      this.ageRestriction})
      : _roles = roles,
        _sharedLibrariesIds = sharedLibrariesIds,
        _labelsAllow = labelsAllow,
        _labelsExclude = labelsExclude;

  factory _$UserDtoImpl.fromJson(Map<String, dynamic> json) =>
      _$$UserDtoImplFromJson(json);

  @override
  final String id;
  @override
  final String email;
  final Set<String> _roles;
  @override
  Set<String> get roles {
    if (_roles is EqualUnmodifiableSetView) return _roles;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableSetView(_roles);
  }

  @override
  final bool sharedAllLibraries;
  final Set<String> _sharedLibrariesIds;
  @override
  Set<String> get sharedLibrariesIds {
    if (_sharedLibrariesIds is EqualUnmodifiableSetView)
      return _sharedLibrariesIds;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableSetView(_sharedLibrariesIds);
  }

  final Set<String> _labelsAllow;
  @override
  Set<String> get labelsAllow {
    if (_labelsAllow is EqualUnmodifiableSetView) return _labelsAllow;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableSetView(_labelsAllow);
  }

  final Set<String> _labelsExclude;
  @override
  Set<String> get labelsExclude {
    if (_labelsExclude is EqualUnmodifiableSetView) return _labelsExclude;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableSetView(_labelsExclude);
  }

  @override
  final AgeRestrictionDto? ageRestriction;

  @override
  String toString() {
    return 'UserDto(id: $id, email: $email, roles: $roles, sharedAllLibraries: $sharedAllLibraries, sharedLibrariesIds: $sharedLibrariesIds, labelsAllow: $labelsAllow, labelsExclude: $labelsExclude, ageRestriction: $ageRestriction)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$UserDtoImpl &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.email, email) || other.email == email) &&
            const DeepCollectionEquality().equals(other._roles, _roles) &&
            (identical(other.sharedAllLibraries, sharedAllLibraries) ||
                other.sharedAllLibraries == sharedAllLibraries) &&
            const DeepCollectionEquality()
                .equals(other._sharedLibrariesIds, _sharedLibrariesIds) &&
            const DeepCollectionEquality()
                .equals(other._labelsAllow, _labelsAllow) &&
            const DeepCollectionEquality()
                .equals(other._labelsExclude, _labelsExclude) &&
            (identical(other.ageRestriction, ageRestriction) ||
                other.ageRestriction == ageRestriction));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      id,
      email,
      const DeepCollectionEquality().hash(_roles),
      sharedAllLibraries,
      const DeepCollectionEquality().hash(_sharedLibrariesIds),
      const DeepCollectionEquality().hash(_labelsAllow),
      const DeepCollectionEquality().hash(_labelsExclude),
      ageRestriction);

  @override
  Map<String, dynamic> toJson() {
    return _$$UserDtoImplToJson(
      this,
    );
  }
}

abstract class _UserDto implements UserDto {
  const factory _UserDto(
      {required final String id,
      required final String email,
      required final Set<String> roles,
      required final bool sharedAllLibraries,
      required final Set<String> sharedLibrariesIds,
      required final Set<String> labelsAllow,
      required final Set<String> labelsExclude,
      final AgeRestrictionDto? ageRestriction}) = _$UserDtoImpl;

  factory _UserDto.fromJson(Map<String, dynamic> json) = _$UserDtoImpl.fromJson;

  @override
  String get id;
  @override
  String get email;
  @override
  Set<String> get roles;
  @override
  bool get sharedAllLibraries;
  @override
  Set<String> get sharedLibrariesIds;
  @override
  Set<String> get labelsAllow;
  @override
  Set<String> get labelsExclude;
  @override
  AgeRestrictionDto? get ageRestriction;
}
