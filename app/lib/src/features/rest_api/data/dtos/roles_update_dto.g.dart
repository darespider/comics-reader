// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'roles_update_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$RolesUpdateDtoImpl _$$RolesUpdateDtoImplFromJson(Map<String, dynamic> json) =>
    _$RolesUpdateDtoImpl(
      roles: (json['roles'] as List<dynamic>).map((e) => e as String).toList(),
    );

Map<String, dynamic> _$$RolesUpdateDtoImplToJson(
        _$RolesUpdateDtoImpl instance) =>
    <String, dynamic>{
      'roles': instance.roles,
    };
