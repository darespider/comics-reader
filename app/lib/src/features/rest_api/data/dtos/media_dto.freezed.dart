// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'media_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

MediaDto _$MediaDtoFromJson(Map<String, dynamic> json) {
  return _MediaDto.fromJson(json);
}

/// @nodoc
mixin _$MediaDto {
  String get status => throw _privateConstructorUsedError;
  String get mediaType => throw _privateConstructorUsedError;
  int get pagesCount => throw _privateConstructorUsedError;
  String get comment => throw _privateConstructorUsedError;

  /// Serializes this MediaDto to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
}

/// @nodoc
@JsonSerializable()
class _$MediaDtoImpl implements _MediaDto {
  const _$MediaDtoImpl(
      {required this.status,
      required this.mediaType,
      required this.pagesCount,
      required this.comment});

  factory _$MediaDtoImpl.fromJson(Map<String, dynamic> json) =>
      _$$MediaDtoImplFromJson(json);

  @override
  final String status;
  @override
  final String mediaType;
  @override
  final int pagesCount;
  @override
  final String comment;

  @override
  String toString() {
    return 'MediaDto(status: $status, mediaType: $mediaType, pagesCount: $pagesCount, comment: $comment)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$MediaDtoImpl &&
            (identical(other.status, status) || other.status == status) &&
            (identical(other.mediaType, mediaType) ||
                other.mediaType == mediaType) &&
            (identical(other.pagesCount, pagesCount) ||
                other.pagesCount == pagesCount) &&
            (identical(other.comment, comment) || other.comment == comment));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode =>
      Object.hash(runtimeType, status, mediaType, pagesCount, comment);

  @override
  Map<String, dynamic> toJson() {
    return _$$MediaDtoImplToJson(
      this,
    );
  }
}

abstract class _MediaDto implements MediaDto {
  const factory _MediaDto(
      {required final String status,
      required final String mediaType,
      required final int pagesCount,
      required final String comment}) = _$MediaDtoImpl;

  factory _MediaDto.fromJson(Map<String, dynamic> json) =
      _$MediaDtoImpl.fromJson;

  @override
  String get status;
  @override
  String get mediaType;
  @override
  int get pagesCount;
  @override
  String get comment;
}
