// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'book_metadata_update_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

BookMetadataUpdateDto _$BookMetadataUpdateDtoFromJson(
    Map<String, dynamic> json) {
  return _BookMetadataUpdateDto.fromJson(json);
}

/// @nodoc
mixin _$BookMetadataUpdateDto {
  String? get title => throw _privateConstructorUsedError;
  bool? get titleLock => throw _privateConstructorUsedError;
  String? get summary => throw _privateConstructorUsedError;
  bool? get summaryLock => throw _privateConstructorUsedError;
  String? get number => throw _privateConstructorUsedError;
  bool? get numberLock => throw _privateConstructorUsedError;
  double? get numberSort => throw _privateConstructorUsedError;
  bool? get numberSortLock => throw _privateConstructorUsedError;
  bool? get releaseDateLock => throw _privateConstructorUsedError;
  bool? get authorsLock => throw _privateConstructorUsedError;
  bool? get tagsLock => throw _privateConstructorUsedError;
  Set<String>? get tags => throw _privateConstructorUsedError;
  AuthorUpdateDto? get authors => throw _privateConstructorUsedError;
  DateTime? get releaseDate => throw _privateConstructorUsedError;

  /// Serializes this BookMetadataUpdateDto to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
}

/// @nodoc
@JsonSerializable()
class _$BookMetadataUpdateDtoImpl implements _BookMetadataUpdateDto {
  const _$BookMetadataUpdateDtoImpl(
      {this.title,
      this.titleLock,
      this.summary,
      this.summaryLock,
      this.number,
      this.numberLock,
      this.numberSort,
      this.numberSortLock,
      this.releaseDateLock,
      this.authorsLock,
      this.tagsLock,
      final Set<String>? tags,
      this.authors,
      this.releaseDate})
      : _tags = tags;

  factory _$BookMetadataUpdateDtoImpl.fromJson(Map<String, dynamic> json) =>
      _$$BookMetadataUpdateDtoImplFromJson(json);

  @override
  final String? title;
  @override
  final bool? titleLock;
  @override
  final String? summary;
  @override
  final bool? summaryLock;
  @override
  final String? number;
  @override
  final bool? numberLock;
  @override
  final double? numberSort;
  @override
  final bool? numberSortLock;
  @override
  final bool? releaseDateLock;
  @override
  final bool? authorsLock;
  @override
  final bool? tagsLock;
  final Set<String>? _tags;
  @override
  Set<String>? get tags {
    final value = _tags;
    if (value == null) return null;
    if (_tags is EqualUnmodifiableSetView) return _tags;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableSetView(value);
  }

  @override
  final AuthorUpdateDto? authors;
  @override
  final DateTime? releaseDate;

  @override
  String toString() {
    return 'BookMetadataUpdateDto(title: $title, titleLock: $titleLock, summary: $summary, summaryLock: $summaryLock, number: $number, numberLock: $numberLock, numberSort: $numberSort, numberSortLock: $numberSortLock, releaseDateLock: $releaseDateLock, authorsLock: $authorsLock, tagsLock: $tagsLock, tags: $tags, authors: $authors, releaseDate: $releaseDate)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$BookMetadataUpdateDtoImpl &&
            (identical(other.title, title) || other.title == title) &&
            (identical(other.titleLock, titleLock) ||
                other.titleLock == titleLock) &&
            (identical(other.summary, summary) || other.summary == summary) &&
            (identical(other.summaryLock, summaryLock) ||
                other.summaryLock == summaryLock) &&
            (identical(other.number, number) || other.number == number) &&
            (identical(other.numberLock, numberLock) ||
                other.numberLock == numberLock) &&
            (identical(other.numberSort, numberSort) ||
                other.numberSort == numberSort) &&
            (identical(other.numberSortLock, numberSortLock) ||
                other.numberSortLock == numberSortLock) &&
            (identical(other.releaseDateLock, releaseDateLock) ||
                other.releaseDateLock == releaseDateLock) &&
            (identical(other.authorsLock, authorsLock) ||
                other.authorsLock == authorsLock) &&
            (identical(other.tagsLock, tagsLock) ||
                other.tagsLock == tagsLock) &&
            const DeepCollectionEquality().equals(other._tags, _tags) &&
            (identical(other.authors, authors) || other.authors == authors) &&
            (identical(other.releaseDate, releaseDate) ||
                other.releaseDate == releaseDate));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      title,
      titleLock,
      summary,
      summaryLock,
      number,
      numberLock,
      numberSort,
      numberSortLock,
      releaseDateLock,
      authorsLock,
      tagsLock,
      const DeepCollectionEquality().hash(_tags),
      authors,
      releaseDate);

  @override
  Map<String, dynamic> toJson() {
    return _$$BookMetadataUpdateDtoImplToJson(
      this,
    );
  }
}

abstract class _BookMetadataUpdateDto implements BookMetadataUpdateDto {
  const factory _BookMetadataUpdateDto(
      {final String? title,
      final bool? titleLock,
      final String? summary,
      final bool? summaryLock,
      final String? number,
      final bool? numberLock,
      final double? numberSort,
      final bool? numberSortLock,
      final bool? releaseDateLock,
      final bool? authorsLock,
      final bool? tagsLock,
      final Set<String>? tags,
      final AuthorUpdateDto? authors,
      final DateTime? releaseDate}) = _$BookMetadataUpdateDtoImpl;

  factory _BookMetadataUpdateDto.fromJson(Map<String, dynamic> json) =
      _$BookMetadataUpdateDtoImpl.fromJson;

  @override
  String? get title;
  @override
  bool? get titleLock;
  @override
  String? get summary;
  @override
  bool? get summaryLock;
  @override
  String? get number;
  @override
  bool? get numberLock;
  @override
  double? get numberSort;
  @override
  bool? get numberSortLock;
  @override
  bool? get releaseDateLock;
  @override
  bool? get authorsLock;
  @override
  bool? get tagsLock;
  @override
  Set<String>? get tags;
  @override
  AuthorUpdateDto? get authors;
  @override
  DateTime? get releaseDate;
}
