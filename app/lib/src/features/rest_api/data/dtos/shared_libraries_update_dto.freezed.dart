// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'shared_libraries_update_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

SharedLibrariesUpdateDto _$SharedLibrariesUpdateDtoFromJson(
    Map<String, dynamic> json) {
  return _SharedLibrariesUpdateDto.fromJson(json);
}

/// @nodoc
mixin _$SharedLibrariesUpdateDto {
  bool get all => throw _privateConstructorUsedError;
  Set<String> get libraryIds => throw _privateConstructorUsedError;

  /// Serializes this SharedLibrariesUpdateDto to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
}

/// @nodoc
@JsonSerializable()
class _$SharedLibrariesUpdateDtoImpl implements _SharedLibrariesUpdateDto {
  const _$SharedLibrariesUpdateDtoImpl(
      {required this.all, required final Set<String> libraryIds})
      : _libraryIds = libraryIds;

  factory _$SharedLibrariesUpdateDtoImpl.fromJson(Map<String, dynamic> json) =>
      _$$SharedLibrariesUpdateDtoImplFromJson(json);

  @override
  final bool all;
  final Set<String> _libraryIds;
  @override
  Set<String> get libraryIds {
    if (_libraryIds is EqualUnmodifiableSetView) return _libraryIds;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableSetView(_libraryIds);
  }

  @override
  String toString() {
    return 'SharedLibrariesUpdateDto(all: $all, libraryIds: $libraryIds)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SharedLibrariesUpdateDtoImpl &&
            (identical(other.all, all) || other.all == all) &&
            const DeepCollectionEquality()
                .equals(other._libraryIds, _libraryIds));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => Object.hash(
      runtimeType, all, const DeepCollectionEquality().hash(_libraryIds));

  @override
  Map<String, dynamic> toJson() {
    return _$$SharedLibrariesUpdateDtoImplToJson(
      this,
    );
  }
}

abstract class _SharedLibrariesUpdateDto implements SharedLibrariesUpdateDto {
  const factory _SharedLibrariesUpdateDto(
      {required final bool all,
      required final Set<String> libraryIds}) = _$SharedLibrariesUpdateDtoImpl;

  factory _SharedLibrariesUpdateDto.fromJson(Map<String, dynamic> json) =
      _$SharedLibrariesUpdateDtoImpl.fromJson;

  @override
  bool get all;
  @override
  Set<String> get libraryIds;
}
