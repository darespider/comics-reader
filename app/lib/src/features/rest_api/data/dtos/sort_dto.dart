// ignore_for_file: public_member_api_docs
import 'package:freezed_annotation/freezed_annotation.dart';

part 'sort_dto.freezed.dart';
part 'sort_dto.g.dart';

@Freezed(copyWith: false)
class SortDto with _$SortDto {
  const factory SortDto({
    required bool sorted,
    required bool unsorted,
    required bool empty,
  }) = _SortDto;

  factory SortDto.fromJson(Map<String, dynamic> json) =>
      _$SortDtoFromJson(json);
}
