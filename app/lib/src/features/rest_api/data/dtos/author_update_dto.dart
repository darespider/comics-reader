// ignore_for_file: public_member_api_docs
import 'package:freezed_annotation/freezed_annotation.dart';

part 'author_update_dto.freezed.dart';
part 'author_update_dto.g.dart';

@Freezed(copyWith: false)
class AuthorUpdateDto with _$AuthorUpdateDto {
  const factory AuthorUpdateDto({
    required String name,
    required String role,
  }) = _AuthorUpdateDto;

  factory AuthorUpdateDto.fromJson(Map<String, dynamic> json) =>
      _$AuthorUpdateDtoFromJson(json);
}
