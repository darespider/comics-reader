// ignore_for_file: public_member_api_docs
import 'package:freezed_annotation/freezed_annotation.dart';

part 'roles_update_dto.freezed.dart';
part 'roles_update_dto.g.dart';

@Freezed(copyWith: false)
class RolesUpdateDto with _$RolesUpdateDto {
  const factory RolesUpdateDto({
    required List<String> roles,
  }) = _RolesUpdateDto;

  factory RolesUpdateDto.fromJson(Map<String, dynamic> json) =>
      _$RolesUpdateDtoFromJson(json);
}
