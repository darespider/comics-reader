// ignore_for_file: public_member_api_docs
import 'package:freezed_annotation/freezed_annotation.dart';

part 'shared_library_dto.freezed.dart';
part 'shared_library_dto.g.dart';

@Freezed(copyWith: false)
class SharedLibraryDto with _$SharedLibraryDto {
  const factory SharedLibraryDto({
    required String id,
  }) = _SharedLibraryDto;

  factory SharedLibraryDto.fromJson(Map<String, dynamic> json) =>
      _$SharedLibraryDtoFromJson(json);
}
