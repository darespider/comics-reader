// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'collection_creation_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$CollectionCreationDtoImpl _$$CollectionCreationDtoImplFromJson(
        Map<String, dynamic> json) =>
    _$CollectionCreationDtoImpl(
      name: json['name'] as String,
      ordered: json['ordered'] as bool,
      seriesIds:
          (json['seriesIds'] as List<dynamic>).map((e) => e as String).toList(),
    );

Map<String, dynamic> _$$CollectionCreationDtoImplToJson(
        _$CollectionCreationDtoImpl instance) =>
    <String, dynamic>{
      'name': instance.name,
      'ordered': instance.ordered,
      'seriesIds': instance.seriesIds,
    };
