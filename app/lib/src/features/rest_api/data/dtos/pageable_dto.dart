// ignore_for_file: public_member_api_docs
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/sort_dto.dart';

part 'pageable_dto.freezed.dart';
part 'pageable_dto.g.dart';

@Freezed(copyWith: false)
class PageableDto with _$PageableDto {
  const factory PageableDto({
    required SortDto sort,
    required bool paged,
    required bool unpaged,
    required int pageSize,
    required int pageNumber,
    required int offset,
  }) = _PageableDto;

  factory PageableDto.fromJson(Map<String, dynamic> json) =>
      _$PageableDtoFromJson(json);
}
