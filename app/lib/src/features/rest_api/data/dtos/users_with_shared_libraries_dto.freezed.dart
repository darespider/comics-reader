// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'users_with_shared_libraries_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

UsersWithSharedLibrariesDto _$UsersWithSharedLibrariesDtoFromJson(
    Map<String, dynamic> json) {
  return _UsersWithSharedLibrariesDto.fromJson(json);
}

/// @nodoc
mixin _$UsersWithSharedLibrariesDto {
  String get id => throw _privateConstructorUsedError;
  String get email => throw _privateConstructorUsedError;
  List<String> get roles => throw _privateConstructorUsedError;
  bool get sharedAllLibraries => throw _privateConstructorUsedError;
  List<SharedLibraryDto> get sharedLibraries =>
      throw _privateConstructorUsedError;

  /// Serializes this UsersWithSharedLibrariesDto to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
}

/// @nodoc
@JsonSerializable()
class _$UsersWithSharedLibrariesDtoImpl
    implements _UsersWithSharedLibrariesDto {
  const _$UsersWithSharedLibrariesDtoImpl(
      {required this.id,
      required this.email,
      required final List<String> roles,
      required this.sharedAllLibraries,
      required final List<SharedLibraryDto> sharedLibraries})
      : _roles = roles,
        _sharedLibraries = sharedLibraries;

  factory _$UsersWithSharedLibrariesDtoImpl.fromJson(
          Map<String, dynamic> json) =>
      _$$UsersWithSharedLibrariesDtoImplFromJson(json);

  @override
  final String id;
  @override
  final String email;
  final List<String> _roles;
  @override
  List<String> get roles {
    if (_roles is EqualUnmodifiableListView) return _roles;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_roles);
  }

  @override
  final bool sharedAllLibraries;
  final List<SharedLibraryDto> _sharedLibraries;
  @override
  List<SharedLibraryDto> get sharedLibraries {
    if (_sharedLibraries is EqualUnmodifiableListView) return _sharedLibraries;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_sharedLibraries);
  }

  @override
  String toString() {
    return 'UsersWithSharedLibrariesDto(id: $id, email: $email, roles: $roles, sharedAllLibraries: $sharedAllLibraries, sharedLibraries: $sharedLibraries)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$UsersWithSharedLibrariesDtoImpl &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.email, email) || other.email == email) &&
            const DeepCollectionEquality().equals(other._roles, _roles) &&
            (identical(other.sharedAllLibraries, sharedAllLibraries) ||
                other.sharedAllLibraries == sharedAllLibraries) &&
            const DeepCollectionEquality()
                .equals(other._sharedLibraries, _sharedLibraries));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      id,
      email,
      const DeepCollectionEquality().hash(_roles),
      sharedAllLibraries,
      const DeepCollectionEquality().hash(_sharedLibraries));

  @override
  Map<String, dynamic> toJson() {
    return _$$UsersWithSharedLibrariesDtoImplToJson(
      this,
    );
  }
}

abstract class _UsersWithSharedLibrariesDto
    implements UsersWithSharedLibrariesDto {
  const factory _UsersWithSharedLibrariesDto(
          {required final String id,
          required final String email,
          required final List<String> roles,
          required final bool sharedAllLibraries,
          required final List<SharedLibraryDto> sharedLibraries}) =
      _$UsersWithSharedLibrariesDtoImpl;

  factory _UsersWithSharedLibrariesDto.fromJson(Map<String, dynamic> json) =
      _$UsersWithSharedLibrariesDtoImpl.fromJson;

  @override
  String get id;
  @override
  String get email;
  @override
  List<String> get roles;
  @override
  bool get sharedAllLibraries;
  @override
  List<SharedLibraryDto> get sharedLibraries;
}
