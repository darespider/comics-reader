// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pageable_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$PageableDtoImpl _$$PageableDtoImplFromJson(Map<String, dynamic> json) =>
    _$PageableDtoImpl(
      sort: SortDto.fromJson(json['sort'] as Map<String, dynamic>),
      paged: json['paged'] as bool,
      unpaged: json['unpaged'] as bool,
      pageSize: (json['pageSize'] as num).toInt(),
      pageNumber: (json['pageNumber'] as num).toInt(),
      offset: (json['offset'] as num).toInt(),
    );

Map<String, dynamic> _$$PageableDtoImplToJson(_$PageableDtoImpl instance) =>
    <String, dynamic>{
      'sort': instance.sort,
      'paged': instance.paged,
      'unpaged': instance.unpaged,
      'pageSize': instance.pageSize,
      'pageNumber': instance.pageNumber,
      'offset': instance.offset,
    };
