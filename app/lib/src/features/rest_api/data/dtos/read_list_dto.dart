// ignore_for_file: public_member_api_docs
import 'package:freezed_annotation/freezed_annotation.dart';

part 'read_list_dto.freezed.dart';
part 'read_list_dto.g.dart';

@Freezed(copyWith: false)
class ReadListDto with _$ReadListDto {
  const factory ReadListDto({
    required String id,
    required String name,
    required String summary,
    required bool ordered,
    required List<String> bookIds,
    required DateTime createdDate,
    required DateTime lastModifiedDate,
    required bool filtered,
  }) = _ReadListDto;

  factory ReadListDto.fromJson(Map<String, dynamic> json) =>
      _$ReadListDtoFromJson(json);
}
