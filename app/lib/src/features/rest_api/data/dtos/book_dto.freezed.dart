// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'book_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

BookDto _$BookDtoFromJson(Map<String, dynamic> json) {
  return _BookDto.fromJson(json);
}

/// @nodoc
mixin _$BookDto {
  String get id => throw _privateConstructorUsedError;
  String get seriesId => throw _privateConstructorUsedError;
  String get seriesTitle => throw _privateConstructorUsedError;
  String get libraryId => throw _privateConstructorUsedError;
  String get name => throw _privateConstructorUsedError;
  String get url => throw _privateConstructorUsedError;
  bool get deleted => throw _privateConstructorUsedError;
  int get number => throw _privateConstructorUsedError;
  DateTime get created => throw _privateConstructorUsedError;
  DateTime get lastModified => throw _privateConstructorUsedError;
  DateTime get fileLastModified => throw _privateConstructorUsedError;
  int get sizeBytes => throw _privateConstructorUsedError;
  String get size => throw _privateConstructorUsedError;
  MediaDto get media => throw _privateConstructorUsedError;
  BookMetadataDto get metadata => throw _privateConstructorUsedError;
  String get fileHash => throw _privateConstructorUsedError;
  bool get oneshot => throw _privateConstructorUsedError;
  ReadProgressDto? get readProgress => throw _privateConstructorUsedError;

  /// Serializes this BookDto to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
}

/// @nodoc
@JsonSerializable()
class _$BookDtoImpl implements _BookDto {
  const _$BookDtoImpl(
      {required this.id,
      required this.seriesId,
      required this.seriesTitle,
      required this.libraryId,
      required this.name,
      required this.url,
      required this.deleted,
      required this.number,
      required this.created,
      required this.lastModified,
      required this.fileLastModified,
      required this.sizeBytes,
      required this.size,
      required this.media,
      required this.metadata,
      required this.fileHash,
      required this.oneshot,
      this.readProgress});

  factory _$BookDtoImpl.fromJson(Map<String, dynamic> json) =>
      _$$BookDtoImplFromJson(json);

  @override
  final String id;
  @override
  final String seriesId;
  @override
  final String seriesTitle;
  @override
  final String libraryId;
  @override
  final String name;
  @override
  final String url;
  @override
  final bool deleted;
  @override
  final int number;
  @override
  final DateTime created;
  @override
  final DateTime lastModified;
  @override
  final DateTime fileLastModified;
  @override
  final int sizeBytes;
  @override
  final String size;
  @override
  final MediaDto media;
  @override
  final BookMetadataDto metadata;
  @override
  final String fileHash;
  @override
  final bool oneshot;
  @override
  final ReadProgressDto? readProgress;

  @override
  String toString() {
    return 'BookDto(id: $id, seriesId: $seriesId, seriesTitle: $seriesTitle, libraryId: $libraryId, name: $name, url: $url, deleted: $deleted, number: $number, created: $created, lastModified: $lastModified, fileLastModified: $fileLastModified, sizeBytes: $sizeBytes, size: $size, media: $media, metadata: $metadata, fileHash: $fileHash, oneshot: $oneshot, readProgress: $readProgress)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$BookDtoImpl &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.seriesId, seriesId) ||
                other.seriesId == seriesId) &&
            (identical(other.seriesTitle, seriesTitle) ||
                other.seriesTitle == seriesTitle) &&
            (identical(other.libraryId, libraryId) ||
                other.libraryId == libraryId) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.url, url) || other.url == url) &&
            (identical(other.deleted, deleted) || other.deleted == deleted) &&
            (identical(other.number, number) || other.number == number) &&
            (identical(other.created, created) || other.created == created) &&
            (identical(other.lastModified, lastModified) ||
                other.lastModified == lastModified) &&
            (identical(other.fileLastModified, fileLastModified) ||
                other.fileLastModified == fileLastModified) &&
            (identical(other.sizeBytes, sizeBytes) ||
                other.sizeBytes == sizeBytes) &&
            (identical(other.size, size) || other.size == size) &&
            (identical(other.media, media) || other.media == media) &&
            (identical(other.metadata, metadata) ||
                other.metadata == metadata) &&
            (identical(other.fileHash, fileHash) ||
                other.fileHash == fileHash) &&
            (identical(other.oneshot, oneshot) || other.oneshot == oneshot) &&
            (identical(other.readProgress, readProgress) ||
                other.readProgress == readProgress));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      id,
      seriesId,
      seriesTitle,
      libraryId,
      name,
      url,
      deleted,
      number,
      created,
      lastModified,
      fileLastModified,
      sizeBytes,
      size,
      media,
      metadata,
      fileHash,
      oneshot,
      readProgress);

  @override
  Map<String, dynamic> toJson() {
    return _$$BookDtoImplToJson(
      this,
    );
  }
}

abstract class _BookDto implements BookDto {
  const factory _BookDto(
      {required final String id,
      required final String seriesId,
      required final String seriesTitle,
      required final String libraryId,
      required final String name,
      required final String url,
      required final bool deleted,
      required final int number,
      required final DateTime created,
      required final DateTime lastModified,
      required final DateTime fileLastModified,
      required final int sizeBytes,
      required final String size,
      required final MediaDto media,
      required final BookMetadataDto metadata,
      required final String fileHash,
      required final bool oneshot,
      final ReadProgressDto? readProgress}) = _$BookDtoImpl;

  factory _BookDto.fromJson(Map<String, dynamic> json) = _$BookDtoImpl.fromJson;

  @override
  String get id;
  @override
  String get seriesId;
  @override
  String get seriesTitle;
  @override
  String get libraryId;
  @override
  String get name;
  @override
  String get url;
  @override
  bool get deleted;
  @override
  int get number;
  @override
  DateTime get created;
  @override
  DateTime get lastModified;
  @override
  DateTime get fileLastModified;
  @override
  int get sizeBytes;
  @override
  String get size;
  @override
  MediaDto get media;
  @override
  BookMetadataDto get metadata;
  @override
  String get fileHash;
  @override
  bool get oneshot;
  @override
  ReadProgressDto? get readProgress;
}
