// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'collection_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

CollectionDto _$CollectionDtoFromJson(Map<String, dynamic> json) {
  return _CollectionDto.fromJson(json);
}

/// @nodoc
mixin _$CollectionDto {
  String get id => throw _privateConstructorUsedError;
  String get name => throw _privateConstructorUsedError;
  bool get ordered => throw _privateConstructorUsedError;
  List<String> get seriesIds => throw _privateConstructorUsedError;
  DateTime get createdDate => throw _privateConstructorUsedError;
  DateTime get lastModifiedDate => throw _privateConstructorUsedError;
  bool get filtered => throw _privateConstructorUsedError;

  /// Serializes this CollectionDto to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
}

/// @nodoc
@JsonSerializable()
class _$CollectionDtoImpl implements _CollectionDto {
  const _$CollectionDtoImpl(
      {required this.id,
      required this.name,
      required this.ordered,
      required final List<String> seriesIds,
      required this.createdDate,
      required this.lastModifiedDate,
      required this.filtered})
      : _seriesIds = seriesIds;

  factory _$CollectionDtoImpl.fromJson(Map<String, dynamic> json) =>
      _$$CollectionDtoImplFromJson(json);

  @override
  final String id;
  @override
  final String name;
  @override
  final bool ordered;
  final List<String> _seriesIds;
  @override
  List<String> get seriesIds {
    if (_seriesIds is EqualUnmodifiableListView) return _seriesIds;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_seriesIds);
  }

  @override
  final DateTime createdDate;
  @override
  final DateTime lastModifiedDate;
  @override
  final bool filtered;

  @override
  String toString() {
    return 'CollectionDto(id: $id, name: $name, ordered: $ordered, seriesIds: $seriesIds, createdDate: $createdDate, lastModifiedDate: $lastModifiedDate, filtered: $filtered)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CollectionDtoImpl &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.ordered, ordered) || other.ordered == ordered) &&
            const DeepCollectionEquality()
                .equals(other._seriesIds, _seriesIds) &&
            (identical(other.createdDate, createdDate) ||
                other.createdDate == createdDate) &&
            (identical(other.lastModifiedDate, lastModifiedDate) ||
                other.lastModifiedDate == lastModifiedDate) &&
            (identical(other.filtered, filtered) ||
                other.filtered == filtered));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      id,
      name,
      ordered,
      const DeepCollectionEquality().hash(_seriesIds),
      createdDate,
      lastModifiedDate,
      filtered);

  @override
  Map<String, dynamic> toJson() {
    return _$$CollectionDtoImplToJson(
      this,
    );
  }
}

abstract class _CollectionDto implements CollectionDto {
  const factory _CollectionDto(
      {required final String id,
      required final String name,
      required final bool ordered,
      required final List<String> seriesIds,
      required final DateTime createdDate,
      required final DateTime lastModifiedDate,
      required final bool filtered}) = _$CollectionDtoImpl;

  factory _CollectionDto.fromJson(Map<String, dynamic> json) =
      _$CollectionDtoImpl.fromJson;

  @override
  String get id;
  @override
  String get name;
  @override
  bool get ordered;
  @override
  List<String> get seriesIds;
  @override
  DateTime get createdDate;
  @override
  DateTime get lastModifiedDate;
  @override
  bool get filtered;
}
