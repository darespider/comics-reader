// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'age_restriction_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$AgeRestrictionDtoImpl _$$AgeRestrictionDtoImplFromJson(
        Map<String, dynamic> json) =>
    _$AgeRestrictionDtoImpl(
      age: (json['age'] as num).toInt(),
      restriction: json['restriction'] as String,
    );

Map<String, dynamic> _$$AgeRestrictionDtoImplToJson(
        _$AgeRestrictionDtoImpl instance) =>
    <String, dynamic>{
      'age': instance.age,
      'restriction': instance.restriction,
    };
