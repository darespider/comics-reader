// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'read_progress_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

ReadProgressDto _$ReadProgressDtoFromJson(Map<String, dynamic> json) {
  return _ReadProgressDto.fromJson(json);
}

/// @nodoc
mixin _$ReadProgressDto {
  int get page => throw _privateConstructorUsedError;
  bool get completed => throw _privateConstructorUsedError;
  DateTime get created => throw _privateConstructorUsedError;
  DateTime get lastModified => throw _privateConstructorUsedError;

  /// Serializes this ReadProgressDto to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
}

/// @nodoc
@JsonSerializable()
class _$ReadProgressDtoImpl implements _ReadProgressDto {
  const _$ReadProgressDtoImpl(
      {required this.page,
      required this.completed,
      required this.created,
      required this.lastModified});

  factory _$ReadProgressDtoImpl.fromJson(Map<String, dynamic> json) =>
      _$$ReadProgressDtoImplFromJson(json);

  @override
  final int page;
  @override
  final bool completed;
  @override
  final DateTime created;
  @override
  final DateTime lastModified;

  @override
  String toString() {
    return 'ReadProgressDto(page: $page, completed: $completed, created: $created, lastModified: $lastModified)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ReadProgressDtoImpl &&
            (identical(other.page, page) || other.page == page) &&
            (identical(other.completed, completed) ||
                other.completed == completed) &&
            (identical(other.created, created) || other.created == created) &&
            (identical(other.lastModified, lastModified) ||
                other.lastModified == lastModified));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode =>
      Object.hash(runtimeType, page, completed, created, lastModified);

  @override
  Map<String, dynamic> toJson() {
    return _$$ReadProgressDtoImplToJson(
      this,
    );
  }
}

abstract class _ReadProgressDto implements ReadProgressDto {
  const factory _ReadProgressDto(
      {required final int page,
      required final bool completed,
      required final DateTime created,
      required final DateTime lastModified}) = _$ReadProgressDtoImpl;

  factory _ReadProgressDto.fromJson(Map<String, dynamic> json) =
      _$ReadProgressDtoImpl.fromJson;

  @override
  int get page;
  @override
  bool get completed;
  @override
  DateTime get created;
  @override
  DateTime get lastModified;
}
