// ignore_for_file: public_member_api_docs
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/age_restriction_dto.dart';

part 'user_dto.freezed.dart';
part 'user_dto.g.dart';

@Freezed(copyWith: false)
class UserDto with _$UserDto {
  const factory UserDto({
    required String id,
    required String email,
    required Set<String> roles,
    required bool sharedAllLibraries,
    required Set<String> sharedLibrariesIds,
    required Set<String> labelsAllow,
    required Set<String> labelsExclude,
    AgeRestrictionDto? ageRestriction,
  }) = _UserDto;

  factory UserDto.fromJson(Map<String, dynamic> json) =>
      _$UserDtoFromJson(json);
}
