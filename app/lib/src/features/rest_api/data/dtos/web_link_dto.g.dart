// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'web_link_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$WebLinkDtoImpl _$$WebLinkDtoImplFromJson(Map<String, dynamic> json) =>
    _$WebLinkDtoImpl(
      label: json['label'] as String,
      url: Uri.parse(json['url'] as String),
    );

Map<String, dynamic> _$$WebLinkDtoImplToJson(_$WebLinkDtoImpl instance) =>
    <String, dynamic>{
      'label': instance.label,
      'url': instance.url.toString(),
    };
