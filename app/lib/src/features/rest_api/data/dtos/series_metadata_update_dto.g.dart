// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'series_metadata_update_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$SeriesMetadataUpdateDtoImpl _$$SeriesMetadataUpdateDtoImplFromJson(
        Map<String, dynamic> json) =>
    _$SeriesMetadataUpdateDtoImpl(
      status: $enumDecode(_$SeriesStatusEnumMap, json['status']),
      statusLock: json['statusLock'] as bool,
      title: json['title'] as String,
      titleLock: json['titleLock'] as bool,
      titleSort: json['titleSort'] as String,
      titleSortLock: json['titleSortLock'] as bool,
      summary: json['summary'] as String,
      summaryLock: json['summaryLock'] as bool,
      publisher: json['publisher'] as String,
      publisherLock: json['publisherLock'] as bool,
      readingDirectionLock: json['readingDirectionLock'] as bool,
      language: json['language'] as String,
      languageLock: json['languageLock'] as bool,
      genresLock: json['genresLock'] as bool,
      tagsLock: json['tagsLock'] as bool,
      tags: (json['tags'] as List<dynamic>).map((e) => e as String).toSet(),
      readingDirection:
          $enumDecode(_$ReadingDirectionEnumMap, json['readingDirection']),
      ageRating: (json['ageRating'] as num).toInt(),
      genres: (json['genres'] as List<dynamic>).map((e) => e as String).toSet(),
    );

Map<String, dynamic> _$$SeriesMetadataUpdateDtoImplToJson(
        _$SeriesMetadataUpdateDtoImpl instance) =>
    <String, dynamic>{
      'status': _$SeriesStatusEnumMap[instance.status]!,
      'statusLock': instance.statusLock,
      'title': instance.title,
      'titleLock': instance.titleLock,
      'titleSort': instance.titleSort,
      'titleSortLock': instance.titleSortLock,
      'summary': instance.summary,
      'summaryLock': instance.summaryLock,
      'publisher': instance.publisher,
      'publisherLock': instance.publisherLock,
      'readingDirectionLock': instance.readingDirectionLock,
      'language': instance.language,
      'languageLock': instance.languageLock,
      'genresLock': instance.genresLock,
      'tagsLock': instance.tagsLock,
      'tags': instance.tags.toList(),
      'readingDirection': _$ReadingDirectionEnumMap[instance.readingDirection]!,
      'ageRating': instance.ageRating,
      'genres': instance.genres.toList(),
    };

const _$SeriesStatusEnumMap = {
  SeriesStatus.ended: 'ended',
  SeriesStatus.ongoing: 'ongoing',
  SeriesStatus.abandoned: 'abandoned',
  SeriesStatus.hiatus: 'hiatus',
};

const _$ReadingDirectionEnumMap = {
  ReadingDirection.leftToRight: 'leftToRight',
  ReadingDirection.rightToLeft: 'rightToLeft',
  ReadingDirection.vertical: 'vertical',
  ReadingDirection.webtoon: 'webtoon',
};
