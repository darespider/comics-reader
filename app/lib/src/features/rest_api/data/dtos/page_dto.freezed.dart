// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'page_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

PageDto _$PageDtoFromJson(Map<String, dynamic> json) {
  return _PageDto.fromJson(json);
}

/// @nodoc
mixin _$PageDto {
  int get number => throw _privateConstructorUsedError;
  String get fileName => throw _privateConstructorUsedError;
  String get mediaType => throw _privateConstructorUsedError;
  int? get width => throw _privateConstructorUsedError;
  int? get height => throw _privateConstructorUsedError;

  /// Serializes this PageDto to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
}

/// @nodoc
@JsonSerializable()
class _$PageDtoImpl implements _PageDto {
  const _$PageDtoImpl(
      {required this.number,
      required this.fileName,
      required this.mediaType,
      this.width,
      this.height});

  factory _$PageDtoImpl.fromJson(Map<String, dynamic> json) =>
      _$$PageDtoImplFromJson(json);

  @override
  final int number;
  @override
  final String fileName;
  @override
  final String mediaType;
  @override
  final int? width;
  @override
  final int? height;

  @override
  String toString() {
    return 'PageDto(number: $number, fileName: $fileName, mediaType: $mediaType, width: $width, height: $height)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$PageDtoImpl &&
            (identical(other.number, number) || other.number == number) &&
            (identical(other.fileName, fileName) ||
                other.fileName == fileName) &&
            (identical(other.mediaType, mediaType) ||
                other.mediaType == mediaType) &&
            (identical(other.width, width) || other.width == width) &&
            (identical(other.height, height) || other.height == height));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode =>
      Object.hash(runtimeType, number, fileName, mediaType, width, height);

  @override
  Map<String, dynamic> toJson() {
    return _$$PageDtoImplToJson(
      this,
    );
  }
}

abstract class _PageDto implements PageDto {
  const factory _PageDto(
      {required final int number,
      required final String fileName,
      required final String mediaType,
      final int? width,
      final int? height}) = _$PageDtoImpl;

  factory _PageDto.fromJson(Map<String, dynamic> json) = _$PageDtoImpl.fromJson;

  @override
  int get number;
  @override
  String get fileName;
  @override
  String get mediaType;
  @override
  int? get width;
  @override
  int? get height;
}
