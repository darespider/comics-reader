// ignore_for_file: public_member_api_docs
import 'package:freezed_annotation/freezed_annotation.dart';

part 'user_creation_dto.freezed.dart';
part 'user_creation_dto.g.dart';

@Freezed(copyWith: false)
class UserCreationDto with _$UserCreationDto {
  const factory UserCreationDto({
    required String email,
    required String password,
    required List<String> roles,
  }) = _UserCreationDto;

  factory UserCreationDto.fromJson(Map<String, dynamic> json) =>
      _$UserCreationDtoFromJson(json);
}
