// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sort_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$SortDtoImpl _$$SortDtoImplFromJson(Map<String, dynamic> json) =>
    _$SortDtoImpl(
      sorted: json['sorted'] as bool,
      unsorted: json['unsorted'] as bool,
      empty: json['empty'] as bool,
    );

Map<String, dynamic> _$$SortDtoImplToJson(_$SortDtoImpl instance) =>
    <String, dynamic>{
      'sorted': instance.sorted,
      'unsorted': instance.unsorted,
      'empty': instance.empty,
    };
