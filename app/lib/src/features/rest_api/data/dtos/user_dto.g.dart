// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$UserDtoImpl _$$UserDtoImplFromJson(Map<String, dynamic> json) =>
    _$UserDtoImpl(
      id: json['id'] as String,
      email: json['email'] as String,
      roles: (json['roles'] as List<dynamic>).map((e) => e as String).toSet(),
      sharedAllLibraries: json['sharedAllLibraries'] as bool,
      sharedLibrariesIds: (json['sharedLibrariesIds'] as List<dynamic>)
          .map((e) => e as String)
          .toSet(),
      labelsAllow: (json['labelsAllow'] as List<dynamic>)
          .map((e) => e as String)
          .toSet(),
      labelsExclude: (json['labelsExclude'] as List<dynamic>)
          .map((e) => e as String)
          .toSet(),
      ageRestriction: json['ageRestriction'] == null
          ? null
          : AgeRestrictionDto.fromJson(
              json['ageRestriction'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$UserDtoImplToJson(_$UserDtoImpl instance) =>
    <String, dynamic>{
      'id': instance.id,
      'email': instance.email,
      'roles': instance.roles.toList(),
      'sharedAllLibraries': instance.sharedAllLibraries,
      'sharedLibrariesIds': instance.sharedLibrariesIds.toList(),
      'labelsAllow': instance.labelsAllow.toList(),
      'labelsExclude': instance.labelsExclude.toList(),
      'ageRestriction': instance.ageRestriction,
    };
