// ignore_for_file: public_member_api_docs
import 'package:freezed_annotation/freezed_annotation.dart';

part 'directory_request_dto.freezed.dart';
part 'directory_request_dto.g.dart';

@Freezed(copyWith: false)
class DirectoryRequestDto with _$DirectoryRequestDto {
  const factory DirectoryRequestDto({
    required String path,
  }) = _DirectoryRequestDto;

  factory DirectoryRequestDto.fromJson(Map<String, dynamic> json) =>
      _$DirectoryRequestDtoFromJson(json);
}
