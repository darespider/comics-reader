// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'read_progress_update_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

ReadProgressUpdateDto _$ReadProgressUpdateDtoFromJson(
    Map<String, dynamic> json) {
  return _ReadProgressUpdateDto.fromJson(json);
}

/// @nodoc
mixin _$ReadProgressUpdateDto {
  int? get page => throw _privateConstructorUsedError;
  bool? get completed => throw _privateConstructorUsedError;

  /// Serializes this ReadProgressUpdateDto to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
}

/// @nodoc
@JsonSerializable()
class _$ReadProgressUpdateDtoImpl implements _ReadProgressUpdateDto {
  const _$ReadProgressUpdateDtoImpl({this.page, this.completed});

  factory _$ReadProgressUpdateDtoImpl.fromJson(Map<String, dynamic> json) =>
      _$$ReadProgressUpdateDtoImplFromJson(json);

  @override
  final int? page;
  @override
  final bool? completed;

  @override
  String toString() {
    return 'ReadProgressUpdateDto(page: $page, completed: $completed)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ReadProgressUpdateDtoImpl &&
            (identical(other.page, page) || other.page == page) &&
            (identical(other.completed, completed) ||
                other.completed == completed));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => Object.hash(runtimeType, page, completed);

  @override
  Map<String, dynamic> toJson() {
    return _$$ReadProgressUpdateDtoImplToJson(
      this,
    );
  }
}

abstract class _ReadProgressUpdateDto implements ReadProgressUpdateDto {
  const factory _ReadProgressUpdateDto(
      {final int? page, final bool? completed}) = _$ReadProgressUpdateDtoImpl;

  factory _ReadProgressUpdateDto.fromJson(Map<String, dynamic> json) =
      _$ReadProgressUpdateDtoImpl.fromJson;

  @override
  int? get page;
  @override
  bool? get completed;
}
