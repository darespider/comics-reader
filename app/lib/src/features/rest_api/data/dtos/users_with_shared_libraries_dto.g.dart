// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'users_with_shared_libraries_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$UsersWithSharedLibrariesDtoImpl _$$UsersWithSharedLibrariesDtoImplFromJson(
        Map<String, dynamic> json) =>
    _$UsersWithSharedLibrariesDtoImpl(
      id: json['id'] as String,
      email: json['email'] as String,
      roles: (json['roles'] as List<dynamic>).map((e) => e as String).toList(),
      sharedAllLibraries: json['sharedAllLibraries'] as bool,
      sharedLibraries: (json['sharedLibraries'] as List<dynamic>)
          .map((e) => SharedLibraryDto.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$UsersWithSharedLibrariesDtoImplToJson(
        _$UsersWithSharedLibrariesDtoImpl instance) =>
    <String, dynamic>{
      'id': instance.id,
      'email': instance.email,
      'roles': instance.roles,
      'sharedAllLibraries': instance.sharedAllLibraries,
      'sharedLibraries': instance.sharedLibraries,
    };
