// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'author_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$AuthorDtoImpl _$$AuthorDtoImplFromJson(Map<String, dynamic> json) =>
    _$AuthorDtoImpl(
      name: json['name'] as String,
      role: json['role'] as String,
    );

Map<String, dynamic> _$$AuthorDtoImplToJson(_$AuthorDtoImpl instance) =>
    <String, dynamic>{
      'name': instance.name,
      'role': instance.role,
    };
