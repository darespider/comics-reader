// ignore_for_file: public_member_api_docs
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/book_metadata_aggregation_dto.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/series_metadata_dto.dart';

part 'series_dto.freezed.dart';
part 'series_dto.g.dart';

@Freezed(copyWith: false)
class SeriesDto with _$SeriesDto {
  const factory SeriesDto({
    required String id,
    required String libraryId,
    required String name,
    required String url,
    required DateTime created,
    required DateTime lastModified,
    required DateTime fileLastModified,
    required int booksCount,
    required int booksReadCount,
    required int booksUnreadCount,
    required int booksInProgressCount,
    required SeriesMetadataDto metadata,
    required BookMetadataAggregationDto booksMetadata,
    required bool deleted,
    @Default(false) bool oneshot,
  }) = _SeriesDto;

  factory SeriesDto.fromJson(Map<String, dynamic> json) =>
      _$SeriesDtoFromJson(json);
}
