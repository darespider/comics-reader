// ignore_for_file: public_member_api_docs
import 'package:freezed_annotation/freezed_annotation.dart';

part 'read_progress_dto.freezed.dart';
part 'read_progress_dto.g.dart';

@Freezed(copyWith: false)
class ReadProgressDto with _$ReadProgressDto {
  const factory ReadProgressDto({
    required int page,
    required bool completed,
    required DateTime created,
    required DateTime lastModified,
  }) = _ReadProgressDto;

  factory ReadProgressDto.fromJson(Map<String, dynamic> json) =>
      _$ReadProgressDtoFromJson(json);
}
