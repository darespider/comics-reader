// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'book_metadata_update_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$BookMetadataUpdateDtoImpl _$$BookMetadataUpdateDtoImplFromJson(
        Map<String, dynamic> json) =>
    _$BookMetadataUpdateDtoImpl(
      title: json['title'] as String?,
      titleLock: json['titleLock'] as bool?,
      summary: json['summary'] as String?,
      summaryLock: json['summaryLock'] as bool?,
      number: json['number'] as String?,
      numberLock: json['numberLock'] as bool?,
      numberSort: (json['numberSort'] as num?)?.toDouble(),
      numberSortLock: json['numberSortLock'] as bool?,
      releaseDateLock: json['releaseDateLock'] as bool?,
      authorsLock: json['authorsLock'] as bool?,
      tagsLock: json['tagsLock'] as bool?,
      tags: (json['tags'] as List<dynamic>?)?.map((e) => e as String).toSet(),
      authors: json['authors'] == null
          ? null
          : AuthorUpdateDto.fromJson(json['authors'] as Map<String, dynamic>),
      releaseDate: json['releaseDate'] == null
          ? null
          : DateTime.parse(json['releaseDate'] as String),
    );

Map<String, dynamic> _$$BookMetadataUpdateDtoImplToJson(
        _$BookMetadataUpdateDtoImpl instance) =>
    <String, dynamic>{
      'title': instance.title,
      'titleLock': instance.titleLock,
      'summary': instance.summary,
      'summaryLock': instance.summaryLock,
      'number': instance.number,
      'numberLock': instance.numberLock,
      'numberSort': instance.numberSort,
      'numberSortLock': instance.numberSortLock,
      'releaseDateLock': instance.releaseDateLock,
      'authorsLock': instance.authorsLock,
      'tagsLock': instance.tagsLock,
      'tags': instance.tags?.toList(),
      'authors': instance.authors,
      'releaseDate': instance.releaseDate?.toIso8601String(),
    };
