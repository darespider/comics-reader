// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'book_metadata_aggregation_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$BookMetadataAggregationDtoImpl _$$BookMetadataAggregationDtoImplFromJson(
        Map<String, dynamic> json) =>
    _$BookMetadataAggregationDtoImpl(
      authors: (json['authors'] as List<dynamic>)
          .map((e) => AuthorDto.fromJson(e as Map<String, dynamic>))
          .toList(),
      tags: (json['tags'] as List<dynamic>).map((e) => e as String).toSet(),
      summary: json['summary'] as String,
      summaryNumber: json['summaryNumber'] as String,
      created: DateTime.parse(json['created'] as String),
      lastModified: DateTime.parse(json['lastModified'] as String),
      releaseDate: json['releaseDate'] == null
          ? null
          : DateTime.parse(json['releaseDate'] as String),
    );

Map<String, dynamic> _$$BookMetadataAggregationDtoImplToJson(
        _$BookMetadataAggregationDtoImpl instance) =>
    <String, dynamic>{
      'authors': instance.authors,
      'tags': instance.tags.toList(),
      'summary': instance.summary,
      'summaryNumber': instance.summaryNumber,
      'created': instance.created.toIso8601String(),
      'lastModified': instance.lastModified.toIso8601String(),
      'releaseDate': instance.releaseDate?.toIso8601String(),
    };
