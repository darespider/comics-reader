// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'author_update_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

AuthorUpdateDto _$AuthorUpdateDtoFromJson(Map<String, dynamic> json) {
  return _AuthorUpdateDto.fromJson(json);
}

/// @nodoc
mixin _$AuthorUpdateDto {
  String get name => throw _privateConstructorUsedError;
  String get role => throw _privateConstructorUsedError;

  /// Serializes this AuthorUpdateDto to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
}

/// @nodoc
@JsonSerializable()
class _$AuthorUpdateDtoImpl implements _AuthorUpdateDto {
  const _$AuthorUpdateDtoImpl({required this.name, required this.role});

  factory _$AuthorUpdateDtoImpl.fromJson(Map<String, dynamic> json) =>
      _$$AuthorUpdateDtoImplFromJson(json);

  @override
  final String name;
  @override
  final String role;

  @override
  String toString() {
    return 'AuthorUpdateDto(name: $name, role: $role)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$AuthorUpdateDtoImpl &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.role, role) || other.role == role));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => Object.hash(runtimeType, name, role);

  @override
  Map<String, dynamic> toJson() {
    return _$$AuthorUpdateDtoImplToJson(
      this,
    );
  }
}

abstract class _AuthorUpdateDto implements AuthorUpdateDto {
  const factory _AuthorUpdateDto(
      {required final String name,
      required final String role}) = _$AuthorUpdateDtoImpl;

  factory _AuthorUpdateDto.fromJson(Map<String, dynamic> json) =
      _$AuthorUpdateDtoImpl.fromJson;

  @override
  String get name;
  @override
  String get role;
}
