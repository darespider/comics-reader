// ignore_for_file: public_member_api_docs
import 'package:freezed_annotation/freezed_annotation.dart';

part 'shared_libraries_update_dto.freezed.dart';
part 'shared_libraries_update_dto.g.dart';

@Freezed(copyWith: false)
class SharedLibrariesUpdateDto with _$SharedLibrariesUpdateDto {
  const factory SharedLibrariesUpdateDto({
    required bool all,
    required Set<String> libraryIds,
  }) = _SharedLibrariesUpdateDto;

  factory SharedLibrariesUpdateDto.fromJson(Map<String, dynamic> json) =>
      _$SharedLibrariesUpdateDtoFromJson(json);
}
