// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'read_list_update_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$ReadListUpdateDtoImpl _$$ReadListUpdateDtoImplFromJson(
        Map<String, dynamic> json) =>
    _$ReadListUpdateDtoImpl(
      name: json['name'] as String?,
      summary: json['summary'] as String?,
      bookIds:
          (json['bookIds'] as List<dynamic>?)?.map((e) => e as String).toList(),
    );

Map<String, dynamic> _$$ReadListUpdateDtoImplToJson(
        _$ReadListUpdateDtoImpl instance) =>
    <String, dynamic>{
      'name': instance.name,
      'summary': instance.summary,
      'bookIds': instance.bookIds,
    };
