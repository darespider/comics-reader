// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'read_list_creation_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

ReadListCreationDto _$ReadListCreationDtoFromJson(Map<String, dynamic> json) {
  return _ReadListCreationDto.fromJson(json);
}

/// @nodoc
mixin _$ReadListCreationDto {
  String get name => throw _privateConstructorUsedError;
  List<String> get bookIds => throw _privateConstructorUsedError;

  /// Serializes this ReadListCreationDto to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
}

/// @nodoc
@JsonSerializable()
class _$ReadListCreationDtoImpl implements _ReadListCreationDto {
  const _$ReadListCreationDtoImpl(
      {required this.name, required final List<String> bookIds})
      : _bookIds = bookIds;

  factory _$ReadListCreationDtoImpl.fromJson(Map<String, dynamic> json) =>
      _$$ReadListCreationDtoImplFromJson(json);

  @override
  final String name;
  final List<String> _bookIds;
  @override
  List<String> get bookIds {
    if (_bookIds is EqualUnmodifiableListView) return _bookIds;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_bookIds);
  }

  @override
  String toString() {
    return 'ReadListCreationDto(name: $name, bookIds: $bookIds)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ReadListCreationDtoImpl &&
            (identical(other.name, name) || other.name == name) &&
            const DeepCollectionEquality().equals(other._bookIds, _bookIds));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => Object.hash(
      runtimeType, name, const DeepCollectionEquality().hash(_bookIds));

  @override
  Map<String, dynamic> toJson() {
    return _$$ReadListCreationDtoImplToJson(
      this,
    );
  }
}

abstract class _ReadListCreationDto implements ReadListCreationDto {
  const factory _ReadListCreationDto(
      {required final String name,
      required final List<String> bookIds}) = _$ReadListCreationDtoImpl;

  factory _ReadListCreationDto.fromJson(Map<String, dynamic> json) =
      _$ReadListCreationDtoImpl.fromJson;

  @override
  String get name;
  @override
  List<String> get bookIds;
}
