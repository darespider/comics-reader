// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'claim_status.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ClaimStatus _$ClaimStatusFromJson(Map<String, dynamic> json) {
  return _ClaimStatus.fromJson(json);
}

/// @nodoc
mixin _$ClaimStatus {
  bool get isClaimed => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ClaimStatusCopyWith<ClaimStatus> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ClaimStatusCopyWith<$Res> {
  factory $ClaimStatusCopyWith(
          ClaimStatus value, $Res Function(ClaimStatus) then) =
      _$ClaimStatusCopyWithImpl<$Res, ClaimStatus>;
  @useResult
  $Res call({bool isClaimed});
}

/// @nodoc
class _$ClaimStatusCopyWithImpl<$Res, $Val extends ClaimStatus>
    implements $ClaimStatusCopyWith<$Res> {
  _$ClaimStatusCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? isClaimed = null,
  }) {
    return _then(_value.copyWith(
      isClaimed: null == isClaimed
          ? _value.isClaimed
          : isClaimed // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_ClaimStatusCopyWith<$Res>
    implements $ClaimStatusCopyWith<$Res> {
  factory _$$_ClaimStatusCopyWith(
          _$_ClaimStatus value, $Res Function(_$_ClaimStatus) then) =
      __$$_ClaimStatusCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({bool isClaimed});
}

/// @nodoc
class __$$_ClaimStatusCopyWithImpl<$Res>
    extends _$ClaimStatusCopyWithImpl<$Res, _$_ClaimStatus>
    implements _$$_ClaimStatusCopyWith<$Res> {
  __$$_ClaimStatusCopyWithImpl(
      _$_ClaimStatus _value, $Res Function(_$_ClaimStatus) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? isClaimed = null,
  }) {
    return _then(_$_ClaimStatus(
      isClaimed: null == isClaimed
          ? _value.isClaimed
          : isClaimed // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_ClaimStatus implements _ClaimStatus {
  const _$_ClaimStatus({required this.isClaimed});

  factory _$_ClaimStatus.fromJson(Map<String, dynamic> json) =>
      _$$_ClaimStatusFromJson(json);

  @override
  final bool isClaimed;

  @override
  String toString() {
    return 'ClaimStatus(isClaimed: $isClaimed)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ClaimStatus &&
            (identical(other.isClaimed, isClaimed) ||
                other.isClaimed == isClaimed));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, isClaimed);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ClaimStatusCopyWith<_$_ClaimStatus> get copyWith =>
      __$$_ClaimStatusCopyWithImpl<_$_ClaimStatus>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ClaimStatusToJson(
      this,
    );
  }
}

abstract class _ClaimStatus implements ClaimStatus {
  const factory _ClaimStatus({required final bool isClaimed}) = _$_ClaimStatus;

  factory _ClaimStatus.fromJson(Map<String, dynamic> json) =
      _$_ClaimStatus.fromJson;

  @override
  bool get isClaimed;
  @override
  @JsonKey(ignore: true)
  _$$_ClaimStatusCopyWith<_$_ClaimStatus> get copyWith =>
      throw _privateConstructorUsedError;
}
