// ignore_for_file: public_member_api_docs
import 'package:freezed_annotation/freezed_annotation.dart';

part 'web_link_dto.freezed.dart';
part 'web_link_dto.g.dart';

@Freezed(copyWith: false)
class WebLinkDto with _$WebLinkDto {
  const factory WebLinkDto({
    required String label,
    required Uri url,
  }) = _WebLinkDto;

  factory WebLinkDto.fromJson(Map<String, dynamic> json) =>
      _$WebLinkDtoFromJson(json);
}
