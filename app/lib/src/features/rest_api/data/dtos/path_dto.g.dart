// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'path_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$PathDtoImpl _$$PathDtoImplFromJson(Map<String, dynamic> json) =>
    _$PathDtoImpl(
      type: json['type'] as String,
      name: json['name'] as String,
      path: json['path'] as String,
    );

Map<String, dynamic> _$$PathDtoImplToJson(_$PathDtoImpl instance) =>
    <String, dynamic>{
      'type': instance.type,
      'name': instance.name,
      'path': instance.path,
    };
