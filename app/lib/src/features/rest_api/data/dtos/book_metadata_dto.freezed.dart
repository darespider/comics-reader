// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'book_metadata_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

BookMetadataDto _$BookMetadataDtoFromJson(Map<String, dynamic> json) {
  return _BookMetadataDto.fromJson(json);
}

/// @nodoc
mixin _$BookMetadataDto {
  String get title => throw _privateConstructorUsedError;
  bool get titleLock => throw _privateConstructorUsedError;
  String get summary => throw _privateConstructorUsedError;
  bool get summaryLock => throw _privateConstructorUsedError;
  String get number => throw _privateConstructorUsedError;
  bool get numberLock => throw _privateConstructorUsedError;
  double get numberSort => throw _privateConstructorUsedError;
  bool get numberSortLock => throw _privateConstructorUsedError;
  bool get releaseDateLock => throw _privateConstructorUsedError;
  List<AuthorDto> get authors => throw _privateConstructorUsedError;
  bool get authorsLock => throw _privateConstructorUsedError;
  Set<String> get tags => throw _privateConstructorUsedError;
  bool get tagsLock => throw _privateConstructorUsedError;
  DateTime get created => throw _privateConstructorUsedError;
  DateTime get lastModified => throw _privateConstructorUsedError;
  String get isbn => throw _privateConstructorUsedError;
  bool get isbnLock => throw _privateConstructorUsedError;
  bool get linksLock => throw _privateConstructorUsedError;
  List<WebLinkDto> get links => throw _privateConstructorUsedError;
  DateTime? get releaseDate => throw _privateConstructorUsedError;

  /// Serializes this BookMetadataDto to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
}

/// @nodoc
@JsonSerializable()
class _$BookMetadataDtoImpl implements _BookMetadataDto {
  const _$BookMetadataDtoImpl(
      {required this.title,
      required this.titleLock,
      required this.summary,
      required this.summaryLock,
      required this.number,
      required this.numberLock,
      required this.numberSort,
      required this.numberSortLock,
      required this.releaseDateLock,
      required final List<AuthorDto> authors,
      required this.authorsLock,
      required final Set<String> tags,
      required this.tagsLock,
      required this.created,
      required this.lastModified,
      required this.isbn,
      required this.isbnLock,
      required this.linksLock,
      final List<WebLinkDto> links = const [],
      this.releaseDate})
      : _authors = authors,
        _tags = tags,
        _links = links;

  factory _$BookMetadataDtoImpl.fromJson(Map<String, dynamic> json) =>
      _$$BookMetadataDtoImplFromJson(json);

  @override
  final String title;
  @override
  final bool titleLock;
  @override
  final String summary;
  @override
  final bool summaryLock;
  @override
  final String number;
  @override
  final bool numberLock;
  @override
  final double numberSort;
  @override
  final bool numberSortLock;
  @override
  final bool releaseDateLock;
  final List<AuthorDto> _authors;
  @override
  List<AuthorDto> get authors {
    if (_authors is EqualUnmodifiableListView) return _authors;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_authors);
  }

  @override
  final bool authorsLock;
  final Set<String> _tags;
  @override
  Set<String> get tags {
    if (_tags is EqualUnmodifiableSetView) return _tags;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableSetView(_tags);
  }

  @override
  final bool tagsLock;
  @override
  final DateTime created;
  @override
  final DateTime lastModified;
  @override
  final String isbn;
  @override
  final bool isbnLock;
  @override
  final bool linksLock;
  final List<WebLinkDto> _links;
  @override
  @JsonKey()
  List<WebLinkDto> get links {
    if (_links is EqualUnmodifiableListView) return _links;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_links);
  }

  @override
  final DateTime? releaseDate;

  @override
  String toString() {
    return 'BookMetadataDto(title: $title, titleLock: $titleLock, summary: $summary, summaryLock: $summaryLock, number: $number, numberLock: $numberLock, numberSort: $numberSort, numberSortLock: $numberSortLock, releaseDateLock: $releaseDateLock, authors: $authors, authorsLock: $authorsLock, tags: $tags, tagsLock: $tagsLock, created: $created, lastModified: $lastModified, isbn: $isbn, isbnLock: $isbnLock, linksLock: $linksLock, links: $links, releaseDate: $releaseDate)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$BookMetadataDtoImpl &&
            (identical(other.title, title) || other.title == title) &&
            (identical(other.titleLock, titleLock) ||
                other.titleLock == titleLock) &&
            (identical(other.summary, summary) || other.summary == summary) &&
            (identical(other.summaryLock, summaryLock) ||
                other.summaryLock == summaryLock) &&
            (identical(other.number, number) || other.number == number) &&
            (identical(other.numberLock, numberLock) ||
                other.numberLock == numberLock) &&
            (identical(other.numberSort, numberSort) ||
                other.numberSort == numberSort) &&
            (identical(other.numberSortLock, numberSortLock) ||
                other.numberSortLock == numberSortLock) &&
            (identical(other.releaseDateLock, releaseDateLock) ||
                other.releaseDateLock == releaseDateLock) &&
            const DeepCollectionEquality().equals(other._authors, _authors) &&
            (identical(other.authorsLock, authorsLock) ||
                other.authorsLock == authorsLock) &&
            const DeepCollectionEquality().equals(other._tags, _tags) &&
            (identical(other.tagsLock, tagsLock) ||
                other.tagsLock == tagsLock) &&
            (identical(other.created, created) || other.created == created) &&
            (identical(other.lastModified, lastModified) ||
                other.lastModified == lastModified) &&
            (identical(other.isbn, isbn) || other.isbn == isbn) &&
            (identical(other.isbnLock, isbnLock) ||
                other.isbnLock == isbnLock) &&
            (identical(other.linksLock, linksLock) ||
                other.linksLock == linksLock) &&
            const DeepCollectionEquality().equals(other._links, _links) &&
            (identical(other.releaseDate, releaseDate) ||
                other.releaseDate == releaseDate));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => Object.hashAll([
        runtimeType,
        title,
        titleLock,
        summary,
        summaryLock,
        number,
        numberLock,
        numberSort,
        numberSortLock,
        releaseDateLock,
        const DeepCollectionEquality().hash(_authors),
        authorsLock,
        const DeepCollectionEquality().hash(_tags),
        tagsLock,
        created,
        lastModified,
        isbn,
        isbnLock,
        linksLock,
        const DeepCollectionEquality().hash(_links),
        releaseDate
      ]);

  @override
  Map<String, dynamic> toJson() {
    return _$$BookMetadataDtoImplToJson(
      this,
    );
  }
}

abstract class _BookMetadataDto implements BookMetadataDto {
  const factory _BookMetadataDto(
      {required final String title,
      required final bool titleLock,
      required final String summary,
      required final bool summaryLock,
      required final String number,
      required final bool numberLock,
      required final double numberSort,
      required final bool numberSortLock,
      required final bool releaseDateLock,
      required final List<AuthorDto> authors,
      required final bool authorsLock,
      required final Set<String> tags,
      required final bool tagsLock,
      required final DateTime created,
      required final DateTime lastModified,
      required final String isbn,
      required final bool isbnLock,
      required final bool linksLock,
      final List<WebLinkDto> links,
      final DateTime? releaseDate}) = _$BookMetadataDtoImpl;

  factory _BookMetadataDto.fromJson(Map<String, dynamic> json) =
      _$BookMetadataDtoImpl.fromJson;

  @override
  String get title;
  @override
  bool get titleLock;
  @override
  String get summary;
  @override
  bool get summaryLock;
  @override
  String get number;
  @override
  bool get numberLock;
  @override
  double get numberSort;
  @override
  bool get numberSortLock;
  @override
  bool get releaseDateLock;
  @override
  List<AuthorDto> get authors;
  @override
  bool get authorsLock;
  @override
  Set<String> get tags;
  @override
  bool get tagsLock;
  @override
  DateTime get created;
  @override
  DateTime get lastModified;
  @override
  String get isbn;
  @override
  bool get isbnLock;
  @override
  bool get linksLock;
  @override
  List<WebLinkDto> get links;
  @override
  DateTime? get releaseDate;
}
