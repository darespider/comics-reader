// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'read_list_update_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

ReadListUpdateDto _$ReadListUpdateDtoFromJson(Map<String, dynamic> json) {
  return _ReadListUpdateDto.fromJson(json);
}

/// @nodoc
mixin _$ReadListUpdateDto {
  String? get name => throw _privateConstructorUsedError;
  String? get summary => throw _privateConstructorUsedError;
  List<String>? get bookIds => throw _privateConstructorUsedError;

  /// Serializes this ReadListUpdateDto to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
}

/// @nodoc
@JsonSerializable()
class _$ReadListUpdateDtoImpl implements _ReadListUpdateDto {
  const _$ReadListUpdateDtoImpl(
      {this.name, this.summary, final List<String>? bookIds})
      : _bookIds = bookIds;

  factory _$ReadListUpdateDtoImpl.fromJson(Map<String, dynamic> json) =>
      _$$ReadListUpdateDtoImplFromJson(json);

  @override
  final String? name;
  @override
  final String? summary;
  final List<String>? _bookIds;
  @override
  List<String>? get bookIds {
    final value = _bookIds;
    if (value == null) return null;
    if (_bookIds is EqualUnmodifiableListView) return _bookIds;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  String toString() {
    return 'ReadListUpdateDto(name: $name, summary: $summary, bookIds: $bookIds)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ReadListUpdateDtoImpl &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.summary, summary) || other.summary == summary) &&
            const DeepCollectionEquality().equals(other._bookIds, _bookIds));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => Object.hash(runtimeType, name, summary,
      const DeepCollectionEquality().hash(_bookIds));

  @override
  Map<String, dynamic> toJson() {
    return _$$ReadListUpdateDtoImplToJson(
      this,
    );
  }
}

abstract class _ReadListUpdateDto implements ReadListUpdateDto {
  const factory _ReadListUpdateDto(
      {final String? name,
      final String? summary,
      final List<String>? bookIds}) = _$ReadListUpdateDtoImpl;

  factory _ReadListUpdateDto.fromJson(Map<String, dynamic> json) =
      _$ReadListUpdateDtoImpl.fromJson;

  @override
  String? get name;
  @override
  String? get summary;
  @override
  List<String>? get bookIds;
}
