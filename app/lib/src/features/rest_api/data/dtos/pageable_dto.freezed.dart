// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'pageable_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

PageableDto _$PageableDtoFromJson(Map<String, dynamic> json) {
  return _PageableDto.fromJson(json);
}

/// @nodoc
mixin _$PageableDto {
  SortDto get sort => throw _privateConstructorUsedError;
  bool get paged => throw _privateConstructorUsedError;
  bool get unpaged => throw _privateConstructorUsedError;
  int get pageSize => throw _privateConstructorUsedError;
  int get pageNumber => throw _privateConstructorUsedError;
  int get offset => throw _privateConstructorUsedError;

  /// Serializes this PageableDto to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
}

/// @nodoc
@JsonSerializable()
class _$PageableDtoImpl implements _PageableDto {
  const _$PageableDtoImpl(
      {required this.sort,
      required this.paged,
      required this.unpaged,
      required this.pageSize,
      required this.pageNumber,
      required this.offset});

  factory _$PageableDtoImpl.fromJson(Map<String, dynamic> json) =>
      _$$PageableDtoImplFromJson(json);

  @override
  final SortDto sort;
  @override
  final bool paged;
  @override
  final bool unpaged;
  @override
  final int pageSize;
  @override
  final int pageNumber;
  @override
  final int offset;

  @override
  String toString() {
    return 'PageableDto(sort: $sort, paged: $paged, unpaged: $unpaged, pageSize: $pageSize, pageNumber: $pageNumber, offset: $offset)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$PageableDtoImpl &&
            (identical(other.sort, sort) || other.sort == sort) &&
            (identical(other.paged, paged) || other.paged == paged) &&
            (identical(other.unpaged, unpaged) || other.unpaged == unpaged) &&
            (identical(other.pageSize, pageSize) ||
                other.pageSize == pageSize) &&
            (identical(other.pageNumber, pageNumber) ||
                other.pageNumber == pageNumber) &&
            (identical(other.offset, offset) || other.offset == offset));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => Object.hash(
      runtimeType, sort, paged, unpaged, pageSize, pageNumber, offset);

  @override
  Map<String, dynamic> toJson() {
    return _$$PageableDtoImplToJson(
      this,
    );
  }
}

abstract class _PageableDto implements PageableDto {
  const factory _PageableDto(
      {required final SortDto sort,
      required final bool paged,
      required final bool unpaged,
      required final int pageSize,
      required final int pageNumber,
      required final int offset}) = _$PageableDtoImpl;

  factory _PageableDto.fromJson(Map<String, dynamic> json) =
      _$PageableDtoImpl.fromJson;

  @override
  SortDto get sort;
  @override
  bool get paged;
  @override
  bool get unpaged;
  @override
  int get pageSize;
  @override
  int get pageNumber;
  @override
  int get offset;
}
