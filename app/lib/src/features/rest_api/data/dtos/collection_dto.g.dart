// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'collection_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$CollectionDtoImpl _$$CollectionDtoImplFromJson(Map<String, dynamic> json) =>
    _$CollectionDtoImpl(
      id: json['id'] as String,
      name: json['name'] as String,
      ordered: json['ordered'] as bool,
      seriesIds:
          (json['seriesIds'] as List<dynamic>).map((e) => e as String).toList(),
      createdDate: DateTime.parse(json['createdDate'] as String),
      lastModifiedDate: DateTime.parse(json['lastModifiedDate'] as String),
      filtered: json['filtered'] as bool,
    );

Map<String, dynamic> _$$CollectionDtoImplToJson(_$CollectionDtoImpl instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'ordered': instance.ordered,
      'seriesIds': instance.seriesIds,
      'createdDate': instance.createdDate.toIso8601String(),
      'lastModifiedDate': instance.lastModifiedDate.toIso8601String(),
      'filtered': instance.filtered,
    };
