/// Direction to control the reading flow
enum ReadingDirection {
  /// ->
  leftToRight,

  /// <-
  rightToLeft,

  /// ^
  /// |
  /// v
  vertical,

  /// as webtoon
  webtoon;
}
