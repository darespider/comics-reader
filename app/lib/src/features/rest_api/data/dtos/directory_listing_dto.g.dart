// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'directory_listing_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$DirectoryListingDtoImpl _$$DirectoryListingDtoImplFromJson(
        Map<String, dynamic> json) =>
    _$DirectoryListingDtoImpl(
      parent: json['parent'] as String,
      directories: (json['directories'] as List<dynamic>)
          .map((e) => PathDto.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$DirectoryListingDtoImplToJson(
        _$DirectoryListingDtoImpl instance) =>
    <String, dynamic>{
      'parent': instance.parent,
      'directories': instance.directories,
    };
