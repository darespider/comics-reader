// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'password_update_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

PasswordUpdateDto _$PasswordUpdateDtoFromJson(Map<String, dynamic> json) {
  return _PasswordUpdateDto.fromJson(json);
}

/// @nodoc
mixin _$PasswordUpdateDto {
  String get password => throw _privateConstructorUsedError;

  /// Serializes this PasswordUpdateDto to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
}

/// @nodoc
@JsonSerializable()
class _$PasswordUpdateDtoImpl implements _PasswordUpdateDto {
  const _$PasswordUpdateDtoImpl({required this.password});

  factory _$PasswordUpdateDtoImpl.fromJson(Map<String, dynamic> json) =>
      _$$PasswordUpdateDtoImplFromJson(json);

  @override
  final String password;

  @override
  String toString() {
    return 'PasswordUpdateDto(password: $password)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$PasswordUpdateDtoImpl &&
            (identical(other.password, password) ||
                other.password == password));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => Object.hash(runtimeType, password);

  @override
  Map<String, dynamic> toJson() {
    return _$$PasswordUpdateDtoImplToJson(
      this,
    );
  }
}

abstract class _PasswordUpdateDto implements PasswordUpdateDto {
  const factory _PasswordUpdateDto({required final String password}) =
      _$PasswordUpdateDtoImpl;

  factory _PasswordUpdateDto.fromJson(Map<String, dynamic> json) =
      _$PasswordUpdateDtoImpl.fromJson;

  @override
  String get password;
}
