// ignore_for_file: public_member_api_docs
import 'package:freezed_annotation/freezed_annotation.dart';

part 'page_dto.freezed.dart';
part 'page_dto.g.dart';

@Freezed(copyWith: false)
class PageDto with _$PageDto {
  const factory PageDto({
    required int number,
    required String fileName,
    required String mediaType,
    int? width,
    int? height,
  }) = _PageDto;
  factory PageDto.fromJson(Map<String, dynamic> json) =>
      _$PageDtoFromJson(json);
}
