// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'pagination_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

PaginationDto<T> _$PaginationDtoFromJson<T>(
    Map<String, dynamic> json, T Function(Object?) fromJsonT) {
  return _PaginationDto<T>.fromJson(json, fromJsonT);
}

/// @nodoc
mixin _$PaginationDto<T> {
  PageableDto get pageable => throw _privateConstructorUsedError;
  bool get last => throw _privateConstructorUsedError;
  int get totalElements => throw _privateConstructorUsedError;
  int get totalPages => throw _privateConstructorUsedError;
  int get size => throw _privateConstructorUsedError;
  int get number => throw _privateConstructorUsedError;
  SortDto get sort => throw _privateConstructorUsedError;
  bool get first => throw _privateConstructorUsedError;
  int get numberOfElements => throw _privateConstructorUsedError;
  bool get empty => throw _privateConstructorUsedError;
  List<T> get content => throw _privateConstructorUsedError;

  /// Serializes this PaginationDto to a JSON map.
  Map<String, dynamic> toJson(Object? Function(T) toJsonT) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
@JsonSerializable(genericArgumentFactories: true)
class _$PaginationDtoImpl<T> extends _PaginationDto<T> {
  const _$PaginationDtoImpl(
      {required this.pageable,
      required this.last,
      required this.totalElements,
      required this.totalPages,
      required this.size,
      required this.number,
      required this.sort,
      required this.first,
      required this.numberOfElements,
      required this.empty,
      final List<T> content = const []})
      : _content = content,
        super._();

  factory _$PaginationDtoImpl.fromJson(
          Map<String, dynamic> json, T Function(Object?) fromJsonT) =>
      _$$PaginationDtoImplFromJson(json, fromJsonT);

  @override
  final PageableDto pageable;
  @override
  final bool last;
  @override
  final int totalElements;
  @override
  final int totalPages;
  @override
  final int size;
  @override
  final int number;
  @override
  final SortDto sort;
  @override
  final bool first;
  @override
  final int numberOfElements;
  @override
  final bool empty;
  final List<T> _content;
  @override
  @JsonKey()
  List<T> get content {
    if (_content is EqualUnmodifiableListView) return _content;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_content);
  }

  @override
  String toString() {
    return 'PaginationDto<$T>(pageable: $pageable, last: $last, totalElements: $totalElements, totalPages: $totalPages, size: $size, number: $number, sort: $sort, first: $first, numberOfElements: $numberOfElements, empty: $empty, content: $content)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$PaginationDtoImpl<T> &&
            (identical(other.pageable, pageable) ||
                other.pageable == pageable) &&
            (identical(other.last, last) || other.last == last) &&
            (identical(other.totalElements, totalElements) ||
                other.totalElements == totalElements) &&
            (identical(other.totalPages, totalPages) ||
                other.totalPages == totalPages) &&
            (identical(other.size, size) || other.size == size) &&
            (identical(other.number, number) || other.number == number) &&
            (identical(other.sort, sort) || other.sort == sort) &&
            (identical(other.first, first) || other.first == first) &&
            (identical(other.numberOfElements, numberOfElements) ||
                other.numberOfElements == numberOfElements) &&
            (identical(other.empty, empty) || other.empty == empty) &&
            const DeepCollectionEquality().equals(other._content, _content));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      pageable,
      last,
      totalElements,
      totalPages,
      size,
      number,
      sort,
      first,
      numberOfElements,
      empty,
      const DeepCollectionEquality().hash(_content));

  @override
  Map<String, dynamic> toJson(Object? Function(T) toJsonT) {
    return _$$PaginationDtoImplToJson<T>(this, toJsonT);
  }
}

abstract class _PaginationDto<T> extends PaginationDto<T> {
  const factory _PaginationDto(
      {required final PageableDto pageable,
      required final bool last,
      required final int totalElements,
      required final int totalPages,
      required final int size,
      required final int number,
      required final SortDto sort,
      required final bool first,
      required final int numberOfElements,
      required final bool empty,
      final List<T> content}) = _$PaginationDtoImpl<T>;
  const _PaginationDto._() : super._();

  factory _PaginationDto.fromJson(
          Map<String, dynamic> json, T Function(Object?) fromJsonT) =
      _$PaginationDtoImpl<T>.fromJson;

  @override
  PageableDto get pageable;
  @override
  bool get last;
  @override
  int get totalElements;
  @override
  int get totalPages;
  @override
  int get size;
  @override
  int get number;
  @override
  SortDto get sort;
  @override
  bool get first;
  @override
  int get numberOfElements;
  @override
  bool get empty;
  @override
  List<T> get content;
}
