// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'series_metadata_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

SeriesMetadataDto _$SeriesMetadataDtoFromJson(Map<String, dynamic> json) {
  return _SeriesMetadataDto.fromJson(json);
}

/// @nodoc
mixin _$SeriesMetadataDto {
  String get status => throw _privateConstructorUsedError;
  bool get statusLock => throw _privateConstructorUsedError;
  String get title => throw _privateConstructorUsedError;
  bool get titleLock => throw _privateConstructorUsedError;
  String get titleSort => throw _privateConstructorUsedError;
  bool get titleSortLock => throw _privateConstructorUsedError;
  String get summary => throw _privateConstructorUsedError;
  bool get summaryLock => throw _privateConstructorUsedError;
  String get readingDirection => throw _privateConstructorUsedError;
  bool get readingDirectionLock => throw _privateConstructorUsedError;
  String get publisher => throw _privateConstructorUsedError;
  bool get publisherLock => throw _privateConstructorUsedError;
  bool get ageRatingLock => throw _privateConstructorUsedError;
  String get language => throw _privateConstructorUsedError;
  bool get languageLock => throw _privateConstructorUsedError;
  Set<String> get genres => throw _privateConstructorUsedError;
  bool get genresLock => throw _privateConstructorUsedError;
  Set<String> get tags => throw _privateConstructorUsedError;
  bool get tagsLock => throw _privateConstructorUsedError;
  DateTime get created => throw _privateConstructorUsedError;
  DateTime get lastModified => throw _privateConstructorUsedError;
  bool get totalBookCountLock => throw _privateConstructorUsedError;
  Set<String> get sharingLabels => throw _privateConstructorUsedError;
  bool get sharingLabelsLock => throw _privateConstructorUsedError;
  bool get alternateTitlesLock => throw _privateConstructorUsedError;
  bool get linksLock => throw _privateConstructorUsedError;
  Set<String> get alternateTitles => throw _privateConstructorUsedError;
  Set<String> get links => throw _privateConstructorUsedError;
  int? get totalBookCount => throw _privateConstructorUsedError;
  int? get ageRating => throw _privateConstructorUsedError;

  /// Serializes this SeriesMetadataDto to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
}

/// @nodoc
@JsonSerializable()
class _$SeriesMetadataDtoImpl implements _SeriesMetadataDto {
  const _$SeriesMetadataDtoImpl(
      {required this.status,
      required this.statusLock,
      required this.title,
      required this.titleLock,
      required this.titleSort,
      required this.titleSortLock,
      required this.summary,
      required this.summaryLock,
      required this.readingDirection,
      required this.readingDirectionLock,
      required this.publisher,
      required this.publisherLock,
      required this.ageRatingLock,
      required this.language,
      required this.languageLock,
      required final Set<String> genres,
      required this.genresLock,
      required final Set<String> tags,
      required this.tagsLock,
      required this.created,
      required this.lastModified,
      required this.totalBookCountLock,
      required final Set<String> sharingLabels,
      required this.sharingLabelsLock,
      this.alternateTitlesLock = false,
      this.linksLock = false,
      final Set<String> alternateTitles = const {},
      final Set<String> links = const {},
      this.totalBookCount,
      this.ageRating})
      : _genres = genres,
        _tags = tags,
        _sharingLabels = sharingLabels,
        _alternateTitles = alternateTitles,
        _links = links;

  factory _$SeriesMetadataDtoImpl.fromJson(Map<String, dynamic> json) =>
      _$$SeriesMetadataDtoImplFromJson(json);

  @override
  final String status;
  @override
  final bool statusLock;
  @override
  final String title;
  @override
  final bool titleLock;
  @override
  final String titleSort;
  @override
  final bool titleSortLock;
  @override
  final String summary;
  @override
  final bool summaryLock;
  @override
  final String readingDirection;
  @override
  final bool readingDirectionLock;
  @override
  final String publisher;
  @override
  final bool publisherLock;
  @override
  final bool ageRatingLock;
  @override
  final String language;
  @override
  final bool languageLock;
  final Set<String> _genres;
  @override
  Set<String> get genres {
    if (_genres is EqualUnmodifiableSetView) return _genres;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableSetView(_genres);
  }

  @override
  final bool genresLock;
  final Set<String> _tags;
  @override
  Set<String> get tags {
    if (_tags is EqualUnmodifiableSetView) return _tags;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableSetView(_tags);
  }

  @override
  final bool tagsLock;
  @override
  final DateTime created;
  @override
  final DateTime lastModified;
  @override
  final bool totalBookCountLock;
  final Set<String> _sharingLabels;
  @override
  Set<String> get sharingLabels {
    if (_sharingLabels is EqualUnmodifiableSetView) return _sharingLabels;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableSetView(_sharingLabels);
  }

  @override
  final bool sharingLabelsLock;
  @override
  @JsonKey()
  final bool alternateTitlesLock;
  @override
  @JsonKey()
  final bool linksLock;
  final Set<String> _alternateTitles;
  @override
  @JsonKey()
  Set<String> get alternateTitles {
    if (_alternateTitles is EqualUnmodifiableSetView) return _alternateTitles;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableSetView(_alternateTitles);
  }

  final Set<String> _links;
  @override
  @JsonKey()
  Set<String> get links {
    if (_links is EqualUnmodifiableSetView) return _links;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableSetView(_links);
  }

  @override
  final int? totalBookCount;
  @override
  final int? ageRating;

  @override
  String toString() {
    return 'SeriesMetadataDto(status: $status, statusLock: $statusLock, title: $title, titleLock: $titleLock, titleSort: $titleSort, titleSortLock: $titleSortLock, summary: $summary, summaryLock: $summaryLock, readingDirection: $readingDirection, readingDirectionLock: $readingDirectionLock, publisher: $publisher, publisherLock: $publisherLock, ageRatingLock: $ageRatingLock, language: $language, languageLock: $languageLock, genres: $genres, genresLock: $genresLock, tags: $tags, tagsLock: $tagsLock, created: $created, lastModified: $lastModified, totalBookCountLock: $totalBookCountLock, sharingLabels: $sharingLabels, sharingLabelsLock: $sharingLabelsLock, alternateTitlesLock: $alternateTitlesLock, linksLock: $linksLock, alternateTitles: $alternateTitles, links: $links, totalBookCount: $totalBookCount, ageRating: $ageRating)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SeriesMetadataDtoImpl &&
            (identical(other.status, status) || other.status == status) &&
            (identical(other.statusLock, statusLock) ||
                other.statusLock == statusLock) &&
            (identical(other.title, title) || other.title == title) &&
            (identical(other.titleLock, titleLock) ||
                other.titleLock == titleLock) &&
            (identical(other.titleSort, titleSort) ||
                other.titleSort == titleSort) &&
            (identical(other.titleSortLock, titleSortLock) ||
                other.titleSortLock == titleSortLock) &&
            (identical(other.summary, summary) || other.summary == summary) &&
            (identical(other.summaryLock, summaryLock) ||
                other.summaryLock == summaryLock) &&
            (identical(other.readingDirection, readingDirection) ||
                other.readingDirection == readingDirection) &&
            (identical(other.readingDirectionLock, readingDirectionLock) ||
                other.readingDirectionLock == readingDirectionLock) &&
            (identical(other.publisher, publisher) ||
                other.publisher == publisher) &&
            (identical(other.publisherLock, publisherLock) ||
                other.publisherLock == publisherLock) &&
            (identical(other.ageRatingLock, ageRatingLock) ||
                other.ageRatingLock == ageRatingLock) &&
            (identical(other.language, language) ||
                other.language == language) &&
            (identical(other.languageLock, languageLock) ||
                other.languageLock == languageLock) &&
            const DeepCollectionEquality().equals(other._genres, _genres) &&
            (identical(other.genresLock, genresLock) ||
                other.genresLock == genresLock) &&
            const DeepCollectionEquality().equals(other._tags, _tags) &&
            (identical(other.tagsLock, tagsLock) ||
                other.tagsLock == tagsLock) &&
            (identical(other.created, created) || other.created == created) &&
            (identical(other.lastModified, lastModified) ||
                other.lastModified == lastModified) &&
            (identical(other.totalBookCountLock, totalBookCountLock) ||
                other.totalBookCountLock == totalBookCountLock) &&
            const DeepCollectionEquality()
                .equals(other._sharingLabels, _sharingLabels) &&
            (identical(other.sharingLabelsLock, sharingLabelsLock) ||
                other.sharingLabelsLock == sharingLabelsLock) &&
            (identical(other.alternateTitlesLock, alternateTitlesLock) ||
                other.alternateTitlesLock == alternateTitlesLock) &&
            (identical(other.linksLock, linksLock) ||
                other.linksLock == linksLock) &&
            const DeepCollectionEquality()
                .equals(other._alternateTitles, _alternateTitles) &&
            const DeepCollectionEquality().equals(other._links, _links) &&
            (identical(other.totalBookCount, totalBookCount) ||
                other.totalBookCount == totalBookCount) &&
            (identical(other.ageRating, ageRating) ||
                other.ageRating == ageRating));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => Object.hashAll([
        runtimeType,
        status,
        statusLock,
        title,
        titleLock,
        titleSort,
        titleSortLock,
        summary,
        summaryLock,
        readingDirection,
        readingDirectionLock,
        publisher,
        publisherLock,
        ageRatingLock,
        language,
        languageLock,
        const DeepCollectionEquality().hash(_genres),
        genresLock,
        const DeepCollectionEquality().hash(_tags),
        tagsLock,
        created,
        lastModified,
        totalBookCountLock,
        const DeepCollectionEquality().hash(_sharingLabels),
        sharingLabelsLock,
        alternateTitlesLock,
        linksLock,
        const DeepCollectionEquality().hash(_alternateTitles),
        const DeepCollectionEquality().hash(_links),
        totalBookCount,
        ageRating
      ]);

  @override
  Map<String, dynamic> toJson() {
    return _$$SeriesMetadataDtoImplToJson(
      this,
    );
  }
}

abstract class _SeriesMetadataDto implements SeriesMetadataDto {
  const factory _SeriesMetadataDto(
      {required final String status,
      required final bool statusLock,
      required final String title,
      required final bool titleLock,
      required final String titleSort,
      required final bool titleSortLock,
      required final String summary,
      required final bool summaryLock,
      required final String readingDirection,
      required final bool readingDirectionLock,
      required final String publisher,
      required final bool publisherLock,
      required final bool ageRatingLock,
      required final String language,
      required final bool languageLock,
      required final Set<String> genres,
      required final bool genresLock,
      required final Set<String> tags,
      required final bool tagsLock,
      required final DateTime created,
      required final DateTime lastModified,
      required final bool totalBookCountLock,
      required final Set<String> sharingLabels,
      required final bool sharingLabelsLock,
      final bool alternateTitlesLock,
      final bool linksLock,
      final Set<String> alternateTitles,
      final Set<String> links,
      final int? totalBookCount,
      final int? ageRating}) = _$SeriesMetadataDtoImpl;

  factory _SeriesMetadataDto.fromJson(Map<String, dynamic> json) =
      _$SeriesMetadataDtoImpl.fromJson;

  @override
  String get status;
  @override
  bool get statusLock;
  @override
  String get title;
  @override
  bool get titleLock;
  @override
  String get titleSort;
  @override
  bool get titleSortLock;
  @override
  String get summary;
  @override
  bool get summaryLock;
  @override
  String get readingDirection;
  @override
  bool get readingDirectionLock;
  @override
  String get publisher;
  @override
  bool get publisherLock;
  @override
  bool get ageRatingLock;
  @override
  String get language;
  @override
  bool get languageLock;
  @override
  Set<String> get genres;
  @override
  bool get genresLock;
  @override
  Set<String> get tags;
  @override
  bool get tagsLock;
  @override
  DateTime get created;
  @override
  DateTime get lastModified;
  @override
  bool get totalBookCountLock;
  @override
  Set<String> get sharingLabels;
  @override
  bool get sharingLabelsLock;
  @override
  bool get alternateTitlesLock;
  @override
  bool get linksLock;
  @override
  Set<String> get alternateTitles;
  @override
  Set<String> get links;
  @override
  int? get totalBookCount;
  @override
  int? get ageRating;
}
