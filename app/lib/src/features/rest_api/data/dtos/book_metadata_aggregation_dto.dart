// ignore_for_file: public_member_api_docs
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/author_dto.dart';

part 'book_metadata_aggregation_dto.freezed.dart';
part 'book_metadata_aggregation_dto.g.dart';

@Freezed(copyWith: false)
class BookMetadataAggregationDto with _$BookMetadataAggregationDto {
  const factory BookMetadataAggregationDto({
    required List<AuthorDto> authors,
    required Set<String> tags,
    required String summary,
    required String summaryNumber,
    required DateTime created,
    required DateTime lastModified,
    DateTime? releaseDate,
  }) = _BookMetadataAggregationDto;

  factory BookMetadataAggregationDto.fromJson(Map<String, dynamic> json) =>
      _$BookMetadataAggregationDtoFromJson(json);
}
