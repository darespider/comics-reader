// ignore_for_file: public_member_api_docs
import 'package:freezed_annotation/freezed_annotation.dart';

part 'read_list_creation_dto.freezed.dart';
part 'read_list_creation_dto.g.dart';

@Freezed(copyWith: false)
class ReadListCreationDto with _$ReadListCreationDto {
  const factory ReadListCreationDto({
    required String name,
    required List<String> bookIds,
  }) = _ReadListCreationDto;

  factory ReadListCreationDto.fromJson(Map<String, dynamic> json) =>
      _$ReadListCreationDtoFromJson(json);
}
