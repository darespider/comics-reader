// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'age_restriction_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

AgeRestrictionDto _$AgeRestrictionDtoFromJson(Map<String, dynamic> json) {
  return _AgeRestrictionDto.fromJson(json);
}

/// @nodoc
mixin _$AgeRestrictionDto {
  int get age => throw _privateConstructorUsedError;
  String get restriction => throw _privateConstructorUsedError;

  /// Serializes this AgeRestrictionDto to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
}

/// @nodoc
@JsonSerializable()
class _$AgeRestrictionDtoImpl implements _AgeRestrictionDto {
  const _$AgeRestrictionDtoImpl({required this.age, required this.restriction});

  factory _$AgeRestrictionDtoImpl.fromJson(Map<String, dynamic> json) =>
      _$$AgeRestrictionDtoImplFromJson(json);

  @override
  final int age;
  @override
  final String restriction;

  @override
  String toString() {
    return 'AgeRestrictionDto(age: $age, restriction: $restriction)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$AgeRestrictionDtoImpl &&
            (identical(other.age, age) || other.age == age) &&
            (identical(other.restriction, restriction) ||
                other.restriction == restriction));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => Object.hash(runtimeType, age, restriction);

  @override
  Map<String, dynamic> toJson() {
    return _$$AgeRestrictionDtoImplToJson(
      this,
    );
  }
}

abstract class _AgeRestrictionDto implements AgeRestrictionDto {
  const factory _AgeRestrictionDto(
      {required final int age,
      required final String restriction}) = _$AgeRestrictionDtoImpl;

  factory _AgeRestrictionDto.fromJson(Map<String, dynamic> json) =
      _$AgeRestrictionDtoImpl.fromJson;

  @override
  int get age;
  @override
  String get restriction;
}
