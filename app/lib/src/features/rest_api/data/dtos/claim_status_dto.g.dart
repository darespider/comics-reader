// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'claim_status_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$ClaimStatusDtoImpl _$$ClaimStatusDtoImplFromJson(Map<String, dynamic> json) =>
    _$ClaimStatusDtoImpl(
      isClaimed: json['isClaimed'] as bool,
    );

Map<String, dynamic> _$$ClaimStatusDtoImplToJson(
        _$ClaimStatusDtoImpl instance) =>
    <String, dynamic>{
      'isClaimed': instance.isClaimed,
    };
