// ignore_for_file: public_member_api_docs
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/shared_library_dto.dart';

part 'users_with_shared_libraries_dto.freezed.dart';
part 'users_with_shared_libraries_dto.g.dart';

@Freezed(copyWith: false)
class UsersWithSharedLibrariesDto with _$UsersWithSharedLibrariesDto {
  const factory UsersWithSharedLibrariesDto({
    required String id,
    required String email,
    required List<String> roles,
    required bool sharedAllLibraries,
    required List<SharedLibraryDto> sharedLibraries,
  }) = _UsersWithSharedLibrariesDto;

  factory UsersWithSharedLibrariesDto.fromJson(Map<String, dynamic> json) =>
      _$UsersWithSharedLibrariesDtoFromJson(json);
}
