// ignore_for_file: public_member_api_docs
import 'package:freezed_annotation/freezed_annotation.dart';

part 'age_restriction_dto.freezed.dart';
part 'age_restriction_dto.g.dart';

@Freezed(copyWith: false)
class AgeRestrictionDto with _$AgeRestrictionDto {
  const factory AgeRestrictionDto({
    required int age,
    required String restriction,
  }) = _AgeRestrictionDto;

  factory AgeRestrictionDto.fromJson(Map<String, dynamic> json) =>
      _$AgeRestrictionDtoFromJson(json);
}
