// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'claim_status_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

ClaimStatusDto _$ClaimStatusDtoFromJson(Map<String, dynamic> json) {
  return _ClaimStatusDto.fromJson(json);
}

/// @nodoc
mixin _$ClaimStatusDto {
  bool get isClaimed => throw _privateConstructorUsedError;

  /// Serializes this ClaimStatusDto to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
}

/// @nodoc
@JsonSerializable()
class _$ClaimStatusDtoImpl implements _ClaimStatusDto {
  const _$ClaimStatusDtoImpl({required this.isClaimed});

  factory _$ClaimStatusDtoImpl.fromJson(Map<String, dynamic> json) =>
      _$$ClaimStatusDtoImplFromJson(json);

  @override
  final bool isClaimed;

  @override
  String toString() {
    return 'ClaimStatusDto(isClaimed: $isClaimed)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ClaimStatusDtoImpl &&
            (identical(other.isClaimed, isClaimed) ||
                other.isClaimed == isClaimed));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => Object.hash(runtimeType, isClaimed);

  @override
  Map<String, dynamic> toJson() {
    return _$$ClaimStatusDtoImplToJson(
      this,
    );
  }
}

abstract class _ClaimStatusDto implements ClaimStatusDto {
  const factory _ClaimStatusDto({required final bool isClaimed}) =
      _$ClaimStatusDtoImpl;

  factory _ClaimStatusDto.fromJson(Map<String, dynamic> json) =
      _$ClaimStatusDtoImpl.fromJson;

  @override
  bool get isClaimed;
}
