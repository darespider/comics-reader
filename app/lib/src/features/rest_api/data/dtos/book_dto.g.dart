// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'book_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$BookDtoImpl _$$BookDtoImplFromJson(Map<String, dynamic> json) =>
    _$BookDtoImpl(
      id: json['id'] as String,
      seriesId: json['seriesId'] as String,
      seriesTitle: json['seriesTitle'] as String,
      libraryId: json['libraryId'] as String,
      name: json['name'] as String,
      url: json['url'] as String,
      deleted: json['deleted'] as bool,
      number: (json['number'] as num).toInt(),
      created: DateTime.parse(json['created'] as String),
      lastModified: DateTime.parse(json['lastModified'] as String),
      fileLastModified: DateTime.parse(json['fileLastModified'] as String),
      sizeBytes: (json['sizeBytes'] as num).toInt(),
      size: json['size'] as String,
      media: MediaDto.fromJson(json['media'] as Map<String, dynamic>),
      metadata:
          BookMetadataDto.fromJson(json['metadata'] as Map<String, dynamic>),
      fileHash: json['fileHash'] as String,
      oneshot: json['oneshot'] as bool,
      readProgress: json['readProgress'] == null
          ? null
          : ReadProgressDto.fromJson(
              json['readProgress'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$BookDtoImplToJson(_$BookDtoImpl instance) =>
    <String, dynamic>{
      'id': instance.id,
      'seriesId': instance.seriesId,
      'seriesTitle': instance.seriesTitle,
      'libraryId': instance.libraryId,
      'name': instance.name,
      'url': instance.url,
      'deleted': instance.deleted,
      'number': instance.number,
      'created': instance.created.toIso8601String(),
      'lastModified': instance.lastModified.toIso8601String(),
      'fileLastModified': instance.fileLastModified.toIso8601String(),
      'sizeBytes': instance.sizeBytes,
      'size': instance.size,
      'media': instance.media,
      'metadata': instance.metadata,
      'fileHash': instance.fileHash,
      'oneshot': instance.oneshot,
      'readProgress': instance.readProgress,
    };
