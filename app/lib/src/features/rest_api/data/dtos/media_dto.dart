// ignore_for_file: public_member_api_docs
import 'package:freezed_annotation/freezed_annotation.dart';

part 'media_dto.freezed.dart';
part 'media_dto.g.dart';

@Freezed(copyWith: false)
class MediaDto with _$MediaDto {
  const factory MediaDto({
    required String status,
    required String mediaType,
    required int pagesCount,
    required String comment,
  }) = _MediaDto;

  factory MediaDto.fromJson(Map<String, dynamic> json) =>
      _$MediaDtoFromJson(json);
}
