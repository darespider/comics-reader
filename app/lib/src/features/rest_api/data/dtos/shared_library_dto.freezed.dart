// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'shared_library_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

SharedLibraryDto _$SharedLibraryDtoFromJson(Map<String, dynamic> json) {
  return _SharedLibraryDto.fromJson(json);
}

/// @nodoc
mixin _$SharedLibraryDto {
  String get id => throw _privateConstructorUsedError;

  /// Serializes this SharedLibraryDto to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
}

/// @nodoc
@JsonSerializable()
class _$SharedLibraryDtoImpl implements _SharedLibraryDto {
  const _$SharedLibraryDtoImpl({required this.id});

  factory _$SharedLibraryDtoImpl.fromJson(Map<String, dynamic> json) =>
      _$$SharedLibraryDtoImplFromJson(json);

  @override
  final String id;

  @override
  String toString() {
    return 'SharedLibraryDto(id: $id)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SharedLibraryDtoImpl &&
            (identical(other.id, id) || other.id == id));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => Object.hash(runtimeType, id);

  @override
  Map<String, dynamic> toJson() {
    return _$$SharedLibraryDtoImplToJson(
      this,
    );
  }
}

abstract class _SharedLibraryDto implements SharedLibraryDto {
  const factory _SharedLibraryDto({required final String id}) =
      _$SharedLibraryDtoImpl;

  factory _SharedLibraryDto.fromJson(Map<String, dynamic> json) =
      _$SharedLibraryDtoImpl.fromJson;

  @override
  String get id;
}
