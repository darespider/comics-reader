// ignore_for_file: public_member_api_docs
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kapoow/src/features/library_management/common/models/series_status.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/reading_direction.dart';

part 'series_metadata_update_dto.freezed.dart';
part 'series_metadata_update_dto.g.dart';

@Freezed(copyWith: false)
class SeriesMetadataUpdateDto with _$SeriesMetadataUpdateDto {
  const factory SeriesMetadataUpdateDto({
    required SeriesStatus status,
    required bool statusLock,
    required String title,
    required bool titleLock,
    required String titleSort,
    required bool titleSortLock,
    required String summary,
    required bool summaryLock,
    required String publisher,
    required bool publisherLock,
    required bool readingDirectionLock,
    required String language,
    required bool languageLock,
    required bool genresLock,
    required bool tagsLock,
    required Set<String> tags,
    required ReadingDirection readingDirection,
    required int ageRating,
    required Set<String> genres,
  }) = _SeriesMetadataUpdateDto;

  factory SeriesMetadataUpdateDto.fromJson(Map<String, dynamic> json) =>
      _$SeriesMetadataUpdateDtoFromJson(json);
}
