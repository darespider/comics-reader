// ignore_for_file: public_member_api_docs
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/book_dto.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/collection_dto.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/pageable_dto.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/read_list_dto.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/series_dto.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/sort_dto.dart';

part 'pagination_dto.freezed.dart';
part 'pagination_dto.g.dart';

@Freezed(copyWith: false, genericArgumentFactories: true)
class PaginationDto<T> with _$PaginationDto<T> {
  const factory PaginationDto({
    required PageableDto pageable,
    required bool last,
    required int totalElements,
    required int totalPages,
    required int size,
    required int number,
    required SortDto sort,
    required bool first,
    required int numberOfElements,
    required bool empty,
    @Default([]) List<T> content,
  }) = _PaginationDto<T>;
  const PaginationDto._();

  factory PaginationDto.fromJson(
    Map<String, dynamic> json,
    T Function(Object?) fromJsonT,
  ) =>
      _$PaginationDtoFromJson(json, fromJsonT);

  static PageBookDto pageBookFromJson(Map<String, dynamic> json) =>
      PaginationDto<BookDto>.fromJson(
        json,
        (j) => BookDto.fromJson(j! as Map<String, dynamic>),
      );

  static PageSeriesDto pageSeriesFromJson(Map<String, dynamic> json) =>
      PaginationDto<SeriesDto>.fromJson(
        json,
        (j) => SeriesDto.fromJson(j! as Map<String, dynamic>),
      );

  static PageCollectionDto pageCollectionFromJson(Map<String, dynamic> json) =>
      PaginationDto<CollectionDto>.fromJson(
        json,
        (j) => CollectionDto.fromJson(j! as Map<String, dynamic>),
      );

  static PageReadListDto pageReadListFromJson(Map<String, dynamic> json) =>
      PaginationDto<ReadListDto>.fromJson(
        json,
        (j) => ReadListDto.fromJson(j! as Map<String, dynamic>),
      );
}

typedef PageBookDto = PaginationDto<BookDto>;
typedef PageSeriesDto = PaginationDto<SeriesDto>;
typedef PageCollectionDto = PaginationDto<CollectionDto>;
typedef PageReadListDto = PaginationDto<ReadListDto>;
