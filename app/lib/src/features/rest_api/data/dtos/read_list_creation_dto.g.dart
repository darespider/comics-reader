// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'read_list_creation_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$ReadListCreationDtoImpl _$$ReadListCreationDtoImplFromJson(
        Map<String, dynamic> json) =>
    _$ReadListCreationDtoImpl(
      name: json['name'] as String,
      bookIds:
          (json['bookIds'] as List<dynamic>).map((e) => e as String).toList(),
    );

Map<String, dynamic> _$$ReadListCreationDtoImplToJson(
        _$ReadListCreationDtoImpl instance) =>
    <String, dynamic>{
      'name': instance.name,
      'bookIds': instance.bookIds,
    };
