// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'library_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

LibraryDto _$LibraryDtoFromJson(Map<String, dynamic> json) {
  return _LibraryDto.fromJson(json);
}

/// @nodoc
mixin _$LibraryDto {
  String get id => throw _privateConstructorUsedError;
  String get name => throw _privateConstructorUsedError;
  String get root => throw _privateConstructorUsedError;
  bool get importComicInfoBook => throw _privateConstructorUsedError;
  bool get importComicInfoSeries => throw _privateConstructorUsedError;
  bool get importComicInfoCollection => throw _privateConstructorUsedError;
  bool get importComicInfoReadList => throw _privateConstructorUsedError;
  bool get importComicInfoSeriesAppendVolume =>
      throw _privateConstructorUsedError;
  bool get importEpubBook => throw _privateConstructorUsedError;
  bool get importEpubSeries => throw _privateConstructorUsedError;
  bool get importMylarSeries => throw _privateConstructorUsedError;
  bool get importLocalArtwork => throw _privateConstructorUsedError;
  bool get importBarcodeIsbn => throw _privateConstructorUsedError;
  bool get scanForceModifiedTime => throw _privateConstructorUsedError;
  String get scanInterval => throw _privateConstructorUsedError;
  bool get scanOnStartup => throw _privateConstructorUsedError;
  bool get scanCbx => throw _privateConstructorUsedError;
  bool get scanPdf => throw _privateConstructorUsedError;
  bool get scanEpub => throw _privateConstructorUsedError;
  Set<String> get scanDirectoryExclusions => throw _privateConstructorUsedError;
  bool get repairExtensions => throw _privateConstructorUsedError;
  bool get convertToCbz => throw _privateConstructorUsedError;
  bool get emptyTrashAfterScan => throw _privateConstructorUsedError;
  String get seriesCover => throw _privateConstructorUsedError;
  bool get hashFiles => throw _privateConstructorUsedError;
  bool get hashPages => throw _privateConstructorUsedError;
  bool get analyzeDimensions => throw _privateConstructorUsedError;
  bool get unavailable => throw _privateConstructorUsedError;
  String? get oneshotsDirectory => throw _privateConstructorUsedError;

  /// Serializes this LibraryDto to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
}

/// @nodoc
@JsonSerializable()
class _$LibraryDtoImpl implements _LibraryDto {
  const _$LibraryDtoImpl(
      {required this.id,
      required this.name,
      required this.root,
      required this.importComicInfoBook,
      required this.importComicInfoSeries,
      required this.importComicInfoCollection,
      required this.importComicInfoReadList,
      required this.importComicInfoSeriesAppendVolume,
      required this.importEpubBook,
      required this.importEpubSeries,
      required this.importMylarSeries,
      required this.importLocalArtwork,
      required this.importBarcodeIsbn,
      required this.scanForceModifiedTime,
      required this.scanInterval,
      required this.scanOnStartup,
      required this.scanCbx,
      required this.scanPdf,
      required this.scanEpub,
      required final Set<String> scanDirectoryExclusions,
      required this.repairExtensions,
      required this.convertToCbz,
      required this.emptyTrashAfterScan,
      required this.seriesCover,
      required this.hashFiles,
      required this.hashPages,
      required this.analyzeDimensions,
      required this.unavailable,
      this.oneshotsDirectory})
      : _scanDirectoryExclusions = scanDirectoryExclusions;

  factory _$LibraryDtoImpl.fromJson(Map<String, dynamic> json) =>
      _$$LibraryDtoImplFromJson(json);

  @override
  final String id;
  @override
  final String name;
  @override
  final String root;
  @override
  final bool importComicInfoBook;
  @override
  final bool importComicInfoSeries;
  @override
  final bool importComicInfoCollection;
  @override
  final bool importComicInfoReadList;
  @override
  final bool importComicInfoSeriesAppendVolume;
  @override
  final bool importEpubBook;
  @override
  final bool importEpubSeries;
  @override
  final bool importMylarSeries;
  @override
  final bool importLocalArtwork;
  @override
  final bool importBarcodeIsbn;
  @override
  final bool scanForceModifiedTime;
  @override
  final String scanInterval;
  @override
  final bool scanOnStartup;
  @override
  final bool scanCbx;
  @override
  final bool scanPdf;
  @override
  final bool scanEpub;
  final Set<String> _scanDirectoryExclusions;
  @override
  Set<String> get scanDirectoryExclusions {
    if (_scanDirectoryExclusions is EqualUnmodifiableSetView)
      return _scanDirectoryExclusions;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableSetView(_scanDirectoryExclusions);
  }

  @override
  final bool repairExtensions;
  @override
  final bool convertToCbz;
  @override
  final bool emptyTrashAfterScan;
  @override
  final String seriesCover;
  @override
  final bool hashFiles;
  @override
  final bool hashPages;
  @override
  final bool analyzeDimensions;
  @override
  final bool unavailable;
  @override
  final String? oneshotsDirectory;

  @override
  String toString() {
    return 'LibraryDto(id: $id, name: $name, root: $root, importComicInfoBook: $importComicInfoBook, importComicInfoSeries: $importComicInfoSeries, importComicInfoCollection: $importComicInfoCollection, importComicInfoReadList: $importComicInfoReadList, importComicInfoSeriesAppendVolume: $importComicInfoSeriesAppendVolume, importEpubBook: $importEpubBook, importEpubSeries: $importEpubSeries, importMylarSeries: $importMylarSeries, importLocalArtwork: $importLocalArtwork, importBarcodeIsbn: $importBarcodeIsbn, scanForceModifiedTime: $scanForceModifiedTime, scanInterval: $scanInterval, scanOnStartup: $scanOnStartup, scanCbx: $scanCbx, scanPdf: $scanPdf, scanEpub: $scanEpub, scanDirectoryExclusions: $scanDirectoryExclusions, repairExtensions: $repairExtensions, convertToCbz: $convertToCbz, emptyTrashAfterScan: $emptyTrashAfterScan, seriesCover: $seriesCover, hashFiles: $hashFiles, hashPages: $hashPages, analyzeDimensions: $analyzeDimensions, unavailable: $unavailable, oneshotsDirectory: $oneshotsDirectory)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$LibraryDtoImpl &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.root, root) || other.root == root) &&
            (identical(other.importComicInfoBook, importComicInfoBook) ||
                other.importComicInfoBook == importComicInfoBook) &&
            (identical(other.importComicInfoSeries, importComicInfoSeries) ||
                other.importComicInfoSeries == importComicInfoSeries) &&
            (identical(other.importComicInfoCollection, importComicInfoCollection) ||
                other.importComicInfoCollection == importComicInfoCollection) &&
            (identical(other.importComicInfoReadList, importComicInfoReadList) ||
                other.importComicInfoReadList == importComicInfoReadList) &&
            (identical(other.importComicInfoSeriesAppendVolume,
                    importComicInfoSeriesAppendVolume) ||
                other.importComicInfoSeriesAppendVolume ==
                    importComicInfoSeriesAppendVolume) &&
            (identical(other.importEpubBook, importEpubBook) ||
                other.importEpubBook == importEpubBook) &&
            (identical(other.importEpubSeries, importEpubSeries) ||
                other.importEpubSeries == importEpubSeries) &&
            (identical(other.importMylarSeries, importMylarSeries) ||
                other.importMylarSeries == importMylarSeries) &&
            (identical(other.importLocalArtwork, importLocalArtwork) ||
                other.importLocalArtwork == importLocalArtwork) &&
            (identical(other.importBarcodeIsbn, importBarcodeIsbn) ||
                other.importBarcodeIsbn == importBarcodeIsbn) &&
            (identical(other.scanForceModifiedTime, scanForceModifiedTime) ||
                other.scanForceModifiedTime == scanForceModifiedTime) &&
            (identical(other.scanInterval, scanInterval) ||
                other.scanInterval == scanInterval) &&
            (identical(other.scanOnStartup, scanOnStartup) ||
                other.scanOnStartup == scanOnStartup) &&
            (identical(other.scanCbx, scanCbx) || other.scanCbx == scanCbx) &&
            (identical(other.scanPdf, scanPdf) || other.scanPdf == scanPdf) &&
            (identical(other.scanEpub, scanEpub) ||
                other.scanEpub == scanEpub) &&
            const DeepCollectionEquality().equals(
                other._scanDirectoryExclusions, _scanDirectoryExclusions) &&
            (identical(other.repairExtensions, repairExtensions) ||
                other.repairExtensions == repairExtensions) &&
            (identical(other.convertToCbz, convertToCbz) ||
                other.convertToCbz == convertToCbz) &&
            (identical(other.emptyTrashAfterScan, emptyTrashAfterScan) ||
                other.emptyTrashAfterScan == emptyTrashAfterScan) &&
            (identical(other.seriesCover, seriesCover) ||
                other.seriesCover == seriesCover) &&
            (identical(other.hashFiles, hashFiles) || other.hashFiles == hashFiles) &&
            (identical(other.hashPages, hashPages) || other.hashPages == hashPages) &&
            (identical(other.analyzeDimensions, analyzeDimensions) || other.analyzeDimensions == analyzeDimensions) &&
            (identical(other.unavailable, unavailable) || other.unavailable == unavailable) &&
            (identical(other.oneshotsDirectory, oneshotsDirectory) || other.oneshotsDirectory == oneshotsDirectory));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => Object.hashAll([
        runtimeType,
        id,
        name,
        root,
        importComicInfoBook,
        importComicInfoSeries,
        importComicInfoCollection,
        importComicInfoReadList,
        importComicInfoSeriesAppendVolume,
        importEpubBook,
        importEpubSeries,
        importMylarSeries,
        importLocalArtwork,
        importBarcodeIsbn,
        scanForceModifiedTime,
        scanInterval,
        scanOnStartup,
        scanCbx,
        scanPdf,
        scanEpub,
        const DeepCollectionEquality().hash(_scanDirectoryExclusions),
        repairExtensions,
        convertToCbz,
        emptyTrashAfterScan,
        seriesCover,
        hashFiles,
        hashPages,
        analyzeDimensions,
        unavailable,
        oneshotsDirectory
      ]);

  @override
  Map<String, dynamic> toJson() {
    return _$$LibraryDtoImplToJson(
      this,
    );
  }
}

abstract class _LibraryDto implements LibraryDto {
  const factory _LibraryDto(
      {required final String id,
      required final String name,
      required final String root,
      required final bool importComicInfoBook,
      required final bool importComicInfoSeries,
      required final bool importComicInfoCollection,
      required final bool importComicInfoReadList,
      required final bool importComicInfoSeriesAppendVolume,
      required final bool importEpubBook,
      required final bool importEpubSeries,
      required final bool importMylarSeries,
      required final bool importLocalArtwork,
      required final bool importBarcodeIsbn,
      required final bool scanForceModifiedTime,
      required final String scanInterval,
      required final bool scanOnStartup,
      required final bool scanCbx,
      required final bool scanPdf,
      required final bool scanEpub,
      required final Set<String> scanDirectoryExclusions,
      required final bool repairExtensions,
      required final bool convertToCbz,
      required final bool emptyTrashAfterScan,
      required final String seriesCover,
      required final bool hashFiles,
      required final bool hashPages,
      required final bool analyzeDimensions,
      required final bool unavailable,
      final String? oneshotsDirectory}) = _$LibraryDtoImpl;

  factory _LibraryDto.fromJson(Map<String, dynamic> json) =
      _$LibraryDtoImpl.fromJson;

  @override
  String get id;
  @override
  String get name;
  @override
  String get root;
  @override
  bool get importComicInfoBook;
  @override
  bool get importComicInfoSeries;
  @override
  bool get importComicInfoCollection;
  @override
  bool get importComicInfoReadList;
  @override
  bool get importComicInfoSeriesAppendVolume;
  @override
  bool get importEpubBook;
  @override
  bool get importEpubSeries;
  @override
  bool get importMylarSeries;
  @override
  bool get importLocalArtwork;
  @override
  bool get importBarcodeIsbn;
  @override
  bool get scanForceModifiedTime;
  @override
  String get scanInterval;
  @override
  bool get scanOnStartup;
  @override
  bool get scanCbx;
  @override
  bool get scanPdf;
  @override
  bool get scanEpub;
  @override
  Set<String> get scanDirectoryExclusions;
  @override
  bool get repairExtensions;
  @override
  bool get convertToCbz;
  @override
  bool get emptyTrashAfterScan;
  @override
  String get seriesCover;
  @override
  bool get hashFiles;
  @override
  bool get hashPages;
  @override
  bool get analyzeDimensions;
  @override
  bool get unavailable;
  @override
  String? get oneshotsDirectory;
}
