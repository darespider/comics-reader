// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'book_metadata_aggregation_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

BookMetadataAggregationDto _$BookMetadataAggregationDtoFromJson(
    Map<String, dynamic> json) {
  return _BookMetadataAggregationDto.fromJson(json);
}

/// @nodoc
mixin _$BookMetadataAggregationDto {
  List<AuthorDto> get authors => throw _privateConstructorUsedError;
  Set<String> get tags => throw _privateConstructorUsedError;
  String get summary => throw _privateConstructorUsedError;
  String get summaryNumber => throw _privateConstructorUsedError;
  DateTime get created => throw _privateConstructorUsedError;
  DateTime get lastModified => throw _privateConstructorUsedError;
  DateTime? get releaseDate => throw _privateConstructorUsedError;

  /// Serializes this BookMetadataAggregationDto to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
}

/// @nodoc
@JsonSerializable()
class _$BookMetadataAggregationDtoImpl implements _BookMetadataAggregationDto {
  const _$BookMetadataAggregationDtoImpl(
      {required final List<AuthorDto> authors,
      required final Set<String> tags,
      required this.summary,
      required this.summaryNumber,
      required this.created,
      required this.lastModified,
      this.releaseDate})
      : _authors = authors,
        _tags = tags;

  factory _$BookMetadataAggregationDtoImpl.fromJson(
          Map<String, dynamic> json) =>
      _$$BookMetadataAggregationDtoImplFromJson(json);

  final List<AuthorDto> _authors;
  @override
  List<AuthorDto> get authors {
    if (_authors is EqualUnmodifiableListView) return _authors;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_authors);
  }

  final Set<String> _tags;
  @override
  Set<String> get tags {
    if (_tags is EqualUnmodifiableSetView) return _tags;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableSetView(_tags);
  }

  @override
  final String summary;
  @override
  final String summaryNumber;
  @override
  final DateTime created;
  @override
  final DateTime lastModified;
  @override
  final DateTime? releaseDate;

  @override
  String toString() {
    return 'BookMetadataAggregationDto(authors: $authors, tags: $tags, summary: $summary, summaryNumber: $summaryNumber, created: $created, lastModified: $lastModified, releaseDate: $releaseDate)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$BookMetadataAggregationDtoImpl &&
            const DeepCollectionEquality().equals(other._authors, _authors) &&
            const DeepCollectionEquality().equals(other._tags, _tags) &&
            (identical(other.summary, summary) || other.summary == summary) &&
            (identical(other.summaryNumber, summaryNumber) ||
                other.summaryNumber == summaryNumber) &&
            (identical(other.created, created) || other.created == created) &&
            (identical(other.lastModified, lastModified) ||
                other.lastModified == lastModified) &&
            (identical(other.releaseDate, releaseDate) ||
                other.releaseDate == releaseDate));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(_authors),
      const DeepCollectionEquality().hash(_tags),
      summary,
      summaryNumber,
      created,
      lastModified,
      releaseDate);

  @override
  Map<String, dynamic> toJson() {
    return _$$BookMetadataAggregationDtoImplToJson(
      this,
    );
  }
}

abstract class _BookMetadataAggregationDto
    implements BookMetadataAggregationDto {
  const factory _BookMetadataAggregationDto(
      {required final List<AuthorDto> authors,
      required final Set<String> tags,
      required final String summary,
      required final String summaryNumber,
      required final DateTime created,
      required final DateTime lastModified,
      final DateTime? releaseDate}) = _$BookMetadataAggregationDtoImpl;

  factory _BookMetadataAggregationDto.fromJson(Map<String, dynamic> json) =
      _$BookMetadataAggregationDtoImpl.fromJson;

  @override
  List<AuthorDto> get authors;
  @override
  Set<String> get tags;
  @override
  String get summary;
  @override
  String get summaryNumber;
  @override
  DateTime get created;
  @override
  DateTime get lastModified;
  @override
  DateTime? get releaseDate;
}
