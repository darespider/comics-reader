// ignore_for_file: public_member_api_docs
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/path_dto.dart';

part 'directory_listing_dto.freezed.dart';
part 'directory_listing_dto.g.dart';

@Freezed(copyWith: false)
class DirectoryListingDto with _$DirectoryListingDto {
  const factory DirectoryListingDto({
    required String parent,
    required List<PathDto> directories,
  }) = _DirectoryListingDto;

  factory DirectoryListingDto.fromJson(Map<String, dynamic> json) =>
      _$DirectoryListingDtoFromJson(json);
}
