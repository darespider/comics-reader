// ignore_for_file: public_member_api_docs
import 'package:freezed_annotation/freezed_annotation.dart';

part 'password_update_dto.freezed.dart';
part 'password_update_dto.g.dart';

@Freezed(copyWith: false)
class PasswordUpdateDto with _$PasswordUpdateDto {
  const factory PasswordUpdateDto({
    required String password,
  }) = _PasswordUpdateDto;

  factory PasswordUpdateDto.fromJson(Map<String, dynamic> json) =>
      _$PasswordUpdateDtoFromJson(json);
}
