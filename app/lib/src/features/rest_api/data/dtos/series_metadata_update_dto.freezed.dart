// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'series_metadata_update_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

SeriesMetadataUpdateDto _$SeriesMetadataUpdateDtoFromJson(
    Map<String, dynamic> json) {
  return _SeriesMetadataUpdateDto.fromJson(json);
}

/// @nodoc
mixin _$SeriesMetadataUpdateDto {
  SeriesStatus get status => throw _privateConstructorUsedError;
  bool get statusLock => throw _privateConstructorUsedError;
  String get title => throw _privateConstructorUsedError;
  bool get titleLock => throw _privateConstructorUsedError;
  String get titleSort => throw _privateConstructorUsedError;
  bool get titleSortLock => throw _privateConstructorUsedError;
  String get summary => throw _privateConstructorUsedError;
  bool get summaryLock => throw _privateConstructorUsedError;
  String get publisher => throw _privateConstructorUsedError;
  bool get publisherLock => throw _privateConstructorUsedError;
  bool get readingDirectionLock => throw _privateConstructorUsedError;
  String get language => throw _privateConstructorUsedError;
  bool get languageLock => throw _privateConstructorUsedError;
  bool get genresLock => throw _privateConstructorUsedError;
  bool get tagsLock => throw _privateConstructorUsedError;
  Set<String> get tags => throw _privateConstructorUsedError;
  ReadingDirection get readingDirection => throw _privateConstructorUsedError;
  int get ageRating => throw _privateConstructorUsedError;
  Set<String> get genres => throw _privateConstructorUsedError;

  /// Serializes this SeriesMetadataUpdateDto to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
}

/// @nodoc
@JsonSerializable()
class _$SeriesMetadataUpdateDtoImpl implements _SeriesMetadataUpdateDto {
  const _$SeriesMetadataUpdateDtoImpl(
      {required this.status,
      required this.statusLock,
      required this.title,
      required this.titleLock,
      required this.titleSort,
      required this.titleSortLock,
      required this.summary,
      required this.summaryLock,
      required this.publisher,
      required this.publisherLock,
      required this.readingDirectionLock,
      required this.language,
      required this.languageLock,
      required this.genresLock,
      required this.tagsLock,
      required final Set<String> tags,
      required this.readingDirection,
      required this.ageRating,
      required final Set<String> genres})
      : _tags = tags,
        _genres = genres;

  factory _$SeriesMetadataUpdateDtoImpl.fromJson(Map<String, dynamic> json) =>
      _$$SeriesMetadataUpdateDtoImplFromJson(json);

  @override
  final SeriesStatus status;
  @override
  final bool statusLock;
  @override
  final String title;
  @override
  final bool titleLock;
  @override
  final String titleSort;
  @override
  final bool titleSortLock;
  @override
  final String summary;
  @override
  final bool summaryLock;
  @override
  final String publisher;
  @override
  final bool publisherLock;
  @override
  final bool readingDirectionLock;
  @override
  final String language;
  @override
  final bool languageLock;
  @override
  final bool genresLock;
  @override
  final bool tagsLock;
  final Set<String> _tags;
  @override
  Set<String> get tags {
    if (_tags is EqualUnmodifiableSetView) return _tags;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableSetView(_tags);
  }

  @override
  final ReadingDirection readingDirection;
  @override
  final int ageRating;
  final Set<String> _genres;
  @override
  Set<String> get genres {
    if (_genres is EqualUnmodifiableSetView) return _genres;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableSetView(_genres);
  }

  @override
  String toString() {
    return 'SeriesMetadataUpdateDto(status: $status, statusLock: $statusLock, title: $title, titleLock: $titleLock, titleSort: $titleSort, titleSortLock: $titleSortLock, summary: $summary, summaryLock: $summaryLock, publisher: $publisher, publisherLock: $publisherLock, readingDirectionLock: $readingDirectionLock, language: $language, languageLock: $languageLock, genresLock: $genresLock, tagsLock: $tagsLock, tags: $tags, readingDirection: $readingDirection, ageRating: $ageRating, genres: $genres)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SeriesMetadataUpdateDtoImpl &&
            (identical(other.status, status) || other.status == status) &&
            (identical(other.statusLock, statusLock) ||
                other.statusLock == statusLock) &&
            (identical(other.title, title) || other.title == title) &&
            (identical(other.titleLock, titleLock) ||
                other.titleLock == titleLock) &&
            (identical(other.titleSort, titleSort) ||
                other.titleSort == titleSort) &&
            (identical(other.titleSortLock, titleSortLock) ||
                other.titleSortLock == titleSortLock) &&
            (identical(other.summary, summary) || other.summary == summary) &&
            (identical(other.summaryLock, summaryLock) ||
                other.summaryLock == summaryLock) &&
            (identical(other.publisher, publisher) ||
                other.publisher == publisher) &&
            (identical(other.publisherLock, publisherLock) ||
                other.publisherLock == publisherLock) &&
            (identical(other.readingDirectionLock, readingDirectionLock) ||
                other.readingDirectionLock == readingDirectionLock) &&
            (identical(other.language, language) ||
                other.language == language) &&
            (identical(other.languageLock, languageLock) ||
                other.languageLock == languageLock) &&
            (identical(other.genresLock, genresLock) ||
                other.genresLock == genresLock) &&
            (identical(other.tagsLock, tagsLock) ||
                other.tagsLock == tagsLock) &&
            const DeepCollectionEquality().equals(other._tags, _tags) &&
            (identical(other.readingDirection, readingDirection) ||
                other.readingDirection == readingDirection) &&
            (identical(other.ageRating, ageRating) ||
                other.ageRating == ageRating) &&
            const DeepCollectionEquality().equals(other._genres, _genres));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => Object.hashAll([
        runtimeType,
        status,
        statusLock,
        title,
        titleLock,
        titleSort,
        titleSortLock,
        summary,
        summaryLock,
        publisher,
        publisherLock,
        readingDirectionLock,
        language,
        languageLock,
        genresLock,
        tagsLock,
        const DeepCollectionEquality().hash(_tags),
        readingDirection,
        ageRating,
        const DeepCollectionEquality().hash(_genres)
      ]);

  @override
  Map<String, dynamic> toJson() {
    return _$$SeriesMetadataUpdateDtoImplToJson(
      this,
    );
  }
}

abstract class _SeriesMetadataUpdateDto implements SeriesMetadataUpdateDto {
  const factory _SeriesMetadataUpdateDto(
      {required final SeriesStatus status,
      required final bool statusLock,
      required final String title,
      required final bool titleLock,
      required final String titleSort,
      required final bool titleSortLock,
      required final String summary,
      required final bool summaryLock,
      required final String publisher,
      required final bool publisherLock,
      required final bool readingDirectionLock,
      required final String language,
      required final bool languageLock,
      required final bool genresLock,
      required final bool tagsLock,
      required final Set<String> tags,
      required final ReadingDirection readingDirection,
      required final int ageRating,
      required final Set<String> genres}) = _$SeriesMetadataUpdateDtoImpl;

  factory _SeriesMetadataUpdateDto.fromJson(Map<String, dynamic> json) =
      _$SeriesMetadataUpdateDtoImpl.fromJson;

  @override
  SeriesStatus get status;
  @override
  bool get statusLock;
  @override
  String get title;
  @override
  bool get titleLock;
  @override
  String get titleSort;
  @override
  bool get titleSortLock;
  @override
  String get summary;
  @override
  bool get summaryLock;
  @override
  String get publisher;
  @override
  bool get publisherLock;
  @override
  bool get readingDirectionLock;
  @override
  String get language;
  @override
  bool get languageLock;
  @override
  bool get genresLock;
  @override
  bool get tagsLock;
  @override
  Set<String> get tags;
  @override
  ReadingDirection get readingDirection;
  @override
  int get ageRating;
  @override
  Set<String> get genres;
}
