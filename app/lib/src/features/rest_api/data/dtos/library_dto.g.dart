// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'library_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$LibraryDtoImpl _$$LibraryDtoImplFromJson(Map<String, dynamic> json) =>
    _$LibraryDtoImpl(
      id: json['id'] as String,
      name: json['name'] as String,
      root: json['root'] as String,
      importComicInfoBook: json['importComicInfoBook'] as bool,
      importComicInfoSeries: json['importComicInfoSeries'] as bool,
      importComicInfoCollection: json['importComicInfoCollection'] as bool,
      importComicInfoReadList: json['importComicInfoReadList'] as bool,
      importComicInfoSeriesAppendVolume:
          json['importComicInfoSeriesAppendVolume'] as bool,
      importEpubBook: json['importEpubBook'] as bool,
      importEpubSeries: json['importEpubSeries'] as bool,
      importMylarSeries: json['importMylarSeries'] as bool,
      importLocalArtwork: json['importLocalArtwork'] as bool,
      importBarcodeIsbn: json['importBarcodeIsbn'] as bool,
      scanForceModifiedTime: json['scanForceModifiedTime'] as bool,
      scanInterval: json['scanInterval'] as String,
      scanOnStartup: json['scanOnStartup'] as bool,
      scanCbx: json['scanCbx'] as bool,
      scanPdf: json['scanPdf'] as bool,
      scanEpub: json['scanEpub'] as bool,
      scanDirectoryExclusions:
          (json['scanDirectoryExclusions'] as List<dynamic>)
              .map((e) => e as String)
              .toSet(),
      repairExtensions: json['repairExtensions'] as bool,
      convertToCbz: json['convertToCbz'] as bool,
      emptyTrashAfterScan: json['emptyTrashAfterScan'] as bool,
      seriesCover: json['seriesCover'] as String,
      hashFiles: json['hashFiles'] as bool,
      hashPages: json['hashPages'] as bool,
      analyzeDimensions: json['analyzeDimensions'] as bool,
      unavailable: json['unavailable'] as bool,
      oneshotsDirectory: json['oneshotsDirectory'] as String?,
    );

Map<String, dynamic> _$$LibraryDtoImplToJson(_$LibraryDtoImpl instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'root': instance.root,
      'importComicInfoBook': instance.importComicInfoBook,
      'importComicInfoSeries': instance.importComicInfoSeries,
      'importComicInfoCollection': instance.importComicInfoCollection,
      'importComicInfoReadList': instance.importComicInfoReadList,
      'importComicInfoSeriesAppendVolume':
          instance.importComicInfoSeriesAppendVolume,
      'importEpubBook': instance.importEpubBook,
      'importEpubSeries': instance.importEpubSeries,
      'importMylarSeries': instance.importMylarSeries,
      'importLocalArtwork': instance.importLocalArtwork,
      'importBarcodeIsbn': instance.importBarcodeIsbn,
      'scanForceModifiedTime': instance.scanForceModifiedTime,
      'scanInterval': instance.scanInterval,
      'scanOnStartup': instance.scanOnStartup,
      'scanCbx': instance.scanCbx,
      'scanPdf': instance.scanPdf,
      'scanEpub': instance.scanEpub,
      'scanDirectoryExclusions': instance.scanDirectoryExclusions.toList(),
      'repairExtensions': instance.repairExtensions,
      'convertToCbz': instance.convertToCbz,
      'emptyTrashAfterScan': instance.emptyTrashAfterScan,
      'seriesCover': instance.seriesCover,
      'hashFiles': instance.hashFiles,
      'hashPages': instance.hashPages,
      'analyzeDimensions': instance.analyzeDimensions,
      'unavailable': instance.unavailable,
      'oneshotsDirectory': instance.oneshotsDirectory,
    };
