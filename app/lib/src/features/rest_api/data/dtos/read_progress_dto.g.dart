// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'read_progress_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$ReadProgressDtoImpl _$$ReadProgressDtoImplFromJson(
        Map<String, dynamic> json) =>
    _$ReadProgressDtoImpl(
      page: (json['page'] as num).toInt(),
      completed: json['completed'] as bool,
      created: DateTime.parse(json['created'] as String),
      lastModified: DateTime.parse(json['lastModified'] as String),
    );

Map<String, dynamic> _$$ReadProgressDtoImplToJson(
        _$ReadProgressDtoImpl instance) =>
    <String, dynamic>{
      'page': instance.page,
      'completed': instance.completed,
      'created': instance.created.toIso8601String(),
      'lastModified': instance.lastModified.toIso8601String(),
    };
