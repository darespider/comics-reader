import 'package:kapoow/src/features/rest_api/domain/rest_api/rest_json_api.dart';
import 'package:kapoow/src/features/rest_api/utils/extensions.dart';

/// Class used to do requests to the referential endpoints
/// it consumes an [RestJsonApi] to do the requests
class ReferentialDatasource {
  /// Creates instance of [ReferentialDatasource]
  const ReferentialDatasource(this._api);

  final RestJsonApi _api;

  /// Runs a `get` request to the `/api/v1/age-ratings` endpoint
  Future<List<String>> getAgeRatings({
    String? collectionId,
    String? libraryId,
  }) async {
    final uri = 'api' / 'v1' / 'age-ratings' &
        {
          if (libraryId != null) 'library_id': libraryId,
          if (collectionId != null) 'collection_id': collectionId,
        };

    return _api.get(
      uri,
      dtoParser: (json) => (json! as List).cast<String>(),
    );
  }

  /// Runs a `get` request to the `/api/v1/genres` endpoint
  Future<List<String>> getGenres({
    String? collectionId,
    String? libraryId,
  }) async {
    final uri = 'api' / 'v1' / 'genres' &
        {
          if (libraryId != null) 'library_id': libraryId,
          if (collectionId != null) 'collection_id': collectionId,
        };
    return _api.get(
      uri,
      dtoParser: (json) => (json! as List).cast<String>(),
    );
  }

  /// Runs a `get` request to the `/api/v1/languages` endpoint
  Future<List<String>> getLanguages({
    String? collectionId,
    String? libraryId,
  }) async {
    final uri = 'api' / 'v1' / 'languages' &
        {
          if (libraryId != null) 'library_id': libraryId,
          if (collectionId != null) 'collection_id': collectionId,
        };
    return _api.get(
      uri,
      dtoParser: (json) => (json! as List).cast<String>(),
    );
  }

  /// Runs a `get` request to the `/api/v1/publishers` endpoint
  Future<List<String>> getPublishers({
    String? collectionId,
    String? libraryId,
  }) async {
    final uri = 'api' / 'v1' / 'publishers' &
        {
          if (libraryId != null) 'library_id': libraryId,
          if (collectionId != null) 'collection_id': collectionId,
        };
    return _api.get(
      uri,
      dtoParser: (json) => (json! as List).cast<String>(),
    );
  }
}
