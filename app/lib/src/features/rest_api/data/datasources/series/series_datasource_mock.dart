// coverage:ignore-file
import 'package:kapoow/src/features/library_management/common/models/media_status.dart';
import 'package:kapoow/src/features/library_management/common/models/read_status.dart';
import 'package:kapoow/src/features/library_management/common/models/series_status.dart';
import 'package:kapoow/src/features/rest_api/data/datasources/series/series_datasource.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/pagination_dto.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/series_dto.dart';

/// Series Api Client that return mock data
class SeriesDatasourceMock implements SeriesDatasource {
  @override
  Future<PageBookDto> getSeriesBooks({
    required String seriesId,
    MediaStatus? mediaStatus,
    ReadStatus? readStatus,
    List<String>? tags,
    bool? deleted,
    bool? unpaged,
    int? page,
    int? pageSize,
    List<String>? sort,
    List<String>? authors,
  }) {
    // TODO(rurickdev): implement fetchBooks
    throw UnimplementedError();
  }

  @override
  Future<SeriesDto> getSeriesById(String id) {
    // TODO(rurickdev): implement fetchSeriesById
    throw UnimplementedError();
  }

  @override
  Future<PageSeriesDto> getLatestSeries({
    String? libraryId,
    bool? oneshot,
    bool? deleted,
    bool? unpaged,
    int? page,
    int? pageSize,
  }) {
    // TODO(rurickdev): implement getLatestSeries
    throw UnimplementedError();
  }

  @override
  Future<PageSeriesDto> getNewSeries({
    String? libraryId,
    bool? oneshot,
    bool? deleted,
    bool? unpaged,
    int? page,
    int? pageSize,
  }) {
    // TODO(rurickdev): implement getNewSeries
    throw UnimplementedError();
  }

  @override
  Future<PageSeriesDto> getSeries({
    String? search,
    List<String>? libraryIds,
    List<String>? collectionsIds,
    List<SeriesStatus>? seriesStatuses,
    List<ReadStatus>? readStatus,
    List<String>? publishers,
    List<String>? languages,
    List<String>? genres,
    List<String>? tags,
    List<String>? ageRating,
    List<DateTime>? releaseYears,
    bool? deleted,
    bool? completed,
    bool? unpaged,
    String? searchRegex,
    int? page,
    int? size,
    List<String>? sort,
    List<String>? authors,
    bool? oneshot,
  }) {
    // TODO(rurickdev): implement getSeries
    throw UnimplementedError();
  }

  @override
  Future<PageSeriesDto> getUpdatedSeries({
    String? libraryId,
    bool? oneshot,
    bool? deleted,
    bool? unpaged,
    int? page,
    int? pageSize,
  }) {
    // TODO(rurickdev): implement getUpdatedSeries
    throw UnimplementedError();
  }
}
