import 'package:kapoow/src/features/library_management/common/models/read_status.dart';
import 'package:kapoow/src/features/library_management/common/models/series_status.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/collection_dto.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/pagination_dto.dart';
import 'package:kapoow/src/features/rest_api/domain/rest_api/rest_json_api.dart';
import 'package:kapoow/src/features/rest_api/utils/extensions.dart';

/// Class used to do requests to the collections endpoints
/// it consumes an [RestJsonApi] to do the requests
class CollectionsDatasource {
  /// Creates instance of [CollectionsDatasource]
  const CollectionsDatasource(this._api);

  final RestJsonApi _api;

  /// Runs a `get` request to the `/api/v1/collections` endpoint
  Future<PageCollectionDto> getCollections({
    String? search,
    List<String>? libraryIds,
    bool? unpaged,
    int? page,
    int? size,
  }) async {
    final uri = 'api' / 'v1' / 'collections' &
        {
          if (search?.isNotEmpty ?? false)
            'search': Uri.encodeQueryComponent(search!),
          if (libraryIds?.isNotEmpty ?? false) 'library_id': libraryIds,
          if (unpaged != null) 'unpaged': unpaged.toString(),
          if (page != null) 'page': page.toString(),
          if (size != null) 'size': size.toString(),
        };
    return _api.get(uri, dtoParser: PaginationDto.pageCollectionFromJson);
  }

  /// Runs a `get` request to the `/api/v1/collections/{{id}}` endpoint
  Future<CollectionDto> getCollectionById(String id) async {
    final uri = 'api' / 'v1' / 'collections' / id;
    return _api.get(uri, dtoParser: CollectionDto.fromJson);
  }

  /// Runs a `get` request to the
  /// `/api/v1/collections/{{id}}/series` endpoint
  Future<PageSeriesDto> getCollectionSeries({
    required String collectionId,
    List<String>? libraryIds,
    List<SeriesStatus>? status,
    ReadStatus? readStatus,
    List<String>? publisher,
    List<String>? language,
    List<String>? genre,
    List<String>? tags,
    List<String>? ageRating,
    List<String>? releaseYears,
    bool? deleted,
    bool? complete,
    bool? unpaged,
    int? page,
    int? size,
    List<String>? authors,
  }) async {
    final uri = 'api' / 'v1' / 'collections' / collectionId / 'series' &
        {
          if (libraryIds != null && libraryIds.isNotEmpty)
            'library_id': libraryIds,
          if (status != null && status.isNotEmpty)
            'status': status.map((e) => e.value).toList(),
          if (readStatus != null) 'read_status': readStatus.value,
          if (publisher != null && publisher.isNotEmpty) 'publisher': publisher,
          if (language != null && language.isNotEmpty) 'language': language,
          if (genre != null && genre.isNotEmpty) 'genre': genre,
          if (tags != null && tags.isNotEmpty) 'tags': tags,
          if (ageRating != null && ageRating.isNotEmpty)
            'age_rating': ageRating,
          if (releaseYears != null && releaseYears.isNotEmpty)
            'release_years': releaseYears,
          if (deleted != null) 'deleted': deleted.toString(),
          if (complete != null) 'complete': complete.toString(),
          if (unpaged != null) 'unpaged': unpaged.toString(),
          if (page != null) 'page': page.toString(),
          if (size != null) 'size': size.toString(),
          if (authors != null && authors.isNotEmpty) 'authors': authors,
        };

    return _api.get(uri, dtoParser: PaginationDto.pageSeriesFromJson);
  }
}
