// coverage:ignore-file
import 'package:kapoow/src/features/library_management/common/models/media_status.dart';
import 'package:kapoow/src/features/library_management/common/models/read_status.dart';
import 'package:kapoow/src/features/rest_api/data/datasources/read_lists/read_lists_datasource.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/book_dto.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/pagination_dto.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/read_list_dto.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/read_list_update_dto.dart';

/// Read Lists api client that return mock data
class ReadListsDatasourceMock implements ReadListsDatasource {
  @override
  Future<PageBookDto> getReadListBooks({
    required String readlistId,
    List<String>? libraryIds,
    List<ReadStatus>? readStatus,
    List<String>? tags,
    List<MediaStatus>? mediaStatus,
    bool? deleted,
    bool? unpaged,
    int? page,
    int? size,
    List<String>? authors,
  }) {
    // TODO(rurickdev): implement getBooks
    throw UnimplementedError();
  }

  @override
  Future<BookDto?> getReadListNextBook(String id, String bookId) {
    // TODO(rurickdev): implement getNext
    throw UnimplementedError();
  }

  @override
  Future<BookDto?> getReadListPreviousBook(String id, String bookId) {
    // TODO(rurickdev): implement getPrevious
    throw UnimplementedError();
  }

  @override
  Future<ReadListDto> getReadListById(String id) {
    // TODO(rurickdev): implement getReadListById
    throw UnimplementedError();
  }

  @override
  Future<PageReadListDto> getReadLists({
    String? search,
    List<String>? libraryIds,
    bool? unpaged,
    int? page,
    int? size,
  }) {
    // TODO(rurickdev): implement getReadLists
    throw UnimplementedError();
  }

  @override
  Future<void> patchReadList(String id, ReadListUpdateDto patchData) {
    // TODO(rurickdev): implement patchReadList
    throw UnimplementedError();
  }
}
