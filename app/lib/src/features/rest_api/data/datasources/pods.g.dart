// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pods.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$booksDatasourceHash() => r'e273abffa578276dde95de2f168d01230ecd4d1b';

/// Provides an instance of an implementation of [BooksDatasource]
///
/// Copied from [booksDatasource].
@ProviderFor(booksDatasource)
final booksDatasourcePod = Provider<BooksDatasource>.internal(
  booksDatasource,
  name: r'booksDatasourcePod',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$booksDatasourceHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef BooksDatasourceRef = ProviderRef<BooksDatasource>;
String _$collectionsDatasourceHash() =>
    r'5e76a51adadb5b80294912c5386bb97d0a1958f7';

/// Provides an instance of an implementation of [CollectionsDatasource]
///
/// Copied from [collectionsDatasource].
@ProviderFor(collectionsDatasource)
final collectionsDatasourcePod = Provider<CollectionsDatasource>.internal(
  collectionsDatasource,
  name: r'collectionsDatasourcePod',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$collectionsDatasourceHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef CollectionsDatasourceRef = ProviderRef<CollectionsDatasource>;
String _$librariesDatasourceHash() =>
    r'8460e26f2d88cef3c729290127d15d4782f5aec1';

/// Provides an instance of an implementation of [LibrariesDatasource]
///
/// Copied from [librariesDatasource].
@ProviderFor(librariesDatasource)
final librariesDatasourcePod = Provider<LibrariesDatasource>.internal(
  librariesDatasource,
  name: r'librariesDatasourcePod',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$librariesDatasourceHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef LibrariesDatasourceRef = ProviderRef<LibrariesDatasource>;
String _$readListsDatasourceHash() =>
    r'a2c8747aa2bd30500172bed2ebf0ff50f5eb5285';

/// Provides an instance of an implementation of [ReadListsDatasource]
///
/// Copied from [readListsDatasource].
@ProviderFor(readListsDatasource)
final readListsDatasourcePod = Provider<ReadListsDatasource>.internal(
  readListsDatasource,
  name: r'readListsDatasourcePod',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$readListsDatasourceHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef ReadListsDatasourceRef = ProviderRef<ReadListsDatasource>;
String _$referentialDatasourceHash() =>
    r'7aef0cd91c560a4362cf87785924f5ab9a40befe';

/// Provides an instance of an implementation of [ReferentialDatasource]
///
/// Copied from [referentialDatasource].
@ProviderFor(referentialDatasource)
final referentialDatasourcePod = Provider<ReferentialDatasource>.internal(
  referentialDatasource,
  name: r'referentialDatasourcePod',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$referentialDatasourceHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef ReferentialDatasourceRef = ProviderRef<ReferentialDatasource>;
String _$seriesDatasourceHash() => r'686be9bc1633cc041f9b21e6dbd840acbdcc36f8';

/// Provides an instance of an implementation of [SeriesDatasource]
///
/// Copied from [seriesDatasource].
@ProviderFor(seriesDatasource)
final seriesDatasourcePod = Provider<SeriesDatasource>.internal(
  seriesDatasource,
  name: r'seriesDatasourcePod',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$seriesDatasourceHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef SeriesDatasourceRef = ProviderRef<SeriesDatasource>;
String _$userDatasourceHash() => r'037a6718c7ca3aa4f4de09abaa20c32f67f9a6f9';

/// Provides an instance of an implementation of [UserDatasource]
///
/// Copied from [userDatasource].
@ProviderFor(userDatasource)
final userDatasourcePod = Provider<UserDatasource>.internal(
  userDatasource,
  name: r'userDatasourcePod',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$userDatasourceHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef UserDatasourceRef = ProviderRef<UserDatasource>;
String _$cachedImagesDatasourceHash() =>
    r'4202561bf34ca25064ea0bde16ef53e7af51387b';

/// Provides an instance of an implementation of [UserDatasource]
///
/// Copied from [cachedImagesDatasource].
@ProviderFor(cachedImagesDatasource)
final cachedImagesDatasourcePod = Provider<CachedImagesDatasource>.internal(
  cachedImagesDatasource,
  name: r'cachedImagesDatasourcePod',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$cachedImagesDatasourceHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef CachedImagesDatasourceRef = ProviderRef<CachedImagesDatasource>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
