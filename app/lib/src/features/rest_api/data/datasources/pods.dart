// coverage:ignore-file
import 'package:kapoow/src/features/rest_api/data/datasources/books/books_datasource.dart';
import 'package:kapoow/src/features/rest_api/data/datasources/cached_images/cached_images_datasource.dart';
import 'package:kapoow/src/features/rest_api/data/datasources/collections/collections_datasource.dart';
import 'package:kapoow/src/features/rest_api/data/datasources/libraries/libraries_datasource.dart';
import 'package:kapoow/src/features/rest_api/data/datasources/read_lists/read_lists_datasource.dart';
import 'package:kapoow/src/features/rest_api/data/datasources/referential/referential_datasource.dart';
import 'package:kapoow/src/features/rest_api/data/datasources/series/series_datasource.dart';
import 'package:kapoow/src/features/rest_api/data/datasources/user/user_datasource.dart';
import 'package:kapoow/src/features/rest_api/domain/pods.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'pods.g.dart';

/// Provides an instance of an implementation of [BooksDatasource]
@Riverpod(keepAlive: true)
BooksDatasource booksDatasource(BooksDatasourceRef ref) {
  return BooksDatasource(
    ref.watch(restJsonApiPod),
    ref.watch(downloadsApiPod),
  );
}

/// Provides an instance of an implementation of [CollectionsDatasource]
@Riverpod(keepAlive: true)
CollectionsDatasource collectionsDatasource(CollectionsDatasourceRef ref) {
  return CollectionsDatasource(ref.watch(restJsonApiPod));
}

/// Provides an instance of an implementation of [LibrariesDatasource]
@Riverpod(keepAlive: true)
LibrariesDatasource librariesDatasource(LibrariesDatasourceRef ref) {
  return LibrariesDatasource(ref.watch(restJsonApiPod));
}

/// Provides an instance of an implementation of [ReadListsDatasource]
@Riverpod(keepAlive: true)
ReadListsDatasource readListsDatasource(ReadListsDatasourceRef ref) {
  return ReadListsDatasource(ref.watch(restJsonApiPod));
}

/// Provides an instance of an implementation of [ReferentialDatasource]
@Riverpod(keepAlive: true)
ReferentialDatasource referentialDatasource(ReferentialDatasourceRef ref) {
  return ReferentialDatasource(ref.watch(restJsonApiPod));
}

/// Provides an instance of an implementation of [SeriesDatasource]
@Riverpod(keepAlive: true)
SeriesDatasource seriesDatasource(SeriesDatasourceRef ref) {
  return SeriesDatasource(ref.watch(restJsonApiPod));
}

/// Provides an instance of an implementation of [UserDatasource]
@Riverpod(keepAlive: true)
UserDatasource userDatasource(UserDatasourceRef ref) {
  return UserDatasource(ref.watch(restJsonApiPod));
}

/// Provides an instance of an implementation of [CachedImagesDatasource]
@Riverpod(keepAlive: true)
CachedImagesDatasource cachedImagesDatasource(CachedImagesDatasourceRef ref) {
  return CachedImagesDatasource(ref.watch(cacheDownloadApiPod));
}
