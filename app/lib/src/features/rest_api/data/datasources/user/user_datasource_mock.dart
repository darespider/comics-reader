// coverage:ignore-file
import 'package:kapoow/src/features/rest_api/data/datasources/user/user_datasource.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/user_dto.dart';

/// User Api Client that returns mock data
class UserDatasourceMock implements UserDatasource {
  @override
  Future<UserDto> getUserMe() async {
    await Future<void>.delayed(const Duration(seconds: 2));
    return const UserDto(
      id: '001',
      email: 'mock@email.fake',
      roles: {},
      sharedAllLibraries: true,
      sharedLibrariesIds: {},
      labelsAllow: {},
      labelsExclude: {},
    );
  }
}
