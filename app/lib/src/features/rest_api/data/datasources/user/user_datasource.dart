import 'package:kapoow/src/features/rest_api/data/dtos/user_dto.dart';
import 'package:kapoow/src/features/rest_api/domain/rest_api/rest_json_api.dart';
import 'package:kapoow/src/features/rest_api/utils/extensions.dart';

/// Class used to do requests to the user endpoints
/// it consumes an [RestJsonApi] to do the requests
class UserDatasource {
  /// Creates instance of [UserDatasource]
  const UserDatasource(this._api);

  final RestJsonApi _api;

  /// Runs a `get` request to the `/api/v2/users/me` endpoint
  Future<UserDto> getUserMe() async {
    return _api.get(
      'api' / 'v2' / 'users' / 'me',
      dtoParser: UserDto.fromJson,
    );
  }
}
