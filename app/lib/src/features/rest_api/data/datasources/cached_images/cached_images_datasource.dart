import 'dart:typed_data';

import 'package:kapoow/src/features/rest_api/domain/rest_api/cache_downloads_api.dart';
import 'package:kapoow/src/features/rest_api/utils/extensions.dart';

/// Datasource that reads files from cache if the file exists, will download
/// and save it on cache and provide the file from there otherwise
class CachedImagesDatasource {
  /// Creates instance of [CachedImagesDatasource]
  const CachedImagesDatasource(this._cache);

  final CacheDownloadsApi _cache;

  /// Runs a `get` request to the `/api/v1/books/{{id}}/pages` endpoint
  Future<Uint8List?> getCachePageByBookId(
    String id, {
    required int page,
  }) async {
    return _cache.tryGetImageFile(
      'api' / 'v1' / 'books' / id / 'pages' / page.toString(),
    );
  }

  /// Saves the [bytes] of the [page] image from the book with th provided [id]
  Future<void> putCachePageByBookId(
    String id, {
    required int page,
    required Uint8List bytes,
  }) async {
    return _cache.putFile(
      'api' / 'v1' / 'books' / id / 'pages' / page.toString(),
      bytes,
    );
  }
}
