import 'package:kapoow/src/features/pagination/domain/models/pagination.dart';
import 'package:kapoow/src/features/pagination/ui/controllers/paginated_state.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

/// Mixin Class intended to be used on Notifiers created with riverpod.
///
/// It allows to reuse logic to handle pagination for "infinite" scrolling
/// lists.
///
/// The [params] property is intended to be the `build(P params)` from the
/// notifier class
///
/// The [state] property is intended to be the `state` from the Notifier
///
/// The [build] method is intended to be the `build` from the Notifier
///
/// Due some limitations with Class Mixins we can't limit the use of this
/// Mixin to only Notifiers.
/// https://dart.dev/language/mixins#class-mixin-or-mixin-class
abstract mixin class PaginatedMixin<T, P> {
  /// Params that will be used for the pagination
  abstract P params;

  /// State used to rebuild the notifier
  abstract PaginatedState<T> state;

  /// Enforce the params from the notifier to be [P] type
  PaginatedState<T> build([P params]);

  /// Defines how the notifier should fetch the next page for the pagination
  ///
  /// This method is not intended to be used outside of the notifier but it
  /// can be without any issue.
  Future<Pagination<T>> fetchPage(int page);

  /// This method will generate a [PaginatedState] based on the result of
  /// [firstPageAsync].
  PaginatedState<T> buildInitState(AsyncValue<Pagination<T>> firstPageAsync) {
    final (firstPage, loading, error) = firstPageAsync.when(
      loading: () => (null, true, null),
      error: (error, stackTrace) => (null, false, error),
      data: (data) => (data, false, null),
    );

    return PaginatedState<T>(
      pages: [
        if (firstPage != null) firstPage,
      ],
      loadingPage: loading,
      errorLoadingPage: error,
    );
  }

  /// Indicates to this notifier to start fetching the next page
  Future<void> loadNextPage() async {
    if (state.last || state.loadingPage) return;

    state = state.copyWith(
      loadingPage: true,
    );

    try {
      final nextPage = await fetchPage(state.nextPage);

      final updatedPages = [
        ...state.pages,
        nextPage,
      ];

      state = state.copyWith(
        loadingPage: false,
        pages: updatedPages,
      );
    } catch (e) {
      state = state.copyWith(
        loadingPage: false,
        errorLoadingPage: e,
      );
    }
  }
}
