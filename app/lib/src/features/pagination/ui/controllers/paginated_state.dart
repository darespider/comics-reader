import 'package:kapoow/src/features/pagination/domain/models/pagination.dart';

/// State representation for pagination of a Library
class PaginatedState<T> {
  /// Creates a [PaginatedState] instance
  const PaginatedState({
    required this.pages,
    this.loadingPage = false,
    this.errorLoadingPage,
  });

  /// Collection of all the fetched pages
  final List<Pagination<T>> pages;

  /// Indicates if a new pages if being fetched
  final bool loadingPage;

  /// Error object if exist when page fetched
  final Object? errorLoadingPage;

  /// `true` if [errorLoadingPage] is not `null` `false` otherwise
  bool get hasError => errorLoadingPage != null;

  /// `true` if [loadingPage] is `true`, `false` otherwise
  bool get isLoading => loadingPage;

  /// `true` if [loadingPage] is `true` and [pages] or [content] are empty
  /// and the object has no error, `false` otherwise
  bool get isLoadingFirstPage =>
      loadingPage && !hasError && (pages.isEmpty || content.isEmpty);

  /// Expanded list of all contents of all pages
  List<T> get content => pages.expand((page) => page.content).toList();

  /// `true` if [pages] is not empty && the last page is not the last in the api
  bool get last => pages.isNotEmpty && pages.last.last;

  /// `true` if there are more pages
  bool get hasNextPage => !last && pages.isNotEmpty;

  /// Number of the page if exists in the api
  int get nextPage => pages.isNotEmpty ? pages.last.pageable.pageNumber + 1 : 1;

  /// Total of items, if there are no pages returns 0
  int get totalItems => pages.isNotEmpty ? pages.first.totalElements : 0;

  /// `true` is at least one page has loaded and there are no elements
  bool get hasNoResults => !isLoadingFirstPage && totalItems == 0;

  /// Creates a new instance based on the current one
  PaginatedState<T> copyWith({
    List<Pagination<T>>? pages,
    bool? loadingPage,
    Object? errorLoadingPage,
  }) =>
      PaginatedState<T>(
        pages: pages ?? this.pages,
        errorLoadingPage: errorLoadingPage ?? this.errorLoadingPage,
        loadingPage: loadingPage ?? this.loadingPage,
      );
}
