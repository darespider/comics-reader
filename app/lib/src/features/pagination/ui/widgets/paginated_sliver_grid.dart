import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:kapoow/src/features/library_management/common/ui/widgets/item_cover/cover_aspect_ratio.dart';
import 'package:kapoow/src/features/library_management/common/ui/widgets/item_cover/cover_card_error.dart';
import 'package:kapoow/src/features/pagination/ui/controllers/paginated_state.dart';

// TODO(rurickdev): Create a VerticalPaginatedGrid and HorizontalPaginatedGrid

/// Displays an "infinite" list of items T.
class PaginatedSliverGrid<T> extends ConsumerWidget {
  /// Creates a [PaginatedSliverGrid] instance
  const PaginatedSliverGrid({
    required this.paginatedState,
    required this.nextPageLoader,
    required this.itemBuilder,
    required this.loadingPageIndicator,
    required this.scrollDirection,
    this.errorInPageIndicator,
    this.gridDelegate = const CoverGridDelegate(),
    this.loadNextPage,
    this.initialLoadIndicator,
    this.emptyIndicator,
    super.key,
  });

  /// {@template kapoow.pagination.paginatedList.paginatedState}
  /// State from where to get pagination data and content
  /// {@endtemplate}
  final PaginatedState<T> paginatedState;

  /// {@template kapoow.pagination.paginatedList.nextPageLoader}
  /// Callback to trigger the loading of the next page
  /// {@endtemplate}
  final VoidCallback nextPageLoader;

  /// {@template kapoow.pagination.paginatedList.loadNextPage}
  /// Callback called on every item render to know if the [loadNextPage]
  /// callback should be called
  /// {@endtemplate}
  final bool Function(int index)? loadNextPage;

  /// {@template kapoow.pagination.paginatedList.loadingPageIndicator}
  /// Widget to display the loading next page state
  /// {@endtemplate}
  final Widget loadingPageIndicator;

  /// {@template kapoow.pagination.paginatedList.initialLoadIndicator}
  /// Widget to display the loading the first page and there is no error state.
  /// if not provided [loadingPageIndicator] will be used instead
  /// {@endtemplate}
  final Widget? initialLoadIndicator;

  /// {@template kapoow.pagination.paginatedList.errorInPageIndicator}
  /// Widget to display when there is an error loading the next page
  /// {@endtemplate}
  final Widget? errorInPageIndicator;

  /// {@template kapoow.pagination.paginatedList.emptyIndicator}
  /// Widget to display if there are no books for any page.
  /// {@endtemplate}
  final Widget? emptyIndicator;

  /// {@template kapoow.pagination.paginatedList.itemBuilder}
  /// Builder for list items
  /// {@endtemplate}
  final Widget Function(BuildContext context, T item) itemBuilder;

  /// {@template kapoow.pagination.paginatedList.scrollDirection}
  /// Direction of the scroll
  /// {@endtemplate}
  final Axis scrollDirection;

  /// Delegate of the grid
  final SliverGridDelegateWithFixedCrossAxisCount gridDelegate;

  bool _defaultShouldLoadNextPage(int i) =>
      paginatedState.hasNextPage &&
      i == (paginatedState.content.length * 0.7).round();

  int _fillRow() {
    final missingCount = paginatedState.content.length % 3;
    return switch (missingCount) {
      1 => 2,
      2 => 1,
      _ => 3,
    };
  }

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    if (paginatedState.isLoadingFirstPage) {
      return SliverToBoxAdapter(
        child: initialLoadIndicator ?? loadingPageIndicator,
      );
    }

    if (paginatedState.totalItems == 0 && emptyIndicator != null) {
      return SliverToBoxAdapter(child: emptyIndicator);
    }

    var missingItems = 0;
    var itemCount = paginatedState.content.length;
    if (paginatedState.isLoading && !paginatedState.hasError) {
      missingItems = _fillRow();
      itemCount += missingItems;
    }

    if (paginatedState.hasError) {
      itemCount++;
    }

    return SliverGrid.builder(
      itemCount: itemCount,
      gridDelegate: gridDelegate,
      itemBuilder: (context, index) {
        if (paginatedState.hasError && index == itemCount - 1) {
          return errorInPageIndicator ??
              const CoverCardError(
                // TODO(rurickdev): [l10n] translate
                error: 'Something failed when loading next items.',
              );
        }
        if (loadNextPage?.call(index) ?? _defaultShouldLoadNextPage(index)) {
          Future(nextPageLoader);
        }

        if (index >= (itemCount - missingItems) && paginatedState.isLoading) {
          return loadingPageIndicator;
        }

        return itemBuilder(context, paginatedState.content[index]);
      },
    );
  }
}
