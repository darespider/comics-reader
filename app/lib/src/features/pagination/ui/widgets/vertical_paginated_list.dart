import 'package:flutter/material.dart';
import 'package:gap/gap.dart';
import 'package:kapoow/src/common_widgets/app_loader.dart';
import 'package:kapoow/src/constants/app_paddings.dart';
import 'package:kapoow/src/features/pagination/domain/models/pagination.dart';
import 'package:kapoow/src/features/pagination/ui/controllers/paginated_state.dart';
import 'package:kapoow/src/features/pagination/ui/widgets/paginated_list.dart';

/// Displays an "infinite" list of items T.
class VerticalPaginatedList<T> extends PaginatedList<T> {
  /// Creates a [VerticalPaginatedList] instance
  VerticalPaginatedList({
    required super.paginatedState,
    required super.nextPageLoader,
    required super.itemBuilder,
    super.loadNextPage,
    super.trailingBuilder,
    Widget? loadingPageIndicator,
    Widget? errorInPageIndicator,
    Widget? separator,
    super.key,
  }) : super(
          loadingPageIndicator: loadingPageIndicator ??
              const ListTile(
                title: AppLoader(scale: 50),
              ),
          errorInPageIndicator: errorInPageIndicator ??
              ListTile(
                title: Text(
                  paginatedState.errorLoadingPage?.toString() ??
                      'VerticalPaginatedList Error',
                  overflow: TextOverflow.ellipsis,
                  maxLines: 3,
                ),
              ),
          separator: separator ?? const Gap(AppPaddings.small),
          scrollDirection: Axis.vertical,
        );

  /// Creates a [VerticalPaginatedList] which Paginated state will
  /// have only one page
  VerticalPaginatedList.singlePage({
    required Pagination<T> page,
    required super.itemBuilder,
    Widget? separator,
    super.trailingBuilder,
    super.key,
  }) : super(
          paginatedState: PaginatedState(pages: [page]),
          loadNextPage: (_) => false,
          nextPageLoader: () {},
          loadingPageIndicator: const SizedBox.shrink(),
          errorInPageIndicator: const SizedBox.shrink(),
          separator: separator ?? const Gap(AppPaddings.small),
          scrollDirection: Axis.horizontal,
        );
}
