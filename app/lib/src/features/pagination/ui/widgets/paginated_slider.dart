import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:kapoow/src/features/pagination/ui/controllers/paginated_state.dart';

/// Displays an "infinite" list of items T.
class PaginatedSlider<T> extends ConsumerWidget {
  /// Creates a [PaginatedSlider] instance
  PaginatedSlider({
    required this.paginatedState,
    required this.nextPageLoader,
    required this.itemBuilder,
    required this.loadingPageIndicator,
    required this.errorInPageIndicator,
    required this.controller,
    bool Function(int itemIndex)? loadNextPage,
    this.physics,
    this.initialLoadIndicator,
    this.onPageChanged,
    super.key,
  }) : loadNextPage = loadNextPage ??
            ((int index) {
              return paginatedState.hasNextPage &&
                  index == (paginatedState.content.length * 0.8).round();
            });

  /// {@macro kapoow.pagination.paginatedList.paginatedState}
  final PaginatedState<T> paginatedState;

  /// {@macro kapoow.pagination.paginatedList.nextPageLoader}
  final VoidCallback nextPageLoader;

  /// {@macro kapoow.pagination.paginatedList.loadNextPage}
  final bool Function(int itemIndex) loadNextPage;

  /// {@macro kapoow.pagination.paginatedList.loadingPageIndicator}
  final Widget loadingPageIndicator;

  /// {@macro kapoow.pagination.paginatedList.initialLoadIndicator}
  final Widget? initialLoadIndicator;

  /// {@macro kapoow.pagination.paginatedList.errorInPageIndicator}
  final Widget errorInPageIndicator;

  /// {@macro kapoow.pagination.paginatedList.itemBuilder}
  final Widget Function(BuildContext context, T item) itemBuilder;

  /// An object that can be used to control the position to which this page
  /// view is scrolled.
  final PageController controller;

  /// Physics for the page scroll
  final ScrollPhysics? physics;

  /// Side effect for page change
  final ValueChanged<int>? onPageChanged;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    if (paginatedState.isLoadingFirstPage) {
      return initialLoadIndicator ?? loadingPageIndicator;
    }

    var itemCount = paginatedState.content.length;
    if (paginatedState.isLoading || paginatedState.hasError) {
      itemCount++;
    }

    return PageView.builder(
      controller: controller,
      itemCount: itemCount,
      physics: physics,
      onPageChanged: onPageChanged,
      itemBuilder: (context, index) {
        final shouldLoadNextPage = loadNextPage(index);
        if (shouldLoadNextPage) {
          WidgetsBinding.instance.addPostFrameCallback((_) => nextPageLoader());
        }

        if (index == itemCount - 1) {
          if (paginatedState.isLoading) return loadingPageIndicator;
          if (paginatedState.hasError) return errorInPageIndicator;
        }

        return itemBuilder(context, paginatedState.content[index]);
      },
    );
  }
}
