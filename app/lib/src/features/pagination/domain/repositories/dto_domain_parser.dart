import 'package:kapoow/src/features/library_management/books/domain/repositories/dto_domain_parser.dart';
import 'package:kapoow/src/features/library_management/collections/domain/repositories/dto_domain_parser.dart';
import 'package:kapoow/src/features/library_management/read_lists/domain/repositories/dto_domain_parser.dart';
import 'package:kapoow/src/features/library_management/series/domain/repositories/dto_domain_parser.dart';
import 'package:kapoow/src/features/pagination/domain/models/exceptions.dart';
import 'package:kapoow/src/features/pagination/domain/models/pageable.dart';
import 'package:kapoow/src/features/pagination/domain/models/pagination.dart';
import 'package:kapoow/src/features/pagination/domain/models/sort.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/book_dto.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/collection_dto.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/pageable_dto.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/pagination_dto.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/read_list_dto.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/series_dto.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/sort_dto.dart';

/// Extension to parse Dto list into list of domain objects
extension PaginationListModelParser<Dto> on List<PaginationDto<Dto>> {
  /// Converts every this list into a domain T object list
  List<Pagination<T>> mapToDomain<T>() => map((p) => p.toDomain<T>()).toList();
}

/// Extension to parse Dto into domain object
extension PaginationModelParser<Dto> on PaginationDto<Dto> {
  T _dtoToDomainAdapter<T>(Dto dto) {
    final model = switch (Dto) {
      BookDto => (dto as BookDto).toDomain(),
      CollectionDto => (dto as CollectionDto).toDomain(),
      ReadListDto => (dto as ReadListDto).toDomain(),
      SeriesDto => (dto as SeriesDto).toDomain(),
      _ => throw NotValidPaginationDtoError(),
    };
    return model as T;
  }

  /// Creates a Pagination<T> domain object from this
  Pagination<T> toDomain<T>() => Pagination<T>(
        totalPages: totalPages,
        totalElements: totalElements,
        first: first,
        sort: sort.toDomain(),
        pageable: pageable.toDomain(),
        numberOfElements: numberOfElements,
        last: last,
        size: size,
        content: content.map(_dtoToDomainAdapter<T>).toList(),
        number: number,
        empty: empty,
      );
}

/// Extension to parse Dto into domain object
extension SortModelParser on SortDto {
  /// Creates a Sort domain object from this
  Sort toDomain() => Sort(
        empty: empty,
        sorted: sorted,
        unsorted: unsorted,
      );
}

/// Extension to parse Dto into domain object
extension PageableModelParser on PageableDto {
  /// Creates a Pageable domain object from this
  Pageable toDomain() => Pageable(
        sort: sort.toDomain(),
        offset: offset,
        pageNumber: pageNumber,
        pageSize: pageSize,
        paged: paged,
        unpaged: unpaged,
      );
}
