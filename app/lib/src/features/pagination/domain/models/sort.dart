import 'package:freezed_annotation/freezed_annotation.dart';

part 'sort.freezed.dart';

/// Sorting data for pagination
@freezed
class Sort with _$Sort {
  /// Creates a [Sort] instance
  const factory Sort({
    required bool sorted,
    required bool unsorted,
    required bool empty,
  }) = _Sort;
}
