import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kapoow/src/features/session/domain/models/age_restriction.dart';

part 'user.freezed.dart';

/// User Data
@freezed
class User with _$User {
  /// Creates a [User] instance
  const factory User({
    required String id,
    required String email,
    required Set<String> roles,
    required bool sharedAllLibraries,
    required Set<String> sharedLibrariesIds,
    required Set<String> labelsAllow,
    required Set<String> labelsExclude,
    AgeRestriction? ageRestriction,
  }) = _User;
}
