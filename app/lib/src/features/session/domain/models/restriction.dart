/// Restriction types from the api
enum Restriction {
  /// Allows only some things
  allowOnly,

  /// Exclude some things
  exclude;

  factory Restriction.fromString(String name) => switch (name) {
        'ALLOW_ONLY' => Restriction.allowOnly,
        'EXCLUDE' => Restriction.exclude,
        _ => throw Exception('Unknown Restriction: $name'),
      };
}
