// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'age_restriction.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$AgeRestriction {
  int get age => throw _privateConstructorUsedError;
  Restriction get restriction => throw _privateConstructorUsedError;

  /// Create a copy of AgeRestriction
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $AgeRestrictionCopyWith<AgeRestriction> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AgeRestrictionCopyWith<$Res> {
  factory $AgeRestrictionCopyWith(
          AgeRestriction value, $Res Function(AgeRestriction) then) =
      _$AgeRestrictionCopyWithImpl<$Res, AgeRestriction>;
  @useResult
  $Res call({int age, Restriction restriction});
}

/// @nodoc
class _$AgeRestrictionCopyWithImpl<$Res, $Val extends AgeRestriction>
    implements $AgeRestrictionCopyWith<$Res> {
  _$AgeRestrictionCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of AgeRestriction
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? age = null,
    Object? restriction = null,
  }) {
    return _then(_value.copyWith(
      age: null == age
          ? _value.age
          : age // ignore: cast_nullable_to_non_nullable
              as int,
      restriction: null == restriction
          ? _value.restriction
          : restriction // ignore: cast_nullable_to_non_nullable
              as Restriction,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$AgeRestrictionImplCopyWith<$Res>
    implements $AgeRestrictionCopyWith<$Res> {
  factory _$$AgeRestrictionImplCopyWith(_$AgeRestrictionImpl value,
          $Res Function(_$AgeRestrictionImpl) then) =
      __$$AgeRestrictionImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({int age, Restriction restriction});
}

/// @nodoc
class __$$AgeRestrictionImplCopyWithImpl<$Res>
    extends _$AgeRestrictionCopyWithImpl<$Res, _$AgeRestrictionImpl>
    implements _$$AgeRestrictionImplCopyWith<$Res> {
  __$$AgeRestrictionImplCopyWithImpl(
      _$AgeRestrictionImpl _value, $Res Function(_$AgeRestrictionImpl) _then)
      : super(_value, _then);

  /// Create a copy of AgeRestriction
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? age = null,
    Object? restriction = null,
  }) {
    return _then(_$AgeRestrictionImpl(
      age: null == age
          ? _value.age
          : age // ignore: cast_nullable_to_non_nullable
              as int,
      restriction: null == restriction
          ? _value.restriction
          : restriction // ignore: cast_nullable_to_non_nullable
              as Restriction,
    ));
  }
}

/// @nodoc

class _$AgeRestrictionImpl implements _AgeRestriction {
  const _$AgeRestrictionImpl({required this.age, required this.restriction});

  @override
  final int age;
  @override
  final Restriction restriction;

  @override
  String toString() {
    return 'AgeRestriction(age: $age, restriction: $restriction)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$AgeRestrictionImpl &&
            (identical(other.age, age) || other.age == age) &&
            (identical(other.restriction, restriction) ||
                other.restriction == restriction));
  }

  @override
  int get hashCode => Object.hash(runtimeType, age, restriction);

  /// Create a copy of AgeRestriction
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$AgeRestrictionImplCopyWith<_$AgeRestrictionImpl> get copyWith =>
      __$$AgeRestrictionImplCopyWithImpl<_$AgeRestrictionImpl>(
          this, _$identity);
}

abstract class _AgeRestriction implements AgeRestriction {
  const factory _AgeRestriction(
      {required final int age,
      required final Restriction restriction}) = _$AgeRestrictionImpl;

  @override
  int get age;
  @override
  Restriction get restriction;

  /// Create a copy of AgeRestriction
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$AgeRestrictionImplCopyWith<_$AgeRestrictionImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
