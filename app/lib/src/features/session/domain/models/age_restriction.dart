import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kapoow/src/features/session/domain/models/restriction.dart';

part 'age_restriction.freezed.dart';

/// Model that indicates if an specific age is allowed or not
@freezed
class AgeRestriction with _$AgeRestriction {
  /// Creates an [AgeRestriction] instance
  const factory AgeRestriction({
    required int age,
    required Restriction restriction,
  }) = _AgeRestriction;
}
