import 'package:kapoow/src/features/rest_api/data/dtos/age_restriction_dto.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/user_dto.dart';
import 'package:kapoow/src/features/session/domain/models/age_restriction.dart';
import 'package:kapoow/src/features/session/domain/models/restriction.dart';
import 'package:kapoow/src/features/session/domain/models/user.dart';

/// Extension to parse Dto into domain object
extension UserModelParser on UserDto {
  /// Creates a [User] domain object from this
  User toDomain() => User(
        id: id,
        email: email,
        roles: roles,
        sharedAllLibraries: sharedAllLibraries,
        sharedLibrariesIds: sharedLibrariesIds,
        labelsAllow: labelsAllow,
        labelsExclude: labelsExclude,
        ageRestriction: ageRestriction?.toDomain(),
      );
}

/// Extension to parse Dto into domain object
extension AgeRestrictionModelParser on AgeRestrictionDto {
  /// Creates a [AgeRestriction] domain object from this
  AgeRestriction toDomain() => AgeRestriction(
        age: age,
        restriction: Restriction.fromString(restriction),
      );
}
