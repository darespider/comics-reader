import 'package:kapoow/src/features/session/data/daos/session_dao.dart';
import 'package:kapoow/src/features/session/data/repositories/session_repository.dart';
import 'package:kapoow/src/features/session/domain/models/session_data.dart';

/// Repository to read and save session data from the secure storage
class SessionRepositoryImp implements SessionRepository {
  /// Creates a [SessionRepositoryImp] instance
  const SessionRepositoryImp(this._dao);

  final SessionDao _dao;

  @override
  Future<void> deleteSession() async {
    await _dao.delete();
  }

  @override
  Future<SessionData> getLocalSession() async {
    final (host, token) = await _dao.load();

    if (host == null || token == null) return const SessionData.invalid();

    return SessionData(token: token, host: host);
  }

  @override
  Future<void> saveSession(SessionData sessionData) async {
    if (sessionData.invalid) return;
    await _dao.save(
      host: sessionData.host,
      token: sessionData.token,
    );
  }
}
