// coverage:ignore-file
import 'package:kapoow/src/features/rest_api/data/datasources/pods.dart';
import 'package:kapoow/src/features/session/data/daos/secure_storage_session_dao.dart';
import 'package:kapoow/src/features/session/data/daos/session_dao.dart';
import 'package:kapoow/src/features/session/data/repositories/session_repository.dart';
import 'package:kapoow/src/features/session/data/repositories/user_repository.dart';
import 'package:kapoow/src/features/session/domain/repositories/session_repository_imp.dart';
import 'package:kapoow/src/features/session/domain/repositories/user_repository_imp.dart';
import 'package:kapoow/src/services/secure_storage/secure_storage_pods.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'session_pods.g.dart';

/// Provides a [UserRepository]
@Riverpod(keepAlive: true)
UserRepository userRepository(UserRepositoryRef ref) {
  return UserRepositoryImp(ref.watch(userDatasourcePod));
}

/// Provides a [SessionDao]
@Riverpod(keepAlive: true)
SessionDao sessionDao(SessionDaoRef ref) {
  return SecureStorageSessionDao(ref.watch(secureStorageServicePod));
}

/// Provides a [SessionRepository]
@Riverpod(keepAlive: true)
SessionRepository sessionRepository(SessionRepositoryRef ref) {
  return SessionRepositoryImp(ref.watch(sessionDaoPod));
}
