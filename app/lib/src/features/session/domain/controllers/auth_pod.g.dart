// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'auth_pod.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$authHash() => r'b25ba2c7735021bb63431fdf555d538b99fab74f';

/// Controller that handles the login and logout of the user
///
/// Copied from [Auth].
@ProviderFor(Auth)
final authPod = AutoDisposeAsyncNotifierProvider<Auth, void>.internal(
  Auth.new,
  name: r'authPod',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$authHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$Auth = AutoDisposeAsyncNotifier<void>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
