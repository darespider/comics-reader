// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pods.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$sessionFormHash() => r'2d5971f7ee93489f53d789b03a7661c21d3e0260';

/// Provides the session form state
///
/// Copied from [SessionForm].
@ProviderFor(SessionForm)
final sessionFormPod =
    AutoDisposeNotifierProvider<SessionForm, SessionFormState>.internal(
  SessionForm.new,
  name: r'sessionFormPod',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$sessionFormHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$SessionForm = AutoDisposeNotifier<SessionFormState>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
