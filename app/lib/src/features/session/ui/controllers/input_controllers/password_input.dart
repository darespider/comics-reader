import 'package:flutter/material.dart';
import 'package:formz/formz.dart';
import 'package:kapoow/src/utils/extensions.dart';

/// Posible errors
enum PasswordInputValidationError {
  /// The value is empty
  empty;
}

/// Converts errors to text using translation from context
extension PasswordInputValidationErrorText on PasswordInputValidationError {
  /// gets the label equivalent of the error
  String text(BuildContext context) {
    return switch (this) {
      PasswordInputValidationError.empty =>
        context.translations.sessionWidgetsInputPasswordError,
    };
  }
}

/// Input state controller for password field
class PasswordInput extends FormzInput<String, PasswordInputValidationError>
    with FormzInputErrorCacheMixin {
  /// Constructor which create a `pure` [PasswordInput] with a given value.
  PasswordInput.pure([super.value = '']) : super.pure();

  /// Constructor which create a `dirty` [PasswordInput] with a given value.
  PasswordInput.dirty([super.value = '']) : super.dirty();

  @override
  PasswordInputValidationError? validator(String value) {
    return value.isNotEmpty ? null : PasswordInputValidationError.empty;
  }
}
