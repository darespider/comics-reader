import 'package:flutter/material.dart';
import 'package:formz/formz.dart';
import 'package:kapoow/src/utils/extensions.dart';

/// Posible errors
enum EmailInputValidationError {
  /// The value doesn't respects the format
  invalid;
}

/// Converts errors to text using translation from context
extension EmailInputValidationErrorText on EmailInputValidationError {
  /// gets the label equivalent of the error
  String text(BuildContext context) {
    return switch (this) {
      EmailInputValidationError.invalid =>
        context.translations.sessionWidgetsInputEmailError,
    };
  }
}

/// Input state controller for email field
class EmailInput extends FormzInput<String, EmailInputValidationError>
    with FormzInputErrorCacheMixin {
  /// Constructor which create a `pure` [EmailInput] with a given value.
  EmailInput.pure([super.value = '']) : super.pure();

  /// Constructor which create a `dirty` [EmailInput] with a given value.
  EmailInput.dirty([super.value = '']) : super.dirty();

  static final _emailRegExp = RegExp(
    r'^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$',
  );

  @override
  EmailInputValidationError? validator(String value) {
    return _emailRegExp.hasMatch(value)
        ? null
        : EmailInputValidationError.invalid;
  }
}
