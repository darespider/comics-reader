import 'package:flutter/material.dart';
import 'package:formz/formz.dart';
import 'package:kapoow/src/utils/extensions.dart';

/// Posible errors
enum HostInputValidationError {
  /// The value doesn't respects the format
  invalid;
}

/// Converts errors to text using translation from context
extension HostInputValidationErrorText on HostInputValidationError {
  /// gets the label equivalent of the error
  String text(BuildContext context) {
    return switch (this) {
      HostInputValidationError.invalid =>
        context.translations.sessionWidgetsInputHostError,
    };
  }
}

/// Input state controller for host field
class HostInput extends FormzInput<String, HostInputValidationError>
    with FormzInputErrorCacheMixin {
  /// Constructor which create a `pure` [HostInput] with a given value.
  HostInput.pure([super.value = '']) : super.pure();

  /// Constructor which create a `dirty` [HostInput] with a given value.
  HostInput.dirty([super.value = '']) : super.dirty();

  static final _ipRegExp = RegExp(
    r'^(http(s?):\/\/)?(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$',
  );

  static final _domainRegExp = RegExp(
    r'^(http(s?):\/\/)[a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9]((?:\.[a-zA-Z]{2,})+)$',
  );

  /// returns `true` if [_ipRegExp] has match for [value]
  bool get isIp {
    if (value.isEmpty) return false;
    return _ipRegExp.hasMatch(value);
  }

  @override
  HostInputValidationError? validator(String value) {
    final domainMatch = _domainRegExp.hasMatch(value);
    final ipMatch = _ipRegExp.hasMatch(value);
    return domainMatch || ipMatch ? null : HostInputValidationError.invalid;
  }
}
