import 'package:flutter/material.dart';
import 'package:formz/formz.dart';
import 'package:kapoow/src/utils/extensions.dart';

/// Posible errors
enum PortInputValidationError {
  /// The value doesn't respects the format
  invalid;
}

/// Converts errors to text using translation from context
extension PortInputValidationErrorText on PortInputValidationError {
  /// gets the label equivalent of the error
  String text(BuildContext context) {
    return switch (this) {
      PortInputValidationError.invalid =>
        context.translations.sessionWidgetsInputPortError,
    };
  }
}

/// Input state controller for port field
class PortInput extends FormzInput<int?, PortInputValidationError>
    with FormzInputErrorCacheMixin {
  /// Constructor which create a `pure` [PortInput] with a given value.
  PortInput.pure([super.value]) : super.pure();

  /// Constructor which create a `dirty` [PortInput] with a given value.
  PortInput.dirty([super.value]) : super.dirty();

  @override
  PortInputValidationError? validator(int? value) {
    if (value == null) return null;
    return (value > 0 && value < 65536)
        ? null
        : PortInputValidationError.invalid;
  }
}
