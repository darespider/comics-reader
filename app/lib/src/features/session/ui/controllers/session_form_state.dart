import 'package:formz/formz.dart';
import 'package:kapoow/src/features/session/ui/controllers/input_controllers/email_input.dart';
import 'package:kapoow/src/features/session/ui/controllers/input_controllers/host_input.dart';
import 'package:kapoow/src/features/session/ui/controllers/input_controllers/password_input.dart';
import 'package:kapoow/src/features/session/ui/controllers/input_controllers/port_input.dart';

/// Class encapsulating the session form fields state
class SessionFormState with FormzMixin {
  /// Creates a [SessionFormState] with `pure` inputs
  SessionFormState({
    HostInput? host,
    PortInput? port,
    EmailInput? email,
    PasswordInput? password,
  })  : host = host ?? HostInput.pure(),
        port = port ?? PortInput.pure(),
        email = email ?? EmailInput.pure(),
        password = password ?? PasswordInput.pure();

  /// Input for the host field
  final HostInput host;

  /// Input for the port field
  final PortInput port;

  /// Input for the email field
  final EmailInput email;

  /// Input for the password field
  final PasswordInput password;

  /// Helper method that created a new instance from the `this` one
  SessionFormState copyWith({
    HostInput? host,
    PortInput? port,
    EmailInput? email,
    PasswordInput? password,
    FormzSubmissionStatus? status,
  }) {
    return SessionFormState(
      email: email ?? this.email,
      password: password ?? this.password,
      host: host ?? this.host,
      port: port ?? this.port,
    );
  }

  /// Returns the whole host base on the form host and port fields values
  ///
  /// if the host is an ip address then the port will be included, otherwise
  /// it will be ignored
  String get fullHost {
    var result = host.value;
    if (host.isIp) {
      result = '$result:${port.value}';
    }
    return result;
  }

  /// Validator for the server part of the form
  bool get isValidServer {
    var portValid = port.isValid;
    if (host.isIp) {
      portValid = port.isValid && !port.isPure;
    }

    return host.isValid && !host.isPure && portValid;
  }

  @override
  List<FormzInput<dynamic, dynamic>> get inputs => [
        host,
        port,
        email,
        password,
      ];
}
