import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:kapoow/gen/assets.gen.dart';
import 'package:kapoow/src/common_widgets/extensions.dart';
import 'package:kapoow/src/constants/app_paddings.dart';
import 'package:kapoow/src/features/rest_api/domain/pods.dart';
import 'package:kapoow/src/features/session/domain/controllers/auth_pod.dart';
import 'package:kapoow/src/features/session/ui/controllers/pods.dart';
import 'package:kapoow/src/features/session/ui/widgets/input_fields/email_input_field.dart';
import 'package:kapoow/src/features/session/ui/widgets/input_fields/password_input_field.dart';
import 'package:kapoow/src/features/session/ui/widgets/onboarding_scaffold.dart';
import 'package:kapoow/src/routes/onboarding_routes_data.dart';
import 'package:kapoow/src/utils/extensions.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';

/// Screen with a form to put user credentials
class LoginScreen extends HookConsumerWidget {
  /// Creates instance of [LoginScreen] widget
  const LoginScreen({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final formController = ref.watch(sessionFormPod);
    final authAsyncState = ref.watch(authPod);

    final translations = context.translations;

    return OnboardingScaffoldBody(
      headerText: translations.sessionScreenLoginTitle,
      headerImage: Assets.images.onboarding.headerLogin.svg(),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const EmailInputField(),
          const PasswordInputField(),
        ].separated(mainAxisExtent: AppPaddings.medium),
      ),
      footer: Row(
        children: [
          Expanded(
            child: FilledButton.tonalIcon(
              label: Text(translations.sessionScreenEditServerButton),
              icon: PhosphorIcon(
                PhosphorIcons.arrowLeft(PhosphorIconsStyle.duotone),
              ),
              onPressed: () => const ServerInfoFormRoute().go(context),
            ),
          ),
          Expanded(
            flex: 2,
            child: authAsyncState.isLoading
                ? const FilledButton(
                    onPressed: null,
                    child: SizedBox.square(
                      dimension: 18,
                      child: CircularProgressIndicator(
                        strokeWidth: 3,
                      ),
                    ),
                  )
                : FilledButton.icon(
                    onPressed: formController.isNotValid
                        ? null
                        : () async {
                            final authResult =
                                await ref.read(authPod.notifier).tryLogin(
                                      host: formController.fullHost,
                                      email: formController.email.value,
                                      password: formController.password.value,
                                    );

                            if (!context.mounted) {
                              // TODO(rurickdev): show unknown error snack
                              return;
                            }

                            authResult.when(
                              // TODO(rurickdev): show login success snack
                              success: () {
                                ref.invalidate(sessionAsyncPod);
                              },
                              // TODO(rurickdev): show wrong credentials
                              //  error snack
                              unauthorized: () =>
                                  debugPrint('Unauthorized Auth'),
                              // TODO(rurickdev): show wrong server error snack
                              failure: () => debugPrint('Failed Auth'),
                            );
                          },
                    icon: PhosphorIcon(
                      PhosphorIcons.signIn(PhosphorIconsStyle.duotone),
                    ),
                    label: Text(translations.sessionScreenLoginButton),
                  ),
          ),
        ].separated(mainAxisExtent: AppPaddings.small),
      ),
    );
  }
}
