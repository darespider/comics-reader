import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:kapoow/gen/assets.gen.dart';
import 'package:kapoow/src/common_widgets/extensions.dart';
import 'package:kapoow/src/constants/app_paddings.dart';
import 'package:kapoow/src/features/session/ui/controllers/pods.dart';
import 'package:kapoow/src/features/session/ui/widgets/input_fields/host_input_field.dart';
import 'package:kapoow/src/features/session/ui/widgets/input_fields/port_input_field.dart';
import 'package:kapoow/src/features/session/ui/widgets/onboarding_scaffold.dart';
import 'package:kapoow/src/routes/onboarding_routes_data.dart';
import 'package:kapoow/src/utils/extensions.dart';

/// Screen with a form to put host url
///
/// if the host url is an ip it will enable a port input field
class InputServerScreen extends ConsumerWidget {
  /// Creates instance of [InputServerScreen] Widget
  const InputServerScreen({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final formController = ref.watch(sessionFormPod);

    final translations = context.translations;

    return OnboardingScaffoldBody(
      headerText: translations.sessionScreenInputServerTitle,
      headerImage: Assets.images.onboarding.headerServer.svg(),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          const HostInputField(),
          const PortInputField(),
        ].separated(mainAxisExtent: AppPaddings.medium),
      ),
      footer: FilledButton(
        onPressed: !formController.isValidServer
            ? null
            : () async {
                const LoginFormRoute().go(context);
              },
        child: Center(
          child: Text(translations.sessionScreenContinueButton),
        ),
      ),
    );
  }
}
