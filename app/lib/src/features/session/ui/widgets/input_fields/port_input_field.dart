import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:kapoow/src/common_widgets/app_text_field.dart';
import 'package:kapoow/src/features/session/ui/controllers/input_controllers/port_input.dart';
import 'package:kapoow/src/features/session/ui/controllers/pods.dart';
import 'package:kapoow/src/utils/extensions.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';

/// Port Input widget that implements `AppTextFiel` widget
/// It shows a number keyboard and validates if the value
/// is a valid server port.
class PortInputField extends ConsumerWidget {
  /// PortInput constructor
  const PortInputField({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final portController =
        ref.watch(sessionFormPod.select((value) => value.port));

    final translations = context.translations;

    return AppTextField(
      initialValue: portController.value?.toString(),
      hint: translations.sessionWidgetsInputPortHint,
      label: translations.sessionWidgetsInputPort,
      prefixIcon: PhosphorIcon(PhosphorIcons.hash(PhosphorIconsStyle.duotone)),
      inputFormatters: [
        FilteringTextInputFormatter.digitsOnly,
      ],
      keyboardType: TextInputType.number,
      validator: (value) {
        if (value == null) return null;
        return portController.validator(int.parse(value))?.text(context);
      },
      onChanged: (value) {
        ref.read(sessionFormPod.notifier).setPort(value);
      },
    );
  }
}
