import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:kapoow/src/common_widgets/app_text_field.dart';
import 'package:kapoow/src/features/session/ui/controllers/input_controllers/password_input.dart';
import 'package:kapoow/src/features/session/ui/controllers/pods.dart';
import 'package:kapoow/src/utils/extensions.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';

/// Password Input widget that use an `AppTextFiel` widget
/// It obscure the text inside it for security reasons.
/// It lets the user toggle the obscure functionality
class PasswordInputField extends HookConsumerWidget {
  /// Password input constructor
  const PasswordInputField({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final passwordController =
        ref.watch(sessionFormPod.select((value) => value.password));

    final translations = context.translations;
    final obscureText = useState<bool>(true);

    return AppTextField(
      initialValue: passwordController.value,
      autofillHints: const [AutofillHints.password],
      hint: translations.sessionWidgetsInputPasswordHint,
      label: translations.sessionWidgetsInputPassword,
      prefixIcon: PhosphorIcon(
        PhosphorIcons.password(PhosphorIconsStyle.duotone),
      ),
      suffixIcon: IconButton(
        icon: PhosphorIcon(
          obscureText.value
              ? PhosphorIcons.eye(PhosphorIconsStyle.duotone)
              : PhosphorIcons.eyeClosed(PhosphorIconsStyle.duotone),
        ),
        onPressed: () {
          obscureText.value = !obscureText.value;
        },
      ),
      keyboardType: TextInputType.visiblePassword,
      obscureText: obscureText.value,
      validator: (value) {
        if (value == null) return null;
        return passwordController.validator(value)?.text(context);
      },
      onChanged: (value) {
        ref.read(sessionFormPod.notifier).setPassword(value);
      },
    );
  }
}
