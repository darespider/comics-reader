import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:kapoow/src/common_widgets/app_text_field.dart';
import 'package:kapoow/src/features/session/ui/controllers/input_controllers/email_input.dart';
import 'package:kapoow/src/features/session/ui/controllers/pods.dart';
import 'package:kapoow/src/utils/extensions.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';

/// Email Input widget that uses an `AppTextFiel` widget
/// It shows a email keyboard and validates if the value
/// is a valid email.
class EmailInputField extends ConsumerWidget {
  /// Creates instance of [EmailInputField] widget
  const EmailInputField({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final emailController =
        ref.watch(sessionFormPod.select((value) => value.email));

    final translations = context.translations;

    return AppTextField(
      initialValue: emailController.value,
      autofillHints: const [
        AutofillHints.email,
        AutofillHints.username,
      ],
      hint: translations.sessionWidgetsInputEmailHint,
      label: translations.sessionWidgetsInputEmail,
      prefixIcon: PhosphorIcon(
        PhosphorIcons.envelope(PhosphorIconsStyle.duotone),
      ),
      keyboardType: TextInputType.emailAddress,
      validator: (value) {
        if (value == null) return null;

        return emailController.validator(value)?.text(context);
      },
      onChanged: (value) {
        ref.watch(sessionFormPod.notifier).setEmail(value);
      },
    );
  }
}
