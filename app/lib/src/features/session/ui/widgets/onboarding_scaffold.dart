import 'package:flutter/material.dart';
import 'package:flutter_svg_provider/flutter_svg_provider.dart';
import 'package:kapoow/gen/assets.gen.dart';
import 'package:kapoow/src/common_widgets/extensions.dart';
import 'package:kapoow/src/constants/app_paddings.dart';
import 'package:kapoow/src/theme/extensions.dart';

/// Scaffold with the background image for the onboarding form
class OnboardingScaffold extends StatelessWidget {
  /// Creates a [OnboardingScaffold] instance
  const OnboardingScaffold({
    required this.child,
    super.key,
  });

  /// Widget to render inside the scaffold
  final Widget child;

  @override
  Widget build(BuildContext context) {
    final brightness = context.theme.brightness;

    return DecoratedBox(
      decoration: BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.cover,
          image: Svg(
            switch (brightness) {
              Brightness.dark => Assets.images.onboarding.backgroundDark.path,
              Brightness.light => Assets.images.onboarding.backgroundLight.path,
            },
          ),
        ),
      ),
      child: child,
    );
  }
}

/// Scaffold that follows the layout
/// - Text Title
/// - Header Image
/// - Body
/// - Footer
///
/// It doesn't have an app bar or navigation bars
class OnboardingScaffoldBody extends StatelessWidget {
  /// Creates instance of [OnboardingScaffoldBody] widget
  const OnboardingScaffoldBody({
    required this.headerText,
    required this.headerImage,
    required this.body,
    super.key,
    this.footer,
  });

  /// Text show at the top of the page with a `headline4` style
  final String headerText;

  /// Image show below the header with a height of `250`
  final Widget headerImage;

  /// Widget show bellow the [headerImage] and over the [footer]
  /// it can scroll if it is too big for the screen
  final Widget body;

  /// Widget how at the bottom of the screen
  /// it has a fixed position, if the rest of the widgets do scroll
  /// it will stay in his position
  final Widget? footer;

  @override
  Widget build(BuildContext context) {
    final textColor = context.colorScheme.onSurface;

    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Center(
        child: Container(
          constraints: const BoxConstraints(
            maxWidth: 500,
          ),
          padding: const EdgeInsets.all(AppPaddings.small),
          child: Column(
            children: [
              Expanded(
                child: ListView(
                  children: [
                    Text(
                      headerText,
                      style: context.textTheme.headlineSmall?.copyWith(
                        color: textColor,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    const SizedBox(height: AppPaddings.small),
                    SizedBox(
                      height: 250,
                      width: double.infinity,
                      child: headerImage,
                    ),
                    body,
                  ].separated(),
                ),
              ),
              if (footer != null)
                Padding(
                  padding: const EdgeInsets.symmetric(
                    vertical: AppPaddings.small,
                  ),
                  child: footer,
                ),
            ],
          ),
        ),
      ),
    );
  }
}
