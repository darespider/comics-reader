import 'package:kapoow/src/features/rest_api/data/dtos/user_dto.dart';
import 'package:kapoow/src/features/session/domain/models/user.dart';

/// Interface describing how the users repositories should work
abstract class UserRepository {
  /// Returns the [UserDto] of the current user
  Future<User> getMeData();

  /// Returns the [UserDto] of the user with the respective [id]
  Future<User> getUserData(String id);

  /// Returns a list of [UserDto] with the users of the server
  Future<List<User>> getUsersData();
}
