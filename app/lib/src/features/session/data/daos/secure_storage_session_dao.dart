import 'package:kapoow/src/features/session/data/daos/session_dao.dart';
import 'package:kapoow/src/features/session/data/daos/session_dao_exceptions.dart';
import 'package:kapoow/src/services/secure_storage/secure_storage_service.dart';
import 'package:meta/meta.dart';

/// DAO that fetch the session from the secure storage
class SecureStorageSessionDao implements SessionDao {
  /// Creates instance of [SessionDao]
  SecureStorageSessionDao(this._storage);

  final SecureStorageService _storage;

  /// identifier for storage for token
  @visibleForTesting
  static const tokenKey = 'token';

  /// identifier for storage for host
  @visibleForTesting
  static const hostKey = 'host';

  Future<void> _saveToken(String token) => _storage.saveValue(tokenKey, token);

  Future<void> _saveHost(String host) => _storage.saveValue(hostKey, host);

  @override
  Future<void> save({required String host, required String token}) async {
    await Future.wait([
      _saveToken(token),
      _saveHost(host),
    ]);
  }

  Future<String?> _getToken() => _storage.getValue(tokenKey);

  Future<String?> _getHost() => _storage.getValue(hostKey);

  @override
  Future<(String? host, String? token)> load() async {
    final String? token;
    final String? host;
    try {
      token = await _getToken();
    } catch (e) {
      throw RetrievingTokenException();
    }
    try {
      host = await _getHost();
    } catch (e) {
      throw RetrievingHostException();
    }

    return (host, token);
  }

  Future<void> _deleteToken() => _storage.deleteValue(tokenKey);

  Future<void> _deleteHost() => _storage.deleteValue(hostKey);

  @override
  Future<void> delete() => Future.wait([
        _deleteToken(),
        _deleteHost(),
      ]);
}
