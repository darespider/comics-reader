/// Contract for the session properties
abstract class SessionDao {
  /// Saves the `token` and `host` in the secure storage
  Future<void> save({required String host, required String token});

  /// Reads the `token` and `host` from the secure storage
  Future<(String? host, String? token)> load();

  /// Deletes the value with the keys `token` and `host` from the secure storage
  Future<void> delete();
}
