import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:intl/intl.dart' as intl;

import 'l10n_en.gen.dart';
import 'l10n_es.gen.dart';

// ignore_for_file: type=lint

/// Callers can lookup localized strings with an instance of Translations
/// returned by `Translations.of(context)`.
///
/// Applications need to include `Translations.delegate()` in their app's
/// `localizationDelegates` list, and the locales they support in the app's
/// `supportedLocales` list. For example:
///
/// ```dart
/// import 'gen/l10n.gen.dart';
///
/// return MaterialApp(
///   localizationsDelegates: Translations.localizationsDelegates,
///   supportedLocales: Translations.supportedLocales,
///   home: MyApplicationHome(),
/// );
/// ```
///
/// ## Update pubspec.yaml
///
/// Please make sure to update your pubspec.yaml to include the following
/// packages:
///
/// ```yaml
/// dependencies:
///   # Internationalization support.
///   flutter_localizations:
///     sdk: flutter
///   intl: any # Use the pinned version from flutter_localizations
///
///   # Rest of dependencies
/// ```
///
/// ## iOS Applications
///
/// iOS applications define key application metadata, including supported
/// locales, in an Info.plist file that is built into the application bundle.
/// To configure the locales supported by your app, you’ll need to edit this
/// file.
///
/// First, open your project’s ios/Runner.xcworkspace Xcode workspace file.
/// Then, in the Project Navigator, open the Info.plist file under the Runner
/// project’s Runner folder.
///
/// Next, select the Information Property List item, select Add Item from the
/// Editor menu, then select Localizations from the pop-up menu.
///
/// Select and expand the newly-created Localizations item then, for each
/// locale your application supports, add a new item and select the locale
/// you wish to add from the pop-up menu in the Value field. This list should
/// be consistent with the languages listed in the Translations.supportedLocales
/// property.
abstract class Translations {
  Translations(String locale)
      : localeName = intl.Intl.canonicalizedLocale(locale.toString());

  final String localeName;

  static Translations of(BuildContext context) {
    return Localizations.of<Translations>(context, Translations)!;
  }

  static const LocalizationsDelegate<Translations> delegate =
      _TranslationsDelegate();

  /// A list of this localizations delegate along with the default localizations
  /// delegates.
  ///
  /// Returns a list of localizations delegates containing this delegate along with
  /// GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate,
  /// and GlobalWidgetsLocalizations.delegate.
  ///
  /// Additional delegates can be added by appending to this list in
  /// MaterialApp. This list does not have to be used at all if a custom list
  /// of delegates is preferred or required.
  static const List<LocalizationsDelegate<dynamic>> localizationsDelegates =
      <LocalizationsDelegate<dynamic>>[
    delegate,
    GlobalMaterialLocalizations.delegate,
    GlobalCupertinoLocalizations.delegate,
    GlobalWidgetsLocalizations.delegate,
  ];

  /// A list of this localizations delegate's supported locales.
  static const List<Locale> supportedLocales = <Locale>[
    Locale('en'),
    Locale('es')
  ];

  /// No description provided for @appName.
  ///
  /// In en, this message translates to:
  /// **'Kapoow!'**
  String get appName;

  /// Loading Message
  ///
  /// In en, this message translates to:
  /// **'Loading...'**
  String get splashScreenLoading;

  /// Title for Input Server Screen
  ///
  /// In en, this message translates to:
  /// **'Server Connection Info'**
  String get sessionScreenInputServerTitle;

  /// Label for continue button
  ///
  /// In en, this message translates to:
  /// **'Continue'**
  String get sessionScreenContinueButton;

  /// label for host input field
  ///
  /// In en, this message translates to:
  /// **'Host'**
  String get sessionWidgetsInputHost;

  /// hint for host input field
  ///
  /// In en, this message translates to:
  /// **'https://komga.example.com'**
  String get sessionWidgetsInputHostHint;

  /// Error message for host input field
  ///
  /// In en, this message translates to:
  /// **'Invalid Host'**
  String get sessionWidgetsInputHostError;

  /// label for port input field
  ///
  /// In en, this message translates to:
  /// **'Port'**
  String get sessionWidgetsInputPort;

  /// hint for port input field
  ///
  /// In en, this message translates to:
  /// **'8080'**
  String get sessionWidgetsInputPortHint;

  /// Error message for Port input field
  ///
  /// In en, this message translates to:
  /// **'Invalid Port'**
  String get sessionWidgetsInputPortError;

  /// Title for Input Server Screen
  ///
  /// In en, this message translates to:
  /// **'User Credentials'**
  String get sessionScreenLoginTitle;

  /// Label for edit server button
  ///
  /// In en, this message translates to:
  /// **'Edit Server'**
  String get sessionScreenEditServerButton;

  /// Label for login button
  ///
  /// In en, this message translates to:
  /// **'Login'**
  String get sessionScreenLoginButton;

  /// label for email input field
  ///
  /// In en, this message translates to:
  /// **'E-mail'**
  String get sessionWidgetsInputEmail;

  /// hint for email input field
  ///
  /// In en, this message translates to:
  /// **'email@example.com'**
  String get sessionWidgetsInputEmailHint;

  /// Error message for email input field
  ///
  /// In en, this message translates to:
  /// **'Invalid E-mail'**
  String get sessionWidgetsInputEmailError;

  /// label for password input field
  ///
  /// In en, this message translates to:
  /// **'Password'**
  String get sessionWidgetsInputPassword;

  /// hint for port input field
  ///
  /// In en, this message translates to:
  /// **'pass123'**
  String get sessionWidgetsInputPasswordHint;

  /// Error message for password input field
  ///
  /// In en, this message translates to:
  /// **'Password required'**
  String get sessionWidgetsInputPasswordError;

  /// Advice about selector
  ///
  /// In en, this message translates to:
  /// **'Explore all your books and series, or filter by library with the selector at the top.'**
  String get libraryScreenAdvice;

  /// Label value for all libraries option
  ///
  /// In en, this message translates to:
  /// **'All Libraries'**
  String get libraryWidgetsLibrarySelector;
}

class _TranslationsDelegate extends LocalizationsDelegate<Translations> {
  const _TranslationsDelegate();

  @override
  Future<Translations> load(Locale locale) {
    return SynchronousFuture<Translations>(lookupTranslations(locale));
  }

  @override
  bool isSupported(Locale locale) =>
      <String>['en', 'es'].contains(locale.languageCode);

  @override
  bool shouldReload(_TranslationsDelegate old) => false;
}

Translations lookupTranslations(Locale locale) {
  // Lookup logic when only language code is specified.
  switch (locale.languageCode) {
    case 'en':
      return TranslationsEn();
    case 'es':
      return TranslationsEs();
  }

  throw FlutterError(
      'Translations.delegate failed to load unsupported locale "$locale". This is likely '
      'an issue with the localizations generation tool. Please file an issue '
      'on GitHub with a reproducible sample app and the gen-l10n configuration '
      'that was used.');
}
