import 'l10n.gen.dart';

// ignore_for_file: type=lint

/// The translations for Spanish Castilian (`es`).
class TranslationsEs extends Translations {
  TranslationsEs([String locale = 'es']) : super(locale);

  @override
  String get appName => 'Kapoow!';

  @override
  String get splashScreenLoading => 'Cargando...';

  @override
  String get sessionScreenInputServerTitle =>
      'Información de Conexión del Server';

  @override
  String get sessionScreenContinueButton => 'Continuar';

  @override
  String get sessionWidgetsInputHost => 'Host';

  @override
  String get sessionWidgetsInputHostHint => 'https://komga.example.com';

  @override
  String get sessionWidgetsInputHostError => 'Host Invalido';

  @override
  String get sessionWidgetsInputPort => 'Puerto';

  @override
  String get sessionWidgetsInputPortHint => '8080';

  @override
  String get sessionWidgetsInputPortError => 'Puerto Invalido';

  @override
  String get sessionScreenLoginTitle => 'Credenciales de Usuario';

  @override
  String get sessionScreenEditServerButton => 'Editar Servidor';

  @override
  String get sessionScreenLoginButton => 'Login';

  @override
  String get sessionWidgetsInputEmail => 'E-mail';

  @override
  String get sessionWidgetsInputEmailHint => 'email@example.com';

  @override
  String get sessionWidgetsInputEmailError => 'E-mail Invalido';

  @override
  String get sessionWidgetsInputPassword => 'Contraseña';

  @override
  String get sessionWidgetsInputPasswordHint => 'contraseña123';

  @override
  String get sessionWidgetsInputPasswordError => 'Contraseña requerida';

  @override
  String get libraryScreenAdvice =>
      'Explora todos tus libros y series, o filtra por librería con el selector superior';

  @override
  String get libraryWidgetsLibrarySelector => 'Todas las Librerías';
}
