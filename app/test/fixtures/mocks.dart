import 'package:http/http.dart' as http;
import 'package:mocktail/mocktail.dart';

import 'utils.dart';

class MockClient extends Mock implements http.Client {}

extension MockResponses on http.Client {
  void mockGet([String response = '', int statusCode = 200]) => when(
        // ignore: discarded_futures
        () => get(
          any(),
          headers: any(named: 'headers'),
        ),
      ).thenAnswer(
        (_) async => http.Response(
          response,
          statusCode,
          headers: commonHeaders,
        ),
      );

  void mockPost([String response = '', int statusCode = 200]) => when(
        // ignore: discarded_futures
        () => post(
          any(),
          headers: any(named: 'headers'),
          body: any(named: 'body'),
        ),
      ).thenAnswer((_) async => http.Response(response, statusCode));

  void mockPatch([String response = '', int statusCode = 200]) => when(
        // ignore: discarded_futures
        () => patch(
          any(),
          headers: any(named: 'headers'),
          body: any(named: 'body'),
        ),
      ).thenAnswer((_) async => http.Response(response, statusCode));

  void mockPut([String response = '', int statusCode = 200]) => when(
        // ignore: discarded_futures
        () => put(
          any(),
          headers: any(named: 'headers'),
          body: any(named: 'body'),
        ),
      ).thenAnswer((_) async => http.Response(response, statusCode));

  void mockDelete([String response = '', int statusCode = 200]) => when(
        // ignore: discarded_futures
        () => delete(
          any(),
          headers: any(named: 'headers'),
          body: any(named: 'body'),
        ),
      ).thenAnswer((_) async => http.Response(response, statusCode));
}

extension CaptureRequest on http.Client {
  List<dynamic> capturedGet() => verify(
        // ignore: discarded_futures
        () => get(
          captureAny(),
          headers: any(named: 'headers'),
        ),
      ).captured;

  List<dynamic> capturedPost() => verify(
        // ignore: discarded_futures
        () => post(
          captureAny(),
          headers: captureAny(named: 'headers'),
          body: captureAny(named: 'body'),
        ),
      ).captured;

  List<dynamic> capturedPatch() => verify(
        // ignore: discarded_futures
        () => patch(
          captureAny(),
          headers: captureAny(named: 'headers'),
          body: captureAny(named: 'body'),
        ),
      ).captured;

  List<dynamic> capturedPut() => verify(
        // ignore: discarded_futures
        () => put(
          captureAny(),
          headers: captureAny(named: 'headers'),
          body: captureAny(named: 'body'),
        ),
      ).captured;

  List<dynamic> capturedDelete() => verify(
        // ignore: discarded_futures
        () => delete(
          captureAny(),
          headers: captureAny(named: 'headers'),
          body: captureAny(named: 'body'),
        ),
      ).captured;
}
