// ignore_for_file: lines_longer_than_80_chars
import 'package:kapoow/src/features/rest_api/data/dtos/book_metadata_aggregation_dto.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/collection_dto.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/pageable_dto.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/pagination_dto.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/series_dto.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/series_metadata_dto.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/sort_dto.dart';

final collectionDto = CollectionDto(
  id: '0AEA769D9Z133',
  name: 'Civil War',
  ordered: false,
  seriesIds: ['0AJ6D2Z0XJDWS'],
  createdDate: DateTime.parse('2022-11-04T16:40:14'),
  lastModifiedDate: DateTime.parse('2022-11-16T18:55:19'),
  filtered: false,
);

final pageCollectionDto = PageCollectionDto(
  content: [
    CollectionDto(
      id: '0AEA769D9Z133',
      name: 'Civil War',
      ordered: false,
      seriesIds: ['0AJ6D2Z0XJDWS'],
      createdDate: DateTime.parse('2022-11-04T16:40:14'),
      lastModifiedDate: DateTime.parse('2022-11-16T18:55:19'),
      filtered: false,
    ),
  ],
  pageable: const PageableDto(
    sort: SortDto(
      empty: false,
      sorted: true,
      unsorted: false,
    ),
    offset: 0,
    pageNumber: 0,
    pageSize: 20,
    paged: true,
    unpaged: false,
  ),
  totalElements: 1,
  last: true,
  totalPages: 1,
  size: 20,
  number: 0,
  sort: const SortDto(
    empty: false,
    sorted: true,
    unsorted: false,
  ),
  first: true,
  numberOfElements: 1,
  empty: false,
);

final collectionSeriesDto = PageSeriesDto(
  content: [
    SeriesDto(
      id: '0AJ6D2Z0XJDWS',
      libraryId: '0AE5SF447QPSM',
      name: 'Civil War',
      url: '',
      created: DateTime.parse('2022-11-16T18:53:58'),
      lastModified: DateTime.parse('2022-11-16T19:31:50'),
      fileLastModified: DateTime.parse('2021-06-23T03:54:12'),
      booksCount: 86,
      booksReadCount: 0,
      booksUnreadCount: 86,
      booksInProgressCount: 0,
      metadata: SeriesMetadataDto(
        status: 'ONGOING',
        statusLock: false,
        title: 'Amazing Spider-Man',
        titleLock: false,
        titleSort: 'Amazing Spider-Man',
        titleSortLock: false,
        summary: '',
        summaryLock: false,
        readingDirection: '',
        readingDirectionLock: false,
        publisher: '',
        publisherLock: false,
        ageRatingLock: false,
        language: '',
        languageLock: false,
        genres: {},
        genresLock: false,
        tags: {},
        tagsLock: false,
        totalBookCount: 7,
        totalBookCountLock: false,
        sharingLabels: {},
        sharingLabelsLock: false,
        created: DateTime.parse('2022-11-16T18:53:58'),
        lastModified: DateTime.parse('2022-11-16T20:04:15'),
        alternateTitles: {},
        links: {},
      ),
      booksMetadata: BookMetadataAggregationDto(
        authors: [],
        tags: {},
        summary: '',
        summaryNumber: '',
        created: DateTime.parse('2022-11-16T12:53:58'),
        lastModified: DateTime.parse('2022-11-16T14:05:42'),
      ),
      deleted: false,
    ),
  ],
  pageable: const PageableDto(
    sort: SortDto(
      empty: false,
      sorted: true,
      unsorted: false,
    ),
    offset: 0,
    pageNumber: 0,
    pageSize: 20,
    paged: true,
    unpaged: false,
  ),
  totalElements: 1,
  last: true,
  totalPages: 1,
  size: 20,
  number: 0,
  sort: const SortDto(
    empty: false,
    sorted: true,
    unsorted: false,
  ),
  first: true,
  numberOfElements: 1,
  empty: false,
);
