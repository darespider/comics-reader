// ignore_for_file: lines_longer_than_80_chars
const publishersDto = [
  'Ablaze',
  'Action Lab',
  'Aftershock Comics',
  'Ballantine Books',
  'Benitez Productions',
  'Black Mask Studios',
  'Boom! Studios',
  'Dark Horse',
  'Dark Horse Comics',
  'DC',
  'DC Comics',
  'Delcourt',
  'Dynamite Entertainment',
  'Ediciones Zinco',
  'Europe Comics',
  'IDW',
  'IDW Publishing',
  'Image',
  'Marvel',
  'Marvel Knights',
  'Valiant',
];

const ageRatingsDto = ['None', '18'];

const languagesDto = ['en'];

const genresDto = [
  'action/adventure',
  'anthropomorphic',
  "children's",
  'comedy',
  'crime',
  'fantasy',
  'horror',
  'leading ladies',
  'martial arts',
  'movies & tv',
  'non-fiction',
  'science fiction',
  'superhero',
  'supernatural/occult',
  'video games',
];
