// ignore_for_file: lines_longer_than_80_chars
import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart' as http;
import 'package:kapoow/src/features/library_management/common/models/media_status.dart';
import 'package:kapoow/src/features/library_management/common/models/read_status.dart';
import 'package:kapoow/src/features/library_management/common/models/series_status.dart';
import 'package:kapoow/src/features/rest_api/data/datasources/series/series_datasource.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/pagination_dto.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/series_dto.dart';
import 'package:kapoow/src/features/rest_api/domain/rest_api/rest_api.dart';
import 'package:kapoow/src/features/rest_api/domain/rest_api/rest_json_api.dart';
import 'package:mocktail/mocktail.dart';

import '../../../../../../fixtures/mocks.dart';
import '../../../../../../fixtures/utils.dart';
import '../../../fixtures/series_dtos.dart';

void main() {
  setUpAll(() {
    registerFallbackValue(Uri());
  });

  late http.Client client;
  late SeriesDatasource datasource;

  setUp(() {
    client = MockClient();
    datasource = SeriesDatasource(
      RestJsonApi(
        RestApi(
          client: client,
          apiUrl: Uri(),
          headers: commonHeaders,
        ),
      ),
    );
  });

  group(
    'getSeriesBooks',
    () {
      late String file;
      setUp(() {
        file = readJsonFixture('api/v1/series/id/books.json');
        client.mockGet(file);
      });

      test('Should make the right request', () async {
        await datasource.getSeriesBooks(
          seriesId: '1',
          mediaStatus: MediaStatus.ready,
          readStatus: ReadStatus.unread,
          authors: ['1'],
          page: 1,
          deleted: false,
          pageSize: 1,
          sort: ['asc'],
          tags: ['tag'],
          unpaged: false,
        );

        expect(
          client.capturedGet(),
          [
            Uri.parse(
              'api/v1/series/1/books?media_status=READY&read_status=UNREAD&tag=tag&deleted=false&unpaged=false&page=1&size=1&sort=asc&author=1',
            ),
          ],
        );
      });

      test('Should parse the response', () async {
        final response = await datasource.getSeriesBooks(
          seriesId: '1',
          mediaStatus: MediaStatus.ready,
          readStatus: ReadStatus.unread,
          authors: ['1'],
          page: 1,
          deleted: false,
          pageSize: 1,
          sort: ['asc'],
          tags: ['tag'],
          unpaged: false,
        );

        expect(response, isA<PageBookDto>());
        expect(response, seriesBooksDto);
      });
    },
  );

  group(
    'getLatestSeries',
    () {
      late String file;
      setUp(() {
        file = readJsonFixture('api/v1/series/latest.json');
        client.mockGet(file);
      });

      test('Should make the right request', () async {
        await datasource.getLatestSeries(
          deleted: true,
          libraryId: '1',
          page: 1,
          pageSize: 10,
          unpaged: true,
        );

        expect(
          client.capturedGet(),
          [
            Uri.parse(
              'api/v1/series/latest?library_id=1&deleted=true&unpaged=true&page=1&size=10',
            ),
          ],
        );
      });

      test('Should parse the response', () async {
        final response = await datasource.getLatestSeries(
          deleted: true,
          libraryId: '1',
          page: 1,
          pageSize: 10,
          unpaged: true,
        );

        expect(response, isA<PageSeriesDto>());
        expect(response, latestSeriesPageDto);
      });
    },
  );

  group(
    'getSeries',
    () {
      late String file;
      setUp(() {
        file = readJsonFixture('api/v1/series.json');
        client.mockGet(file);
      });

      test('Should make the right request', () async {
        await datasource.getSeries(
          authors: ['1'],
          deleted: false,
          libraryIds: ['1'],
          page: 1,
          readStatus: [ReadStatus.unread],
          size: 1,
          tags: ['tag'],
          unpaged: true,
          ageRating: ['A'],
          collectionsIds: ['1'],
          completed: false,
          genres: ['abc'],
          languages: ['en'],
          publishers: ['pub'],
          releaseYears: [DateTime(1)],
          search: 'search',
          searchRegex: 'regex',
          seriesStatuses: [SeriesStatus.ongoing],
          sort: ['asc'],
        );

        expect(
          client.capturedGet(),
          [
            Uri.parse(
              'api/v1/series?search=search&library_id=1&collection_id=1&status=ONGOING&read_status=UNREAD&publisher=pub&language=en&genre=abc&tag=tag&age_rating=A&release_year=1&deleted=false&completed=false&unpaged=true&search_regex=regex&page=1&size=1&sort=asc&author=1',
            ),
          ],
        );
      });

      test('Should parse the response', () async {
        final response = await datasource.getSeries(
          authors: ['1'],
          deleted: false,
          libraryIds: ['1'],
          page: 1,
          readStatus: [ReadStatus.unread],
          size: 1,
          tags: ['tag'],
          unpaged: true,
          ageRating: ['A'],
          collectionsIds: ['1'],
          completed: false,
          genres: ['abc'],
          languages: ['en'],
          publishers: ['pub'],
          releaseYears: [DateTime(1)],
          search: 'search',
          searchRegex: 'regex',
          seriesStatuses: [SeriesStatus.ongoing],
          sort: ['asc'],
        );

        expect(response, isA<PageSeriesDto>());
        expect(response, seriesPageDto);
      });
    },
  );

  group(
    'getSeriesById',
    () {
      late String file;
      setUp(() {
        file = readJsonFixture('api/v1/series/id.json');
        client.mockGet(file);
      });

      test('Should make the right request', () async {
        await datasource.getSeriesById('1');

        expect(
          client.capturedGet(),
          [
            Uri.parse(
              'api/v1/series/1',
            ),
          ],
        );
      });

      test('Should parse the response', () async {
        final response = await datasource.getSeriesById('1');

        expect(response, isA<SeriesDto>());
        expect(response, seriesDto);
      });
    },
  );
}
