// ignore_for_file: lines_longer_than_80_chars
import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart' as http;
import 'package:kapoow/src/features/library_management/common/models/read_status.dart';
import 'package:kapoow/src/features/library_management/common/models/series_status.dart';
import 'package:kapoow/src/features/rest_api/data/datasources/collections/collections_datasource.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/collection_dto.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/pagination_dto.dart';
import 'package:kapoow/src/features/rest_api/domain/rest_api/rest_api.dart';
import 'package:kapoow/src/features/rest_api/domain/rest_api/rest_json_api.dart';
import 'package:mocktail/mocktail.dart';

import '../../../../../../fixtures/mocks.dart';
import '../../../../../../fixtures/utils.dart';
import '../../../fixtures/collections_dtos.dart';

void main() {
  setUpAll(() {
    registerFallbackValue(Uri());
  });

  late http.Client client;
  late CollectionsDatasource datasource;

  setUp(() {
    client = MockClient();
    datasource = CollectionsDatasource(
      RestJsonApi(
        RestApi(
          client: client,
          apiUrl: Uri(),
          headers: commonHeaders,
        ),
      ),
    );
  });

  group(
    'getCollections',
    () {
      late String file;
      setUp(() {
        file = readJsonFixture('api/v1/collections.json');
        client.mockGet(file);
      });

      test('Should make the right request', () async {
        await datasource.getCollections(
          libraryIds: ['1'],
          page: 1,
          search: 'search',
          size: 1,
          unpaged: false,
        );

        expect(
          client.capturedGet(),
          [
            Uri.parse(
              'api/v1/collections?search=search&library_id=1&unpaged=false&page=1&size=1',
            ),
          ],
        );
      });

      test('Should parse the response', () async {
        final response = await datasource.getCollections();

        expect(response, isA<PageCollectionDto>());
        expect(response, pageCollectionDto);
      });
    },
  );

  group(
    'getCollectionById',
    () {
      late String file;
      setUp(() {
        file = readJsonFixture('api/v1/collections/id.json');
        client.mockGet(file);
      });

      test('Should make the right request', () async {
        await datasource.getCollectionById('1');

        expect(
          client.capturedGet(),
          [
            Uri.parse(
              'api/v1/collections/1',
            ),
          ],
        );
      });

      test('Should parse the response', () async {
        final response = await datasource.getCollectionById('1');

        expect(response, isA<CollectionDto>());
        expect(response, collectionDto);
      });
    },
  );

  group(
    'getCollectionSeries',
    () {
      late String file;
      setUp(() {
        file = readJsonFixture('api/v1/collections/id/series.json');
        client.mockGet(file);
      });

      test('Should make the right request', () async {
        await datasource.getCollectionSeries(
          collectionId: '1',
          ageRating: ['18'],
          authors: ['author'],
          complete: false,
          deleted: false,
          genre: ['genre'],
          language: ['en'],
          libraryIds: ['1'],
          page: 1,
          publisher: ['pub'],
          readStatus: ReadStatus.read,
          releaseYears: ['2023'],
          size: 1,
          status: [SeriesStatus.ongoing],
          tags: ['tag'],
          unpaged: false,
        );

        expect(
          client.capturedGet(),
          [
            Uri.parse(
              'api/v1/collections/1/series?library_id=1&status=ONGOING&read_status=READ&publisher=pub&language=en&genre=genre&tags=tag&age_rating=18&release_years=2023&deleted=false&complete=false&unpaged=false&page=1&size=1&authors=author',
            ),
          ],
        );
      });

      test('Should parse the response', () async {
        final response =
            await datasource.getCollectionSeries(collectionId: '1');

        expect(response, isA<PageSeriesDto>());
        expect(response, collectionSeriesDto);
      });
    },
  );
}
