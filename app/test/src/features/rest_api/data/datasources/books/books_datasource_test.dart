// ignore_for_file: lines_longer_than_80_chars
import 'dart:io';

import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart' as http;
import 'package:kapoow/src/features/library_management/common/models/media_status.dart';
import 'package:kapoow/src/features/library_management/common/models/read_status.dart';
import 'package:kapoow/src/features/rest_api/data/datasources/books/books_datasource.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/book_dto.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/page_dto.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/pagination_dto.dart';
import 'package:kapoow/src/features/rest_api/domain/rest_api/downloads_api.dart';
import 'package:kapoow/src/features/rest_api/domain/rest_api/rest_api.dart';
import 'package:kapoow/src/features/rest_api/domain/rest_api/rest_json_api.dart';
import 'package:mocktail/mocktail.dart';

import '../../../../../../fixtures/mocks.dart';
import '../../../../../../fixtures/utils.dart';
import '../../../fixtures/books_dtos.dart';

void main() {
  setUpAll(() {
    registerFallbackValue(Uri());
  });

  late http.Client client;
  late BooksDatasource datasource;
  late RestApi api;

  setUp(() {
    client = MockClient();
    api = RestApi(
      client: client,
      apiUrl: Uri(),
      headers: commonHeaders,
    );
    datasource = BooksDatasource(
      RestJsonApi(api),
      DownloadsApi(api),
    );
  });

  group(
    'deleteBook',
    () {
      test('Should make the right request', () async {
        client.mockDelete();

        await datasource.deleteBookById('1');

        expect(
          client.capturedDelete(),
          [
            Uri.parse('api/v1/books/1/file'),
            {'content-type': 'application/json; charset=utf-8'},
            'null',
          ],
        );
      });
    },
  );

  group(
    'getBookById',
    () {
      late String file;
      setUp(() {
        file = readJsonFixture('api/v1/books/id.json');
        client.mockGet(file);
      });

      test('Should make the right request', () async {
        await datasource.getBookById('1');

        expect(
          client.capturedGet(),
          [
            Uri.parse('api/v1/books/1'),
          ],
        );
      });

      test('Should parse the response', () async {
        final response = await datasource.getBookById('1');

        expect(response, isA<BookDto>());
        expect(response, bookDto);
      });
    },
  );

  group(
    'getBookPagesById',
    () {
      late String file;
      setUp(() {
        file = readJsonFixture('api/v1/books/id/pages.json');
        client.mockGet(file);
      });

      test('Should make the right request', () async {
        await datasource.getPagesByBookId('1');

        expect(
          client.capturedGet(),
          [
            Uri.parse('api/v1/books/1/pages'),
          ],
        );
      });

      test('Should parse the response', () async {
        final response = await datasource.getPagesByBookId('1');

        expect(response, isA<List<PageDto>>());
        expect(response, pagesDtos);
      });
    },
  );

  group(
    'getBooks',
    () {
      late String file;
      setUp(() {
        file = readJsonFixture('api/v1/books.json');
        client.mockGet(file);
      });

      test('Should make the right request', () async {
        await datasource.getBooks(
          libraryIds: ['1'],
          mediaStatus: [MediaStatus.ready],
          page: 1,
          readStatus: [ReadStatus.unread],
          releasedAfter: DateTime(1),
          search: 'search',
          size: 1,
          sort: ['asc'],
          tags: ['tag'],
          unpaged: false,
        );

        expect(
          client.capturedGet(),
          [
            Uri.parse(
              'api/v1/books?search=search&library_id=1&media_status=READY&read_status=UNREAD&released_after=0001-01-01&tags=tag&unpaged=false&page=1&size=1&sort=asc',
            ),
          ],
        );
      });

      test('Should parse the response', () async {
        final response = await datasource.getBooks();

        expect(response, isA<PageBookDto>());
        expect(response, pageBookDto);
      });
    },
  );

  group(
    'getBooksOnDeck',
    () {
      late String file;
      setUp(() {
        file = readJsonFixture('api/v1/books/ondeck.json');
        client.mockGet(file);
      });

      test('Should make the right request', () async {
        await datasource.getBooksOnDeck(
          libraryId: '1',
          page: 1,
          pageSize: 1,
        );

        expect(
          client.capturedGet(),
          [
            Uri.parse('api/v1/books/ondeck?library_id=1&page=1&size=1'),
          ],
        );
      });

      test('Should parse the response', () async {
        final response = await datasource.getBooksOnDeck();

        expect(response, isA<PageBookDto>());
        expect(response, onDeckDto);
      });
    },
  );

  group(
    'getPreviousBookById',
    () {
      late String file;
      setUp(() {
        file = readJsonFixture('api/v1/books/id/previous.json');
        client.mockGet(file);
      });

      test('Should make the right request', () async {
        await datasource.getPreviousBookById('1');

        expect(
          client.capturedGet(),
          [
            Uri.parse('api/v1/books/1/previous'),
          ],
        );
      });

      test('Should parse the response if found', () async {
        final response = await datasource.getPreviousBookById('1');

        expect(response, isA<BookDto>());
        expect(response, bookDto);
      });

      test('Should return null if NotFoundApiApiException is throw', () async {
        client.mockGet('', HttpStatus.notFound);

        final response = await datasource.getPreviousBookById('1');

        expect(response, isNull);
      });
    },
  );

  group(
    'getNextBookById',
    () {
      late String file;
      setUp(() {
        file = readJsonFixture('api/v1/books/id/next.json');
        client.mockGet(file);
      });

      test('Should make the right request', () async {
        await datasource.getNextBookById('1');

        expect(
          client.capturedGet(),
          [
            Uri.parse('api/v1/books/1/next'),
          ],
        );
      });

      test('Should parse the response if found', () async {
        final response = await datasource.getNextBookById('1');

        expect(response, isA<BookDto>());
        expect(response, bookDto);
      });

      test('Should return null if NotFoundApiApiException is throw', () async {
        client.mockGet('', HttpStatus.notFound);

        final response = await datasource.getNextBookById('1');

        expect(response, isNull);
      });
    },
  );

  group(
    'patchBookProgressById',
    () {
      setUp(() {
        client.mockPatch();
      });

      test('Should make the right request', () async {
        await datasource.patchBookProgressById(
          '1',
          completed: false,
          page: 1,
        );

        expect(
          client.capturedPatch(),
          [
            Uri.parse('api/v1/books/1/read-progress'),
            {'content-type': 'application/json; charset=utf-8'},
            '{"completed":"false","page":"1"}',
          ],
        );
      });
    },
  );

  group(
    'postAnalyzeBookById',
    () {
      setUp(() {
        client.mockPost();
      });

      test('Should make the right request', () async {
        await datasource.postAnalyzeBookById('1');

        expect(
          client.capturedPost(),
          [
            Uri.parse('api/v1/books/1/analyze'),
            {'content-type': 'application/json; charset=utf-8'},
            'null',
          ],
        );
      });
    },
  );

  group(
    'postRefreshBookById',
    () {
      setUp(() {
        client.mockPost();
      });

      test('Should make the right request', () async {
        await datasource.postRefreshBookById('1');

        expect(
          client.capturedPost(),
          [
            Uri.parse('api/v1/books/1/metadata/refresh'),
            {'content-type': 'application/json; charset=utf-8'},
            'null',
          ],
        );
      });
    },
  );
}
