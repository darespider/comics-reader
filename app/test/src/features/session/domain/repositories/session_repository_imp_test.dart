// ignore_for_file: lines_longer_than_80_chars
import 'package:flutter_test/flutter_test.dart';
import 'package:kapoow/src/features/session/data/daos/session_dao.dart';
import 'package:kapoow/src/features/session/domain/models/session_data.dart';
import 'package:kapoow/src/features/session/domain/repositories/session_repository_imp.dart';
import 'package:mocktail/mocktail.dart';

class MockSessionDao extends Mock implements SessionDao {}

void main() {
  late SessionDao dao;
  late SessionRepositoryImp repository;

  setUp(() {
    dao = MockSessionDao();
    repository = SessionRepositoryImp(dao);

    when(() => dao.delete()).thenAnswer((_) async {});
    when(
      () => dao.save(
        host: any(named: 'host'),
        token: any(named: 'token'),
      ),
    ).thenAnswer((_) async {});
  });

  group(
    'deleteSession',
    () {
      test(
        'Should call [SessionDao.delete] once',
        () async {
          await repository.deleteSession();

          verify(() => dao.delete()).called(1);
        },
      );
    },
  );

  group(
    'saveSession',
    () {
      test(
        'If sessionData is invalid Should never call [SessionDao.save]',
        () async {
          await repository.saveSession(const SessionData.invalid());

          verifyNever(
            () => dao.save(
              host: any(named: 'host'),
              token: any(named: 'token'),
            ),
          );
        },
      );
      test(
        'If sessionData is valid Should call [SessionDao.save] once',
        () async {
          await repository.saveSession(
            const SessionData(
              host: 'host_value',
              token: 'token_value',
            ),
          );

          final verified = verify(
            () => dao.save(
              host: captureAny(named: 'host'),
              token: captureAny(named: 'token'),
            ),
          )..called(1);

          expect(verified.captured, ['host_value', 'token_value']);
        },
      );
    },
  );

  group(
    'getLocalSession',
    () {
      test(
        'Should call [SessionDao.load] once and return a SessionData with the '
        'values returned from the dao',
        () async {
          when(() => dao.load())
              .thenAnswer((_) async => ('host_value', 'token_value'));

          final session = await repository.getLocalSession();

          expect(session.host, 'host_value');
          expect(session.token, 'token_value');

          verify(() => dao.load()).called(1);
        },
      );
      test(
        'Should call [SessionDao.load] once and return a SessionData.invalid '
        'if token value is null',
        () async {
          when(() => dao.load()).thenAnswer((_) async => ('host_value', null));

          final session = await repository.getLocalSession();

          expect(session, const SessionData.invalid());

          verify(() => dao.load()).called(1);
        },
      );
      test(
        'Should call [SessionDao.load] once and return a SessionData.invalid '
        'if host value is null',
        () async {
          when(() => dao.load()).thenAnswer((_) async => (null, 'token_value'));

          final session = await repository.getLocalSession();

          expect(session, const SessionData.invalid());

          verify(() => dao.load()).called(1);
        },
      );
    },
  );
}
