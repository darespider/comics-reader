// ignore_for_file: lines_longer_than_80_chars
import 'package:flutter_test/flutter_test.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/age_restriction_dto.dart';
import 'package:kapoow/src/features/rest_api/data/dtos/user_dto.dart';
import 'package:kapoow/src/features/session/domain/models/age_restriction.dart';
import 'package:kapoow/src/features/session/domain/models/restriction.dart';
import 'package:kapoow/src/features/session/domain/repositories/dto_domain_parser.dart';

void main() {
  group(
    'AgeRestrictionModelParser',
    () {
      test(
        'Should parse dto to domain model',
        () {
          const dto = AgeRestrictionDto(
            age: 1,
            restriction: 'ALLOW_ONLY',
          );
          final model = dto.toDomain();

          expect(model.age, 1);
          expect(model.restriction, Restriction.allowOnly);
        },
      );
    },
  );

  group(
    'UserModelParser',
    () {
      test(
        'Should parse dto to domain model',
        () {
          const dto = UserDto(
            email: 'email',
            id: '1',
            labelsAllow: {'label'},
            labelsExclude: {'other'},
            roles: {'admin'},
            sharedAllLibraries: true,
            sharedLibrariesIds: {'1'},
            ageRestriction: AgeRestrictionDto(
              age: 1,
              restriction: 'ALLOW_ONLY',
            ),
          );
          final model = dto.toDomain();

          expect(model.email, 'email');
          expect(model.id, '1');
          expect(model.labelsAllow, {'label'});
          expect(model.labelsExclude, {'other'});
          expect(model.roles, {'admin'});
          expect(model.sharedAllLibraries, true);
          expect(model.sharedLibrariesIds, {'1'});
          expect(model.ageRestriction, isA<AgeRestriction>());
        },
      );
    },
  );
}
