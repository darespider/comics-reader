# Kapoow

Comics reader for local comics and remote [Komga Server](https://github.com/gotson/komga)

## Objective

Create a modern multi platform comics reader using Flutter

## Road Map

- [x] Login to custom server
- [x] List and see details for collections, series, readlists and books
- [x] Global search
- [x] Read Books
    - [x] Read Book
        - [x] Display
        - [x] Cache images
    - [x] Read Books as incognito
    - [x] Read Books with Readlist context
    - [x] Show interactive thumbnails
    - [x] See book details while reading
    - [x] Navigate to Read Context on details
- [ ] Navigate to next/previous books based on context after finish book
- [ ] Custom Application Settings
    - [ ] Icons Style
    - [ ] Reading Overlay Tint
    - [ ] Read Direction
- [ ] Edit server Settings
- [ ] Multiple server support
    - [ ] Edit server settings by selected server
    - [ ] Global search by server
- [ ] Desktop layout

## Resources

### Images / Illustrations

Using the [Storyset Amico](https://storyset.com/amico) variant.

### Icons

Using [Phosphoricons](https://phosphoricons.com/) with a preference for duo tone icons.