//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of komga_dtos;

class OAuth2ClientDto {
  /// Returns a new [OAuth2ClientDto] instance.
  OAuth2ClientDto({
    required this.name,
    required this.registrationId,
  });

  String name;

  String registrationId;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is OAuth2ClientDto &&
          other.name == name &&
          other.registrationId == registrationId;

  @override
  int get hashCode =>
      // ignore: unnecessary_parenthesis
      (name.hashCode) + (registrationId.hashCode);

  @override
  String toString() =>
      'OAuth2ClientDto[name=$name, registrationId=$registrationId]';

  Map<String, dynamic> toJson() {
    final _json = <String, dynamic>{};
    _json[r'name'] = name;
    _json[r'registrationId'] = registrationId;
    return _json;
  }

  /// Returns a new [OAuth2ClientDto] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static OAuth2ClientDto? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();

      // Ensure that the map contains the required keys.
      // Note 1: the values aren't checked for validity beyond being non-null.
      // Note 2: this code is stripped in release mode!
      assert(() {
        requiredKeys.forEach((key) {
          assert(json.containsKey(key),
              'Required key "OAuth2ClientDto[$key]" is missing from JSON.');
          assert(json[key] != null,
              'Required key "OAuth2ClientDto[$key]" has a null value in JSON.');
        });
        return true;
      }());

      return OAuth2ClientDto(
        name: mapValueOfType<String>(json, r'name')!,
        registrationId: mapValueOfType<String>(json, r'registrationId')!,
      );
    }
    return null;
  }

  static List<OAuth2ClientDto>? listFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final result = <OAuth2ClientDto>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = OAuth2ClientDto.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }

  static Map<String, OAuth2ClientDto> mapFromJson(dynamic json) {
    final map = <String, OAuth2ClientDto>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = OAuth2ClientDto.fromJson(entry.value);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  // maps a json object with a list of OAuth2ClientDto-objects as value to a dart map
  static Map<String, List<OAuth2ClientDto>> mapListFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final map = <String, List<OAuth2ClientDto>>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = OAuth2ClientDto.listFromJson(
          entry.value,
          growable: growable,
        );
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  /// The list of required keys that must be present in a JSON.
  static const requiredKeys = <String>{
    'name',
    'registrationId',
  };
}
