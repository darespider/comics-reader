//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of komga_dtos;

class AgeRestrictionUpdateDto {
  /// Returns a new [AgeRestrictionUpdateDto] instance.
  AgeRestrictionUpdateDto({
    required this.age,
    required this.restriction,
  });

  int age;

  AgeRestrictionUpdateDtoRestrictionEnum restriction;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AgeRestrictionUpdateDto &&
          other.age == age &&
          other.restriction == restriction;

  @override
  int get hashCode =>
      // ignore: unnecessary_parenthesis
      (age.hashCode) + (restriction.hashCode);

  @override
  String toString() =>
      'AgeRestrictionUpdateDto[age=$age, restriction=$restriction]';

  Map<String, dynamic> toJson() {
    final _json = <String, dynamic>{};
    _json[r'age'] = age;
    _json[r'restriction'] = restriction;
    return _json;
  }

  /// Returns a new [AgeRestrictionUpdateDto] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static AgeRestrictionUpdateDto? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();

      // Ensure that the map contains the required keys.
      // Note 1: the values aren't checked for validity beyond being non-null.
      // Note 2: this code is stripped in release mode!
      assert(() {
        requiredKeys.forEach((key) {
          assert(json.containsKey(key),
              'Required key "AgeRestrictionUpdateDto[$key]" is missing from JSON.');
          assert(json[key] != null,
              'Required key "AgeRestrictionUpdateDto[$key]" has a null value in JSON.');
        });
        return true;
      }());

      return AgeRestrictionUpdateDto(
        age: mapValueOfType<int>(json, r'age')!,
        restriction: AgeRestrictionUpdateDtoRestrictionEnum.fromJson(
            json[r'restriction'])!,
      );
    }
    return null;
  }

  static List<AgeRestrictionUpdateDto>? listFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final result = <AgeRestrictionUpdateDto>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = AgeRestrictionUpdateDto.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }

  static Map<String, AgeRestrictionUpdateDto> mapFromJson(dynamic json) {
    final map = <String, AgeRestrictionUpdateDto>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = AgeRestrictionUpdateDto.fromJson(entry.value);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  // maps a json object with a list of AgeRestrictionUpdateDto-objects as value to a dart map
  static Map<String, List<AgeRestrictionUpdateDto>> mapListFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final map = <String, List<AgeRestrictionUpdateDto>>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = AgeRestrictionUpdateDto.listFromJson(
          entry.value,
          growable: growable,
        );
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  /// The list of required keys that must be present in a JSON.
  static const requiredKeys = <String>{
    'age',
    'restriction',
  };
}

class AgeRestrictionUpdateDtoRestrictionEnum {
  /// Instantiate a new enum with the provided [value].
  const AgeRestrictionUpdateDtoRestrictionEnum._(this.value);

  /// The underlying value of this enum member.
  final String value;

  @override
  String toString() => value;

  String toJson() => value;

  static const ALLOW_ONLY =
      AgeRestrictionUpdateDtoRestrictionEnum._(r'ALLOW_ONLY');
  static const EXCLUDE = AgeRestrictionUpdateDtoRestrictionEnum._(r'EXCLUDE');

  /// List of all possible values in this [enum][AgeRestrictionUpdateDtoRestrictionEnum].
  static const values = <AgeRestrictionUpdateDtoRestrictionEnum>[
    ALLOW_ONLY,
    EXCLUDE,
  ];

  static AgeRestrictionUpdateDtoRestrictionEnum? fromJson(dynamic value) =>
      AgeRestrictionUpdateDtoRestrictionEnumTypeTransformer().decode(value);

  static List<AgeRestrictionUpdateDtoRestrictionEnum>? listFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final result = <AgeRestrictionUpdateDtoRestrictionEnum>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = AgeRestrictionUpdateDtoRestrictionEnum.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }
}

/// Transformation class that can [encode] an instance of [AgeRestrictionUpdateDtoRestrictionEnum] to String,
/// and [decode] dynamic data back to [AgeRestrictionUpdateDtoRestrictionEnum].
class AgeRestrictionUpdateDtoRestrictionEnumTypeTransformer {
  factory AgeRestrictionUpdateDtoRestrictionEnumTypeTransformer() =>
      _instance ??=
          const AgeRestrictionUpdateDtoRestrictionEnumTypeTransformer._();

  const AgeRestrictionUpdateDtoRestrictionEnumTypeTransformer._();

  String encode(AgeRestrictionUpdateDtoRestrictionEnum data) => data.value;

  /// Decodes a [dynamic value][data] to a AgeRestrictionUpdateDtoRestrictionEnum.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  AgeRestrictionUpdateDtoRestrictionEnum? decode(dynamic data,
      {bool allowNull = true}) {
    if (data != null) {
      switch (data.toString()) {
        case r'ALLOW_ONLY':
          return AgeRestrictionUpdateDtoRestrictionEnum.ALLOW_ONLY;
        case r'EXCLUDE':
          return AgeRestrictionUpdateDtoRestrictionEnum.EXCLUDE;
        default:
          if (!allowNull) {
            throw ArgumentError('Unknown enum value to decode: $data');
          }
      }
    }
    return null;
  }

  /// Singleton [AgeRestrictionUpdateDtoRestrictionEnumTypeTransformer] instance.
  static AgeRestrictionUpdateDtoRestrictionEnumTypeTransformer? _instance;
}
