//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of komga_dtos;

class LibraryDto {
  /// Returns a new [LibraryDto] instance.
  LibraryDto({
    required this.id,
    required this.name,
    required this.root,
    required this.importComicInfoBook,
    required this.importComicInfoSeries,
    required this.importComicInfoCollection,
    required this.importComicInfoReadList,
    required this.importEpubBook,
    required this.importEpubSeries,
    required this.importMylarSeries,
    required this.importLocalArtwork,
    required this.importBarcodeIsbn,
    required this.scanForceModifiedTime,
    required this.scanDeep,
    required this.repairExtensions,
    required this.convertToCbz,
    required this.emptyTrashAfterScan,
    required this.seriesCover,
    required this.hashFiles,
    required this.hashPages,
    required this.analyzeDimensions,
    required this.unavailable,
  });

  String id;

  String name;

  String root;

  bool importComicInfoBook;

  bool importComicInfoSeries;

  bool importComicInfoCollection;

  bool importComicInfoReadList;

  bool importEpubBook;

  bool importEpubSeries;

  bool importMylarSeries;

  bool importLocalArtwork;

  bool importBarcodeIsbn;

  bool scanForceModifiedTime;

  bool scanDeep;

  bool repairExtensions;

  bool convertToCbz;

  bool emptyTrashAfterScan;

  LibraryDtoSeriesCoverEnum seriesCover;

  bool hashFiles;

  bool hashPages;

  bool analyzeDimensions;

  bool unavailable;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is LibraryDto &&
          other.id == id &&
          other.name == name &&
          other.root == root &&
          other.importComicInfoBook == importComicInfoBook &&
          other.importComicInfoSeries == importComicInfoSeries &&
          other.importComicInfoCollection == importComicInfoCollection &&
          other.importComicInfoReadList == importComicInfoReadList &&
          other.importEpubBook == importEpubBook &&
          other.importEpubSeries == importEpubSeries &&
          other.importMylarSeries == importMylarSeries &&
          other.importLocalArtwork == importLocalArtwork &&
          other.importBarcodeIsbn == importBarcodeIsbn &&
          other.scanForceModifiedTime == scanForceModifiedTime &&
          other.scanDeep == scanDeep &&
          other.repairExtensions == repairExtensions &&
          other.convertToCbz == convertToCbz &&
          other.emptyTrashAfterScan == emptyTrashAfterScan &&
          other.seriesCover == seriesCover &&
          other.hashFiles == hashFiles &&
          other.hashPages == hashPages &&
          other.analyzeDimensions == analyzeDimensions &&
          other.unavailable == unavailable;

  @override
  int get hashCode =>
      // ignore: unnecessary_parenthesis
      (id.hashCode) +
      (name.hashCode) +
      (root.hashCode) +
      (importComicInfoBook.hashCode) +
      (importComicInfoSeries.hashCode) +
      (importComicInfoCollection.hashCode) +
      (importComicInfoReadList.hashCode) +
      (importEpubBook.hashCode) +
      (importEpubSeries.hashCode) +
      (importMylarSeries.hashCode) +
      (importLocalArtwork.hashCode) +
      (importBarcodeIsbn.hashCode) +
      (scanForceModifiedTime.hashCode) +
      (scanDeep.hashCode) +
      (repairExtensions.hashCode) +
      (convertToCbz.hashCode) +
      (emptyTrashAfterScan.hashCode) +
      (seriesCover.hashCode) +
      (hashFiles.hashCode) +
      (hashPages.hashCode) +
      (analyzeDimensions.hashCode) +
      (unavailable.hashCode);

  @override
  String toString() =>
      'LibraryDto[id=$id, name=$name, root=$root, importComicInfoBook=$importComicInfoBook, importComicInfoSeries=$importComicInfoSeries, importComicInfoCollection=$importComicInfoCollection, importComicInfoReadList=$importComicInfoReadList, importEpubBook=$importEpubBook, importEpubSeries=$importEpubSeries, importMylarSeries=$importMylarSeries, importLocalArtwork=$importLocalArtwork, importBarcodeIsbn=$importBarcodeIsbn, scanForceModifiedTime=$scanForceModifiedTime, scanDeep=$scanDeep, repairExtensions=$repairExtensions, convertToCbz=$convertToCbz, emptyTrashAfterScan=$emptyTrashAfterScan, seriesCover=$seriesCover, hashFiles=$hashFiles, hashPages=$hashPages, analyzeDimensions=$analyzeDimensions, unavailable=$unavailable]';

  Map<String, dynamic> toJson() {
    final _json = <String, dynamic>{};
    _json[r'id'] = id;
    _json[r'name'] = name;
    _json[r'root'] = root;
    _json[r'importComicInfoBook'] = importComicInfoBook;
    _json[r'importComicInfoSeries'] = importComicInfoSeries;
    _json[r'importComicInfoCollection'] = importComicInfoCollection;
    _json[r'importComicInfoReadList'] = importComicInfoReadList;
    _json[r'importEpubBook'] = importEpubBook;
    _json[r'importEpubSeries'] = importEpubSeries;
    _json[r'importMylarSeries'] = importMylarSeries;
    _json[r'importLocalArtwork'] = importLocalArtwork;
    _json[r'importBarcodeIsbn'] = importBarcodeIsbn;
    _json[r'scanForceModifiedTime'] = scanForceModifiedTime;
    _json[r'scanDeep'] = scanDeep;
    _json[r'repairExtensions'] = repairExtensions;
    _json[r'convertToCbz'] = convertToCbz;
    _json[r'emptyTrashAfterScan'] = emptyTrashAfterScan;
    _json[r'seriesCover'] = seriesCover;
    _json[r'hashFiles'] = hashFiles;
    _json[r'hashPages'] = hashPages;
    _json[r'analyzeDimensions'] = analyzeDimensions;
    _json[r'unavailable'] = unavailable;
    return _json;
  }

  /// Returns a new [LibraryDto] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static LibraryDto? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();

      // Ensure that the map contains the required keys.
      // Note 1: the values aren't checked for validity beyond being non-null.
      // Note 2: this code is stripped in release mode!
      assert(() {
        requiredKeys.forEach((key) {
          assert(json.containsKey(key),
              'Required key "LibraryDto[$key]" is missing from JSON.');
          assert(json[key] != null,
              'Required key "LibraryDto[$key]" has a null value in JSON.');
        });
        return true;
      }());

      return LibraryDto(
        id: mapValueOfType<String>(json, r'id')!,
        name: mapValueOfType<String>(json, r'name')!,
        root: mapValueOfType<String>(json, r'root')!,
        importComicInfoBook:
            mapValueOfType<bool>(json, r'importComicInfoBook')!,
        importComicInfoSeries:
            mapValueOfType<bool>(json, r'importComicInfoSeries')!,
        importComicInfoCollection:
            mapValueOfType<bool>(json, r'importComicInfoCollection')!,
        importComicInfoReadList:
            mapValueOfType<bool>(json, r'importComicInfoReadList')!,
        importEpubBook: mapValueOfType<bool>(json, r'importEpubBook')!,
        importEpubSeries: mapValueOfType<bool>(json, r'importEpubSeries')!,
        importMylarSeries: mapValueOfType<bool>(json, r'importMylarSeries')!,
        importLocalArtwork: mapValueOfType<bool>(json, r'importLocalArtwork')!,
        importBarcodeIsbn: mapValueOfType<bool>(json, r'importBarcodeIsbn')!,
        scanForceModifiedTime:
            mapValueOfType<bool>(json, r'scanForceModifiedTime')!,
        scanDeep: mapValueOfType<bool>(json, r'scanDeep')!,
        repairExtensions: mapValueOfType<bool>(json, r'repairExtensions')!,
        convertToCbz: mapValueOfType<bool>(json, r'convertToCbz')!,
        emptyTrashAfterScan:
            mapValueOfType<bool>(json, r'emptyTrashAfterScan')!,
        seriesCover: LibraryDtoSeriesCoverEnum.fromJson(json[r'seriesCover'])!,
        hashFiles: mapValueOfType<bool>(json, r'hashFiles')!,
        hashPages: mapValueOfType<bool>(json, r'hashPages')!,
        analyzeDimensions: mapValueOfType<bool>(json, r'analyzeDimensions')!,
        unavailable: mapValueOfType<bool>(json, r'unavailable')!,
      );
    }
    return null;
  }

  static List<LibraryDto>? listFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final result = <LibraryDto>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = LibraryDto.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }

  static Map<String, LibraryDto> mapFromJson(dynamic json) {
    final map = <String, LibraryDto>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = LibraryDto.fromJson(entry.value);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  // maps a json object with a list of LibraryDto-objects as value to a dart map
  static Map<String, List<LibraryDto>> mapListFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final map = <String, List<LibraryDto>>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = LibraryDto.listFromJson(
          entry.value,
          growable: growable,
        );
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  /// The list of required keys that must be present in a JSON.
  static const requiredKeys = <String>{
    'id',
    'name',
    'root',
    'importComicInfoBook',
    'importComicInfoSeries',
    'importComicInfoCollection',
    'importComicInfoReadList',
    'importEpubBook',
    'importEpubSeries',
    'importMylarSeries',
    'importLocalArtwork',
    'importBarcodeIsbn',
    'scanForceModifiedTime',
    'scanDeep',
    'repairExtensions',
    'convertToCbz',
    'emptyTrashAfterScan',
    'seriesCover',
    'hashFiles',
    'hashPages',
    'analyzeDimensions',
    'unavailable',
  };
}

class LibraryDtoSeriesCoverEnum {
  /// Instantiate a new enum with the provided [value].
  const LibraryDtoSeriesCoverEnum._(this.value);

  /// The underlying value of this enum member.
  final String value;

  @override
  String toString() => value;

  String toJson() => value;

  static const FIRST = LibraryDtoSeriesCoverEnum._(r'FIRST');
  static const FIRST_UNREAD_OR_FIRST =
      LibraryDtoSeriesCoverEnum._(r'FIRST_UNREAD_OR_FIRST');
  static const FIRST_UNREAD_OR_LAST =
      LibraryDtoSeriesCoverEnum._(r'FIRST_UNREAD_OR_LAST');
  static const LAST = LibraryDtoSeriesCoverEnum._(r'LAST');

  /// List of all possible values in this [enum][LibraryDtoSeriesCoverEnum].
  static const values = <LibraryDtoSeriesCoverEnum>[
    FIRST,
    FIRST_UNREAD_OR_FIRST,
    FIRST_UNREAD_OR_LAST,
    LAST,
  ];

  static LibraryDtoSeriesCoverEnum? fromJson(dynamic value) =>
      LibraryDtoSeriesCoverEnumTypeTransformer().decode(value);

  static List<LibraryDtoSeriesCoverEnum>? listFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final result = <LibraryDtoSeriesCoverEnum>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = LibraryDtoSeriesCoverEnum.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }
}

/// Transformation class that can [encode] an instance of [LibraryDtoSeriesCoverEnum] to String,
/// and [decode] dynamic data back to [LibraryDtoSeriesCoverEnum].
class LibraryDtoSeriesCoverEnumTypeTransformer {
  factory LibraryDtoSeriesCoverEnumTypeTransformer() =>
      _instance ??= const LibraryDtoSeriesCoverEnumTypeTransformer._();

  const LibraryDtoSeriesCoverEnumTypeTransformer._();

  String encode(LibraryDtoSeriesCoverEnum data) => data.value;

  /// Decodes a [dynamic value][data] to a LibraryDtoSeriesCoverEnum.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  LibraryDtoSeriesCoverEnum? decode(dynamic data, {bool allowNull = true}) {
    if (data != null) {
      switch (data.toString()) {
        case r'FIRST':
          return LibraryDtoSeriesCoverEnum.FIRST;
        case r'FIRST_UNREAD_OR_FIRST':
          return LibraryDtoSeriesCoverEnum.FIRST_UNREAD_OR_FIRST;
        case r'FIRST_UNREAD_OR_LAST':
          return LibraryDtoSeriesCoverEnum.FIRST_UNREAD_OR_LAST;
        case r'LAST':
          return LibraryDtoSeriesCoverEnum.LAST;
        default:
          if (!allowNull) {
            throw ArgumentError('Unknown enum value to decode: $data');
          }
      }
    }
    return null;
  }

  /// Singleton [LibraryDtoSeriesCoverEnumTypeTransformer] instance.
  static LibraryDtoSeriesCoverEnumTypeTransformer? _instance;
}
