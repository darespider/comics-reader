//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of komga_dtos;

class ReadProgressDto {
  /// Returns a new [ReadProgressDto] instance.
  ReadProgressDto({
    required this.page,
    required this.completed,
    required this.readDate,
    required this.created,
    required this.lastModified,
  });

  int page;

  bool completed;

  DateTime readDate;

  DateTime created;

  DateTime lastModified;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ReadProgressDto &&
          other.page == page &&
          other.completed == completed &&
          other.readDate == readDate &&
          other.created == created &&
          other.lastModified == lastModified;

  @override
  int get hashCode =>
      // ignore: unnecessary_parenthesis
      (page.hashCode) +
      (completed.hashCode) +
      (readDate.hashCode) +
      (created.hashCode) +
      (lastModified.hashCode);

  @override
  String toString() =>
      'ReadProgressDto[page=$page, completed=$completed, readDate=$readDate, created=$created, lastModified=$lastModified]';

  Map<String, dynamic> toJson() {
    final _json = <String, dynamic>{};
    _json[r'page'] = page;
    _json[r'completed'] = completed;
    _json[r'readDate'] = readDate.toUtc().toIso8601String();
    _json[r'created'] = created.toUtc().toIso8601String();
    _json[r'lastModified'] = lastModified.toUtc().toIso8601String();
    return _json;
  }

  /// Returns a new [ReadProgressDto] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static ReadProgressDto? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();

      // Ensure that the map contains the required keys.
      // Note 1: the values aren't checked for validity beyond being non-null.
      // Note 2: this code is stripped in release mode!
      assert(() {
        requiredKeys.forEach((key) {
          assert(json.containsKey(key),
              'Required key "ReadProgressDto[$key]" is missing from JSON.');
          assert(json[key] != null,
              'Required key "ReadProgressDto[$key]" has a null value in JSON.');
        });
        return true;
      }());

      return ReadProgressDto(
        page: mapValueOfType<int>(json, r'page')!,
        completed: mapValueOfType<bool>(json, r'completed')!,
        readDate: mapDateTime(json, r'readDate', '')!,
        created: mapDateTime(json, r'created', '')!,
        lastModified: mapDateTime(json, r'lastModified', '')!,
      );
    }
    return null;
  }

  static List<ReadProgressDto>? listFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final result = <ReadProgressDto>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = ReadProgressDto.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }

  static Map<String, ReadProgressDto> mapFromJson(dynamic json) {
    final map = <String, ReadProgressDto>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = ReadProgressDto.fromJson(entry.value);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  // maps a json object with a list of ReadProgressDto-objects as value to a dart map
  static Map<String, List<ReadProgressDto>> mapListFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final map = <String, List<ReadProgressDto>>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = ReadProgressDto.listFromJson(
          entry.value,
          growable: growable,
        );
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  /// The list of required keys that must be present in a JSON.
  static const requiredKeys = <String>{
    'page',
    'completed',
    'readDate',
    'created',
    'lastModified',
  };
}
