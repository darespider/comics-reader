//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of komga_dtos;

class BookMetadataDto {
  /// Returns a new [BookMetadataDto] instance.
  BookMetadataDto({
    required this.title,
    required this.titleLock,
    required this.summary,
    required this.summaryLock,
    required this.number,
    required this.numberLock,
    required this.numberSort,
    required this.numberSortLock,
    this.releaseDate,
    required this.releaseDateLock,
    this.authors = const [],
    required this.authorsLock,
    this.tags = const {},
    required this.tagsLock,
    required this.isbn,
    required this.isbnLock,
    this.links = const [],
    required this.linksLock,
    required this.created,
    required this.lastModified,
  });

  String title;

  bool titleLock;

  String summary;

  bool summaryLock;

  String number;

  bool numberLock;

  double numberSort;

  bool numberSortLock;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  DateTime? releaseDate;

  bool releaseDateLock;

  List<AuthorDto> authors;

  bool authorsLock;

  Set<String> tags;

  bool tagsLock;

  String isbn;

  bool isbnLock;

  List<WebLinkDto> links;

  bool linksLock;

  DateTime created;

  DateTime lastModified;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is BookMetadataDto &&
          other.title == title &&
          other.titleLock == titleLock &&
          other.summary == summary &&
          other.summaryLock == summaryLock &&
          other.number == number &&
          other.numberLock == numberLock &&
          other.numberSort == numberSort &&
          other.numberSortLock == numberSortLock &&
          other.releaseDate == releaseDate &&
          other.releaseDateLock == releaseDateLock &&
          other.authors == authors &&
          other.authorsLock == authorsLock &&
          other.tags == tags &&
          other.tagsLock == tagsLock &&
          other.isbn == isbn &&
          other.isbnLock == isbnLock &&
          other.links == links &&
          other.linksLock == linksLock &&
          other.created == created &&
          other.lastModified == lastModified;

  @override
  int get hashCode =>
      // ignore: unnecessary_parenthesis
      (title.hashCode) +
      (titleLock.hashCode) +
      (summary.hashCode) +
      (summaryLock.hashCode) +
      (number.hashCode) +
      (numberLock.hashCode) +
      (numberSort.hashCode) +
      (numberSortLock.hashCode) +
      (releaseDate == null ? 0 : releaseDate!.hashCode) +
      (releaseDateLock.hashCode) +
      (authors.hashCode) +
      (authorsLock.hashCode) +
      (tags.hashCode) +
      (tagsLock.hashCode) +
      (isbn.hashCode) +
      (isbnLock.hashCode) +
      (links.hashCode) +
      (linksLock.hashCode) +
      (created.hashCode) +
      (lastModified.hashCode);

  @override
  String toString() =>
      'BookMetadataDto[title=$title, titleLock=$titleLock, summary=$summary, summaryLock=$summaryLock, number=$number, numberLock=$numberLock, numberSort=$numberSort, numberSortLock=$numberSortLock, releaseDate=$releaseDate, releaseDateLock=$releaseDateLock, authors=$authors, authorsLock=$authorsLock, tags=$tags, tagsLock=$tagsLock, isbn=$isbn, isbnLock=$isbnLock, links=$links, linksLock=$linksLock, created=$created, lastModified=$lastModified]';

  Map<String, dynamic> toJson() {
    final _json = <String, dynamic>{};
    _json[r'title'] = title;
    _json[r'titleLock'] = titleLock;
    _json[r'summary'] = summary;
    _json[r'summaryLock'] = summaryLock;
    _json[r'number'] = number;
    _json[r'numberLock'] = numberLock;
    _json[r'numberSort'] = numberSort;
    _json[r'numberSortLock'] = numberSortLock;
    if (releaseDate != null) {
      _json[r'releaseDate'] = dateFormatter.format(releaseDate!.toUtc());
    }
    _json[r'releaseDateLock'] = releaseDateLock;
    _json[r'authors'] = authors;
    _json[r'authorsLock'] = authorsLock;
    _json[r'tags'] = tags;
    _json[r'tagsLock'] = tagsLock;
    _json[r'isbn'] = isbn;
    _json[r'isbnLock'] = isbnLock;
    _json[r'links'] = links;
    _json[r'linksLock'] = linksLock;
    _json[r'created'] = created.toUtc().toIso8601String();
    _json[r'lastModified'] = lastModified.toUtc().toIso8601String();
    return _json;
  }

  /// Returns a new [BookMetadataDto] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static BookMetadataDto? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();

      // Ensure that the map contains the required keys.
      // Note 1: the values aren't checked for validity beyond being non-null.
      // Note 2: this code is stripped in release mode!
      assert(() {
        requiredKeys.forEach((key) {
          assert(json.containsKey(key),
              'Required key "BookMetadataDto[$key]" is missing from JSON.');
          assert(json[key] != null,
              'Required key "BookMetadataDto[$key]" has a null value in JSON.');
        });
        return true;
      }());

      return BookMetadataDto(
        title: mapValueOfType<String>(json, r'title')!,
        titleLock: mapValueOfType<bool>(json, r'titleLock')!,
        summary: mapValueOfType<String>(json, r'summary')!,
        summaryLock: mapValueOfType<bool>(json, r'summaryLock')!,
        number: mapValueOfType<String>(json, r'number')!,
        numberLock: mapValueOfType<bool>(json, r'numberLock')!,
        numberSort: mapValueOfType<double>(json, r'numberSort')!,
        numberSortLock: mapValueOfType<bool>(json, r'numberSortLock')!,
        releaseDate: mapDateTime(json, r'releaseDate', ''),
        releaseDateLock: mapValueOfType<bool>(json, r'releaseDateLock')!,
        authors: AuthorDto.listFromJson(json[r'authors'])!,
        authorsLock: mapValueOfType<bool>(json, r'authorsLock')!,
        tags: json[r'tags'] is Set
            ? (json[r'tags'] as Set).cast<String>()
            : const {},
        tagsLock: mapValueOfType<bool>(json, r'tagsLock')!,
        isbn: mapValueOfType<String>(json, r'isbn')!,
        isbnLock: mapValueOfType<bool>(json, r'isbnLock')!,
        links: WebLinkDto.listFromJson(json[r'links'])!,
        linksLock: mapValueOfType<bool>(json, r'linksLock')!,
        created: mapDateTime(json, r'created', '')!,
        lastModified: mapDateTime(json, r'lastModified', '')!,
      );
    }
    return null;
  }

  static List<BookMetadataDto>? listFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final result = <BookMetadataDto>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = BookMetadataDto.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }

  static Map<String, BookMetadataDto> mapFromJson(dynamic json) {
    final map = <String, BookMetadataDto>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = BookMetadataDto.fromJson(entry.value);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  // maps a json object with a list of BookMetadataDto-objects as value to a dart map
  static Map<String, List<BookMetadataDto>> mapListFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final map = <String, List<BookMetadataDto>>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = BookMetadataDto.listFromJson(
          entry.value,
          growable: growable,
        );
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  /// The list of required keys that must be present in a JSON.
  static const requiredKeys = <String>{
    'title',
    'titleLock',
    'summary',
    'summaryLock',
    'number',
    'numberLock',
    'numberSort',
    'numberSortLock',
    'releaseDateLock',
    'authors',
    'authorsLock',
    'tags',
    'tagsLock',
    'isbn',
    'isbnLock',
    'links',
    'linksLock',
    'created',
    'lastModified',
  };
}
