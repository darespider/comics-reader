//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of komga_dtos;

class UserUpdateDto {
  /// Returns a new [UserUpdateDto] instance.
  UserUpdateDto({
    this.roles = const {},
    this.ageRestriction,
    this.labelsAllow = const {},
    this.labelsExclude = const {},
    this.sharedLibraries,
  });

  Set<String> roles;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  AgeRestrictionUpdateDto? ageRestriction;

  Set<String> labelsAllow;

  Set<String> labelsExclude;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  SharedLibrariesUpdateDto? sharedLibraries;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is UserUpdateDto &&
          other.roles == roles &&
          other.ageRestriction == ageRestriction &&
          other.labelsAllow == labelsAllow &&
          other.labelsExclude == labelsExclude &&
          other.sharedLibraries == sharedLibraries;

  @override
  int get hashCode =>
      // ignore: unnecessary_parenthesis
      (roles.hashCode) +
      (ageRestriction == null ? 0 : ageRestriction!.hashCode) +
      (labelsAllow.hashCode) +
      (labelsExclude.hashCode) +
      (sharedLibraries == null ? 0 : sharedLibraries!.hashCode);

  @override
  String toString() =>
      'UserUpdateDto[roles=$roles, ageRestriction=$ageRestriction, labelsAllow=$labelsAllow, labelsExclude=$labelsExclude, sharedLibraries=$sharedLibraries]';

  Map<String, dynamic> toJson() {
    final _json = <String, dynamic>{};
    _json[r'roles'] = roles;
    if (ageRestriction != null) {
      _json[r'ageRestriction'] = ageRestriction;
    }
    _json[r'labelsAllow'] = labelsAllow;
    _json[r'labelsExclude'] = labelsExclude;
    if (sharedLibraries != null) {
      _json[r'sharedLibraries'] = sharedLibraries;
    }
    return _json;
  }

  /// Returns a new [UserUpdateDto] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static UserUpdateDto? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();

      // Ensure that the map contains the required keys.
      // Note 1: the values aren't checked for validity beyond being non-null.
      // Note 2: this code is stripped in release mode!
      assert(() {
        requiredKeys.forEach((key) {
          assert(json.containsKey(key),
              'Required key "UserUpdateDto[$key]" is missing from JSON.');
          assert(json[key] != null,
              'Required key "UserUpdateDto[$key]" has a null value in JSON.');
        });
        return true;
      }());

      return UserUpdateDto(
        roles: json[r'roles'] is Set
            ? (json[r'roles'] as Set).cast<String>()
            : const {},
        ageRestriction:
            AgeRestrictionUpdateDto.fromJson(json[r'ageRestriction']),
        labelsAllow: json[r'labelsAllow'] is Set
            ? (json[r'labelsAllow'] as Set).cast<String>()
            : const {},
        labelsExclude: json[r'labelsExclude'] is Set
            ? (json[r'labelsExclude'] as Set).cast<String>()
            : const {},
        sharedLibraries:
            SharedLibrariesUpdateDto.fromJson(json[r'sharedLibraries']),
      );
    }
    return null;
  }

  static List<UserUpdateDto>? listFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final result = <UserUpdateDto>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = UserUpdateDto.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }

  static Map<String, UserUpdateDto> mapFromJson(dynamic json) {
    final map = <String, UserUpdateDto>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = UserUpdateDto.fromJson(entry.value);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  // maps a json object with a list of UserUpdateDto-objects as value to a dart map
  static Map<String, List<UserUpdateDto>> mapListFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final map = <String, List<UserUpdateDto>>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = UserUpdateDto.listFromJson(
          entry.value,
          growable: growable,
        );
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  /// The list of required keys that must be present in a JSON.
  static const requiredKeys = <String>{};
}
