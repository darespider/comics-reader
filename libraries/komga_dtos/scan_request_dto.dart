//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of komga_dtos;

class ScanRequestDto {
  /// Returns a new [ScanRequestDto] instance.
  ScanRequestDto({
    required this.path,
  });

  String path;

  @override
  bool operator ==(Object other) =>
      identical(this, other) || other is ScanRequestDto && other.path == path;

  @override
  int get hashCode =>
      // ignore: unnecessary_parenthesis
      (path.hashCode);

  @override
  String toString() => 'ScanRequestDto[path=$path]';

  Map<String, dynamic> toJson() {
    final _json = <String, dynamic>{};
    _json[r'path'] = path;
    return _json;
  }

  /// Returns a new [ScanRequestDto] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static ScanRequestDto? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();

      // Ensure that the map contains the required keys.
      // Note 1: the values aren't checked for validity beyond being non-null.
      // Note 2: this code is stripped in release mode!
      assert(() {
        requiredKeys.forEach((key) {
          assert(json.containsKey(key),
              'Required key "ScanRequestDto[$key]" is missing from JSON.');
          assert(json[key] != null,
              'Required key "ScanRequestDto[$key]" has a null value in JSON.');
        });
        return true;
      }());

      return ScanRequestDto(
        path: mapValueOfType<String>(json, r'path')!,
      );
    }
    return null;
  }

  static List<ScanRequestDto>? listFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final result = <ScanRequestDto>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = ScanRequestDto.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }

  static Map<String, ScanRequestDto> mapFromJson(dynamic json) {
    final map = <String, ScanRequestDto>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = ScanRequestDto.fromJson(entry.value);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  // maps a json object with a list of ScanRequestDto-objects as value to a dart map
  static Map<String, List<ScanRequestDto>> mapListFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final map = <String, List<ScanRequestDto>>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = ScanRequestDto.listFromJson(
          entry.value,
          growable: growable,
        );
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  /// The list of required keys that must be present in a JSON.
  static const requiredKeys = <String>{
    'path',
  };
}
