//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of komga_dtos;

class WebLinkUpdateDto {
  /// Returns a new [WebLinkUpdateDto] instance.
  WebLinkUpdateDto({
    required this.label,
    this.url,
  });

  String label;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  String? url;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is WebLinkUpdateDto && other.label == label && other.url == url;

  @override
  int get hashCode =>
      // ignore: unnecessary_parenthesis
      (label.hashCode) + (url == null ? 0 : url!.hashCode);

  @override
  String toString() => 'WebLinkUpdateDto[label=$label, url=$url]';

  Map<String, dynamic> toJson() {
    final _json = <String, dynamic>{};
    _json[r'label'] = label;
    if (url != null) {
      _json[r'url'] = url;
    }
    return _json;
  }

  /// Returns a new [WebLinkUpdateDto] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static WebLinkUpdateDto? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();

      // Ensure that the map contains the required keys.
      // Note 1: the values aren't checked for validity beyond being non-null.
      // Note 2: this code is stripped in release mode!
      assert(() {
        requiredKeys.forEach((key) {
          assert(json.containsKey(key),
              'Required key "WebLinkUpdateDto[$key]" is missing from JSON.');
          assert(json[key] != null,
              'Required key "WebLinkUpdateDto[$key]" has a null value in JSON.');
        });
        return true;
      }());

      return WebLinkUpdateDto(
        label: mapValueOfType<String>(json, r'label')!,
        url: mapValueOfType<String>(json, r'url'),
      );
    }
    return null;
  }

  static List<WebLinkUpdateDto>? listFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final result = <WebLinkUpdateDto>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = WebLinkUpdateDto.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }

  static Map<String, WebLinkUpdateDto> mapFromJson(dynamic json) {
    final map = <String, WebLinkUpdateDto>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = WebLinkUpdateDto.fromJson(entry.value);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  // maps a json object with a list of WebLinkUpdateDto-objects as value to a dart map
  static Map<String, List<WebLinkUpdateDto>> mapListFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final map = <String, List<WebLinkUpdateDto>>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = WebLinkUpdateDto.listFromJson(
          entry.value,
          growable: growable,
        );
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  /// The list of required keys that must be present in a JSON.
  static const requiredKeys = <String>{
    'label',
  };
}
