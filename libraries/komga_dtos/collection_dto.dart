//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of komga_dtos;

class CollectionDto {
  /// Returns a new [CollectionDto] instance.
  CollectionDto({
    required this.id,
    required this.name,
    required this.ordered,
    this.seriesIds = const [],
    required this.createdDate,
    required this.lastModifiedDate,
    required this.filtered,
  });

  String id;

  String name;

  bool ordered;

  List<String> seriesIds;

  DateTime createdDate;

  DateTime lastModifiedDate;

  bool filtered;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CollectionDto &&
          other.id == id &&
          other.name == name &&
          other.ordered == ordered &&
          other.seriesIds == seriesIds &&
          other.createdDate == createdDate &&
          other.lastModifiedDate == lastModifiedDate &&
          other.filtered == filtered;

  @override
  int get hashCode =>
      // ignore: unnecessary_parenthesis
      (id.hashCode) +
      (name.hashCode) +
      (ordered.hashCode) +
      (seriesIds.hashCode) +
      (createdDate.hashCode) +
      (lastModifiedDate.hashCode) +
      (filtered.hashCode);

  @override
  String toString() =>
      'CollectionDto[id=$id, name=$name, ordered=$ordered, seriesIds=$seriesIds, createdDate=$createdDate, lastModifiedDate=$lastModifiedDate, filtered=$filtered]';

  Map<String, dynamic> toJson() {
    final _json = <String, dynamic>{};
    _json[r'id'] = id;
    _json[r'name'] = name;
    _json[r'ordered'] = ordered;
    _json[r'seriesIds'] = seriesIds;
    _json[r'createdDate'] = createdDate.toUtc().toIso8601String();
    _json[r'lastModifiedDate'] = lastModifiedDate.toUtc().toIso8601String();
    _json[r'filtered'] = filtered;
    return _json;
  }

  /// Returns a new [CollectionDto] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static CollectionDto? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();

      // Ensure that the map contains the required keys.
      // Note 1: the values aren't checked for validity beyond being non-null.
      // Note 2: this code is stripped in release mode!
      assert(() {
        requiredKeys.forEach((key) {
          assert(json.containsKey(key),
              'Required key "CollectionDto[$key]" is missing from JSON.');
          assert(json[key] != null,
              'Required key "CollectionDto[$key]" has a null value in JSON.');
        });
        return true;
      }());

      return CollectionDto(
        id: mapValueOfType<String>(json, r'id')!,
        name: mapValueOfType<String>(json, r'name')!,
        ordered: mapValueOfType<bool>(json, r'ordered')!,
        seriesIds: json[r'seriesIds'] is List
            ? (json[r'seriesIds'] as List).cast<String>()
            : const [],
        createdDate: mapDateTime(json, r'createdDate', '')!,
        lastModifiedDate: mapDateTime(json, r'lastModifiedDate', '')!,
        filtered: mapValueOfType<bool>(json, r'filtered')!,
      );
    }
    return null;
  }

  static List<CollectionDto>? listFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final result = <CollectionDto>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = CollectionDto.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }

  static Map<String, CollectionDto> mapFromJson(dynamic json) {
    final map = <String, CollectionDto>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = CollectionDto.fromJson(entry.value);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  // maps a json object with a list of CollectionDto-objects as value to a dart map
  static Map<String, List<CollectionDto>> mapListFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final map = <String, List<CollectionDto>>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = CollectionDto.listFromJson(
          entry.value,
          growable: growable,
        );
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  /// The list of required keys that must be present in a JSON.
  static const requiredKeys = <String>{
    'id',
    'name',
    'ordered',
    'seriesIds',
    'createdDate',
    'lastModifiedDate',
    'filtered',
  };
}
