//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of komga_dtos;

class UserDtoV2 {
  /// Returns a new [UserDtoV2] instance.
  UserDtoV2({
    required this.id,
    required this.email,
    this.roles = const {},
    required this.sharedAllLibraries,
    this.sharedLibrariesIds = const {},
    this.labelsAllow = const {},
    this.labelsExclude = const {},
    this.ageRestriction,
  });

  String id;

  String email;

  Set<String> roles;

  bool sharedAllLibraries;

  Set<String> sharedLibrariesIds;

  Set<String> labelsAllow;

  Set<String> labelsExclude;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  AgeRestrictionDto? ageRestriction;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is UserDtoV2 &&
          other.id == id &&
          other.email == email &&
          other.roles == roles &&
          other.sharedAllLibraries == sharedAllLibraries &&
          other.sharedLibrariesIds == sharedLibrariesIds &&
          other.labelsAllow == labelsAllow &&
          other.labelsExclude == labelsExclude &&
          other.ageRestriction == ageRestriction;

  @override
  int get hashCode =>
      // ignore: unnecessary_parenthesis
      (id.hashCode) +
      (email.hashCode) +
      (roles.hashCode) +
      (sharedAllLibraries.hashCode) +
      (sharedLibrariesIds.hashCode) +
      (labelsAllow.hashCode) +
      (labelsExclude.hashCode) +
      (ageRestriction == null ? 0 : ageRestriction!.hashCode);

  @override
  String toString() =>
      'UserDtoV2[id=$id, email=$email, roles=$roles, sharedAllLibraries=$sharedAllLibraries, sharedLibrariesIds=$sharedLibrariesIds, labelsAllow=$labelsAllow, labelsExclude=$labelsExclude, ageRestriction=$ageRestriction]';

  Map<String, dynamic> toJson() {
    final _json = <String, dynamic>{};
    _json[r'id'] = id;
    _json[r'email'] = email;
    _json[r'roles'] = roles;
    _json[r'sharedAllLibraries'] = sharedAllLibraries;
    _json[r'sharedLibrariesIds'] = sharedLibrariesIds;
    _json[r'labelsAllow'] = labelsAllow;
    _json[r'labelsExclude'] = labelsExclude;
    if (ageRestriction != null) {
      _json[r'ageRestriction'] = ageRestriction;
    }
    return _json;
  }

  /// Returns a new [UserDtoV2] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static UserDtoV2? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();

      // Ensure that the map contains the required keys.
      // Note 1: the values aren't checked for validity beyond being non-null.
      // Note 2: this code is stripped in release mode!
      assert(() {
        requiredKeys.forEach((key) {
          assert(json.containsKey(key),
              'Required key "UserDtoV2[$key]" is missing from JSON.');
          assert(json[key] != null,
              'Required key "UserDtoV2[$key]" has a null value in JSON.');
        });
        return true;
      }());

      return UserDtoV2(
        id: mapValueOfType<String>(json, r'id')!,
        email: mapValueOfType<String>(json, r'email')!,
        roles: json[r'roles'] is Set
            ? (json[r'roles'] as Set).cast<String>()
            : const {},
        sharedAllLibraries: mapValueOfType<bool>(json, r'sharedAllLibraries')!,
        sharedLibrariesIds: json[r'sharedLibrariesIds'] is Set
            ? (json[r'sharedLibrariesIds'] as Set).cast<String>()
            : const {},
        labelsAllow: json[r'labelsAllow'] is Set
            ? (json[r'labelsAllow'] as Set).cast<String>()
            : const {},
        labelsExclude: json[r'labelsExclude'] is Set
            ? (json[r'labelsExclude'] as Set).cast<String>()
            : const {},
        ageRestriction: AgeRestrictionDto.fromJson(json[r'ageRestriction']),
      );
    }
    return null;
  }

  static List<UserDtoV2>? listFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final result = <UserDtoV2>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = UserDtoV2.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }

  static Map<String, UserDtoV2> mapFromJson(dynamic json) {
    final map = <String, UserDtoV2>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = UserDtoV2.fromJson(entry.value);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  // maps a json object with a list of UserDtoV2-objects as value to a dart map
  static Map<String, List<UserDtoV2>> mapListFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final map = <String, List<UserDtoV2>>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = UserDtoV2.listFromJson(
          entry.value,
          growable: growable,
        );
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  /// The list of required keys that must be present in a JSON.
  static const requiredKeys = <String>{
    'id',
    'email',
    'roles',
    'sharedAllLibraries',
    'sharedLibrariesIds',
    'labelsAllow',
    'labelsExclude',
  };
}
