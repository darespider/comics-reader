//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of komga_dtos;

class PageHashCreationDto {
  /// Returns a new [PageHashCreationDto] instance.
  PageHashCreationDto({
    required this.hash,
    required this.mediaType,
    this.size,
    required this.action,
  });

  String hash;

  String mediaType;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  int? size;

  PageHashCreationDtoActionEnum action;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is PageHashCreationDto &&
          other.hash == hash &&
          other.mediaType == mediaType &&
          other.size == size &&
          other.action == action;

  @override
  int get hashCode =>
      // ignore: unnecessary_parenthesis
      (hash.hashCode) +
      (mediaType.hashCode) +
      (size == null ? 0 : size!.hashCode) +
      (action.hashCode);

  @override
  String toString() =>
      'PageHashCreationDto[hash=$hash, mediaType=$mediaType, size=$size, action=$action]';

  Map<String, dynamic> toJson() {
    final _json = <String, dynamic>{};
    _json[r'hash'] = hash;
    _json[r'mediaType'] = mediaType;
    if (size != null) {
      _json[r'size'] = size;
    }
    _json[r'action'] = action;
    return _json;
  }

  /// Returns a new [PageHashCreationDto] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static PageHashCreationDto? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();

      // Ensure that the map contains the required keys.
      // Note 1: the values aren't checked for validity beyond being non-null.
      // Note 2: this code is stripped in release mode!
      assert(() {
        requiredKeys.forEach((key) {
          assert(json.containsKey(key),
              'Required key "PageHashCreationDto[$key]" is missing from JSON.');
          assert(json[key] != null,
              'Required key "PageHashCreationDto[$key]" has a null value in JSON.');
        });
        return true;
      }());

      return PageHashCreationDto(
        hash: mapValueOfType<String>(json, r'hash')!,
        mediaType: mapValueOfType<String>(json, r'mediaType')!,
        size: mapValueOfType<int>(json, r'size'),
        action: PageHashCreationDtoActionEnum.fromJson(json[r'action'])!,
      );
    }
    return null;
  }

  static List<PageHashCreationDto>? listFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final result = <PageHashCreationDto>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = PageHashCreationDto.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }

  static Map<String, PageHashCreationDto> mapFromJson(dynamic json) {
    final map = <String, PageHashCreationDto>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = PageHashCreationDto.fromJson(entry.value);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  // maps a json object with a list of PageHashCreationDto-objects as value to a dart map
  static Map<String, List<PageHashCreationDto>> mapListFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final map = <String, List<PageHashCreationDto>>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = PageHashCreationDto.listFromJson(
          entry.value,
          growable: growable,
        );
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  /// The list of required keys that must be present in a JSON.
  static const requiredKeys = <String>{
    'hash',
    'mediaType',
    'action',
  };
}

class PageHashCreationDtoActionEnum {
  /// Instantiate a new enum with the provided [value].
  const PageHashCreationDtoActionEnum._(this.value);

  /// The underlying value of this enum member.
  final String value;

  @override
  String toString() => value;

  String toJson() => value;

  static const DELETE_AUTO = PageHashCreationDtoActionEnum._(r'DELETE_AUTO');
  static const DELETE_MANUAL =
      PageHashCreationDtoActionEnum._(r'DELETE_MANUAL');
  static const IGNORE = PageHashCreationDtoActionEnum._(r'IGNORE');

  /// List of all possible values in this [enum][PageHashCreationDtoActionEnum].
  static const values = <PageHashCreationDtoActionEnum>[
    DELETE_AUTO,
    DELETE_MANUAL,
    IGNORE,
  ];

  static PageHashCreationDtoActionEnum? fromJson(dynamic value) =>
      PageHashCreationDtoActionEnumTypeTransformer().decode(value);

  static List<PageHashCreationDtoActionEnum>? listFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final result = <PageHashCreationDtoActionEnum>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = PageHashCreationDtoActionEnum.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }
}

/// Transformation class that can [encode] an instance of [PageHashCreationDtoActionEnum] to String,
/// and [decode] dynamic data back to [PageHashCreationDtoActionEnum].
class PageHashCreationDtoActionEnumTypeTransformer {
  factory PageHashCreationDtoActionEnumTypeTransformer() =>
      _instance ??= const PageHashCreationDtoActionEnumTypeTransformer._();

  const PageHashCreationDtoActionEnumTypeTransformer._();

  String encode(PageHashCreationDtoActionEnum data) => data.value;

  /// Decodes a [dynamic value][data] to a PageHashCreationDtoActionEnum.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  PageHashCreationDtoActionEnum? decode(dynamic data, {bool allowNull = true}) {
    if (data != null) {
      switch (data.toString()) {
        case r'DELETE_AUTO':
          return PageHashCreationDtoActionEnum.DELETE_AUTO;
        case r'DELETE_MANUAL':
          return PageHashCreationDtoActionEnum.DELETE_MANUAL;
        case r'IGNORE':
          return PageHashCreationDtoActionEnum.IGNORE;
        default:
          if (!allowNull) {
            throw ArgumentError('Unknown enum value to decode: $data');
          }
      }
    }
    return null;
  }

  /// Singleton [PageHashCreationDtoActionEnumTypeTransformer] instance.
  static PageHashCreationDtoActionEnumTypeTransformer? _instance;
}
