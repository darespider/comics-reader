//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of komga_dtos;

class TachiyomiReadProgressUpdateDto {
  /// Returns a new [TachiyomiReadProgressUpdateDto] instance.
  TachiyomiReadProgressUpdateDto({
    required this.lastBookRead,
  });

  int lastBookRead;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is TachiyomiReadProgressUpdateDto &&
          other.lastBookRead == lastBookRead;

  @override
  int get hashCode =>
      // ignore: unnecessary_parenthesis
      (lastBookRead.hashCode);

  @override
  String toString() =>
      'TachiyomiReadProgressUpdateDto[lastBookRead=$lastBookRead]';

  Map<String, dynamic> toJson() {
    final _json = <String, dynamic>{};
    _json[r'lastBookRead'] = lastBookRead;
    return _json;
  }

  /// Returns a new [TachiyomiReadProgressUpdateDto] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static TachiyomiReadProgressUpdateDto? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();

      // Ensure that the map contains the required keys.
      // Note 1: the values aren't checked for validity beyond being non-null.
      // Note 2: this code is stripped in release mode!
      assert(() {
        requiredKeys.forEach((key) {
          assert(json.containsKey(key),
              'Required key "TachiyomiReadProgressUpdateDto[$key]" is missing from JSON.');
          assert(json[key] != null,
              'Required key "TachiyomiReadProgressUpdateDto[$key]" has a null value in JSON.');
        });
        return true;
      }());

      return TachiyomiReadProgressUpdateDto(
        lastBookRead: mapValueOfType<int>(json, r'lastBookRead')!,
      );
    }
    return null;
  }

  static List<TachiyomiReadProgressUpdateDto>? listFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final result = <TachiyomiReadProgressUpdateDto>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = TachiyomiReadProgressUpdateDto.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }

  static Map<String, TachiyomiReadProgressUpdateDto> mapFromJson(dynamic json) {
    final map = <String, TachiyomiReadProgressUpdateDto>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = TachiyomiReadProgressUpdateDto.fromJson(entry.value);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  // maps a json object with a list of TachiyomiReadProgressUpdateDto-objects as value to a dart map
  static Map<String, List<TachiyomiReadProgressUpdateDto>> mapListFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final map = <String, List<TachiyomiReadProgressUpdateDto>>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = TachiyomiReadProgressUpdateDto.listFromJson(
          entry.value,
          growable: growable,
        );
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  /// The list of required keys that must be present in a JSON.
  static const requiredKeys = <String>{
    'lastBookRead',
  };
}
