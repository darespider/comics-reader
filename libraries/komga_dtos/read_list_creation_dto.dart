//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of komga_dtos;

class ReadListCreationDto {
  /// Returns a new [ReadListCreationDto] instance.
  ReadListCreationDto({
    required this.name,
    required this.summary,
    this.bookIds = const [],
  });

  String name;

  String summary;

  List<String> bookIds;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ReadListCreationDto &&
          other.name == name &&
          other.summary == summary &&
          other.bookIds == bookIds;

  @override
  int get hashCode =>
      // ignore: unnecessary_parenthesis
      (name.hashCode) + (summary.hashCode) + (bookIds.hashCode);

  @override
  String toString() =>
      'ReadListCreationDto[name=$name, summary=$summary, bookIds=$bookIds]';

  Map<String, dynamic> toJson() {
    final _json = <String, dynamic>{};
    _json[r'name'] = name;
    _json[r'summary'] = summary;
    _json[r'bookIds'] = bookIds;
    return _json;
  }

  /// Returns a new [ReadListCreationDto] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static ReadListCreationDto? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();

      // Ensure that the map contains the required keys.
      // Note 1: the values aren't checked for validity beyond being non-null.
      // Note 2: this code is stripped in release mode!
      assert(() {
        requiredKeys.forEach((key) {
          assert(json.containsKey(key),
              'Required key "ReadListCreationDto[$key]" is missing from JSON.');
          assert(json[key] != null,
              'Required key "ReadListCreationDto[$key]" has a null value in JSON.');
        });
        return true;
      }());

      return ReadListCreationDto(
        name: mapValueOfType<String>(json, r'name')!,
        summary: mapValueOfType<String>(json, r'summary')!,
        bookIds: json[r'bookIds'] is List
            ? (json[r'bookIds'] as List).cast<String>()
            : const [],
      );
    }
    return null;
  }

  static List<ReadListCreationDto>? listFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final result = <ReadListCreationDto>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = ReadListCreationDto.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }

  static Map<String, ReadListCreationDto> mapFromJson(dynamic json) {
    final map = <String, ReadListCreationDto>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = ReadListCreationDto.fromJson(entry.value);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  // maps a json object with a list of ReadListCreationDto-objects as value to a dart map
  static Map<String, List<ReadListCreationDto>> mapListFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final map = <String, List<ReadListCreationDto>>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = ReadListCreationDto.listFromJson(
          entry.value,
          growable: growable,
        );
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  /// The list of required keys that must be present in a JSON.
  static const requiredKeys = <String>{
    'name',
    'summary',
    'bookIds',
  };
}
