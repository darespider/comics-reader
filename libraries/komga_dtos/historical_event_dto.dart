//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of komga_dtos;

class HistoricalEventDto {
  /// Returns a new [HistoricalEventDto] instance.
  HistoricalEventDto({
    required this.type,
    required this.timestamp,
    this.bookId,
    this.seriesId,
    this.properties = const {},
  });

  String type;

  DateTime timestamp;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  String? bookId;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  String? seriesId;

  Map<String, String> properties;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is HistoricalEventDto &&
          other.type == type &&
          other.timestamp == timestamp &&
          other.bookId == bookId &&
          other.seriesId == seriesId &&
          other.properties == properties;

  @override
  int get hashCode =>
      // ignore: unnecessary_parenthesis
      (type.hashCode) +
      (timestamp.hashCode) +
      (bookId == null ? 0 : bookId!.hashCode) +
      (seriesId == null ? 0 : seriesId!.hashCode) +
      (properties.hashCode);

  @override
  String toString() =>
      'HistoricalEventDto[type=$type, timestamp=$timestamp, bookId=$bookId, seriesId=$seriesId, properties=$properties]';

  Map<String, dynamic> toJson() {
    final _json = <String, dynamic>{};
    _json[r'type'] = type;
    _json[r'timestamp'] = timestamp.toUtc().toIso8601String();
    if (bookId != null) {
      _json[r'bookId'] = bookId;
    }
    if (seriesId != null) {
      _json[r'seriesId'] = seriesId;
    }
    _json[r'properties'] = properties;
    return _json;
  }

  /// Returns a new [HistoricalEventDto] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static HistoricalEventDto? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();

      // Ensure that the map contains the required keys.
      // Note 1: the values aren't checked for validity beyond being non-null.
      // Note 2: this code is stripped in release mode!
      assert(() {
        requiredKeys.forEach((key) {
          assert(json.containsKey(key),
              'Required key "HistoricalEventDto[$key]" is missing from JSON.');
          assert(json[key] != null,
              'Required key "HistoricalEventDto[$key]" has a null value in JSON.');
        });
        return true;
      }());

      return HistoricalEventDto(
        type: mapValueOfType<String>(json, r'type')!,
        timestamp: mapDateTime(json, r'timestamp', '')!,
        bookId: mapValueOfType<String>(json, r'bookId'),
        seriesId: mapValueOfType<String>(json, r'seriesId'),
        properties: mapCastOfType<String, String>(json, r'properties')!,
      );
    }
    return null;
  }

  static List<HistoricalEventDto>? listFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final result = <HistoricalEventDto>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = HistoricalEventDto.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }

  static Map<String, HistoricalEventDto> mapFromJson(dynamic json) {
    final map = <String, HistoricalEventDto>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = HistoricalEventDto.fromJson(entry.value);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  // maps a json object with a list of HistoricalEventDto-objects as value to a dart map
  static Map<String, List<HistoricalEventDto>> mapListFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final map = <String, List<HistoricalEventDto>>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = HistoricalEventDto.listFromJson(
          entry.value,
          growable: growable,
        );
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  /// The list of required keys that must be present in a JSON.
  static const requiredKeys = <String>{
    'type',
    'timestamp',
    'properties',
  };
}
