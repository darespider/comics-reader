//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of komga_dtos;

class RolesUpdateDto {
  /// Returns a new [RolesUpdateDto] instance.
  RolesUpdateDto({
    this.roles = const [],
  });

  List<String> roles;

  @override
  bool operator ==(Object other) =>
      identical(this, other) || other is RolesUpdateDto && other.roles == roles;

  @override
  int get hashCode =>
      // ignore: unnecessary_parenthesis
      (roles.hashCode);

  @override
  String toString() => 'RolesUpdateDto[roles=$roles]';

  Map<String, dynamic> toJson() {
    final _json = <String, dynamic>{};
    _json[r'roles'] = roles;
    return _json;
  }

  /// Returns a new [RolesUpdateDto] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static RolesUpdateDto? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();

      // Ensure that the map contains the required keys.
      // Note 1: the values aren't checked for validity beyond being non-null.
      // Note 2: this code is stripped in release mode!
      assert(() {
        requiredKeys.forEach((key) {
          assert(json.containsKey(key),
              'Required key "RolesUpdateDto[$key]" is missing from JSON.');
          assert(json[key] != null,
              'Required key "RolesUpdateDto[$key]" has a null value in JSON.');
        });
        return true;
      }());

      return RolesUpdateDto(
        roles: json[r'roles'] is List
            ? (json[r'roles'] as List).cast<String>()
            : const [],
      );
    }
    return null;
  }

  static List<RolesUpdateDto>? listFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final result = <RolesUpdateDto>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = RolesUpdateDto.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }

  static Map<String, RolesUpdateDto> mapFromJson(dynamic json) {
    final map = <String, RolesUpdateDto>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = RolesUpdateDto.fromJson(entry.value);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  // maps a json object with a list of RolesUpdateDto-objects as value to a dart map
  static Map<String, List<RolesUpdateDto>> mapListFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final map = <String, List<RolesUpdateDto>>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = RolesUpdateDto.listFromJson(
          entry.value,
          growable: growable,
        );
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  /// The list of required keys that must be present in a JSON.
  static const requiredKeys = <String>{
    'roles',
  };
}
