//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of komga_dtos;

class PageHashKnownDto {
  /// Returns a new [PageHashKnownDto] instance.
  PageHashKnownDto({
    required this.hash,
    required this.mediaType,
    this.size,
    required this.action,
    required this.deleteCount,
    required this.created,
    required this.lastModified,
  });

  String hash;

  String mediaType;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  int? size;

  PageHashKnownDtoActionEnum action;

  int deleteCount;

  DateTime created;

  DateTime lastModified;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is PageHashKnownDto &&
          other.hash == hash &&
          other.mediaType == mediaType &&
          other.size == size &&
          other.action == action &&
          other.deleteCount == deleteCount &&
          other.created == created &&
          other.lastModified == lastModified;

  @override
  int get hashCode =>
      // ignore: unnecessary_parenthesis
      (hash.hashCode) +
      (mediaType.hashCode) +
      (size == null ? 0 : size!.hashCode) +
      (action.hashCode) +
      (deleteCount.hashCode) +
      (created.hashCode) +
      (lastModified.hashCode);

  @override
  String toString() =>
      'PageHashKnownDto[hash=$hash, mediaType=$mediaType, size=$size, action=$action, deleteCount=$deleteCount, created=$created, lastModified=$lastModified]';

  Map<String, dynamic> toJson() {
    final _json = <String, dynamic>{};
    _json[r'hash'] = hash;
    _json[r'mediaType'] = mediaType;
    if (size != null) {
      _json[r'size'] = size;
    }
    _json[r'action'] = action;
    _json[r'deleteCount'] = deleteCount;
    _json[r'created'] = created.toUtc().toIso8601String();
    _json[r'lastModified'] = lastModified.toUtc().toIso8601String();
    return _json;
  }

  /// Returns a new [PageHashKnownDto] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static PageHashKnownDto? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();

      // Ensure that the map contains the required keys.
      // Note 1: the values aren't checked for validity beyond being non-null.
      // Note 2: this code is stripped in release mode!
      assert(() {
        requiredKeys.forEach((key) {
          assert(json.containsKey(key),
              'Required key "PageHashKnownDto[$key]" is missing from JSON.');
          assert(json[key] != null,
              'Required key "PageHashKnownDto[$key]" has a null value in JSON.');
        });
        return true;
      }());

      return PageHashKnownDto(
        hash: mapValueOfType<String>(json, r'hash')!,
        mediaType: mapValueOfType<String>(json, r'mediaType')!,
        size: mapValueOfType<int>(json, r'size'),
        action: PageHashKnownDtoActionEnum.fromJson(json[r'action'])!,
        deleteCount: mapValueOfType<int>(json, r'deleteCount')!,
        created: mapDateTime(json, r'created', '')!,
        lastModified: mapDateTime(json, r'lastModified', '')!,
      );
    }
    return null;
  }

  static List<PageHashKnownDto>? listFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final result = <PageHashKnownDto>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = PageHashKnownDto.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }

  static Map<String, PageHashKnownDto> mapFromJson(dynamic json) {
    final map = <String, PageHashKnownDto>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = PageHashKnownDto.fromJson(entry.value);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  // maps a json object with a list of PageHashKnownDto-objects as value to a dart map
  static Map<String, List<PageHashKnownDto>> mapListFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final map = <String, List<PageHashKnownDto>>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = PageHashKnownDto.listFromJson(
          entry.value,
          growable: growable,
        );
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  /// The list of required keys that must be present in a JSON.
  static const requiredKeys = <String>{
    'hash',
    'mediaType',
    'action',
    'deleteCount',
    'created',
    'lastModified',
  };
}

class PageHashKnownDtoActionEnum {
  /// Instantiate a new enum with the provided [value].
  const PageHashKnownDtoActionEnum._(this.value);

  /// The underlying value of this enum member.
  final String value;

  @override
  String toString() => value;

  String toJson() => value;

  static const DELETE_AUTO = PageHashKnownDtoActionEnum._(r'DELETE_AUTO');
  static const DELETE_MANUAL = PageHashKnownDtoActionEnum._(r'DELETE_MANUAL');
  static const IGNORE = PageHashKnownDtoActionEnum._(r'IGNORE');

  /// List of all possible values in this [enum][PageHashKnownDtoActionEnum].
  static const values = <PageHashKnownDtoActionEnum>[
    DELETE_AUTO,
    DELETE_MANUAL,
    IGNORE,
  ];

  static PageHashKnownDtoActionEnum? fromJson(dynamic value) =>
      PageHashKnownDtoActionEnumTypeTransformer().decode(value);

  static List<PageHashKnownDtoActionEnum>? listFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final result = <PageHashKnownDtoActionEnum>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = PageHashKnownDtoActionEnum.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }
}

/// Transformation class that can [encode] an instance of [PageHashKnownDtoActionEnum] to String,
/// and [decode] dynamic data back to [PageHashKnownDtoActionEnum].
class PageHashKnownDtoActionEnumTypeTransformer {
  factory PageHashKnownDtoActionEnumTypeTransformer() =>
      _instance ??= const PageHashKnownDtoActionEnumTypeTransformer._();

  const PageHashKnownDtoActionEnumTypeTransformer._();

  String encode(PageHashKnownDtoActionEnum data) => data.value;

  /// Decodes a [dynamic value][data] to a PageHashKnownDtoActionEnum.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  PageHashKnownDtoActionEnum? decode(dynamic data, {bool allowNull = true}) {
    if (data != null) {
      switch (data.toString()) {
        case r'DELETE_AUTO':
          return PageHashKnownDtoActionEnum.DELETE_AUTO;
        case r'DELETE_MANUAL':
          return PageHashKnownDtoActionEnum.DELETE_MANUAL;
        case r'IGNORE':
          return PageHashKnownDtoActionEnum.IGNORE;
        default:
          if (!allowNull) {
            throw ArgumentError('Unknown enum value to decode: $data');
          }
      }
    }
    return null;
  }

  /// Singleton [PageHashKnownDtoActionEnumTypeTransformer] instance.
  static PageHashKnownDtoActionEnumTypeTransformer? _instance;
}
