//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of komga_dtos;

class BookMetadataAggregationDto {
  /// Returns a new [BookMetadataAggregationDto] instance.
  BookMetadataAggregationDto({
    this.authors = const [],
    this.tags = const {},
    this.releaseDate,
    required this.summary,
    required this.summaryNumber,
    required this.created,
    required this.lastModified,
  });

  List<AuthorDto> authors;

  Set<String> tags;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  DateTime? releaseDate;

  String summary;

  String summaryNumber;

  DateTime created;

  DateTime lastModified;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is BookMetadataAggregationDto &&
          other.authors == authors &&
          other.tags == tags &&
          other.releaseDate == releaseDate &&
          other.summary == summary &&
          other.summaryNumber == summaryNumber &&
          other.created == created &&
          other.lastModified == lastModified;

  @override
  int get hashCode =>
      // ignore: unnecessary_parenthesis
      (authors.hashCode) +
      (tags.hashCode) +
      (releaseDate == null ? 0 : releaseDate!.hashCode) +
      (summary.hashCode) +
      (summaryNumber.hashCode) +
      (created.hashCode) +
      (lastModified.hashCode);

  @override
  String toString() =>
      'BookMetadataAggregationDto[authors=$authors, tags=$tags, releaseDate=$releaseDate, summary=$summary, summaryNumber=$summaryNumber, created=$created, lastModified=$lastModified]';

  Map<String, dynamic> toJson() {
    final _json = <String, dynamic>{};
    _json[r'authors'] = authors;
    _json[r'tags'] = tags;
    if (releaseDate != null) {
      _json[r'releaseDate'] = dateFormatter.format(releaseDate!.toUtc());
    }
    _json[r'summary'] = summary;
    _json[r'summaryNumber'] = summaryNumber;
    _json[r'created'] = created.toUtc().toIso8601String();
    _json[r'lastModified'] = lastModified.toUtc().toIso8601String();
    return _json;
  }

  /// Returns a new [BookMetadataAggregationDto] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static BookMetadataAggregationDto? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();

      // Ensure that the map contains the required keys.
      // Note 1: the values aren't checked for validity beyond being non-null.
      // Note 2: this code is stripped in release mode!
      assert(() {
        requiredKeys.forEach((key) {
          assert(json.containsKey(key),
              'Required key "BookMetadataAggregationDto[$key]" is missing from JSON.');
          assert(json[key] != null,
              'Required key "BookMetadataAggregationDto[$key]" has a null value in JSON.');
        });
        return true;
      }());

      return BookMetadataAggregationDto(
        authors: AuthorDto.listFromJson(json[r'authors'])!,
        tags: json[r'tags'] is Set
            ? (json[r'tags'] as Set).cast<String>()
            : const {},
        releaseDate: mapDateTime(json, r'releaseDate', ''),
        summary: mapValueOfType<String>(json, r'summary')!,
        summaryNumber: mapValueOfType<String>(json, r'summaryNumber')!,
        created: mapDateTime(json, r'created', '')!,
        lastModified: mapDateTime(json, r'lastModified', '')!,
      );
    }
    return null;
  }

  static List<BookMetadataAggregationDto>? listFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final result = <BookMetadataAggregationDto>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = BookMetadataAggregationDto.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }

  static Map<String, BookMetadataAggregationDto> mapFromJson(dynamic json) {
    final map = <String, BookMetadataAggregationDto>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = BookMetadataAggregationDto.fromJson(entry.value);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  // maps a json object with a list of BookMetadataAggregationDto-objects as value to a dart map
  static Map<String, List<BookMetadataAggregationDto>> mapListFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final map = <String, List<BookMetadataAggregationDto>>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = BookMetadataAggregationDto.listFromJson(
          entry.value,
          growable: growable,
        );
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  /// The list of required keys that must be present in a JSON.
  static const requiredKeys = <String>{
    'authors',
    'tags',
    'summary',
    'summaryNumber',
    'created',
    'lastModified',
  };
}
