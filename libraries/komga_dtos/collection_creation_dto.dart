//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of komga_dtos;

class CollectionCreationDto {
  /// Returns a new [CollectionCreationDto] instance.
  CollectionCreationDto({
    required this.name,
    required this.ordered,
    this.seriesIds = const [],
  });

  String name;

  bool ordered;

  List<String> seriesIds;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CollectionCreationDto &&
          other.name == name &&
          other.ordered == ordered &&
          other.seriesIds == seriesIds;

  @override
  int get hashCode =>
      // ignore: unnecessary_parenthesis
      (name.hashCode) + (ordered.hashCode) + (seriesIds.hashCode);

  @override
  String toString() =>
      'CollectionCreationDto[name=$name, ordered=$ordered, seriesIds=$seriesIds]';

  Map<String, dynamic> toJson() {
    final _json = <String, dynamic>{};
    _json[r'name'] = name;
    _json[r'ordered'] = ordered;
    _json[r'seriesIds'] = seriesIds;
    return _json;
  }

  /// Returns a new [CollectionCreationDto] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static CollectionCreationDto? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();

      // Ensure that the map contains the required keys.
      // Note 1: the values aren't checked for validity beyond being non-null.
      // Note 2: this code is stripped in release mode!
      assert(() {
        requiredKeys.forEach((key) {
          assert(json.containsKey(key),
              'Required key "CollectionCreationDto[$key]" is missing from JSON.');
          assert(json[key] != null,
              'Required key "CollectionCreationDto[$key]" has a null value in JSON.');
        });
        return true;
      }());

      return CollectionCreationDto(
        name: mapValueOfType<String>(json, r'name')!,
        ordered: mapValueOfType<bool>(json, r'ordered')!,
        seriesIds: json[r'seriesIds'] is List
            ? (json[r'seriesIds'] as List).cast<String>()
            : const [],
      );
    }
    return null;
  }

  static List<CollectionCreationDto>? listFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final result = <CollectionCreationDto>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = CollectionCreationDto.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }

  static Map<String, CollectionCreationDto> mapFromJson(dynamic json) {
    final map = <String, CollectionCreationDto>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = CollectionCreationDto.fromJson(entry.value);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  // maps a json object with a list of CollectionCreationDto-objects as value to a dart map
  static Map<String, List<CollectionCreationDto>> mapListFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final map = <String, List<CollectionCreationDto>>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = CollectionCreationDto.listFromJson(
          entry.value,
          growable: growable,
        );
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  /// The list of required keys that must be present in a JSON.
  static const requiredKeys = <String>{
    'name',
    'ordered',
    'seriesIds',
  };
}
