//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of komga_dtos;

class TachiyomiReadProgressV2Dto {
  /// Returns a new [TachiyomiReadProgressV2Dto] instance.
  TachiyomiReadProgressV2Dto({
    required this.booksCount,
    required this.booksReadCount,
    required this.booksUnreadCount,
    required this.booksInProgressCount,
    required this.lastReadContinuousNumberSort,
    required this.maxNumberSort,
  });

  int booksCount;

  int booksReadCount;

  int booksUnreadCount;

  int booksInProgressCount;

  double lastReadContinuousNumberSort;

  double maxNumberSort;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is TachiyomiReadProgressV2Dto &&
          other.booksCount == booksCount &&
          other.booksReadCount == booksReadCount &&
          other.booksUnreadCount == booksUnreadCount &&
          other.booksInProgressCount == booksInProgressCount &&
          other.lastReadContinuousNumberSort == lastReadContinuousNumberSort &&
          other.maxNumberSort == maxNumberSort;

  @override
  int get hashCode =>
      // ignore: unnecessary_parenthesis
      (booksCount.hashCode) +
      (booksReadCount.hashCode) +
      (booksUnreadCount.hashCode) +
      (booksInProgressCount.hashCode) +
      (lastReadContinuousNumberSort.hashCode) +
      (maxNumberSort.hashCode);

  @override
  String toString() =>
      'TachiyomiReadProgressV2Dto[booksCount=$booksCount, booksReadCount=$booksReadCount, booksUnreadCount=$booksUnreadCount, booksInProgressCount=$booksInProgressCount, lastReadContinuousNumberSort=$lastReadContinuousNumberSort, maxNumberSort=$maxNumberSort]';

  Map<String, dynamic> toJson() {
    final _json = <String, dynamic>{};
    _json[r'booksCount'] = booksCount;
    _json[r'booksReadCount'] = booksReadCount;
    _json[r'booksUnreadCount'] = booksUnreadCount;
    _json[r'booksInProgressCount'] = booksInProgressCount;
    _json[r'lastReadContinuousNumberSort'] = lastReadContinuousNumberSort;
    _json[r'maxNumberSort'] = maxNumberSort;
    return _json;
  }

  /// Returns a new [TachiyomiReadProgressV2Dto] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static TachiyomiReadProgressV2Dto? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();

      // Ensure that the map contains the required keys.
      // Note 1: the values aren't checked for validity beyond being non-null.
      // Note 2: this code is stripped in release mode!
      assert(() {
        requiredKeys.forEach((key) {
          assert(json.containsKey(key),
              'Required key "TachiyomiReadProgressV2Dto[$key]" is missing from JSON.');
          assert(json[key] != null,
              'Required key "TachiyomiReadProgressV2Dto[$key]" has a null value in JSON.');
        });
        return true;
      }());

      return TachiyomiReadProgressV2Dto(
        booksCount: mapValueOfType<int>(json, r'booksCount')!,
        booksReadCount: mapValueOfType<int>(json, r'booksReadCount')!,
        booksUnreadCount: mapValueOfType<int>(json, r'booksUnreadCount')!,
        booksInProgressCount:
            mapValueOfType<int>(json, r'booksInProgressCount')!,
        lastReadContinuousNumberSort:
            mapValueOfType<double>(json, r'lastReadContinuousNumberSort')!,
        maxNumberSort: mapValueOfType<double>(json, r'maxNumberSort')!,
      );
    }
    return null;
  }

  static List<TachiyomiReadProgressV2Dto>? listFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final result = <TachiyomiReadProgressV2Dto>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = TachiyomiReadProgressV2Dto.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }

  static Map<String, TachiyomiReadProgressV2Dto> mapFromJson(dynamic json) {
    final map = <String, TachiyomiReadProgressV2Dto>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = TachiyomiReadProgressV2Dto.fromJson(entry.value);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  // maps a json object with a list of TachiyomiReadProgressV2Dto-objects as value to a dart map
  static Map<String, List<TachiyomiReadProgressV2Dto>> mapListFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final map = <String, List<TachiyomiReadProgressV2Dto>>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = TachiyomiReadProgressV2Dto.listFromJson(
          entry.value,
          growable: growable,
        );
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  /// The list of required keys that must be present in a JSON.
  static const requiredKeys = <String>{
    'booksCount',
    'booksReadCount',
    'booksUnreadCount',
    'booksInProgressCount',
    'lastReadContinuousNumberSort',
    'maxNumberSort',
  };
}
