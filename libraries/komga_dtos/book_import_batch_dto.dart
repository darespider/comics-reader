//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of komga_dtos;

class BookImportBatchDto {
  /// Returns a new [BookImportBatchDto] instance.
  BookImportBatchDto({
    this.books = const [],
    required this.copyMode,
  });

  List<BookImportDto> books;

  BookImportBatchDtoCopyModeEnum copyMode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is BookImportBatchDto &&
          other.books == books &&
          other.copyMode == copyMode;

  @override
  int get hashCode =>
      // ignore: unnecessary_parenthesis
      (books.hashCode) + (copyMode.hashCode);

  @override
  String toString() => 'BookImportBatchDto[books=$books, copyMode=$copyMode]';

  Map<String, dynamic> toJson() {
    final _json = <String, dynamic>{};
    _json[r'books'] = books;
    _json[r'copyMode'] = copyMode;
    return _json;
  }

  /// Returns a new [BookImportBatchDto] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static BookImportBatchDto? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();

      // Ensure that the map contains the required keys.
      // Note 1: the values aren't checked for validity beyond being non-null.
      // Note 2: this code is stripped in release mode!
      assert(() {
        requiredKeys.forEach((key) {
          assert(json.containsKey(key),
              'Required key "BookImportBatchDto[$key]" is missing from JSON.');
          assert(json[key] != null,
              'Required key "BookImportBatchDto[$key]" has a null value in JSON.');
        });
        return true;
      }());

      return BookImportBatchDto(
        books: BookImportDto.listFromJson(json[r'books'])!,
        copyMode: BookImportBatchDtoCopyModeEnum.fromJson(json[r'copyMode'])!,
      );
    }
    return null;
  }

  static List<BookImportBatchDto>? listFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final result = <BookImportBatchDto>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = BookImportBatchDto.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }

  static Map<String, BookImportBatchDto> mapFromJson(dynamic json) {
    final map = <String, BookImportBatchDto>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = BookImportBatchDto.fromJson(entry.value);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  // maps a json object with a list of BookImportBatchDto-objects as value to a dart map
  static Map<String, List<BookImportBatchDto>> mapListFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final map = <String, List<BookImportBatchDto>>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = BookImportBatchDto.listFromJson(
          entry.value,
          growable: growable,
        );
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  /// The list of required keys that must be present in a JSON.
  static const requiredKeys = <String>{
    'books',
    'copyMode',
  };
}

class BookImportBatchDtoCopyModeEnum {
  /// Instantiate a new enum with the provided [value].
  const BookImportBatchDtoCopyModeEnum._(this.value);

  /// The underlying value of this enum member.
  final String value;

  @override
  String toString() => value;

  String toJson() => value;

  static const MOVE = BookImportBatchDtoCopyModeEnum._(r'MOVE');
  static const COPY = BookImportBatchDtoCopyModeEnum._(r'COPY');
  static const HARDLINK = BookImportBatchDtoCopyModeEnum._(r'HARDLINK');

  /// List of all possible values in this [enum][BookImportBatchDtoCopyModeEnum].
  static const values = <BookImportBatchDtoCopyModeEnum>[
    MOVE,
    COPY,
    HARDLINK,
  ];

  static BookImportBatchDtoCopyModeEnum? fromJson(dynamic value) =>
      BookImportBatchDtoCopyModeEnumTypeTransformer().decode(value);

  static List<BookImportBatchDtoCopyModeEnum>? listFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final result = <BookImportBatchDtoCopyModeEnum>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = BookImportBatchDtoCopyModeEnum.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }
}

/// Transformation class that can [encode] an instance of [BookImportBatchDtoCopyModeEnum] to String,
/// and [decode] dynamic data back to [BookImportBatchDtoCopyModeEnum].
class BookImportBatchDtoCopyModeEnumTypeTransformer {
  factory BookImportBatchDtoCopyModeEnumTypeTransformer() =>
      _instance ??= const BookImportBatchDtoCopyModeEnumTypeTransformer._();

  const BookImportBatchDtoCopyModeEnumTypeTransformer._();

  String encode(BookImportBatchDtoCopyModeEnum data) => data.value;

  /// Decodes a [dynamic value][data] to a BookImportBatchDtoCopyModeEnum.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  BookImportBatchDtoCopyModeEnum? decode(dynamic data,
      {bool allowNull = true}) {
    if (data != null) {
      switch (data.toString()) {
        case r'MOVE':
          return BookImportBatchDtoCopyModeEnum.MOVE;
        case r'COPY':
          return BookImportBatchDtoCopyModeEnum.COPY;
        case r'HARDLINK':
          return BookImportBatchDtoCopyModeEnum.HARDLINK;
        default:
          if (!allowNull) {
            throw ArgumentError('Unknown enum value to decode: $data');
          }
      }
    }
    return null;
  }

  /// Singleton [BookImportBatchDtoCopyModeEnumTypeTransformer] instance.
  static BookImportBatchDtoCopyModeEnumTypeTransformer? _instance;
}
