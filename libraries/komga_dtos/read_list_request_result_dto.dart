//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of komga_dtos;

class ReadListRequestResultDto {
  /// Returns a new [ReadListRequestResultDto] instance.
  ReadListRequestResultDto({
    this.readList,
    this.unmatchedBooks = const [],
    required this.errorCode,
    required this.requestName,
  });

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  ReadListDto? readList;

  List<ReadListRequestResultBookDto> unmatchedBooks;

  String errorCode;

  String requestName;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ReadListRequestResultDto &&
          other.readList == readList &&
          other.unmatchedBooks == unmatchedBooks &&
          other.errorCode == errorCode &&
          other.requestName == requestName;

  @override
  int get hashCode =>
      // ignore: unnecessary_parenthesis
      (readList == null ? 0 : readList!.hashCode) +
      (unmatchedBooks.hashCode) +
      (errorCode.hashCode) +
      (requestName.hashCode);

  @override
  String toString() =>
      'ReadListRequestResultDto[readList=$readList, unmatchedBooks=$unmatchedBooks, errorCode=$errorCode, requestName=$requestName]';

  Map<String, dynamic> toJson() {
    final _json = <String, dynamic>{};
    if (readList != null) {
      _json[r'readList'] = readList;
    }
    _json[r'unmatchedBooks'] = unmatchedBooks;
    _json[r'errorCode'] = errorCode;
    _json[r'requestName'] = requestName;
    return _json;
  }

  /// Returns a new [ReadListRequestResultDto] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static ReadListRequestResultDto? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();

      // Ensure that the map contains the required keys.
      // Note 1: the values aren't checked for validity beyond being non-null.
      // Note 2: this code is stripped in release mode!
      assert(() {
        requiredKeys.forEach((key) {
          assert(json.containsKey(key),
              'Required key "ReadListRequestResultDto[$key]" is missing from JSON.');
          assert(json[key] != null,
              'Required key "ReadListRequestResultDto[$key]" has a null value in JSON.');
        });
        return true;
      }());

      return ReadListRequestResultDto(
        readList: ReadListDto.fromJson(json[r'readList']),
        unmatchedBooks:
            ReadListRequestResultBookDto.listFromJson(json[r'unmatchedBooks'])!,
        errorCode: mapValueOfType<String>(json, r'errorCode')!,
        requestName: mapValueOfType<String>(json, r'requestName')!,
      );
    }
    return null;
  }

  static List<ReadListRequestResultDto>? listFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final result = <ReadListRequestResultDto>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = ReadListRequestResultDto.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }

  static Map<String, ReadListRequestResultDto> mapFromJson(dynamic json) {
    final map = <String, ReadListRequestResultDto>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = ReadListRequestResultDto.fromJson(entry.value);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  // maps a json object with a list of ReadListRequestResultDto-objects as value to a dart map
  static Map<String, List<ReadListRequestResultDto>> mapListFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final map = <String, List<ReadListRequestResultDto>>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = ReadListRequestResultDto.listFromJson(
          entry.value,
          growable: growable,
        );
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  /// The list of required keys that must be present in a JSON.
  static const requiredKeys = <String>{
    'unmatchedBooks',
    'errorCode',
    'requestName',
  };
}
