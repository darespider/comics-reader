//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of komga_dtos;

class SeriesThumbnailDto {
  /// Returns a new [SeriesThumbnailDto] instance.
  SeriesThumbnailDto({
    required this.id,
    required this.seriesId,
    required this.type,
    required this.selected,
  });

  String id;

  String seriesId;

  String type;

  bool selected;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is SeriesThumbnailDto &&
          other.id == id &&
          other.seriesId == seriesId &&
          other.type == type &&
          other.selected == selected;

  @override
  int get hashCode =>
      // ignore: unnecessary_parenthesis
      (id.hashCode) +
      (seriesId.hashCode) +
      (type.hashCode) +
      (selected.hashCode);

  @override
  String toString() =>
      'SeriesThumbnailDto[id=$id, seriesId=$seriesId, type=$type, selected=$selected]';

  Map<String, dynamic> toJson() {
    final _json = <String, dynamic>{};
    _json[r'id'] = id;
    _json[r'seriesId'] = seriesId;
    _json[r'type'] = type;
    _json[r'selected'] = selected;
    return _json;
  }

  /// Returns a new [SeriesThumbnailDto] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static SeriesThumbnailDto? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();

      // Ensure that the map contains the required keys.
      // Note 1: the values aren't checked for validity beyond being non-null.
      // Note 2: this code is stripped in release mode!
      assert(() {
        requiredKeys.forEach((key) {
          assert(json.containsKey(key),
              'Required key "SeriesThumbnailDto[$key]" is missing from JSON.');
          assert(json[key] != null,
              'Required key "SeriesThumbnailDto[$key]" has a null value in JSON.');
        });
        return true;
      }());

      return SeriesThumbnailDto(
        id: mapValueOfType<String>(json, r'id')!,
        seriesId: mapValueOfType<String>(json, r'seriesId')!,
        type: mapValueOfType<String>(json, r'type')!,
        selected: mapValueOfType<bool>(json, r'selected')!,
      );
    }
    return null;
  }

  static List<SeriesThumbnailDto>? listFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final result = <SeriesThumbnailDto>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = SeriesThumbnailDto.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }

  static Map<String, SeriesThumbnailDto> mapFromJson(dynamic json) {
    final map = <String, SeriesThumbnailDto>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = SeriesThumbnailDto.fromJson(entry.value);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  // maps a json object with a list of SeriesThumbnailDto-objects as value to a dart map
  static Map<String, List<SeriesThumbnailDto>> mapListFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final map = <String, List<SeriesThumbnailDto>>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = SeriesThumbnailDto.listFromJson(
          entry.value,
          growable: growable,
        );
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  /// The list of required keys that must be present in a JSON.
  static const requiredKeys = <String>{
    'id',
    'seriesId',
    'type',
    'selected',
  };
}
