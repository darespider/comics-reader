//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of komga_dtos;

class PageHashMatchDto {
  /// Returns a new [PageHashMatchDto] instance.
  PageHashMatchDto({
    required this.bookId,
    required this.url,
    required this.pageNumber,
    required this.fileName,
  });

  String bookId;

  String url;

  int pageNumber;

  String fileName;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is PageHashMatchDto &&
          other.bookId == bookId &&
          other.url == url &&
          other.pageNumber == pageNumber &&
          other.fileName == fileName;

  @override
  int get hashCode =>
      // ignore: unnecessary_parenthesis
      (bookId.hashCode) +
      (url.hashCode) +
      (pageNumber.hashCode) +
      (fileName.hashCode);

  @override
  String toString() =>
      'PageHashMatchDto[bookId=$bookId, url=$url, pageNumber=$pageNumber, fileName=$fileName]';

  Map<String, dynamic> toJson() {
    final _json = <String, dynamic>{};
    _json[r'bookId'] = bookId;
    _json[r'url'] = url;
    _json[r'pageNumber'] = pageNumber;
    _json[r'fileName'] = fileName;
    return _json;
  }

  /// Returns a new [PageHashMatchDto] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static PageHashMatchDto? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();

      // Ensure that the map contains the required keys.
      // Note 1: the values aren't checked for validity beyond being non-null.
      // Note 2: this code is stripped in release mode!
      assert(() {
        requiredKeys.forEach((key) {
          assert(json.containsKey(key),
              'Required key "PageHashMatchDto[$key]" is missing from JSON.');
          assert(json[key] != null,
              'Required key "PageHashMatchDto[$key]" has a null value in JSON.');
        });
        return true;
      }());

      return PageHashMatchDto(
        bookId: mapValueOfType<String>(json, r'bookId')!,
        url: mapValueOfType<String>(json, r'url')!,
        pageNumber: mapValueOfType<int>(json, r'pageNumber')!,
        fileName: mapValueOfType<String>(json, r'fileName')!,
      );
    }
    return null;
  }

  static List<PageHashMatchDto>? listFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final result = <PageHashMatchDto>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = PageHashMatchDto.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }

  static Map<String, PageHashMatchDto> mapFromJson(dynamic json) {
    final map = <String, PageHashMatchDto>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = PageHashMatchDto.fromJson(entry.value);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  // maps a json object with a list of PageHashMatchDto-objects as value to a dart map
  static Map<String, List<PageHashMatchDto>> mapListFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final map = <String, List<PageHashMatchDto>>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = PageHashMatchDto.listFromJson(
          entry.value,
          growable: growable,
        );
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  /// The list of required keys that must be present in a JSON.
  static const requiredKeys = <String>{
    'bookId',
    'url',
    'pageNumber',
    'fileName',
  };
}
