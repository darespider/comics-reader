//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of komga_dtos;

class BookImportDto {
  /// Returns a new [BookImportDto] instance.
  BookImportDto({
    required this.sourceFile,
    required this.seriesId,
    this.upgradeBookId,
    this.destinationName,
  });

  String sourceFile;

  String seriesId;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  String? upgradeBookId;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  String? destinationName;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is BookImportDto &&
          other.sourceFile == sourceFile &&
          other.seriesId == seriesId &&
          other.upgradeBookId == upgradeBookId &&
          other.destinationName == destinationName;

  @override
  int get hashCode =>
      // ignore: unnecessary_parenthesis
      (sourceFile.hashCode) +
      (seriesId.hashCode) +
      (upgradeBookId == null ? 0 : upgradeBookId!.hashCode) +
      (destinationName == null ? 0 : destinationName!.hashCode);

  @override
  String toString() =>
      'BookImportDto[sourceFile=$sourceFile, seriesId=$seriesId, upgradeBookId=$upgradeBookId, destinationName=$destinationName]';

  Map<String, dynamic> toJson() {
    final _json = <String, dynamic>{};
    _json[r'sourceFile'] = sourceFile;
    _json[r'seriesId'] = seriesId;
    if (upgradeBookId != null) {
      _json[r'upgradeBookId'] = upgradeBookId;
    }
    if (destinationName != null) {
      _json[r'destinationName'] = destinationName;
    }
    return _json;
  }

  /// Returns a new [BookImportDto] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static BookImportDto? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();

      // Ensure that the map contains the required keys.
      // Note 1: the values aren't checked for validity beyond being non-null.
      // Note 2: this code is stripped in release mode!
      assert(() {
        requiredKeys.forEach((key) {
          assert(json.containsKey(key),
              'Required key "BookImportDto[$key]" is missing from JSON.');
          assert(json[key] != null,
              'Required key "BookImportDto[$key]" has a null value in JSON.');
        });
        return true;
      }());

      return BookImportDto(
        sourceFile: mapValueOfType<String>(json, r'sourceFile')!,
        seriesId: mapValueOfType<String>(json, r'seriesId')!,
        upgradeBookId: mapValueOfType<String>(json, r'upgradeBookId'),
        destinationName: mapValueOfType<String>(json, r'destinationName'),
      );
    }
    return null;
  }

  static List<BookImportDto>? listFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final result = <BookImportDto>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = BookImportDto.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }

  static Map<String, BookImportDto> mapFromJson(dynamic json) {
    final map = <String, BookImportDto>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = BookImportDto.fromJson(entry.value);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  // maps a json object with a list of BookImportDto-objects as value to a dart map
  static Map<String, List<BookImportDto>> mapListFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final map = <String, List<BookImportDto>>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = BookImportDto.listFromJson(
          entry.value,
          growable: growable,
        );
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  /// The list of required keys that must be present in a JSON.
  static const requiredKeys = <String>{
    'sourceFile',
    'seriesId',
  };
}
