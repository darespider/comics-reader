//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of komga_dtos;

class SeriesMetadataDto {
  /// Returns a new [SeriesMetadataDto] instance.
  SeriesMetadataDto({
    required this.status,
    required this.statusLock,
    required this.title,
    required this.titleLock,
    required this.titleSort,
    required this.titleSortLock,
    required this.summary,
    required this.summaryLock,
    required this.readingDirection,
    required this.readingDirectionLock,
    required this.publisher,
    required this.publisherLock,
    this.ageRating,
    required this.ageRatingLock,
    required this.language,
    required this.languageLock,
    this.genres = const {},
    required this.genresLock,
    this.tags = const {},
    required this.tagsLock,
    this.totalBookCount,
    required this.totalBookCountLock,
    this.sharingLabels = const {},
    required this.sharingLabelsLock,
    required this.created,
    required this.lastModified,
  });

  String status;

  bool statusLock;

  String title;

  bool titleLock;

  String titleSort;

  bool titleSortLock;

  String summary;

  bool summaryLock;

  String readingDirection;

  bool readingDirectionLock;

  String publisher;

  bool publisherLock;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  int? ageRating;

  bool ageRatingLock;

  String language;

  bool languageLock;

  Set<String> genres;

  bool genresLock;

  Set<String> tags;

  bool tagsLock;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  int? totalBookCount;

  bool totalBookCountLock;

  Set<String> sharingLabels;

  bool sharingLabelsLock;

  DateTime created;

  DateTime lastModified;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is SeriesMetadataDto &&
          other.status == status &&
          other.statusLock == statusLock &&
          other.title == title &&
          other.titleLock == titleLock &&
          other.titleSort == titleSort &&
          other.titleSortLock == titleSortLock &&
          other.summary == summary &&
          other.summaryLock == summaryLock &&
          other.readingDirection == readingDirection &&
          other.readingDirectionLock == readingDirectionLock &&
          other.publisher == publisher &&
          other.publisherLock == publisherLock &&
          other.ageRating == ageRating &&
          other.ageRatingLock == ageRatingLock &&
          other.language == language &&
          other.languageLock == languageLock &&
          other.genres == genres &&
          other.genresLock == genresLock &&
          other.tags == tags &&
          other.tagsLock == tagsLock &&
          other.totalBookCount == totalBookCount &&
          other.totalBookCountLock == totalBookCountLock &&
          other.sharingLabels == sharingLabels &&
          other.sharingLabelsLock == sharingLabelsLock &&
          other.created == created &&
          other.lastModified == lastModified;

  @override
  int get hashCode =>
      // ignore: unnecessary_parenthesis
      (status.hashCode) +
      (statusLock.hashCode) +
      (title.hashCode) +
      (titleLock.hashCode) +
      (titleSort.hashCode) +
      (titleSortLock.hashCode) +
      (summary.hashCode) +
      (summaryLock.hashCode) +
      (readingDirection.hashCode) +
      (readingDirectionLock.hashCode) +
      (publisher.hashCode) +
      (publisherLock.hashCode) +
      (ageRating == null ? 0 : ageRating!.hashCode) +
      (ageRatingLock.hashCode) +
      (language.hashCode) +
      (languageLock.hashCode) +
      (genres.hashCode) +
      (genresLock.hashCode) +
      (tags.hashCode) +
      (tagsLock.hashCode) +
      (totalBookCount == null ? 0 : totalBookCount!.hashCode) +
      (totalBookCountLock.hashCode) +
      (sharingLabels.hashCode) +
      (sharingLabelsLock.hashCode) +
      (created.hashCode) +
      (lastModified.hashCode);

  @override
  String toString() =>
      'SeriesMetadataDto[status=$status, statusLock=$statusLock, title=$title, titleLock=$titleLock, titleSort=$titleSort, titleSortLock=$titleSortLock, summary=$summary, summaryLock=$summaryLock, readingDirection=$readingDirection, readingDirectionLock=$readingDirectionLock, publisher=$publisher, publisherLock=$publisherLock, ageRating=$ageRating, ageRatingLock=$ageRatingLock, language=$language, languageLock=$languageLock, genres=$genres, genresLock=$genresLock, tags=$tags, tagsLock=$tagsLock, totalBookCount=$totalBookCount, totalBookCountLock=$totalBookCountLock, sharingLabels=$sharingLabels, sharingLabelsLock=$sharingLabelsLock, created=$created, lastModified=$lastModified]';

  Map<String, dynamic> toJson() {
    final _json = <String, dynamic>{};
    _json[r'status'] = status;
    _json[r'statusLock'] = statusLock;
    _json[r'title'] = title;
    _json[r'titleLock'] = titleLock;
    _json[r'titleSort'] = titleSort;
    _json[r'titleSortLock'] = titleSortLock;
    _json[r'summary'] = summary;
    _json[r'summaryLock'] = summaryLock;
    _json[r'readingDirection'] = readingDirection;
    _json[r'readingDirectionLock'] = readingDirectionLock;
    _json[r'publisher'] = publisher;
    _json[r'publisherLock'] = publisherLock;
    if (ageRating != null) {
      _json[r'ageRating'] = ageRating;
    }
    _json[r'ageRatingLock'] = ageRatingLock;
    _json[r'language'] = language;
    _json[r'languageLock'] = languageLock;
    _json[r'genres'] = genres;
    _json[r'genresLock'] = genresLock;
    _json[r'tags'] = tags;
    _json[r'tagsLock'] = tagsLock;
    if (totalBookCount != null) {
      _json[r'totalBookCount'] = totalBookCount;
    }
    _json[r'totalBookCountLock'] = totalBookCountLock;
    _json[r'sharingLabels'] = sharingLabels;
    _json[r'sharingLabelsLock'] = sharingLabelsLock;
    _json[r'created'] = created.toUtc().toIso8601String();
    _json[r'lastModified'] = lastModified.toUtc().toIso8601String();
    return _json;
  }

  /// Returns a new [SeriesMetadataDto] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static SeriesMetadataDto? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();

      // Ensure that the map contains the required keys.
      // Note 1: the values aren't checked for validity beyond being non-null.
      // Note 2: this code is stripped in release mode!
      assert(() {
        requiredKeys.forEach((key) {
          assert(json.containsKey(key),
              'Required key "SeriesMetadataDto[$key]" is missing from JSON.');
          assert(json[key] != null,
              'Required key "SeriesMetadataDto[$key]" has a null value in JSON.');
        });
        return true;
      }());

      return SeriesMetadataDto(
        status: mapValueOfType<String>(json, r'status')!,
        statusLock: mapValueOfType<bool>(json, r'statusLock')!,
        title: mapValueOfType<String>(json, r'title')!,
        titleLock: mapValueOfType<bool>(json, r'titleLock')!,
        titleSort: mapValueOfType<String>(json, r'titleSort')!,
        titleSortLock: mapValueOfType<bool>(json, r'titleSortLock')!,
        summary: mapValueOfType<String>(json, r'summary')!,
        summaryLock: mapValueOfType<bool>(json, r'summaryLock')!,
        readingDirection: mapValueOfType<String>(json, r'readingDirection')!,
        readingDirectionLock:
            mapValueOfType<bool>(json, r'readingDirectionLock')!,
        publisher: mapValueOfType<String>(json, r'publisher')!,
        publisherLock: mapValueOfType<bool>(json, r'publisherLock')!,
        ageRating: mapValueOfType<int>(json, r'ageRating'),
        ageRatingLock: mapValueOfType<bool>(json, r'ageRatingLock')!,
        language: mapValueOfType<String>(json, r'language')!,
        languageLock: mapValueOfType<bool>(json, r'languageLock')!,
        genres: json[r'genres'] is Set
            ? (json[r'genres'] as Set).cast<String>()
            : const {},
        genresLock: mapValueOfType<bool>(json, r'genresLock')!,
        tags: json[r'tags'] is Set
            ? (json[r'tags'] as Set).cast<String>()
            : const {},
        tagsLock: mapValueOfType<bool>(json, r'tagsLock')!,
        totalBookCount: mapValueOfType<int>(json, r'totalBookCount'),
        totalBookCountLock: mapValueOfType<bool>(json, r'totalBookCountLock')!,
        sharingLabels: json[r'sharingLabels'] is Set
            ? (json[r'sharingLabels'] as Set).cast<String>()
            : const {},
        sharingLabelsLock: mapValueOfType<bool>(json, r'sharingLabelsLock')!,
        created: mapDateTime(json, r'created', '')!,
        lastModified: mapDateTime(json, r'lastModified', '')!,
      );
    }
    return null;
  }

  static List<SeriesMetadataDto>? listFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final result = <SeriesMetadataDto>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = SeriesMetadataDto.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }

  static Map<String, SeriesMetadataDto> mapFromJson(dynamic json) {
    final map = <String, SeriesMetadataDto>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = SeriesMetadataDto.fromJson(entry.value);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  // maps a json object with a list of SeriesMetadataDto-objects as value to a dart map
  static Map<String, List<SeriesMetadataDto>> mapListFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final map = <String, List<SeriesMetadataDto>>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = SeriesMetadataDto.listFromJson(
          entry.value,
          growable: growable,
        );
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  /// The list of required keys that must be present in a JSON.
  static const requiredKeys = <String>{
    'status',
    'statusLock',
    'title',
    'titleLock',
    'titleSort',
    'titleSortLock',
    'summary',
    'summaryLock',
    'readingDirection',
    'readingDirectionLock',
    'publisher',
    'publisherLock',
    'ageRatingLock',
    'language',
    'languageLock',
    'genres',
    'genresLock',
    'tags',
    'tagsLock',
    'totalBookCountLock',
    'sharingLabels',
    'sharingLabelsLock',
    'created',
    'lastModified',
  };
}
