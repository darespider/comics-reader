//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of komga_dtos;

class SharedLibrariesUpdateDto {
  /// Returns a new [SharedLibrariesUpdateDto] instance.
  SharedLibrariesUpdateDto({
    required this.all,
    this.libraryIds = const {},
  });

  bool all;

  Set<String> libraryIds;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is SharedLibrariesUpdateDto &&
          other.all == all &&
          other.libraryIds == libraryIds;

  @override
  int get hashCode =>
      // ignore: unnecessary_parenthesis
      (all.hashCode) + (libraryIds.hashCode);

  @override
  String toString() =>
      'SharedLibrariesUpdateDto[all=$all, libraryIds=$libraryIds]';

  Map<String, dynamic> toJson() {
    final _json = <String, dynamic>{};
    _json[r'all'] = all;
    _json[r'libraryIds'] = libraryIds;
    return _json;
  }

  /// Returns a new [SharedLibrariesUpdateDto] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static SharedLibrariesUpdateDto? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();

      // Ensure that the map contains the required keys.
      // Note 1: the values aren't checked for validity beyond being non-null.
      // Note 2: this code is stripped in release mode!
      assert(() {
        requiredKeys.forEach((key) {
          assert(json.containsKey(key),
              'Required key "SharedLibrariesUpdateDto[$key]" is missing from JSON.');
          assert(json[key] != null,
              'Required key "SharedLibrariesUpdateDto[$key]" has a null value in JSON.');
        });
        return true;
      }());

      return SharedLibrariesUpdateDto(
        all: mapValueOfType<bool>(json, r'all')!,
        libraryIds: json[r'libraryIds'] is Set
            ? (json[r'libraryIds'] as Set).cast<String>()
            : const {},
      );
    }
    return null;
  }

  static List<SharedLibrariesUpdateDto>? listFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final result = <SharedLibrariesUpdateDto>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = SharedLibrariesUpdateDto.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }

  static Map<String, SharedLibrariesUpdateDto> mapFromJson(dynamic json) {
    final map = <String, SharedLibrariesUpdateDto>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = SharedLibrariesUpdateDto.fromJson(entry.value);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  // maps a json object with a list of SharedLibrariesUpdateDto-objects as value to a dart map
  static Map<String, List<SharedLibrariesUpdateDto>> mapListFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final map = <String, List<SharedLibrariesUpdateDto>>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = SharedLibrariesUpdateDto.listFromJson(
          entry.value,
          growable: growable,
        );
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  /// The list of required keys that must be present in a JSON.
  static const requiredKeys = <String>{
    'all',
    'libraryIds',
  };
}
