//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of komga_dtos;

class ValidationErrorResponse {
  /// Returns a new [ValidationErrorResponse] instance.
  ValidationErrorResponse({
    this.violations = const [],
  });

  List<Violation> violations;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ValidationErrorResponse && other.violations == violations;

  @override
  int get hashCode =>
      // ignore: unnecessary_parenthesis
      (violations.hashCode);

  @override
  String toString() => 'ValidationErrorResponse[violations=$violations]';

  Map<String, dynamic> toJson() {
    final _json = <String, dynamic>{};
    _json[r'violations'] = violations;
    return _json;
  }

  /// Returns a new [ValidationErrorResponse] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static ValidationErrorResponse? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();

      // Ensure that the map contains the required keys.
      // Note 1: the values aren't checked for validity beyond being non-null.
      // Note 2: this code is stripped in release mode!
      assert(() {
        requiredKeys.forEach((key) {
          assert(json.containsKey(key),
              'Required key "ValidationErrorResponse[$key]" is missing from JSON.');
          assert(json[key] != null,
              'Required key "ValidationErrorResponse[$key]" has a null value in JSON.');
        });
        return true;
      }());

      return ValidationErrorResponse(
        violations: Violation.listFromJson(json[r'violations'])!,
      );
    }
    return null;
  }

  static List<ValidationErrorResponse>? listFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final result = <ValidationErrorResponse>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = ValidationErrorResponse.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }

  static Map<String, ValidationErrorResponse> mapFromJson(dynamic json) {
    final map = <String, ValidationErrorResponse>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = ValidationErrorResponse.fromJson(entry.value);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  // maps a json object with a list of ValidationErrorResponse-objects as value to a dart map
  static Map<String, List<ValidationErrorResponse>> mapListFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final map = <String, List<ValidationErrorResponse>>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = ValidationErrorResponse.listFromJson(
          entry.value,
          growable: growable,
        );
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  /// The list of required keys that must be present in a JSON.
  static const requiredKeys = <String>{
    'violations',
  };
}
