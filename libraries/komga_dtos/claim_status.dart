//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of komga_dtos;

class ClaimStatus {
  /// Returns a new [ClaimStatus] instance.
  ClaimStatus({
    required this.isClaimed,
  });

  bool isClaimed;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ClaimStatus && other.isClaimed == isClaimed;

  @override
  int get hashCode =>
      // ignore: unnecessary_parenthesis
      (isClaimed.hashCode);

  @override
  String toString() => 'ClaimStatus[isClaimed=$isClaimed]';

  Map<String, dynamic> toJson() {
    final _json = <String, dynamic>{};
    _json[r'isClaimed'] = isClaimed;
    return _json;
  }

  /// Returns a new [ClaimStatus] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static ClaimStatus? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();

      // Ensure that the map contains the required keys.
      // Note 1: the values aren't checked for validity beyond being non-null.
      // Note 2: this code is stripped in release mode!
      assert(() {
        requiredKeys.forEach((key) {
          assert(json.containsKey(key),
              'Required key "ClaimStatus[$key]" is missing from JSON.');
          assert(json[key] != null,
              'Required key "ClaimStatus[$key]" has a null value in JSON.');
        });
        return true;
      }());

      return ClaimStatus(
        isClaimed: mapValueOfType<bool>(json, r'isClaimed')!,
      );
    }
    return null;
  }

  static List<ClaimStatus>? listFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final result = <ClaimStatus>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = ClaimStatus.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }

  static Map<String, ClaimStatus> mapFromJson(dynamic json) {
    final map = <String, ClaimStatus>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = ClaimStatus.fromJson(entry.value);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  // maps a json object with a list of ClaimStatus-objects as value to a dart map
  static Map<String, List<ClaimStatus>> mapListFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final map = <String, List<ClaimStatus>>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = ClaimStatus.listFromJson(
          entry.value,
          growable: growable,
        );
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  /// The list of required keys that must be present in a JSON.
  static const requiredKeys = <String>{
    'isClaimed',
  };
}
