//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of komga_dtos;

class ReadListRequestBookDto {
  /// Returns a new [ReadListRequestBookDto] instance.
  ReadListRequestBookDto({
    required this.series,
    required this.number,
  });

  String series;

  String number;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ReadListRequestBookDto &&
          other.series == series &&
          other.number == number;

  @override
  int get hashCode =>
      // ignore: unnecessary_parenthesis
      (series.hashCode) + (number.hashCode);

  @override
  String toString() => 'ReadListRequestBookDto[series=$series, number=$number]';

  Map<String, dynamic> toJson() {
    final _json = <String, dynamic>{};
    _json[r'series'] = series;
    _json[r'number'] = number;
    return _json;
  }

  /// Returns a new [ReadListRequestBookDto] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static ReadListRequestBookDto? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();

      // Ensure that the map contains the required keys.
      // Note 1: the values aren't checked for validity beyond being non-null.
      // Note 2: this code is stripped in release mode!
      assert(() {
        requiredKeys.forEach((key) {
          assert(json.containsKey(key),
              'Required key "ReadListRequestBookDto[$key]" is missing from JSON.');
          assert(json[key] != null,
              'Required key "ReadListRequestBookDto[$key]" has a null value in JSON.');
        });
        return true;
      }());

      return ReadListRequestBookDto(
        series: mapValueOfType<String>(json, r'series')!,
        number: mapValueOfType<String>(json, r'number')!,
      );
    }
    return null;
  }

  static List<ReadListRequestBookDto>? listFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final result = <ReadListRequestBookDto>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = ReadListRequestBookDto.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }

  static Map<String, ReadListRequestBookDto> mapFromJson(dynamic json) {
    final map = <String, ReadListRequestBookDto>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = ReadListRequestBookDto.fromJson(entry.value);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  // maps a json object with a list of ReadListRequestBookDto-objects as value to a dart map
  static Map<String, List<ReadListRequestBookDto>> mapListFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final map = <String, List<ReadListRequestBookDto>>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = ReadListRequestBookDto.listFromJson(
          entry.value,
          growable: growable,
        );
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  /// The list of required keys that must be present in a JSON.
  static const requiredKeys = <String>{
    'series',
    'number',
  };
}
