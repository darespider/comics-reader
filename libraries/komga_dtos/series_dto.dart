//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of komga_dtos;

class SeriesDto {
  /// Returns a new [SeriesDto] instance.
  SeriesDto({
    required this.id,
    required this.libraryId,
    required this.name,
    required this.url,
    required this.created,
    required this.lastModified,
    required this.fileLastModified,
    required this.booksCount,
    required this.booksReadCount,
    required this.booksUnreadCount,
    required this.booksInProgressCount,
    required this.metadata,
    required this.booksMetadata,
    required this.deleted,
  });

  String id;

  String libraryId;

  String name;

  String url;

  DateTime created;

  DateTime lastModified;

  DateTime fileLastModified;

  int booksCount;

  int booksReadCount;

  int booksUnreadCount;

  int booksInProgressCount;

  SeriesMetadataDto metadata;

  BookMetadataAggregationDto booksMetadata;

  bool deleted;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is SeriesDto &&
          other.id == id &&
          other.libraryId == libraryId &&
          other.name == name &&
          other.url == url &&
          other.created == created &&
          other.lastModified == lastModified &&
          other.fileLastModified == fileLastModified &&
          other.booksCount == booksCount &&
          other.booksReadCount == booksReadCount &&
          other.booksUnreadCount == booksUnreadCount &&
          other.booksInProgressCount == booksInProgressCount &&
          other.metadata == metadata &&
          other.booksMetadata == booksMetadata &&
          other.deleted == deleted;

  @override
  int get hashCode =>
      // ignore: unnecessary_parenthesis
      (id.hashCode) +
      (libraryId.hashCode) +
      (name.hashCode) +
      (url.hashCode) +
      (created.hashCode) +
      (lastModified.hashCode) +
      (fileLastModified.hashCode) +
      (booksCount.hashCode) +
      (booksReadCount.hashCode) +
      (booksUnreadCount.hashCode) +
      (booksInProgressCount.hashCode) +
      (metadata.hashCode) +
      (booksMetadata.hashCode) +
      (deleted.hashCode);

  @override
  String toString() =>
      'SeriesDto[id=$id, libraryId=$libraryId, name=$name, url=$url, created=$created, lastModified=$lastModified, fileLastModified=$fileLastModified, booksCount=$booksCount, booksReadCount=$booksReadCount, booksUnreadCount=$booksUnreadCount, booksInProgressCount=$booksInProgressCount, metadata=$metadata, booksMetadata=$booksMetadata, deleted=$deleted]';

  Map<String, dynamic> toJson() {
    final _json = <String, dynamic>{};
    _json[r'id'] = id;
    _json[r'libraryId'] = libraryId;
    _json[r'name'] = name;
    _json[r'url'] = url;
    _json[r'created'] = created.toUtc().toIso8601String();
    _json[r'lastModified'] = lastModified.toUtc().toIso8601String();
    _json[r'fileLastModified'] = fileLastModified.toUtc().toIso8601String();
    _json[r'booksCount'] = booksCount;
    _json[r'booksReadCount'] = booksReadCount;
    _json[r'booksUnreadCount'] = booksUnreadCount;
    _json[r'booksInProgressCount'] = booksInProgressCount;
    _json[r'metadata'] = metadata;
    _json[r'booksMetadata'] = booksMetadata;
    _json[r'deleted'] = deleted;
    return _json;
  }

  /// Returns a new [SeriesDto] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static SeriesDto? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();

      // Ensure that the map contains the required keys.
      // Note 1: the values aren't checked for validity beyond being non-null.
      // Note 2: this code is stripped in release mode!
      assert(() {
        requiredKeys.forEach((key) {
          assert(json.containsKey(key),
              'Required key "SeriesDto[$key]" is missing from JSON.');
          assert(json[key] != null,
              'Required key "SeriesDto[$key]" has a null value in JSON.');
        });
        return true;
      }());

      return SeriesDto(
        id: mapValueOfType<String>(json, r'id')!,
        libraryId: mapValueOfType<String>(json, r'libraryId')!,
        name: mapValueOfType<String>(json, r'name')!,
        url: mapValueOfType<String>(json, r'url')!,
        created: mapDateTime(json, r'created', '')!,
        lastModified: mapDateTime(json, r'lastModified', '')!,
        fileLastModified: mapDateTime(json, r'fileLastModified', '')!,
        booksCount: mapValueOfType<int>(json, r'booksCount')!,
        booksReadCount: mapValueOfType<int>(json, r'booksReadCount')!,
        booksUnreadCount: mapValueOfType<int>(json, r'booksUnreadCount')!,
        booksInProgressCount:
            mapValueOfType<int>(json, r'booksInProgressCount')!,
        metadata: SeriesMetadataDto.fromJson(json[r'metadata'])!,
        booksMetadata:
            BookMetadataAggregationDto.fromJson(json[r'booksMetadata'])!,
        deleted: mapValueOfType<bool>(json, r'deleted')!,
      );
    }
    return null;
  }

  static List<SeriesDto>? listFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final result = <SeriesDto>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = SeriesDto.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }

  static Map<String, SeriesDto> mapFromJson(dynamic json) {
    final map = <String, SeriesDto>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = SeriesDto.fromJson(entry.value);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  // maps a json object with a list of SeriesDto-objects as value to a dart map
  static Map<String, List<SeriesDto>> mapListFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final map = <String, List<SeriesDto>>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = SeriesDto.listFromJson(
          entry.value,
          growable: growable,
        );
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  /// The list of required keys that must be present in a JSON.
  static const requiredKeys = <String>{
    'id',
    'libraryId',
    'name',
    'url',
    'created',
    'lastModified',
    'fileLastModified',
    'booksCount',
    'booksReadCount',
    'booksUnreadCount',
    'booksInProgressCount',
    'metadata',
    'booksMetadata',
    'deleted',
  };
}
