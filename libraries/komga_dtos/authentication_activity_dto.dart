//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of komga_dtos;

class AuthenticationActivityDto {
  /// Returns a new [AuthenticationActivityDto] instance.
  AuthenticationActivityDto({
    this.userId,
    this.email,
    this.ip,
    this.userAgent,
    required this.success,
    this.error,
    required this.dateTime,
    this.source_,
  });

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  String? userId;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  String? email;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  String? ip;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  String? userAgent;

  bool success;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  String? error;

  DateTime dateTime;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  String? source_;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AuthenticationActivityDto &&
          other.userId == userId &&
          other.email == email &&
          other.ip == ip &&
          other.userAgent == userAgent &&
          other.success == success &&
          other.error == error &&
          other.dateTime == dateTime &&
          other.source_ == source_;

  @override
  int get hashCode =>
      // ignore: unnecessary_parenthesis
      (userId == null ? 0 : userId!.hashCode) +
      (email == null ? 0 : email!.hashCode) +
      (ip == null ? 0 : ip!.hashCode) +
      (userAgent == null ? 0 : userAgent!.hashCode) +
      (success.hashCode) +
      (error == null ? 0 : error!.hashCode) +
      (dateTime.hashCode) +
      (source_ == null ? 0 : source_!.hashCode);

  @override
  String toString() =>
      'AuthenticationActivityDto[userId=$userId, email=$email, ip=$ip, userAgent=$userAgent, success=$success, error=$error, dateTime=$dateTime, source_=$source_]';

  Map<String, dynamic> toJson() {
    final _json = <String, dynamic>{};
    if (userId != null) {
      _json[r'userId'] = userId;
    }
    if (email != null) {
      _json[r'email'] = email;
    }
    if (ip != null) {
      _json[r'ip'] = ip;
    }
    if (userAgent != null) {
      _json[r'userAgent'] = userAgent;
    }
    _json[r'success'] = success;
    if (error != null) {
      _json[r'error'] = error;
    }
    _json[r'dateTime'] = dateTime.toUtc().toIso8601String();
    if (source_ != null) {
      _json[r'source'] = source_;
    }
    return _json;
  }

  /// Returns a new [AuthenticationActivityDto] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static AuthenticationActivityDto? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();

      // Ensure that the map contains the required keys.
      // Note 1: the values aren't checked for validity beyond being non-null.
      // Note 2: this code is stripped in release mode!
      assert(() {
        requiredKeys.forEach((key) {
          assert(json.containsKey(key),
              'Required key "AuthenticationActivityDto[$key]" is missing from JSON.');
          assert(json[key] != null,
              'Required key "AuthenticationActivityDto[$key]" has a null value in JSON.');
        });
        return true;
      }());

      return AuthenticationActivityDto(
        userId: mapValueOfType<String>(json, r'userId'),
        email: mapValueOfType<String>(json, r'email'),
        ip: mapValueOfType<String>(json, r'ip'),
        userAgent: mapValueOfType<String>(json, r'userAgent'),
        success: mapValueOfType<bool>(json, r'success')!,
        error: mapValueOfType<String>(json, r'error'),
        dateTime: mapDateTime(json, r'dateTime', '')!,
        source_: mapValueOfType<String>(json, r'source'),
      );
    }
    return null;
  }

  static List<AuthenticationActivityDto>? listFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final result = <AuthenticationActivityDto>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = AuthenticationActivityDto.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }

  static Map<String, AuthenticationActivityDto> mapFromJson(dynamic json) {
    final map = <String, AuthenticationActivityDto>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = AuthenticationActivityDto.fromJson(entry.value);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  // maps a json object with a list of AuthenticationActivityDto-objects as value to a dart map
  static Map<String, List<AuthenticationActivityDto>> mapListFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final map = <String, List<AuthenticationActivityDto>>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = AuthenticationActivityDto.listFromJson(
          entry.value,
          growable: growable,
        );
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  /// The list of required keys that must be present in a JSON.
  static const requiredKeys = <String>{
    'success',
    'dateTime',
  };
}
