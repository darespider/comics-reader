//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of komga_dtos;

class AgeRestrictionDto {
  /// Returns a new [AgeRestrictionDto] instance.
  AgeRestrictionDto({
    required this.age,
    required this.restriction,
  });

  int age;

  AgeRestrictionDtoRestrictionEnum restriction;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AgeRestrictionDto &&
          other.age == age &&
          other.restriction == restriction;

  @override
  int get hashCode =>
      // ignore: unnecessary_parenthesis
      (age.hashCode) + (restriction.hashCode);

  @override
  String toString() => 'AgeRestrictionDto[age=$age, restriction=$restriction]';

  Map<String, dynamic> toJson() {
    final _json = <String, dynamic>{};
    _json[r'age'] = age;
    _json[r'restriction'] = restriction;
    return _json;
  }

  /// Returns a new [AgeRestrictionDto] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static AgeRestrictionDto? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();

      // Ensure that the map contains the required keys.
      // Note 1: the values aren't checked for validity beyond being non-null.
      // Note 2: this code is stripped in release mode!
      assert(() {
        requiredKeys.forEach((key) {
          assert(json.containsKey(key),
              'Required key "AgeRestrictionDto[$key]" is missing from JSON.');
          assert(json[key] != null,
              'Required key "AgeRestrictionDto[$key]" has a null value in JSON.');
        });
        return true;
      }());

      return AgeRestrictionDto(
        age: mapValueOfType<int>(json, r'age')!,
        restriction:
            AgeRestrictionDtoRestrictionEnum.fromJson(json[r'restriction'])!,
      );
    }
    return null;
  }

  static List<AgeRestrictionDto>? listFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final result = <AgeRestrictionDto>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = AgeRestrictionDto.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }

  static Map<String, AgeRestrictionDto> mapFromJson(dynamic json) {
    final map = <String, AgeRestrictionDto>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = AgeRestrictionDto.fromJson(entry.value);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  // maps a json object with a list of AgeRestrictionDto-objects as value to a dart map
  static Map<String, List<AgeRestrictionDto>> mapListFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final map = <String, List<AgeRestrictionDto>>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = AgeRestrictionDto.listFromJson(
          entry.value,
          growable: growable,
        );
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  /// The list of required keys that must be present in a JSON.
  static const requiredKeys = <String>{
    'age',
    'restriction',
  };
}

class AgeRestrictionDtoRestrictionEnum {
  /// Instantiate a new enum with the provided [value].
  const AgeRestrictionDtoRestrictionEnum._(this.value);

  /// The underlying value of this enum member.
  final String value;

  @override
  String toString() => value;

  String toJson() => value;

  static const ALLOW_ONLY = AgeRestrictionDtoRestrictionEnum._(r'ALLOW_ONLY');
  static const EXCLUDE = AgeRestrictionDtoRestrictionEnum._(r'EXCLUDE');

  /// List of all possible values in this [enum][AgeRestrictionDtoRestrictionEnum].
  static const values = <AgeRestrictionDtoRestrictionEnum>[
    ALLOW_ONLY,
    EXCLUDE,
  ];

  static AgeRestrictionDtoRestrictionEnum? fromJson(dynamic value) =>
      AgeRestrictionDtoRestrictionEnumTypeTransformer().decode(value);

  static List<AgeRestrictionDtoRestrictionEnum>? listFromJson(
    dynamic json, {
    bool growable = false,
  }) {
    final result = <AgeRestrictionDtoRestrictionEnum>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = AgeRestrictionDtoRestrictionEnum.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }
}

/// Transformation class that can [encode] an instance of [AgeRestrictionDtoRestrictionEnum] to String,
/// and [decode] dynamic data back to [AgeRestrictionDtoRestrictionEnum].
class AgeRestrictionDtoRestrictionEnumTypeTransformer {
  factory AgeRestrictionDtoRestrictionEnumTypeTransformer() =>
      _instance ??= const AgeRestrictionDtoRestrictionEnumTypeTransformer._();

  const AgeRestrictionDtoRestrictionEnumTypeTransformer._();

  String encode(AgeRestrictionDtoRestrictionEnum data) => data.value;

  /// Decodes a [dynamic value][data] to a AgeRestrictionDtoRestrictionEnum.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  AgeRestrictionDtoRestrictionEnum? decode(dynamic data,
      {bool allowNull = true}) {
    if (data != null) {
      switch (data.toString()) {
        case r'ALLOW_ONLY':
          return AgeRestrictionDtoRestrictionEnum.ALLOW_ONLY;
        case r'EXCLUDE':
          return AgeRestrictionDtoRestrictionEnum.EXCLUDE;
        default:
          if (!allowNull) {
            throw ArgumentError('Unknown enum value to decode: $data');
          }
      }
    }
    return null;
  }

  /// Singleton [AgeRestrictionDtoRestrictionEnumTypeTransformer] instance.
  static AgeRestrictionDtoRestrictionEnumTypeTransformer? _instance;
}
